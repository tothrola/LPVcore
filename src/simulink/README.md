# Simulink blocks and MATLAB version compatibility

Due to refactoring in the Simulink implementation across MATLAB versions, this folder contains a directory with special blocks for releases R2024a and later. See [LPVcore GitLab](https://gitlab.com/tothrola/LPVcore/-/merge_requests/347#note_1859467366) for more details.
