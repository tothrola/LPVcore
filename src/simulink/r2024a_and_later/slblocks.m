function blkStruct = slblocks
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository

		Browser.Library = 'lpvcoreLibrary_R2024a';
		% 'mylib' is the name of the library

		Browser.Name = 'LPVcore (R2024a and later)';
		% 'My Library' is the library name that appears 
             % in the Library Browser

		blkStruct.Browser = Browser;