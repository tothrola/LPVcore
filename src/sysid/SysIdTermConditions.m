classdef SysIdTermConditions
    %SYSIDTERMCONDITIONS Enumeration of termination conditions for sys id
    %algorithms
    %   Detailed explanation goes here
    
    enumeration
        FunctionTolerance, StepTolerance, ...
            MaxIterationsReached, NonIterative
    end
end

