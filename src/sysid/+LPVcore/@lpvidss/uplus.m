function A = uplus(A)
% UPLUS   Unary plus for LPVIDSS objects.
%
% +A is the unary plus of A, which returns the original SS system.
%
% See also: PLUS, UMINUS.

% 10/11/2018
%
% (c) Copyright 2018 - Control systems group, University of Eindhoven
% dr. Roland Toth (r.toth@tue.nl).
% 
% Original developer of the script: dr. Pepijn B. Cox (p.b.cox@tue.nl).

end