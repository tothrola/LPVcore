function N = ndims(~)
% NDIMS Number of dimensions of the linear parameter-varying state-space
% representation 
%
%   N = NDIMS(A) always returns 2.
%
% See also: NDIMS, LPVIDSS/SIZE.
%

% 10/11/2018
%
% (c) Copyright 2018 - Control systems group, University of Eindhoven
% dr. Roland Toth (r.toth@tue.nl).
% 
% Original developer of the script: dr. Pepijn B. Cox (p.b.cox@tue.nl).

    N = 2;
end