function [ySys, fit, ic] = compare(data, varargin)
%COMPARE Compare identified model output and measured output
%
%   This function simulates the response of one or more LPV system
%   models, and superimposes the response for each model over the plotted 
%   input/output measurement data contained in data.
%
%   Note that the scheduling signal is not plotted. Use lpviddata/plot to
%   visualize the scheduling signal.
%
%   Syntax:
%       compare(data, sys, LineSpec)
%       compare(data, sys, LineSpec, kstep)
%       compare(data, sys, kstep)
%       compare(data, sys1, sys2, ..., sysn, ___)
%       [y, fit, ic] = compare(data, sys, kstep)
%       compare(__, opt)
%
%   Inputs:
%       data: lpviddata object with the measured input-scheduling-output
%       data
%       sys, sys1, ..., sysn: identified lpvidpoly system to compare with data
%       kstep: number of timesteps to predict the output. If left out, or
%           set to Inf, then the measured output is not used to do the
%           comparison.
%       opt: structure with additional options. Contains the following fields:
%           * 'InitialCondition': handling of initial conditions:
%               * 'e': estimate initial conditions such that the error
%                   criterion associated with the selected horizon in kstep is
%                   minimized. Default.
%               * Specify as a column vector of doubles to use a fixed value
%                   for the initial conditions.
%
%   Outputs:
%     (in case of multimodel comparisons, the below output quantities are cell
%     arrays with each element of the cell array corresponding to the
%     result of a single model)
%       y: simulated or predicted output response.
%       fit: fit in percentage between simulated/predicted and measured output
%           response, based on Normalized Root Mean Square Error (NRMSE)
%           between them.
%       ic: initial condition used in the comparison. For input-output
%           models (such as LPVcore.lpvio and LPVcore.lpvidpoly), the initial
%           condition corresponds to the initial state of a realization (see
%           LPVIOREAL).
%
kstep = [];
syss = {};  % Cell array of systems
sysNames = {};
lineSpecs = {};  % Line specifications
% Check if last argument is an options structure
opt = [];
if isstruct(varargin{end})
    opt = varargin{end};
    varargin(end) = [];
end
% Extract options
if isempty(opt)
    opt = struct();
end
% Validate options
x0_method = getfielddef(opt, 'InitialCondition', 'e');
assert(strcmp(x0_method, 'e') || (iscolumn(x0_method) && isnumeric(x0_method)), ...
    'The ''InitialCondition'' option is invalid');

i = 1;
while i <= numel(varargin)
    v = varargin{i};
    if isa(v, 'LPVcore.lpvrep')
        syss = [syss(:)', {v}];
        sysNames = [sysNames(:)', {inputname(i+1)}];
        % Next argument may be a LineSpec
        if i < numel(varargin) && ischar(varargin{i+1})
            lineSpecs = [lineSpecs(:)', varargin(i+1)];
            i = i + 1;
        else
            lineSpecs = [lineSpecs(:)', {NaN}];
        end
    elseif isint(v) || (isscalar(v) && isinf(v))
        assert(i == numel(varargin), 'kstep must be last argument');
        kstep = v;
        break;
    else
        error('Invalid argument: %s\n', inputname(i+1));
    end
    i = i + 1;
end

p = data.p;
u = data.u;
y = data.y;
t = data.SamplingInstants;

% Prediction or simulation
if isempty(kstep) || isinf(kstep)
    kstep = Inf;
    isPred = false;
else
    isPred = true;
end

% Initial condition handling
ic = cell(1, numel(syss));
if strcmp(x0_method, 'e')
    for i=1:numel(ic)
        % TODO: add initial condition estimation for non-OE IO models
        if hasStaticSchedulingDependence(syss{i}) && ...
            (isa(syss{i}, 'LPVcore.lpvio') || ...
            (isa(syss{i}, 'LPVcore.lpvidpoly') && isoe(syss{i})))
            syss{i} = lpvioreal(syss{i}); %#ok<AGROW>
        end
        if isa(syss{i}, 'LPVcore.lpvlfr') | isa(syss{i}, 'LPVcore.lpvidss')
            ic{i} = findstates(syss{i}, data, kstep);
        else
            if isfield(opt, 'InitialCondition')
                warning(['Initial condition estimation is currently only supported for ', ...
                    'models of class LPVcore.lpvio, LPVcore.lpvlfr and LPVcore.lpvidpoly (OE structure). ', ...
                    'Model ', int2str(i), ' is of type ', class(syss{i}), '. Initial condition will be set to 0.']);
            end
            ic{i} = 0;
        end
    end
else
    for i=1:numel(ic)
        ic{i} = x0_method;
    end
end

% Simulate or predict response
ySys = cell(numel(syss), 1);
fit = cell(numel(syss), 1);
for i=1:numel(syss)
    sys = syss{i};
    x0 = ic{i};
    if ~isPred
        % Simulation
        if isa(sys, 'LPVcore.lpvidpoly')
            % Get the deterministic output (4th)
            [~, ~, ~, ySys{i}] = lsim(sys, p, u, t);
        elseif isa(sys, 'LPVcore.lpvidss')
            [~, ~, ~, ySys{i}] = lsim(sys, p, u, t, x0);
        elseif isa(sys, 'LPVcore.lpvlfr')
            ySys{i} = lsim(sys, p, u, t, 'x0', x0);
        else
            error('Unexpected error occured: unsupported model class %s', ...
                class(sys));
        end
    else
        % Prediction
        if ~isa(sys, 'LPVcore.lpvidsys')
            error(['Input ', sysNames{i}, 'is not a model with identifiable parameters. ', ...
                'This is not supported at the moment for kstep = 1']);
        end
        ySys{i} = predict(sys, data, kstep);
    end
    % Calculate fit
    fit{i} = 100 * (1 - LPVcore.goodnessOfFit(ySys{i}, y, 'NRMSE'));
end

if nargout == 0
    % Show plot
    ny = sys.Ny;
    tile = tiledlayout(ny, 1);
    for i=1:ny
        ax(i) = nexttile; %#ok<AGROW>
        plot(t, y(:, i), 'k'); hold on;
        for j=1:numel(syss)
            tmp = ySys{j};
            if ~isnan(lineSpecs{j})
                plot(t, tmp(:, i), lineSpecs{j});
            else
                plot(t, tmp(:, i));
            end
        end
        if ~isempty(data.OutputName{i})
            outputName = data.OutputName{i};
        else
            outputName = ['y', int2str(i)];
        end
        % Build legend
        legendEntries = cell(1, numel(syss));
        legendEntries{1} = sprintf('%s (%s)', inputname(1), outputName);
        for j=1:numel(syss)
            f = fit{j};
            legendEntries{j+1} = sprintf('%s: %.2f%%', ...
                sysNames{j}, f(i));
        end
        legend(legendEntries, 'interpreter', 'none'); hold off;
        ylabel(outputName);
    end
    % Share x-axis between plots
    linkaxes(ax, 'x');
    % Format figure
    tile.TileSpacing = 'compact';
    if isPred
        type = [int2str(kstep), '-step Predicted'];
    else
        type = 'Simulated';
    end
    title(tile, [type, ' Response Comparison']);
    xlabel(tile, sprintf('Time (%s)', data.TimeUnit));
    ylabel(tile, 'Amplitude');
end

% Convert single-model output arguments from cell array
if isscalar(syss)
    ySys = ySys{1};
    fit = fit{1};
    ic = ic{1};
end

end

