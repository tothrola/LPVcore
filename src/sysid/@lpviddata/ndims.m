function N = ndims(~)
% NDIMS Number of dimensions of the parameter-varying data set
%   N = NDIMS(A) always returns 3.
%
% See also: LPVIDDATA/SIZE.
%

% 10/11/2018
%
% (c) Copyright 2018 - Control systems group, University of Eindhoven
% dr. Roland Toth (r.toth@tue.nl).
% 
% Original developer of the script: dr. Pepijn B. Cox (p.b.cox@tue.nl).

    N = 3;
end