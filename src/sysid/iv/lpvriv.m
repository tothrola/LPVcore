function [sys, pvec, theta] = lpvriv(data, template_sys, options)
%LPVRIV Identification of LPV-BJ model based refined instrumental variables
%
% The refined instrumental variables (RIV) method [1] can identify discrete-time LPV-BJ
% models. Not every LPV-BJ model is compatible with RIV estimation.
% Specifically, the following conditions must be satisfied:
%
%   1) The noise model (C, D polynomials) must be scheduling-independent.
%   2) For multiple-output models the F polynomial must have diagonal
%       structure. That is, the off-diagonal terms must be set to zero AND
%       be marked as non-free.
%   3) The basis functions of each coefficient of the F and B polynomials must be
%       equal. Furthermore, each coefficient of the F polynomial must
%       start with the unity constant phi(x) = 1.
%       More specifically, let the F polynomial be of the following form:
%           F(q) = I + F1 q^-1 + F2 q^-2 + ... + Fn q^-n
%       Then, each Fi, i=1,...,n, must be pmatrix objects of the form
%           Fi(x) = Fi,1 + Fi,2 phi2(x) + ... + Fi,m phim(x)
%       where phij is the j-th basis function.
%
% If all 3 conditions are met, then use LPVRIV using a compatible syntax:
%
%   Syntax:
%       sys = lpvriv(data, template_sys)
%       sys = lpvriv(data, template_sys, options)
%       [sys, pvec, theta] = lpvriv(___)
%
%   Inputs:
%       data: lpviddata object
%       template_sys: template lpvidpoly object.
%       options: lpvrivOptions object. Type DOC LPVRIVOPTIONS for detailed
%           information on the configuration options.
%
%   Outputs:
%       sys: estimated system model.
%       pvec: column vector of parameters of sys oriented according to
%           LPVcore standard: B, C, D, F polynomials in that order.
%       theta: column vector of parameters of sys oriented according to
%           notation in [1]: F, B, C, D polynomials in that order.
%
%   Reference:
%       [1] Algorithm 2 from "Refined instrumental variable methods for
%       identification of LPV Box-Jenkins models" in Automatica 46 (2010)
%       by V. Laurain et. al.
%
%   See also LPVRIVOPTIONS
%

%
% Additional documentation to paper [1].
%
% Size of vectors in (38):
%   yf: same size as y
%   phi_f: see just below (34)

% TODO: add a flag "EnforceStability" that stabilizes the scheduling
% independent part of the F polynomial. By default it is true.

% TODO: add an option for armaxOptions for identifying the noise part.

% TODO: add an option for arxOptions for the initialization.

%% Flags
flag=[];
flag.simplified=1;  % default execution is SRIV
flag.mimo=0;        % default execution is MISO 
flag.legacy_code=1; % for deletation when update is completed

%% Input validation
narginchk(2, 3);
assert(isa(data, 'lpviddata'), '''data'' must be an lpviddata object');
y = data.y;
ny = size(y, 2);
u = data.u;
nu = size(u, 2);
p = data.p;
assert(isa(template_sys, 'LPVcore.lpvidpoly') && ...
    isbj(template_sys), '''template_sys'' must be an lpvidpoly object representing an LPV-BJ model structure');
assert(isdt(template_sys), '''template_sys'' must be discrete-time');
if max(nu,ny)>1
    flag.mimo=1;
end

% Convert scheduling signal to ext. scheduling signal
% TODO: do not add 0s to beginning and end of data_rho. Instead, use proper
% initial and final values.
data_rho = feval(template_sys.SchedulingTimeMap, p);
nrho = size(data_rho, 2);
if flag.legacy_code
    trunc_begin = max([0, -min(template_sys.SchedulingTimeMap.Map)]);
    trunc_end = max([0, max(template_sys.SchedulingTimeMap.Map)]);
    data_rho = [zeros(trunc_begin, nrho); data_rho; zeros(trunc_end, nrho)];
else
    % chop off data to handle initial conditions properly
    % this must be implemeted at the regression level
end

if nargin <= 2
    options = lpvrivOptions;
end
assert(isa(options, 'lpvrivOptions'), '''options'' must be an lpvrivOptions object');
reg_value=options.Regularization;

% Check if noise part is scheduling-independent
assert(isconst(template_sys.C) && isconst(template_sys.D), ...
    '''template_sys'' must have scheduling-independent noise part (C and D)');
% Check if first basis function of each coefficient in F are
% scheduling-independent

% Check if execution of RIV or SRIV estimation is needed
if max(template_sys.C.N,template_sys.D.N)>1
    flag.simplified=0;
    textiv='RIV';
else
    textiv='SRIV';
end

for i=1:template_sys.Nf +1
    F_ = template_sys.F.Mat{i};
     assert(isa(F_.bfuncs{1}, 'pbconst'), 'First basis function of each coefficient in F must be constant');
end
% Check if basis functions of all coefficients in F and B after the first 1 are
% identical (assumed by (5))

if flag.legacy_code
    for i=2:template_sys.Nf + 1
        F_ = template_sys.F.Mat{i};
        if i > 2
            Fprev_ = template_sys.F.Mat{i-1};
            assert(isequal(F_.bfuncs, Fprev_.bfuncs), ...
                'Basis functions of each coefficient (except 1st one) in F must be identical');
        end
    end
    for i=1:template_sys.Nb + 1
        B_ = template_sys.B.Mat{i};
        if i > 1
            Bprev_ = template_sys.B.Mat{i-1};
            assert(isequal(B_.bfuncs, Bprev_.bfuncs), ...
                'Basis functions of each coefficient (except 1st one) in B must be identical');
        end
    end
else
    % TODO: handle non uniform dependency
end
% Check if F is diagonal (MIMO systems are treated as a set of ny MISO
% systems).
assert(isdiag(template_sys.F), ...
    'Off-diagonal elements of F of ''template_sys'' must be 0 and non-free');

% Create GUI if Display is on
gui = progressgui(template_sys, data, options);

%% Algorithm

% Step 1: initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

init = options.Initialization;
if strcmp(init, 'polypre')
    if flag.simplified
        progressmsg(gui, 'Initialization using LPV-ARX pre-estimation.');
        % Define ARX template system
        template_sys_arx = LPVcore.lpvidpoly(template_sys.F, template_sys.B);
        % Solve LPVARX identification
        sys_arx = lpvarx(data, template_sys_arx);
        % Copy ARX results to BJ model structure to use as initialization
        sys = template_sys;
        sys.F = sys_arx.A;
        sys.B = sys_arx.B;
    else
        if flag.legacy_code
        progressmsg(gui, 'Initialization using LPV-ARX pre-estimation.');
        % Define ARX template system
        template_sys_arx = LPVcore.lpvidpoly(template_sys.F, template_sys.B);
        % Solve LPVARX identification
        sys_arx = lpvarx(data, template_sys_arx);
        % Copy ARX results to BJ model structure to use as initialization
        sys = template_sys;
        sys.F = sys_arx.A;
        sys.B = sys_arx.B;
        else
            progressmsg(gui, 'Initialization using LPV-SRIV pre-estimation.');
            % Define OE template system
            template_sys_sriv = LPVcore.lpvidpoly([], template_sys.B,[],[],template_sys.F);
            % Solve LPV-SRIV identification with the given initialization
            % and options
            sys_arx = lpvriv(data, template_sys_sriv,options);
            % Copy ARX results to BJ model structure to use as initialization
            sys = template_sys;
            sys.F = sys_arx.F;
            sys.B = sys_arx.B;
        end
    end
elseif strcmp(init, 'template')
    progressmsg(gui, 'Initialization with provided template coefficients.');
    sys = template_sys;
end

if strcmp(options.Focus, 'simulation')
    [~, fit] = compare(data, sys); 
    progressmsg(gui, ...
        sprintf('Finished initialization. Fit (simulation): [%s]%%\n', num2str(fit')));
else
    [~, fit] = compare(data, sys, 1); 
    progressmsg(gui, ...
        sprintf('Finished initialization. Fit (prediction): [%s]%%\n', num2str(fit')));
end

% (S)RIV estimation
if strcmp(options.Implementation, 'lpvcore')
    % Order of B
    nb = sys.Nb;
    % Number of nonconstant basis functions
    nbeta = size(sys.B.Mat{1}, 3) - 1;
    
    % Order of F (A in paper notation [1])
    na = sys.Nf;
    % Number of nonconstant basis functions (see just below (5))
    nalpha = size(sys.F.Mat{2}, 3) - 1;
    
    % Maximum order of B and F
    n = max([na, nb]);
    
    if strcmp(options.Focus, 'simulation')
        cost_str = 'simulation error';
    else
        cost_str = '1-step ahead prediction error';
    end
    progressmsg(gui, sprintf('Cost used: average 2-norm of the %s', cost_str));
    % Loop over output channels
    sys_per_channel = cell(1, ny);
    for output_idx=1:ny   % TODO: Full MIMO case could be achieved by doing this inside the main iteration loop
        output_name = sys.OutputName{output_idx};
        progressmsg(gui, '');
        if isempty(output_name)
            progressmsg(gui, sprintf('%s identification for output channel %i', textiv, output_idx));
        else
            progressmsg(gui, sprintf('%s identification for output channel %i (''%s'')', ...
                textiv, output_idx, output_name));
        end
        progressmsg(gui, '');
        sysi = sys(output_idx, :);
        yi = y(:, output_idx);
        datai = lpviddata(yi, p, u, data.Ts);

        [theta, rho, eta] = pvec_to_theta(sysi, getpvec(sysi));
        
        % We only need to calculate chihat in the first iteration. In
        % each subsequent iteration, the chihat of the updated model is
        % computed in the previous iteration as it is needed to
        % calculate the cost.
        [~, ~, ~, chihat] = lsim(sysi, p, u);

        % Variables to keep track of the lowest cost achieved across all
        % iterations and return the sysi with the lowest cost if no
        % convergence is achieved
        if strcmp(options.Focus, 'prediction')
            yp = predict(sysi, datai, 1);
            cost = mean(vecnorm(yp - yi, 2, 2));
        else
            cost = mean(vecnorm(chihat - yi, 2, 2));
        end
        selected_sysi = sysi;
        lowest_cost = cost;


        % Loop over iterations for the output channel
        for iter=1:options.MaxIterations
           
%%%%%%%%%%%%% Step 2: Generation of the instruments %%%%%%%%%%%%%%%%%%%%%%%


            % Using simulated output chihatby the model
            % Multiplication of noise-free output with each basis function of F
            % Each row corresponds to a time step:
            %   
            %   Chihat = [chihat1; chihat2; ..., chihatn]
            %
            %   chihat1 = [chihat_{1,1}, ..., chihat_{na, nalpha}] at time k = 1
            %
            Chihat_il = NaN(size(chihat, 1)-n, na * nalpha);

            % Retrieve basis functions outside of loop due to overhead
            fl = cell(nalpha, na);
            for l=1:nalpha
                for j=1:na
                    fl{l,j} = sys.F.Mat{j+1}.bfuncs{l+1};
                end
            end
            % Iterate over time steps
            
            for k=1:size(chihat, 1)-n
                chihati = NaN(na, nalpha);
                % Iterate over coefficients
                for i=1:na
                    % Iterate over basis functions
                    for l=1:nalpha
                        chihati(i, l) = feval(fl{l,j}, data_rho(k+n, :)) * chihat(k + n - i, :);
                    end
                end
                Chihat_il(k, :) = reshape(chihati, 1, na * nalpha);
            end
        
            % Multiplication of input u with each basis function of B as in (32)
            % Each row corresponds to a time step:
            %   
            %   U = [u1; u2; ...; un]
            %
            %   u1 = [u_{1,1}, ..., u_{na, nalpha}] at time k = 1
            %
            U = NaN(size(chihat, 1)-n, (nb + 1) * (nbeta + 1) * nu);
            % Retrieve basis functions outside of loop due to overhead
            gl = cell(nbeta+1, nb+1);
            for l=0:nbeta
                for j=0:nb
                    gl{l+1,j+1} = sys.B.Mat{j+1}.bfuncs{l+1};
                end
            end
            % Iterate over time steps
            for k=1:size(chihat, 1)-n
                uj = NaN(nu, nb + 1, nbeta + 1);
                % Iterate over coefficients
                for l=0:nbeta
                    % Iterate over basis functions
                    for j=0:nb
                        uj(:, j+1, l+1) = feval(gl{l+1,j+1}, data_rho(k+n, :)) * u(k + n - j, :);
                    end
                end
                U(k, :) = reshape(uj, 1, (nb + 1) * (nbeta + 1) * nu);
            end

            % Build the matrix Y and Chihat. Each column is a shifted and truncated
            % version of y:
            %   Y = [yy1, yy2, ..., yyna]
            % yy1.' = [yi(n),      yi(n+1), ...,   yi(end)]
            % yyk.' = [yi(n-k+1),  yi(n-k+2), ..., yi(end-k+1)]
            % yyna.'= [yi(n-na+1), yi(2),   .... , yi(end-na)]
            Y = NaN(size(chihat,1)-n, na);
            Chihat = NaN(size(chihat,1)-n, na);
            for k=1:na
                Y(:, k) = yi(n-k+1:end-k);
                Chihat(:, k) = chihat(n-k+1:end-k);
            end

            % Build the matrix Phi as in (34)
            Phi = [-Y, -Chihat_il, U];

            % Build the matrix Zeta as in Step 4 (just above (40)
            Zeta = [-Chihat, -Chihat_il, U];

            % TODO first accomplish here the noise filter estimation
            if ~flag.legacy_code
              if ~flag.simplified  
                  % estimate noise part C, D
                  vhat = yi - chihat;
                  sysi_noise = armax(iddata(vhat, []), ...
                  [template_sys.Nd, template_sys.Nc], ...
                  armaxOptions('ProgressWindow', 'keep'));  % Needed to keep progress window open
                  % Get parameter vector eta from sys_noise
                  eta = getpvec(sysi_noise);
                  theta = [rho; eta];
                  pvec = theta_to_pvec(sysi, theta);
                  sysi = setpvec(sysi, pvec);
              end
            end
        
%%%%%%%%%%%%% Step 3: compute the estimated filter Q %%%%%%%%%%%%%%%%%%%%%%
           
            % Q is computed as a filter representation:
            % yf = filter(Qb, Qa, y)
            % The monic polynomial Qa may be unstable. Hence, it needs to
            % be stabilized using FSTAB to ensure its root are on the open unit
            % disk. We denote by "Qa_prestab" the Qa polynomial before
            % stabilization.
            [Qb, Qa_prestab] = compute_Q(sysi);
            Qa = fstab(Qa_prestab);
            % Check whether Qa was stabilized
            if ~isequal(Qa, Qa_prestab)
                note = 'Note: Stabilized filter Q';
            else
                note = '';
            end
            yf = filter(Qb, Qa, yi((n+1):end));

%%%%%%%%%%% Step 4: build the regressor phi and filter instrument zeta %%%%

            Phi_f = filter(Qb, Qa, Phi, [], 1);
            Zeta_f = filter(Qb, Qa, Zeta, [], 1);
        
%%%%%%%%%%% Step 5: solve the IV optimization problem %%%%%%%%%%%%%%%%%%%%%

            % We should have Phi_f * rho.' = yf (approximately)
            rho = (Zeta_f.' * Phi_f + reg_value * eye(size(Phi_f, 2))) \ (Zeta_f.' * yf); % (40)
        
%%%%%%%%%%% Step 6: estimate noise part C, D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if flag.legacy_code 
                if ~flag.simplified
                    vhat = yi - chihat;
                    sysi_noise = armax(iddata(vhat, []), ...
                        [template_sys.Nd, template_sys.Nc], ...
                        armaxOptions('ProgressWindow', 'keep'));  % Needed to keep progress window open
                    % Get parameter vector eta from sys_noise
                    eta = getpvec(sysi_noise);
                end
            end
        
            % Step 7: check convergence
            theta = [rho; eta];
            if iter > 1
                step = norm(theta - theta_prev);
                rel_step = step / norm(theta_prev);
            else
                step = 0;
                rel_step = Inf;
            end
            % Store theta to calculate step size in next iteration
            theta_prev = theta;
        
            % Update model parameters
            pvec = theta_to_pvec(sysi, theta);
            sysi = setpvec(sysi, pvec);
    
            [~, ~, ~, chihat] = lsim(sysi, p, u);
            % Calculate cost -- based on user-selected focus
            %
            %   * 'simulation' focus: cost is average 2-norm of output error
            % chihat, the noise-free output of sysi.
            % Equivalent with implementation by Vincent Laurain.
            %   * 'predict' focus: cost is average 2-norm of predicted output
            % error yp.
            if strcmp(options.Focus, 'prediction')
                yp = predict(sysi, datai, 1);
                cost = mean(vecnorm(yp - yi, 2, 2));
            else
                cost = mean(vecnorm(chihat - yi, 2, 2));
            end
            if cost <= lowest_cost
                selected_sysi = sysi;
                lowest_cost = cost;
            end
        
            % Update progress GUI
            update(gui, iter, cost, step, note);

            % Determine termination criterion
            if rel_step < options.Tolerance
                progressmsg(gui, ...
                    sprintf('Terminating identification of output channel %d due to tolerance on step size reached', ...
                    output_idx));
                % If the algorithm converges, then we use the last sysi
                % instead of the best sysi (in terms of cost). This is in
                % order to stay consistent with the implementation
                % contributed by Vincent Laurain.
                selected_sysi = sysi;
                break;
            end
        end
        sys_per_channel{output_idx} = selected_sysi;
    end
    
    % Concatenate sys_per_channel into sys
    sys = sys_per_channel{1};
    for i=2:ny
        sys = [sys; sys_per_channel{i}]; %#ok<AGROW> 
    end

    % Ensure that the properties MustBeMonic_ of the coefficient are
    % appropriate. Otherwise, the number of parameters in sys is incorrect
    sys.A.MustBeMonic_ = true;
    sys.F.MustBeMonic_ = true;
    sys.C.MustBeMonic_ = true;
    sys.D.MustBeMonic_ = true;
    
    terminate(gui, sys, SysIdTermConditions.MaxIterationsReached);
elseif strcmpi(options.Implementation, 'laurain')
    % Use the implementation contributed to LPVcore by Vincent Laurain
    if ~issiso(sys)
        error(['The lpvriv implementation ''laurain'' is ', ...
            'only supported for SISO systems. Use ''lpvcore'' instead.']);
    end
    max_iter = options.MaxIterations;
    tol = options.Tolerance;
    [sys, ~, ~] = riv_laurain(data, sys, tol, max_iter);
end

% Update report
sys.Report.Status = 'Estimated using LPVRIV';
sys.Report.Method = 'lpvriv';
sys.Report.DataUsed = data;
sys.Report.OptionsUsed = options;

end

%% Local functions
function [theta, rho, eta] = pvec_to_theta(sys, p)
    % PVEC_TO_THETA Convert parameter vector from LPVcore to theta in paper notation.
    %
    % This function is the inverse of the local function THETA_TO_PVEC (see
    % below)
    %
    % Inputs:
    %   - sys: system structure containing parameters and dimensions.
    %   - p: parameter vector in LPVcore notation.
    %
    % Outputs:
    %   - theta: parameter vector in paper notation.
    %

    % Extract system dimensions
    nu = sys.Nu;
    nb = sys.Nb;
    nbeta = size(sys.B.Mat{1}, 3) - 1;
    na = sys.Nf;
    nalpha = size(sys.F.Mat{2}, 3) - 1;
    nc = sys.Nc;
    nd = sys.Nd;

    % Calculate the number of parameters
    nrho = na * (nalpha + 1) + (nb + 1) * (nbeta + 1) * nu;
    neta = nc + nd;

    % Ensure the parameter vector has the correct length
    assert(numel(p) == nrho + neta, ...
        '''p'' has %i parameters but must have nrho (%i) + neta (%i) = %i', ...
        numel(p), nrho, neta, nrho + neta);

    % Extract components from p
    b_lpvcore = p(1:(nb + 1) * (nbeta + 1) * nu);
    eta = p((nb + 1) * (nbeta + 1) * nu + 1:(nb + 1) * (nbeta + 1) * nu + neta);
    a = p((nb + 1) * (nbeta + 1) * nu + neta + 1:end);

    % Reshape and permute B_polynomial_params to match the paper notation
    b_permuted = reshape(b_lpvcore, [nu, nbeta + 1, nb + 1]);
    b_array = permute(b_permuted, [1, 3, 2]);
    b = b_array(:);

    % Reshape A_polynomial_params
    a = reshape(a, [], na).';
    a = a(:);

    % Combine rho components
    rho = [a; b];

    % Combine rho and eta to form theta
    theta = [rho; eta];
end

function p = theta_to_pvec(sys, theta)
    % THETA_TO_PVEC Convert theta from paper notation to parameter vector in
    % LPVcore.
    %
    % theta can be split up as follows:
    %   theta = [
    %       rho;    <-- process parameters
    %       eta     <-- noise parameters
    %   ]
    %
    %   rho = [
    %       a;      <-- A-polynomial parameters (sys.F in LPVcore)
    %                   with na * nalpha values
    %       b       <-- B-polynomial parameters (also sys.B in LPVcore)
    %                   with (nb + 1) * (nbeta + 1) * nu values
    %   ]
    %
    %   eta = [
    %       c;      <-- C-polynomial parameters (also sys.C in LPVcore)
    %                   with nc values
    %       d;      <-- D-polynomial parameters (also sys.D in LPVcore)
    %                   with nd values
    %   ]
    %       
    
    nu = sys.Nu;
    nb = sys.Nb;
    nbeta = size(sys.B.Mat{1}, 3) - 1;
    na = sys.Nf;
    nalpha = size(sys.F.Mat{2}, 3) - 1;
    nc = sys.Nc;
    nd = sys.Nd;

    nrho = na * (nalpha + 1) + (nb + 1) * (nbeta + 1) * nu;
    neta = nc + nd;

    assert(numel(theta) == nrho + neta, ...
        '''theta'' has %i parameters but must have nrho (%i) + neta (%i) = %i', ...
        numel(theta), nrho, neta, nrho + neta);
    rho = theta(1:nrho);
    eta = theta(nrho+1:end);
    a = rho(1:na*(nalpha+1));
    b = rho(na*(nalpha+1)+1:end);

    % Note the ordering difference in a/b between paper in LPVcore.
    % In the paper, the parameters are grouped in the following order:
    %   1) By input channel
    %   2) By coefficient
    %   3) By basis function
    % In LPVcore, the parameters are grouped in the following order:
    %   1) By input channel
    %   2) By basis function
    %   3) By coefficient
    % The 2nd and 3rd grouping are reversed. We fix this by a permutation
    % before vectorization.
    a = reshape(a, na, []).'; a = a(:);
    b_array = reshape(b, nu, nb+1, nbeta+1);
    b_permuted = permute(b_array, [1, 3, 2]);
    b_lpvcore = b_permuted(:);
    
    % Order in LPVcore:   A , B, C, D , F   polynomials in LPVcore notation,
    % corresponding to    [], b, [eta], a   in paper notation
    p = [b_lpvcore; eta; a];
end

function [F_constant, F_nonconstant] = split_F(F)
    % SPLIT_F Split F into scheduling independent and scheduling dependent
    % parts
    % Assume first basis function of each coefficient is constant
    Nf = F.N;
    mat_constant = cell(1, Nf); mat_nonconstant = cell(1, Nf);
    free_constant = cell(1, Nf); free_nonconstant = cell(1, Nf);
    min_constant = cell(1, Nf); min_nonconstant = cell(1, Nf);
    max_constant = cell(1, Nf); max_nonconstant = cell(1, Nf);
    for i=1:Nf
        % Get properties of F
        mat = F.Mat{i};
        free = F.Free{i};
        min = F.Min{i};
        max = F.Max{i};
        % Split properties of F into constant and non-constant
        mat_constant{i} = slice(mat, 1);
        free_constant{i} = free(:, :, 1);
        min_constant{i} = min(:, :, 1);
        max_constant{i} = max(:, :, 1);
        if size(mat, 3) > 1
            mat_nonconstant{i} = slice(mat, 2, 'end');
            free_nonconstant{i} = free(:, :, 2:end);
            min_nonconstant{i} = min(:, :, 2:end);
            max_nonconstant{i} = max(:, :, 2:end);
        else
            mat_nonconstant{i} = 0 * mat(:, :, 1);
            free_nonconstant{i} = 0 * free(:, :, 1);
            min_nonconstant{i} = -Inf * min(:, :, 1);
            max_nonconstant{i} = Inf * max(:, :, 1);
        end
    end
    F_constant = pidpoly(mat_constant, free_constant, min_constant, max_constant);
    F_nonconstant = pidpoly(mat_nonconstant, free_nonconstant, min_nonconstant, max_nonconstant);
end

function [Qb, Qa] = compute_Q(sys)
    % NOTE: "F" in paper is NOT "sys.F"!
    % "F" refers to the scheduling-independent part of "sys.F".
    % Q = D / (C * F)       (eq. 37)
    % Consistent with MATLAB FILTER command,
    % Q is decomposed as Qb / Qa.
    % Thus, Qb = D and Qa = C * F

    % Extract LTI part of F (assumed to be the first matrix of each PMATRIX coefficient).
    Flti = extract_Flti(sys);
    % C and D are assumed to be scheduling-independent, so we can use
    % FREEZE.
    Ccell = freeze(sys.C, zeros(1, sys.Np));
    Clti = Ccell{1};
    Dcell = freeze(sys.D, zeros(1, sys.Np));
    Dlti = Dcell{1};
    % Define polynomials
    Qb = Dlti;
    Qa = conv(Clti, Flti);
end

function signal_filtered = apply_Q(D, C, F_constant, signal)
    % TODO: remove this function (support for full MIMO systems is dropped)
    % NOTE: "F" in paper is NOT "sys.F"!
    % "F" refers to the scheduling-independent part of "sys.F".
    % Q = (F * C) \ D
    %
    %   Action of Q on signal s:
    %                 sf = (Q) * s
    %       (F * C) * sf = (D) * s
    %       (F) * C * sf = (D) * s
    %             ------
    %               |
    %              shat
    %
    %       (F) * shat = (D) * s
    %       (C) * sf   = shat
    %

    sys_FD = LPVcore.lpvio(F_constant.Mat, D.Mat);
    sys_C = LPVcore.lpvio(C.Mat, eye(size(C, 1)));

    N = size(signal, 1);
    shat = lsim(sys_FD, NaN(N, sys_FD.Np), signal);
    signal_filtered = lsim(sys_C, NaN(N, sys_C.Np), shat);
end

function Flti = extract_Flti(sys)
    % EXTRACT_FLTI Extract Flti, the scheduling independent part of F, from model defined by (32)
    Flti = NaN(1, sys.Nf + 1);
    for i=1:sys.Nf+1
        Flti(i) = sys.F.Mat{i}.matrices(:, :, 1);
    end
end

