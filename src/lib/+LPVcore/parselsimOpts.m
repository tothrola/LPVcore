function [lsimOpts] = parselsimOpts(sys, sched, u, varargin)
%PARSELSIMOPTS Parse options for LSIM of LPVREP objects
% 
%   Syntax:
%     lsimOpts = parselsimOpts(sys, varargin)
%
%   Inputs:
%       sys: LPVREP system object for which simulation is requested
%
%       sched: can be either a 
%           - scheduling map object (self-scheduled simulation)
%           - scheduling signal as a [Nt, Np] vector
%
%       u: input to the system as a [Nt, Nu] vector or a function
%       handle, i.e. u = @(t) sin(t).
%
%       varargin: a combination of optional positional and name-valued pair
%                arguments. The optional positional arguments are:
%           - t: [Nt, 1] vector of equidistant time instances. Can be an
%               empty matrix ([]) for some DT simulation cases.
%           - e (only if sys isa LPVcore.lpvidpoly): [Nt, Ny] noise vector
%           - x0 (only if sys isa LPVcore.lpvlfr or a LPVcore.lpvidss): 
%               [Nx, 1] vector with initial conditions.
%           - w (only if sys isa LPVcore.lpvidss):
%       The optional name-valued pair arguments are:
%           - 'd': exogenous signal given as a [Nt, Nd] matrix or a
%                 function handle. Currently implemented only for 
%                 self-scheduling mode.
%           - 'p0' (only for scheduling trajectory simulation of DT systems):
%                 initial value of p
%           - 'pf' (only for scheduling trajectory simulation of DT systems):
%                 final value of p
%           - 'u0' (only for LPVcore.lpvio and LPVcore.lpvidpoly DT
%                 systems): input at t=0
%           - 'y0' (only for LPVcore.lpvio and LPVcore.lpvidpoly DT
%                 systems): output at t=0
%           - 'solverMode' (only for LPVcore.lpvio and LPVcore.lpvidpoly DT
%                 systems): choose between 'large' (default) or 'fast'
%           - 'e0'  (only for LPVcore.lpvidpoly DT systems): noise at t=0
%           - 'Solver' (only for CT systems): choose between '@ode15s'
%                 (default), '@ode45', '@ode23', '@ode113'
%           - 'SolverOpts' (only for CT systems): odeset structure 
%                  with solver options.
%           - 'SolverInterpolationMode' (only for scheduling trajectory 
%                  simulation of CT systems): choose between 'accurate'
%                  (default) and 'fast'
%           - 'simidss' (only for self-scheduled simulation of
%                  LPVcore.lpvidss objects with noise inputs 
%                  
%                 
% 
%   Outputs:
%       lsimOpts: struct containing the fields described above for
%       simulation
%
%   Note: the current implementation does not support a partially
%      self-scheduled and partially trajectory based simulation, i.e., a
%      self-scheduled simulation with numerical values for the exogenous
%      signal

% Initial checks and simulation mode selection
assert(sys.Np > 0 || sys.Nu > 0, ...
    ['Simulation of an LPV system with 0 inputs and 0 scheduling signals ', ...
    'is not currently supported']);

if isa(sched,'LPVcore.schedmap')  %Self-scheduled simulation
    self = true; % Define flag to pass simulation mode
elseif isnumeric(sched) % Simulation with scheduling trajectory
    self = false; 
else
    assert(isnumeric(sched) || isa(sched,'LPVcore.schedmap'), ...
       ['Simulation of an LPV system requires a scheduling map object ', ...
    'or a [Nt, Np] scheduling trajectory'] )
end
lsimOpts.self = self;

parser = inputParser;

scalarOrMatOrEmpty = @(x, n, m) isscalar(x) || ...
    isempty(x) || ...
    (isnumeric(x) && all(size(x) == [n, m]));

validModes = {'fast', 'accurate'};
checkInterpMode = @(x) any(validatestring(x,validModes));
validModesIODT = {'fast', 'large'};
possibleSolversIODT = @(x) any(validatestring(x,validModesIODT));

% Time vector
parser.addOptional('t', []);

% Exogenous signal vector
parser.addParameter('d', []);

% Noise
if isa(sys, 'LPVcore.lpvidpoly')
    parser.addOptional('e', [], ...
        @(x) isempty(x) || (isnumeric(x) && size(x, 2) == sys.Ny));
end

% Initial state
if isa(sys, 'LPVcore.lpvlfr') || isa(sys, 'LPVcore.lpvidss')
    if isa(sys, 'LPVcore.lpvidss')
        DefaultX0 = sys.X0;
    else
        DefaultX0 = zeros(sys.Nx, 1);
    end
    parser.addOptional('x0', DefaultX0, ...
        @(x) isnumeric(x) && isvector(x) && ...
            (numel(x) == sys.Nx || isscalar(x)));
end

if isa(sys, 'LPVcore.lpvidss')
    parser.addOptional('w', [], ...
        @(x) isempty(x) || isnumeric(x));
end

if isdt(sys)
    if ~self
        % Size of initial and final values of p
        M0 = -min(min(sys.SchedulingTimeMap.Map), 0);
        Mf = max(max(sys.SchedulingTimeMap.Map), 0);
        parser.addParameter('p0', zeros(M0, sys.Np), ...
            @(x) scalarOrMatOrEmpty(x, M0, sys.Np));
        parser.addParameter('pf', zeros(Mf, sys.Np), ...
            @(x) scalarOrMatOrEmpty(x, Mf, sys.Np));
    end
    if isa(sys, 'LPVcore.lpvio') || isa(sys, 'LPVcore.lpvidpoly')
        parser.addParameter('u0', zeros(sys.Nb, sys.Nu), ...
            @(x) scalarOrMatOrEmpty(x, sys.Nb, sys.Nu));
        parser.addParameter('y0', zeros(sys.Na, sys.Ny), ...
            @(x) scalarOrMatOrEmpty(x, sys.Na, sys.Ny));
        parser.addParameter('solverMode', 'large', ...
            @(x) possibleSolversIODT(x));
    end
    if isa(sys, 'LPVcore.lpvidpoly')
        parser.addParameter('e0', zeros(sys.Nc, sys.Ny), ...
            @(x) scalarOrMatOrEmpty(x, sys.Nc, sys.Ny));
    end
end

% ODE solver options
% -------------------------------------------------------------------------
if isct(sys)
    parser.addParameter('Solver', @ode15s, ...
        @(x) isa(x, 'function_handle') && ...
            isequal(x, @ode45) || isequal(x, @ode23) || ...
            isequal(x, @ode113) || isequal(x, @ode15s));
    parser.addParameter('SolverOpts', odeset, ...
        @(x) isa(x, 'struct'));
    parser.addParameter('SolverInterpolationMode',...
        'accurate', checkInterpMode)
end

%idss object
parser.addOptional('simidss',false)

% Parse PV pairs
parser.parse(varargin{:});
t = parser.Results.t;
d = parser.Results.d;
simidss = parser.Results.simidss;

%% Validate and assign to lsimOpts
% Validation of input
% -------------------------------------------------------------------------
assert(~(~isnumeric(u) && isempty(t)), 'An empty time vector requires a numeric input')
if isempty(u)
    assert(sys.Nu == 0, ...
        'u cannot be empty for a system with at least 1 input');
    u = NaN(size(sched, 1), sys.Nu);
end

if isnumeric(u)
    [uNt, uNu] = size(u);
else
    uNt = numel(t);
    uNu = size(u(1),2);
end
if ~isempty(t)
    nT = length(t);
    % Checks on the input if t is not empty
    if isnumeric(u)
        % Check if t is equidistant
        assert(isvector(t) && ...
            isnumeric(t) && isreal(t) && ...
            norm(diff(t) - mean(diff(t))) / numel(t) < 10 * eps, ...
            't must be a vector of equidistant time instances');
        % Check number of samples
        assert(numel(t) == uNt, ...
            'Number of time samples specified in t, u and p does not match');
        % Check increasing
        assert(all(diff(t) > 0), ...
            't must be an increasing vector');
         % Check if t is of same size as u
        assert(all(size(u) == [nT, sys.Nu]),['Input vector size ' ...
            'should be [Nt, Nu]']);
         % Check if u is of same size as the LPV input dimension
        assert(uNu == sys.Nu,['Number of columns of u (', ...
            int2str(uNu), ') must equal the input dimension (', ...
            int2str(sys.Nu), ')'])
        % Reformulate the input as a function of time (tau)
        ufun = @(tau) interp1(t, u, tau);
    else % Check compatibility of input as a function handle
        assert(all(size(u(zeros(1,nargin(u)))) == [1, sys.Nu]), ['Input ' ...
            'function does not match input dimension, make ' ...
            'sure it outputs a [Nt, Nu] vector']);
        ufun = u;
    end
end

% Validation of scheduling map/signal
% -------------------------------------------------------------------------
switch self
    case true
        assert(sys.Np == sched.Np,['The number of scheduling signals of the LPV system ', ...
            'does not match the one of scheduling map']);
        assert(sys.Nx == sched.Nx,['The number of states of the LPV system ', ...
            'does not match the one of scheduling map']);
        if simidss
            assert(sys.Nu >= sched.Nu,['The number of inputs of the LPV system ', ...
                'does not match the scheduling map']);
        else
            assert(sys.Nu == sched.Nu,['The number of inputs of the LPV system ', ...
                'does not match the scheduling map']);
        end
        lsimOpts.sched = sched.map;
    case false
        if isempty(sched)
            assert(sys.Np == 0, ...
                'p cannot be empty for a system with at least 1 scheduling signal');
        end
        % Expand empty signals to match the number of rows of the other
        if isempty(sched); sched = NaN(uNt, sys.Np); end
        % Check rows match
        if size(sched, 1) ~= uNt
            error('Number of rows of p and u must be equal');
        end
        % Check columns match
        if size(sched, 2) ~= sys.Np
            error(['Number of columns of p (', ...
                int2str(size(sched, 2)), ') must equal the scheduling dimension (', ...
                int2str(sys.Np), ')']);
        end
        lsimOpts.sched = sched;
end
% -------------------------------------------------------------------------

% Validation of the exogenous signal
% -------------------------------------------------------------------------
if self
    if isempty(d)
        assert(sched.Nd == 0, ['The scheduling map expects a [Nt, schedmap.Nd]'...
            ' exogenous signal (it can also be provided as a function of time)'])
        dfun = @(tau) [];
    else
        if isnumeric(d)
            % Check number of samples
            assert(numel(t) == size(d,1), ...
                'Number of time samples specified in t and d does not match');
            % Check if t is of same size as u
            assert(all(size(d) == [numel(t), sched.Nd]),['Exogenous signal size ' ...
                'should be [Nt, schedmap.Nd]']);
            % Reformulate the exogenous signal as a function of time (tau)
            dfun = @(tau) interp1(t, d, tau);
        elseif isa(d,"function_handle")
            assert(all(size(d(zeros(1,nargin(d)))) == [1, sched.Nd]),...
                ['Exogenous signal function does not match the expected' ...
                'dimension, make sure it outputs a [Nt, schedmap.Nd] vector']);
            dfun = d;
        end
    end
else
    dfun = d;
end
t = t(:);
% -------------------------------------------------------------------------

% DT options
% -------------------------------------------------------------------------
if isdt(sys)
    % Check if t is compatible with sampling time Ts
    if ~isempty(t)
        if sys.Ts ~= -1
            assert(norm(diff(t) - sys.Ts) / numel(t) < 10 * eps, ...
                't must be a vector of equidistance time instances corresponding to the sampling time Ts');
        end
    end
    switch self
        case true
            assert(~isempty(t), ['Discrete-time self-scheduled simulation'...
                ' requires a non-empty time vector']);
            if isempty(d)
                d = zeros(nT,1);
            end
        case false
            p0 = parser.Results.p0;
            pf = parser.Results.pf;
            % convert empty inputs to scalars
            if isempty(p0); p0 = 0; end
            if isempty(pf); pf = 0; end
            % convert scalar inputs to matrix
            if isscalar(p0)
                p0 = ones(M0, sys.Np) * p0;
            end
            if isscalar(pf)
                pf = ones(Mf, sys.Np) * pf;
            end
            lsimOpts.p0 = p0; lsimOpts.pf = pf;
    end
    % Parse signals to output lsimOpts structure
    lsimOpts.t = t;
    if isnumeric(u)
        lsimOpts.u = u;
    else
        lsimOpts.u = ufun(t);
    end
    if isnumeric(d)
        lsimOpts.d = d;
    else
        lsimOpts.d = dfun(t);
    end
    % Checks for different system representations
    if isa(sys, 'LPVcore.lpvio') || isa(sys, 'LPVcore.lpvidpoly')
        u0 = parser.Results.u0;
        y0 = parser.Results.y0;
        if isempty(y0); y0 = 0; end
        if isempty(u0); u0 = 0; end
        if isscalar(y0)
            y0 = ones(sys.Na, sys.Ny) * y0;
        end
        if isscalar(u0)
            u0 = ones(sys.Nb, sys.Nu) * u0;
        end
        lsimOpts.y0 = y0; lsimOpts.u0 = u0;
        lsimOpts.solverMode = parser.Results.solverMode;
    end
    if isa(sys, 'LPVcore.lpvidpoly')
        e0 = parser.Results.e0;
        e = parser.Results.e;
        % Generate noise
        [R, cholP] = chol(sys.NoiseVariance);
        isPosDef = cholP == 0;
        if ~isPosDef
            R = zeros(sys.Ny);
        end
        if isempty(e)
            if isempty(e0)
                e0 = randn(sys.Nc, sys.Ny) * R;
            end
            e = randn(size(sched, 1), sys.Ny) * R;
        end
        if isempty(e0)
            e0 = randn(sys.Nc, sys.Ny) * R;
        end
        lsimOpts.e0 = e0;
        lsimOpts.e = e;
    end
    if isa(sys, 'LPVcore.lpvidss')
        w = parser.Results.w;
        % Generate noise
        [R, cholP] = chol(sys.NoiseVariance);
        isPosDef = cholP == 0;
        if ~isPosDef
            R = zeros(sys.Ny);
        end
        if isempty(w)
            if strcmp(typeNoiseModel(sys), 'innovation')
                v = randn(size(sched, 1), sys.Ny) * R;
                e = v;
            else
                w = randn(size(sched, 1), sys.Nx + sys.Nu) * R;
                v = w(:, 1:sys.Nx);
                e = w(:, sys.Nx+1:end);
            end
        else
            % Split w
            if strcmp(typeNoiseModel(sys), 'innovation')
                v = w;
                e = w;
            else
                v = w(:, 1:sys.Nx);
                e = w(:, sys.Nx+1:end);
            end
        end
        lsimOpts.v = v;
        lsimOpts.e = e;
    end
end
% -------------------------------------------------------------------------

% CT options
% -------------------------------------------------------------------------
if isct(sys)
    assert(~isempty(t), 't must not be empty');
    lsimOpts.t = t;
    lsimOpts.u = ufun;
    lsimOpts.d = dfun;
    lsimOpts.Solver = parser.Results.Solver;
    lsimOpts.SolverOpts = parser.Results.SolverOpts;
    lsimOpts.SolverMode = parser.Results.SolverInterpolationMode;
end
% -------------------------------------------------------------------------

% State-space options
% -------------------------------------------------------------------------
if isa(sys, 'LPVcore.lpvlfr') || isa(sys, 'LPVcore.lpvidss')
    x0 = parser.Results.x0;
    % Expand scalar
    if isscalar(x0)
        x0 = x0 * ones(sys.Nx, 1);
    end
    % Make column vector
    x0 = x0(:);
    lsimOpts.x0 = x0;
end
% -------------------------------------------------------------------------
end

