function [y, t, x] = dtsim_(obj, lsimOpts)
%% dtsim_(obj, lsimOpts)
sched = lsimOpts.sched;
tin = lsimOpts.t;
u = lsimOpts.u;
d = lsimOpts.d;
x0 = lsimOpts.x0;
self = lsimOpts.self;

ny = obj.Ny;
nx = obj.Nx;
% get matrices of the LPV LFR model
A = obj.A0;
Bw = obj.Bw;
Bu = obj.Bu;
Dzw = obj.Dzw;
Cz = obj.Cz;
Cy = obj.Cy;
Dyu = obj.Dyu;
Dyw = obj.Dyw;
Dzu = obj.Dzu;
Delta = obj.Delta;
eyeNz = eye(obj.Nz); % size of Dzw*delta(P)

% Apply InputDelay
for i=1:obj.Nu
    u(:, i) = [zeros(obj.InputDelay(i), 1); ...
        u(1:end-obj.InputDelay(i), i)];
end

switch self
    case true
        N = size(tin,1);
        y = NaN(N, obj.Ny);
        x = NaN(N, obj.Nx);
        x(1, :) = x0;
        for i=1:N
            Delta_ = feval(Delta, sched(x(i,:)',u(i,:)',d(i,:)')');
            invTerm = eyeNz-Dzw*Delta_;
            A_ = A   + Bw*Delta_/  invTerm * Cz;
            B_ = Bu  + Bw*Delta_/  invTerm * Dzu;
            C_ = Cy  + Dyw*Delta_/ invTerm * Cz;
            D_ = Dyu + Dyw*Delta_/ invTerm * Dzu;
            y(i, :) = (C_ * x(i, :)' + D_ * u(i, :)')';
            if i < N
                x(i+1,:) = (A_ * x(i, :)' + B_ * u(i, :)')';
            end
        end
        t=tin;

    case false
        tm = obj.SchedulingTimeMap;
        M = size(sched, 1);
        np = size(sched, 2);
        if ~isempty(tm.Map)
            M_min = -min(min(tm.Map), 0);
            M_max = max(max(tm.Map), 0);
        else
            M_min = 0; M_max = 0;
        end
        sched = [zeros(M_min, np); sched; zeros(M_max, np)];
        istart = M_min + 1;
        iend = istart + M - 1;
        y = NaN(M, ny);
        x = NaN(M, nx);
        x(1, :) = x0;
        DeltaP = peval(Delta, sched);
        % loop over time
        for i=istart:iend
            k = i-istart+1;
            DeltaPi = DeltaP(:, :, i-istart+1);
            invTerm = eyeNz-Dzw*DeltaPi;
            A_ = A + Bw*DeltaPi/invTerm * Cz;
            B_ = Bu + Bw*DeltaPi/invTerm * Dzu;
            C_ = Cy + Dyw*DeltaPi/invTerm * Cz;
            D_ = Dyu + Dyw*DeltaPi/invTerm * Dzu;
            y(k, :) = (C_ * x(k, :)' + D_ * u(k, :)')';
            if i < iend
                x(k+1, :) = (A_ * x(k, :)' + B_ * u(k, :)')';
            end
        end

        if obj.Ts == -1
            obj.Ts = 1;
        end
        t=(0:obj.Ts:(M-1)*obj.Ts)';
end
end