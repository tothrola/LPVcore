function [y, t, x] = ctsim_(obj, lsimOpts)
%% ctsim_(obj, lsimOpts)
self = lsimOpts.self;
sched = lsimOpts.sched;
u = lsimOpts.u;
d = lsimOpts.d;
tin = lsimOpts.t;
x0 = lsimOpts.x0;
solver =lsimOpts.Solver;
solverOpts  = lsimOpts.SolverOpts;
solverMode = lsimOpts.SolverMode;

% get matrices of the LPV LFR model
A0 = obj.A0;
Bw = obj.Bw;
Bu = obj.Bu;
Dzw = obj.Dzw;
Cz = obj.Cz;
Cy = obj.Cy;
Dyu = obj.Dyu;
Dyw = obj.Dyw;
Dzu = obj.Dzu;
Delta = obj.Delta;
eyeNz = eye(obj.Nz);
Ny = obj.Ny;

switch self
    case true % Self-scheduled simulation
        % State propagation
        dxdt = @(t, x) xfun(x, u(t), d(t), sched, Delta, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz);
        [t, x] = solver(dxdt, tin, x0, solverOpts);
        % System's output
        N = size(x,1);
        y = hfun(t, x, u, d, sched, Delta, Cy, Cz, Dyw, Dzw, Dzu, Dyu, Ny, N, eyeNz);

    case false % Scheduling trajectory-based simulation
        Ts = tin(2) - tin(1);
        tm = obj.SchedulingTimeMap;
        rho = tm.feval(sched, Ts);
        if size(rho, 2) > 0
            rhofun = @(tau) interp1(tin, rho, tau);
        else
            rhofun = @(tau) [];
        end
        % State propagation
        if strcmp(solverMode,'accurate')
            fun = @(t, x) odefun(x, u(t), rhofun(t), Delta, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz);
            [t, x] = solver(fun, tin, x0, solverOpts);
        elseif strcmp(solverMode, 'fast')
            Delta_ = feval(Delta, rho);
            fun = @(t, x) odefunFast(t, x, u(t), tin, Delta_, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz);
            [t, x] = solver(fun, tin, x0, solverOpts);
        end
        % System's output
        N = size(x,1);
        y = odefuny(t, x, u, rhofun, Delta, Cy, Cz, Dyw, Dzw, Dzu, Dyu, Ny, N, eyeNz);
end

end
%% Self-scheduled simulation functions
function dxdt = xfun(x, u, d, eta, Delta, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz)
    Delta_ = feval(Delta, eta(x,u',d')');
    invTerm = (eyeNz-Dzw*Delta_);
    A_ = A0 + Bw*Delta_/ invTerm * Cz; 
    B_ = Bu + Bw*Delta_/ invTerm * Dzu;
    dxdt = A_*x + B_*u';
end

function y = hfun(t, x, u, d, eta, Delta, Cy, Cz, Dyw, Dzw, Dzu, Dyu, Ny, N, eyeNz)
    y = NaN(N, Ny);
    for i = 1:N
        t_ = t(i);
        u_ = u(t_);
        d_ = d(t_);
        rho_ = eta(x(i,:)',u_',d_')';
        Deltai = feval(Delta, rho_);
        invTerm = (eyeNz-Dzw*Deltai);
        C_ = Cy + Dyw*Deltai/ invTerm * Cz;
        D_ = Dyu + Dyw*Deltai/ invTerm * Dzu;
        y(i,:) = (C_ * x(i, :)' + D_ * u_')';
    end
end

%% Scheduling trajectory-based simulation functions
% Scheduling signal
function dxdt = odefun(x, u, rhofun, Delta, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz)
    Delta_ = feval(Delta, rhofun);
    invTerm = (eyeNz - Dzw*Delta_);
    A_ = A0 + Bw*Delta_/ invTerm * Cz; 
    B_ = Bu + Bw*Delta_/ invTerm * Dzu;
    dxdt = A_ * x + B_ * u';
end

function dxdt = odefunFast(t, x, u, tin, Delta_, A0, Bw, Bu, Cz, Dzu, Dzw, eyeNz)
    Delta__ = permute(interp1(tin, permute(Delta_, [3 2 1]), t), [3 2 1]);
    invTerm = (eyeNz - Dzw*Delta__);
    A_ = A0 + Bw*Delta__/ invTerm * Cz; 
    B_ = Bu + Bw*Delta__/ invTerm * Dzu;
    dxdt = A_ * x + B_ * u';
end

function y = odefuny(t, x, u, rhofun, Delta, Cy, Cz, Dyw, Dzw, Dzu, Dyu, Ny, N, eyeNz)
    y = NaN(N, Ny);
    for i = 1:N
        t_ = t(i);
        u_ = u(t_);
        rho_ = rhofun(t_);
        Deltai = feval(Delta, rho_);
        invTerm = (eyeNz-Dzw*Deltai);
        C_ = Cy + Dyw*Deltai/ invTerm * Cz;
        D_ = Dyu + Dyw*Deltai/ invTerm * Dzu;
        y(i,:) = (C_ * x(i, :)' + D_ * u_')';
    end
end
