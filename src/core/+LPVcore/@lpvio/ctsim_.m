function [y, t] = ctsim_(obj, p, u, tin, solver, solverOpts, solverMode)
% CTSIM_ CT simulation of LPV-IO system
%
%   Syntax:
%       [y, t] = ctsim_(obj, p, u, tin, solver, solverOpts)
%
%   Inputs:
%       p: N-by-np matrix
%       u: N-by-nu matrix
%       tin: vector of N equidistant time instants at which p and u are provided
%       solver: solver specified as function handle (e.g., @ode45)
%       solverOpts: solver options
%
%   Outputs:
%       y: N-by-ny matrix with simulated response
%       t: vector of N time instants at which y is provided
%
%   See also: LPVIO/DTSIM_

    Ts = tin(2) - tin(1);
    tm = obj.SchedulingTimeMap;
    rho = tm.feval(p,Ts);
    urho = ctevalu(obj,u,Ts);
    
    % If Na = 0, then the system represents a static gain y = B u, which
    % can be explicitly calculated
    if obj.Na == 0
        B_ = cellfun(@(x)feval(x, rho), obj.B, 'UniformOutput', false);
        B_ = cell2mat(B_);
        N = size(urho, 1);
        y = NaN(N, obj.Ny);
        for i=1:N
            y(i, :) = urho(i, :) * B_(:, :, i)';
        end
        t = tin;
        return
    end
    
    % If Na > 0, an ODE has to be solved
    % Note that the auxiliary variable x is composed of the differentiated
    % version of y AND the scalar dummy variable z. Hence, the "+1".
    x0 = zeros(obj.Na*obj.Ny + 1, 1);

    % We update some solver options:
    %   * 'MStateDependence' is set to 'none' because the mass matrix only
    %       depends on time, not on the state.
    %   * 'Mass' is set to the function handle giving the mass matrix for
    %       the specified time instance.
    solverOpts.MStateDependence = 'none';
    solverOpts.Mass = @(t) Mfun(t, tin, rho, obj);
    solverOpts.MassSingular = 'yes';
    
    if strcmp(solverMode, 'accurate')
        fun = @(t, x) odefun(t, tin, x, rho, urho, obj);
        [t, x] = solver(fun, tin, x0, solverOpts);
    elseif strcmp(solverMode, 'fast') %default
        Aeval = cell2mat(cellfun(@(x) feval(x, rho), obj.A, 'UniformOutput', false));
        Beval = cell2mat(cellfun(@(x) feval(x, rho), obj.B, 'UniformOutput', false));
        szA = [size(Aeval) obj.Na+1];
        szA(2) = szA(2)/szA(4);
        fun = @(t, x) odefunfast(t, tin, x, urho, obj, Aeval, Beval, szA);
        [t, x] = solver(fun, tin, x0, solverOpts);
    end
    y = x(:,1:obj.Ny);
end

function M = Mfun(t, tin, rho, obj)
    % Evaluate the mass matrix (see documentation of ODEFUN in this file)
    rho_ = interp1(tin, rho, t);
    Aeval = feval(obj.A{end}, rho_);
    M = blkdiag(eye(obj.Ny * (obj.Na - 1)), Aeval, 1);
end

function dxdt = odefun(t, tin, x, rho, urho, obj)
    rho_ = interp1(tin, rho, t);
    u_ = interp1(tin, urho, t)';

    % We simulate the LPVIO model as an ODE by introducing the following
    % auxiliary variable:
    %   x := [y; dy/dt; d^2y/dt^2; ...; d^(Na-1) y /dt^(Na-1); z]
    %
    % where z is a dummy variable that is introduced because the ODE
    % solvers in MATLAB cannot handle the case that the mass matrix is
    % identically 0. However, this can occur, for example, when solving
    %   y(t) + p(t) * dy/dt(t) = u(t)
    % with p(t) at some point going to 0. Thus, we introduce z(t) for which
    % we impose the dynamics
    %   dz/dt(t) = 0
    % In this way, the mass matrix always has non-zero entries.
    %
    % In this way, we can formulate the ODE as:
    %   M dx/dt = F x + G u
    % with
    %
    %   M = blkdiag(I, I, ..., I, A{Na+1}, 1)
    %
    %   F = [0,   I,   0,   ..., 0,    0, 0; ...
    %        0,   0,   I,   ..., 0,    0, 0; ...
    %               ...
    %        0,   0,   0,   ..., 0,    I, 0; ...
    %        -A1, -A2, -A3, ..., -ANa, 0, 0; ...
    %        0,   0,   0,   ..., 0,    0, 0]
    %
    %   G = [0; 0; ...; B; 0]
    %
    % (Note that the last column of F and the last row of F and G
    % correspond to the dummy variable z.)

    % compute F
    Aeval = cellfun(@(x) feval(x, rho_), obj.A, 'UniformOutput', false); 
    Fbot = -cell2mat(Aeval(1:end-1));  
    Ftop = [zeros((obj.Na-1)*obj.Ny, obj.Ny), ...
        eye((obj.Na-1) * obj.Ny)];       
    F = blkdiag([Ftop; Fbot], 0);

    % compute G        
    Beval = cellfun(@(x) feval(x, rho_), obj.B, ...
        'UniformOutput', false);      
    G = [ zeros((obj.Na-1) * obj.Ny, (obj.Nb + 1) * obj.Nu); ...
        cell2mat(Beval); ...
        zeros(1, (obj.Nb + 1) * obj.Nu)];

    dxdt = F * x + G * u_;

end

function dxdt = odefunfast(t, tin, x, urho, obj, Aeval, Beval, szA)
    u_ = interp1(tin, urho, t)';

    % compute F
    Aeval_ = permute(interp1(tin, permute(Aeval,[3 2 1]), t),[3 2 1]); %check
%    Fbot = -Aeval{obj.Na+1} \ cell2mat(Aeval(1:end-1));
    Fbot = -Aeval_(:,1:end-szA(2)); %check
    
    Ftop = [zeros((obj.Na-1)*obj.Ny, obj.Ny), ...
        eye((obj.Na-1) * obj.Ny)];       
    F = [Ftop; Fbot];

    % compute G
    Beval_ = permute(interp1(tin, permute(Beval,[3 2 1]), t), [3 2 1]);
    B_ = Beval_;
    G = [ zeros((obj.Na-1) * obj.Ny, (obj.Nb + 1) * obj.Nu); ...
        B_ ];

    dxdt = F * x + G * u_;
end

function urho = ctevalu(obj, u, Ts)            
    N = size(u, 1);
    urho = NaN(N, (obj.Nb+1) * obj.Nu);
    for j=1:obj.Nb+1
        diff = j - 1;
        urho(:, (j-1)*obj.Nu+1:j*obj.Nu) = u;
        for i=1:diff
            if obj.Nu > 1
                [~, urho(:, (j-1)*obj.Nu+1:j*obj.Nu)] = ...
                gradient(urho(:, (j-1)*obj.Nu+1:j*obj.Nu), Ts);
            else
                urho(:, (j-1)*obj.Nu+1:j*obj.Nu) = ...
                gradient(urho(:, (j-1)*obj.Nu+1:j*obj.Nu), Ts);
            end
        end
    end
end

