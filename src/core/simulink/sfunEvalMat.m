function sfunEvalMat(block)
    %SFUNEVALMAT Returns the evaluation of pmatrix M for the input (scheduling parameter) p.
    setup(block);
end

function setup(block)
    % Register number of ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 1;
    
    % Setup port properties to be inherited or dynamic
    block.SetPreCompInpPortInfoToDynamic;
    block.SetPreCompOutPortInfoToDynamic;
    
    % Override input port properties
    block.InputPort(1).DatatypeID        = 0;   % double
    block.InputPort(1).Complexity        = 'Real';
    block.InputPort(1).DirectFeedthrough = true;
    
    % Override output port properties
    block.OutputPort(1).DatatypeID       = 0;   % double
    block.OutputPort(1).Complexity       = 'Real';
    
    % Register parameters
    block.NumDialogPrms     = 3;    % M, nIn, nOut
    block.DialogPrmsTunable = {'Nontunable', 'Nontunable', 'Nontunable'};
    
    % Register sample times
    block.SampleTimes = [0 0];
    
    % Specify the block simStateCompliance.
    block.SimStateCompliance = 'DefaultSimState';

    % Register block methods
    block.RegBlockMethod('SetInputPortDimensions', @SetInpPortDims);
    block.RegBlockMethod('Outputs', @Outputs);     % Required
    block.RegBlockMethod('Terminate', @Terminate); % Required
end

function SetInpPortDims(block, idx, ~)
    nIn = block.DialogPrm(2).Data;
    nOut = block.DialogPrm(3).Data;

    block.InputPort(idx).Dimensions = nIn;
    block.OutputPort(1).Dimensions  = nOut;
end

function Outputs(block)
    M = block.DialogPrm(1).Data;
    p = block.InputPort(1).Data;

    block.OutputPort(1).Data = freeze(M,p');
end

function Terminate(~)
end