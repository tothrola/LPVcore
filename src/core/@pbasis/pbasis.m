classdef (Abstract) pbasis
    %PBASIS Abstract class for parameter-varying basis functions
    %
    % Parametery-varying basis functions map an extended scheduling vector
    % (rho1, ..., rhoN) to a scalar. They are used to construct PMATRIX
    % objects.
    %
    % Do not create PBASIS objects directly. Instead, use PMATRIX or PREAL
    % to define parameter-varying matrices or scalars.
    %
    properties
       sortKey
       dependenceType
    end
    
    methods
        function c = lt(A, B)
            c = A.sortKey < B.sortKey;
        end
        
        function c = gt(A, B)
            c = lt(B, A);
        end

        function C = mtimes(A, B)
            C = times(A, B);
        end
        
        function A = diff(A, order, schedulingDimension)
            % DIFF Apply higher-order derivative
            %
            %   Syntax:
            %       A = diff(A, order, schedulingDimension)
            %
            %   Inputs:
            %       order: order of the derivative (must be non-negative
            %       integer)
            %
            %       schedulingDimension: dimension of the scheduling
            %       variable (required for re-indexing the basis function
            %       in terms of the ext. scheduling variable)
            %
            if round(order) ~= order || order < 0
                error('Order must be non-negative integer');
            end
            % Higher-order derivative is executed by repeatedly performing
            % first-order derivative
            for i=1:order
                A = diff_(A, schedulingDimension);
            end
        end
        
        function c = eq(A, B)
            c = strcmp(A.sortKey, B.sortKey);
            if c && ~strcmp(A.dependenceType,B.dependenceType)
                warning(['Both basis functions will evaluate the same' ...
                     ' but are of different dependenceType: ', ...
                     A.dependenceType, ' and ' B.dependenceType])
            end
        end
        
        function c = ne(A, B)
            c = ~eq(A, B);
        end
        
        s = str(obj, rhoNames)
            
        function disp(obj, rhoNames)
            fprintf('Basis function with %s scheduling dependence:\n\n', ...
                obj.dependenceType);
            if nargin == 1
                fprintf(['\tphi = ', str(obj), '\n\n']);
            else
                fprintf(['\tphi = ', str(obj, rhoNames), '\n\n']);
            end
        end

        function n = minNRho(obj, varargin)
            % MINNRHO Returns the minimum required number of scheduling
            % signals to evaluate basis functions
            %
            % This function can be used to query the minimum required
            % number of scheduling signals needed to evaluate one or more
            % specified basis functions. This functionality comes in handy
            % when validating the scheduling data for simulation.
            %
            %   Syntax:
            %       N = minNRho(bfunc)
            %       N = minNRho(bfunc1, bfunc2, ..., bfuncN)
            %
            %   Input:
            %       bfunc(1/2/.../N): a scheduling dependent basis function
            %
            %   Outputs:
            %       N: integer denoting the minimum number of scheduling
            %           signals that must be used to evaluate each of the input
            %           basis functions (if more than 1 basis functions are
            %           passed in the input, then N will represent the
            %           maximum of minNRho() applied to each basis function
            %           individually.
            %
            narginchk(1, Inf);
            nargoutchk(0, 1);
            n = minNRho_(obj);
            for i=1:numel(varargin)
                n = max([n, minNRho_(varargin{i})]);
            end
        end

        function n = rhoDependency(varargin)
            %RHODEPENDENCY Returns the indeces of the scheduling signals
            %the basis functions depends on.
            %
            %This functionality is used by PMATRIX/SIMPLIFY in order to
            %remove any unused scheduling dependencies from the timemap.
            %
            %   Syntax:
            %       n = rhoDependency(bfunc)
            %       n = rhoDependency(bfunc1, bfunc2, ..., bfuncN)
            %
            %   Input:
            %       bfunc(1/2/.../N): a scheduling dependent basis function
            %
            %   Outputs:
            %       n: Cell array with N elements containing in the indeces
            %          of the scheduling depency of each basis function.
            %

            narginchk(1, Inf);
            nargoutchk(0, 1);
            n = cellfun(@rhoDependency_, varargin, 'UniformOutput', false);
        end

    end
    
    methods (Hidden)
        function key = sortkey(obj)
            warning('sortkey() is a part of the legacy code, please use obj.sortKey')
            try
                key = obj.sortKey;
            catch
                key = -1;
            end
        end

    end
    
    methods (Hidden, Abstract, Access=protected)
        A = diff_(A, schedulingDimension);
        n = minNRho_(A);
        n = rhoDependency_(A);
    end
end

