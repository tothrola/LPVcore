function [sysr, mapping_fnc, info] = lpvaered(sys, nrho, data, aeopt, optopt)
%LPVAERED AE-based reduction of scheduling signal dimension for LPVLFR
%models based on: "Model Reduction in Linear Parameter-Varying Models using
% Autoencoder Neural Networks" by S. Z. Rizvi, F. Abbasi, and J. M. Velni 
% (ACC 2018). doi: 10.23919/ACC.2018.8431912.
%
%   Syntax:
%       sysr = lpvaered(sys, nrho, data)
%
%       sysr = lpvaered(sys, nrho, data, ...)
%
%       [sysr, mapping_fnc, info] = __
%
%   Inputs:
%       sys: Original model (data) as one of the following:
%           1) LPVcore.lpvss object
%           2) State-space array, where the third dimension represent the
%               model dynamics at the scheduling points in 'data'
%       nrho (int): Dimension of reduced scheduling signal.
%       data: Scheduling data specified as one of the following:
%           1) LPVIDDATA object with scheduling-output-input data. Only the
%               scheduling data is used.
%           2) An N-by-np numeric matrix with N the number of time samples
%               and np the number of scheduling signals.
%
%   Optional name-value pairs related to the AutoEncoder:
%       'maxEpochs' (Default = 1000): The maximum number of training epochs. 
%       'useGPU' (Default = false): Option to specify if GPU should be used
%           for training of the neural network (true) or not (false).
%       'trainingOptions': structure containing other options to train the
%           autoencoder. See "help trainAutoencoder" to find the rest of 
%           the options. We suggest avoiding the "purelin" setting for
%           "DecoderTransferFunction".
%
%   Optional name-value pairs related to the reduced-order model optimization:
%       'feedthrough' (default = 'true'): Forces the reduced order model
%               to have no feedthrough if false.
%       'solver': structure containing solver settings for Yalmip,
%                     based on sdpsettings.
%
%   Outputs:
%       sysr: Model with reduced scheduling signal dimension as 
%           LPVCORE.LPVSS object.
%       mapping_fnc: function handle that maps a scheduling signal
%           trajectory from the original model 'sys' to the reduced-
%           complexity model 'sysr'. Example usage:
%           >> [sysr, mapping_fnc] = lpvaered(sys, 1, data)
%           >> alpha = randn(N, 10)         % Original scheduling signal
%           >> rho = mapping_fnc(alpha)     % Reduced scheduling signal
%           >> lsim(sys, alpha, ...)        % Simulate original model
%           >> lsim(sysr, rho, ...)         % Simulate reduced model
%       info: struct containing the following fields:
%           TODO: description of info and maybe adding other relevant
%           fields.
%

% TODO: add support for non-static dependence

arguments
    sys   {mustBeValidSys}
    nrho  (1,1) {mustBePositive, mustBeInteger}
    data  {mustBeValidData}
    aeopt.MaxEpochs (1,1) {mustBeNonnegative} = 1000;
    aeopt.UseGPU = false;
    aeopt.trainingOptions = struct();
    optopt.feedthrough logical = true;
    optopt.solverOptions = struct();
end

    %% Check input data
    if isa(data, 'lpviddata')
        pDat = data.p;
    else
        pDat = data;
    end
    
    np = sys.Np;
    nx = sys.Nx;
    nu = sys.Nu;
    ny = sys.Ny;
    nt = size(pDat,1);
    if size(pDat,2) ~= np
        pDat = pDat';
        warning('Transposing simulation data, as it should be of dimensions N x Np')
    end
    assert(size(pDat, 2) == np, ['The dimension of scheduling variables of ' ...
        'the simulation data should match the system.'])
    assert(nt >= np, ['The input data should be an N-by-Np matrix, ' ...
        'where N >= Np.'])

    %% Preprocessing of training data
    % We scale the data ourselves to have full control.
    % Normalize scheduling data
    [pDat_bar, pNormalize] = normalizeData(pDat');
    
    % Remove constant rows
    [inputData, removeConstantSchedSettings] = removeconstantrows(pDat_bar);
    % Create matrix such that inputData = Ip * pDat_bar;
    Ip = eye(np);
    Ip(removeConstantSchedSettings.remove, :) = [];

	%% Perform parameter reduction
    % Parse options to trainAutoencoder
    aeTrain.MaxEpochs = aeopt.MaxEpochs;
    aeTrain.UseGPU = aeopt.UseGPU;
    aeTrain.ScaleData = false; 
    names = fieldnames(aeopt.trainingOptions);
    if ~isempty(names)
        for i = 1:numel(names)
            aeTrain.(names{i}) = aeopt.trainingOptions.(names{i});
        end
    end
    parseAE=[fieldnames(aeTrain).'; struct2cell(aeTrain).'];
    parseAE=parseAE(:).';

    % Train AutoEncoder
    autoenc = trainAutoencoder(inputData,nrho,parseAE{:});
    % Save Network data
    info.Phi = encode(autoenc,inputData); % Reduced order scheduling data
    info.Rho_h = predict(autoenc,inputData); % Reconstructed scheduling data
    We = autoenc.EncoderWeights;
    be = autoenc.EncoderBiases;
    
    info.EncoderTransferFunction = autoenc.EncoderTransferFunction;
    info.DecoderTransferFunction = autoenc.DecoderTransferFunction;
   
    %% Construct mapping
    mapping_fnc = @(p) feval(info.EncoderTransferFunction,(We*Ip*pNormalize(p') + be))';

    %%  Optimization problem to find the Reduced Order matrices | YALMIP required
    %FOM system
    Q = [sys.A.matrices, sys.B.matrices; sys.C.matrices, sys.D.matrices];
    Qm = calcMat(Q,pDat'); %Using de-normalized trajectory data!

    %ROM system
    R = sdpvar(nx + ny, nx + nu, nrho + 1,'full');
    if optopt.feedthrough == false
        R(nx+1:end,nx+1:end,:) = 0;
    end
    Rm = calcMat(R,info.Phi);
    
    %Cost function
    diff = Qm-Rm;
    cost = (1/nt)*trace(diff'*diff);

    %Optimization
    solverOpts = sdpsettings;
    names = fieldnames(optopt.solverOptions);
    if ~isempty(names)
        for i = 1:numel(names)
            solverOpts.(names{i}) = optopt.solverOptions.(names{i});
        end
    end
    optimize([],cost,solverOpts);
    R = value(R);
    R(abs(R) < eps ) = 0; % Remove numerical noise
    
    % Construct reduced affine LPV-SS object
    if sys.Ts == 0
        timeStr = 'ct';
    else
        timeStr = 'dt';
    end
    Lmat = pmatrix(cat(3,R(:,:,2:end),R(:,:,1)),'SchedulingDimension', nrho, 'SchedulingDomain',timeStr);

    A = Lmat(1:nx, 1:nx);
    B = Lmat(1:nx, nx+1:end);
    C = Lmat(nx+1:end, 1:nx);
    D = Lmat(nx+1:end, nx+1:end);

    sysr = LPVcore.lpvss(A, B, C, D, sys.Ts);
    sysr = LPVcore.copySignalProperties(sysr, sys, 'full');

    % Assign expected bounds on scheduling variables
    for i = 1:nrho
        sysr.SchedulingTimeMap.Range{i} = [min(info.Phi(:,i)), max(info.Phi(:,i))];
    end
end


function Qm = calcMat(Q,Rho)
    N = size(Rho,2);
    np = size(Rho,1);
    
    Q0 = repmat(reshape(Q(:,:,1),[],1),1,N);
    Qi = reshape(Q(:,:,2:end),numel(Q(:,:,1)),np)*Rho;
    Qm = reshape(permute(reshape(Q0+Qi,size(Q,1),size(Q,2),[]),[1 3 2]),size(Q(:,:,1)).*[N 1]);
end

% Data normalization function
function [dataBar, Ncal, Ncal_inv, N, n] = normalizeData(data)
    data_mean = mean(data, 2);
    dataDiff = max(data,[], 2) - min(data,[],2);
    dataDiff(abs(dataDiff) <= sqrt(eps)) = 1;
    Ncal = @(x) (x - data_mean) ./ dataDiff;
    Ncal_inv = @(x) dataDiff .* x + data_mean; 
    N = dataDiff;
    n = data_mean;
    dataBar = Ncal(data);
end

% Check hiddenLayerSize function
function mustBeValidTransfer(vec)
    if ~strcmp(vec,'logsig') && ~strcmp(vec,'satlin')
        eidType = 'LPVcore:datatype';
        msgType = ['''Encoder/Decoder'' transfer function must be "logsig"' ...
            ' or "satlin"'];
        throwAsCaller(MException(eidType, msgType));
    end
end

% Check input lpv system function
function mustBeValidSys(P)
    mustBeA(P, {'LPVcore.lpvss'});
    if ~hasStaticSchedulingDependence(P)
        eidType = 'LPVcore:staticDependencyCheck';
        msgType = ['The system has dynamic scheduling dependency, ' ...
            'which is currently not supported.'];
        throwAsCaller(MException(eidType, msgType));
    end
end

% Check input data function
function mustBeValidData(data)
    mustBeA(data, {'lpviddata', 'double'});

    if isa(data,'double')
        mustBeNumeric(data);
        if ~ismatrix(data)
            eidType = 'LPVcore:datatype';
            msgType = ['''data'' must be an ''lpviddata''' ...
                ' object or a numeric matrix'];
            throwAsCaller(MException(eidType, msgType));
        end
        mustBeFinite(data);
        mustBeNonNan(data);
    end
end