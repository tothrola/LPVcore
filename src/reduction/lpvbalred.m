function [sysred, Tred, info] = lpvbalred(sys, rx, pgrid, solver, delta, iteropts)

    % LPVGRAMIANRED Model order reduction by gramian-based balanced 
    % reduction for stable LPV-SS models
    %
    % This function implements the LPV model reduction technique proposed in:
    % [1] "Approximation of linear parameter-varying systems,"
    %     by G. Wood, P. Goddard, and K. Glover (1996)
    %
    % Syntax:
    %     sysr = lpvbalred(sys, rx, pgrid)
    %     sysr = lpvbalred(sys, rx, pgrid, solver, delta, iteropts)
    %     [sysr, Tred, info] = lpvbalred(__)
    %
    % Inputs:
    %   sys: LPVSS object. The reduction extracts frozen models and obtains
    %           a transformation from then. The trasnformation is applied
    %           to the original model to obtain the reduced order system.
    %   rx: integer denoting the reduction order.
    %   grid: 
    %       p, p1, ..., pn: 1-by-np vector of frozen scheduling values
    %       s: structure specifying a rectangular grid of operating
    %           points. Each field in the structure corresponds
    %           to a scheduling signal. Example:
    %               s = struct('p', [0, 1], 'q', [-1, 0])
    %           evaluates the LPV representation at 4 operating
    %           points (each combination of p = 0 / 1 and q = -1 /
    %           0).
    %   solver (default = 'sdpt3'): String with the solver that Yalmip will
    %           use to run the LMI-based optimization.
    %   delta (default = 1e-8): Numerical value used to enforce strictness
    %           on the LMI inequalities.
    %   iteropts: structure with the following fields, related to the 
    %           iterative optimization of J = min(P*Q):
    %       accuracy (default = 1e-7): Threshold on the absolute difference
    %           between the cost (J) of the previous and current
    %           iterations.
    %       convergence: (default = 1e-8): Threshold on the relative 
    %           difference between the cost (J) of the previous and current
    %           iterations
    %       maxIter (default = 20): Threshold on the maximum number of
    %           iterations.
    % 

    %TODO: Add support for LPVcore.lpvgridss objects

    arguments
        sys   {mustBeValidSys}
        rx  (1,1) {mustBePositive, mustBeInteger}
        pgrid  {mustBeValidData}
        solver = 'sdpt3';
        delta = 1e-8;
        iteropts.accuracy = 1e-7;   
        iteropts.convergence = 1e-8;
        iteropts.maxIter = 20;      
    end

    %% Input data
    if isa(pgrid, "double")
        if size(pgrid,2) ~= sys.Np
            pgrid = pgrid';
            assert(size(pgrid,2) == sys.Np, 'The grid must be N-by-Np')
        end
            pgrid = num2cell(pgrid,2);
    end
    
    % Check CT or DT
    ct = isct(sys);
    %%
    models_ = extractLocal(sys, pgrid);
    if isa(pgrid,"struct")
        sysarray = models_;
    else
        sysarray = cat(3,models_{:});
    end

    ng = numel(sysarray(1,1,:));
    assert(isstable(sysarray),'The frozen system is not stable at some points of the grid.'); % check if every LTI model is stable
    A = sysarray.A;
    B = sysarray.B;
    C = sysarray.C;

    nx = size(A,1);

    %% Initialization: Computing Controllability Gramians for frozen LTI models    
    P0 = NaN(nx, nx, ng);
    switch ct
        case 1 %Continuous-time
            for i = 1:ng
                P0(:, :, i) = lyap(A(:, :, i), B(:, :, i)*B(:, :, i)');
            end
        case 0 %Discrete-time
            for i = 1:ng
                P0(:, :, i) = dlyap(A(:, :, i), B(:, :, i)*B(:, :, i)');
            end
    end

    %% Step 1: Find Q1
    J1 = 0;
    yalmip('clear')
    Q1 = sdpvar(nx); %Enforces Q1 to be symmetric by default
    constraints = [];
    LMI2 = Q1 >= delta*eye(nx);

    switch ct
        case 1 %Continuous-time
            for k = 1:ng
                LMI1 = A(:,:,k)'*Q1 + Q1*A(:,:,k) + C(:,:,k)'*C(:,:,k) <= -delta*eye(nx);
                constraints = [constraints, LMI1]; %#ok<AGROW>
                J1 = J1 + trace(Q1*P0(:,:,k));
            end
        case 0 %Discrete-time
            for k = 1:ng
                LMI1 = [Q1 - C(:,:,k)'*C(:,:,k), A(:,:,k)*Q1;
                        Q1*A(:,:,k)', Q1] >=  delta*eye(2*nx);
                constraints = [constraints, LMI1]; %#ok<AGROW>
                J1 = J1 + trace(Q1*P0(:,:,k));
            end
    end
    constraints = [constraints, LMI2];
    options = sdpsettings('verbose', 0,'solver',solver, 'warning', 1);
    diag = optimize(constraints, J1, options);
    assert(~diag.problem,'Feasible solution not found. Please, try another solver or system settings');

    %% Step 2: Iterative procedure
    JCurr = 2*value(J1);              % Current cost function
    JPrev = value(J1);      % Cost function from previous iteration
    QCurr = value(Q1);

    counter = 1;
    while (abs(JCurr-JPrev) > iteropts.accuracy) && (100*abs(JCurr-JPrev)/JPrev > iteropts.convergence) && (counter < iteropts.maxIter) 
        yalmip('clear')
        JPrev = JCurr;
        constraints = [];
        % in one iteration we solve for Pi, using previous Qi
        if mod(counter,2) == 1
            P = sdpvar(nx);
            % adding every LMI from the frozen grid
            switch ct
                case 1 %Continuous-time
                    for k = 1:ng
                        LMI1 = A(:,:,k)*P + P*A(:,:,k)' + B(:,:,k)*B(:,:,k)' <= -delta*eye(nx);
                        constraints = [constraints, LMI1]; %#ok<AGROW>
                    end
                case 0 %Discrete-time
                    for k = 1:ng
                        LMI1 = [P - B(:,:,k)*B(:,:,k)', A(:,:,k)*P;
                            P*A(:,:,k)', P] >=  delta*eye(2*nx);
                        constraints = [constraints, LMI1]; %#ok<AGROW>
                    end
            end
            LMI2 = P >= delta*eye(nx);
            constraints = [constraints, LMI2]; %#ok<AGROW> 
            cost = trace(P*QCurr);
            optimize(constraints, cost, options);
            PCurr = value(P);

            % in another iteration we solve for Qi, using previous Pi
        else
            Q = sdpvar(nx,nx);
            switch ct
                case 1 %Continuous-time
                    for k = 1:ng
                        LMI1 = A(:,:,k)'*Q + Q*A(:,:,k) + C(:,:,k)'*C(:,:,k) <= -delta*eye(nx);
                        constraints = [constraints, LMI1]; %#ok<AGROW>
                    end
                case 0 %Discrete-time
                    for k = 1:ng
                    LMI1 = [Q - C(:,:,k)'*C(:,:,k), A(:,:,k)*Q;
                            Q*A(:,:,k)', Q] >=  delta*eye(2*nx);
                        constraints = [constraints, LMI1]; %#ok<AGROW>
                    end
            end
            LMI2 = Q >= delta*eye(nx);
            constraints = [constraints, LMI2]; %#ok<AGROW> 
            cost = trace(Q*PCurr);
            optimize(constraints, cost, options);
            QCurr = value(Q);
        end
        JCurr = value(cost);
        counter = counter + 1;
    end
    
    if counter >= iteropts.maxIter
        warning('Iterative gramian search stopped by max. iter. limit');
    end
    % validation of solution
    P = PCurr;
    Q = QCurr;
    if min(eig(P)) < 0 %Allow some tolerance
        error("P is not positive definite!")
    elseif min(eig(Q)) < 0
        error("Q is not positive definite!")
    end


    %% Balanced transformation
    R = chol(P)';
    [U,Lambda,~] = svd(R'*Q*R);
    T = Lambda^(1/4) * U'/R;

    Pbal = T*P*T';
    Qbal = (T')\Q/(T);
    HSV = sqrt(eig(Pbal*Qbal));

    %Balance original LPVSS system!
    Ab = T*sys.A/T;
    Bb = T*sys.B;
    Cb = sys.C/T;
    Db = sys.D;

    %% Balanced reduction
    Ared = pmatrix(Ab.matrices(1:rx,1:rx,:), Ab.bfuncs, 'SchedulingTimeMap', Ab.timemap);
    Bred = pmatrix(Bb.matrices(1:rx,:,:), Bb.bfuncs, 'SchedulingTimeMap', Bb.timemap);
    Cred = pmatrix(Cb.matrices(:,1:rx,:), Cb.bfuncs, 'SchedulingTimeMap', Cb.timemap);
    Dred = Db;

    %Transformation matrix required to construct the reduced-order
    %scheduling map: \hat{p} = eta(T*x_red,u)
    Tred = T(:,1:rx);
    sysred = LPVcore.lpvss(Ared, Bred, Cred, Dred, sys.Ts);
    info = struct('HSV', HSV, 'JCost', JCurr, 'Transformation',T);

end

function mustBeValidSys(P)
    mustBeA(P, {'LPVcore.lpvss'});
end

% Check input data function
function mustBeValidData(data)
    mustBeA(data, {'cell', 'double','struct'});
    if isa(data,'double')
        mustBeNumeric(data);
        if ~ismatrix(data)
            eidType = 'LPVcore:datatype';
            msgType = ['''data'' must be a' ...
                ' N x Np cell or numeric matrix'];
            throwAsCaller(MException(eidType, msgType));
        end
        mustBeFinite(data);
        mustBeNonNan(data);
    end
end

