function [sysr, mapping_fnc, info] = lpvkpcared(sys, nrho, data, kpcaOpts)
%LPVKPCARED Kernel PCA-based (nonlinear) reduction of scheduling signal 
% dimension for LPV-SS models [1]
%
% Reference:
%       [1] ‘A Kernel-Based PCA Approach to Model Reduction of Linear 
%       Parameter-Varying Systems by S. Z. Rizvi, J. Mohammadpour, 
%       R. Toth, and N. Meskin  (IEEE Trans. Contr. Syst. Technol, 2016)
%
% Syntax:
%   sysr = lpvpcared(sys, nrho, data, kpcaopts)
%   [sysr, mapping_fnc, info] = __
%
% Inputs:
%   sys: original model (LPVLFR object)
%   nrho: dimension of reduced scheduling signal
%   data: simulation data specified as one of the following:
%         1) LPVIDDATA object with scheduling-output-input data. Only the
%           scheduling data is used.
%         2) An N-by-np numeric matrix with N the number of time samples
%           and np the number of scheduling signals.
%   Optional name-value pairs:
%           'normalization' (default = 1): Normalizes and centers the
%               scheduling data. Highly recommended.
%           'feedthrough' (default = 'true'): Forces the reduced order model
%               to have no feedthrough if false.
%           'ker' (default = 'sigmoid'): Choose the kernel type:
%                  'linear'  - for linearly separable data
%                  'rbf'     - rbf function for linearly non-separable data
%                  'poly'    - polynomial for linearly non-separable data
%                  'sigmoid' - sigmoid function
%                  'multiquad' - multiquadratic function
%           'kerParam' (default = [0.1 0.1]) 1x2 array [s c] with 
%               kernel settings:
%                   linear:    s = not applicable,
%                              c = not applicable
%                   poly:      s = degree of polynomial function, 
%                                   i.e. k = (u'*v + 1)^s
%                              c = not applicable
%                   rbf:       s = sigma/variance of the Gaussian function
%                                 s can be a vector of length 'n' if input 
%                                 data has length 'n'
%                              c = not applicable
%                   sigmoid:   k = tanh(s*u'*v + c)
%                   multiquad: k = sqrt( (norm(u-v,2))^2 + s^2 );
%           'display' (default = 'false'): Display a notion of expected 
%                   accuracy in function of the m_index (reduced 
%                   scheduling dimension)
%           'solver': structure containing solver settings for Yalmip,
%                     based on sdpsettings.
%
% Outputs:
%   sysr: model with reduced scheduling signal dimension (LPVCORE.LPVSS object)
%   mapping_fnc: function handle that maps a scheduling signal
%       trajectory from the original model 'sys' to the reduced-complexity
%       model 'sysr'. Example usage:
%           >> [sysr, mapping_fnc] = lpvkpcared(sys, 1, data)
%           >> alpha = randn(N, 10)         % Original scheduling signal
%           >> rho = mapping_fnc(alpha)     % Reduced scheduling signal
%           >> lsim(sys, alpha, ...)        % Simulate original model
%           >> lsim(sysr, rho, ...)         % Simulate reduced model
%   info: struct containing the following fields:
%           'cost': Value of the cost function achieved during the
%           optimization.
%           'Phi': Reduced-order scheduling trajectories obtained during
%               the reduction.
%           'time': Time (seconds) of the optimization step.
%           'Ar, Br, Cr, Dr': Obtained reduced order matrices, with the 
%               following structutre:
%               Ar = A_0 + A_1*p_1 + ... + A_n*p_n

%   TODO:
%       Add support for LPV-LFR models


    arguments
        sys   {mustBeValidSys}
        nrho  (1,1) {mustBePositive, mustBeInteger}
        data  {mustBeValidData}
        kpcaOpts.normalization = 1;
        kpcaOpts.feedthrough logical = true;
        kpcaOpts.ker char  = 'sigmoid';
        kpcaOpts.kerParam = [0.1 0.1];
        kpcaOpts.display logical = false;
        kpcaOpts.solver = struct();
    end

    %% Create input data
    if isa(data, 'lpviddata')
        pDat = data.p;
    else
        pDat = data;
    end
    
    %% Check compatibility of input data
    if size(pDat,2) ~= sys.Np
        pDat = pDat';
        warning('Transposing simulation data, as it should be of dimensions N x Np')
    end
    assert(size(pDat,2) == sys.Np, ['The dimension of scheduling variables of ' ...
        'the simulation data should match the system.'])
    Nt = size(pDat,1); Np = size(pDat,2);
    assert(Nt >= Np, ['The input data should be an N-by-Np matrix, ' ...
        'where N >= Np.'])
    %% Scheduling map normalization step
    % Transpose pDat such that each column => time sample
    [pDat_bar, ~, ~] = normalizeData(pDat', kpcaOpts.normalization);

    %% Compute Gram Matrix
    K = Get_kernel(kpcaOpts.ker, pDat_bar, pDat_bar, kpcaOpts.kerParam(1), kpcaOpts.kerParam(2));
    Knorm = K_normalization(K);

    %% Eigenvalue decomposition + Normalization of eigenvectors
    mapping_kpca = KPCA_projection(Knorm, nrho, Np, kpcaOpts.display);

    %% Projection from np scheduling variables to nrho
    pRed = rho_projection(mapping_kpca,K,Nt);
    info.Phi = pRed;

    %% Scheduling map function for self-scheduling simulations
    K_fnc = @(p) Get_kernel(kpcaOpts.ker, pDat', p', kpcaOpts.kerParam(1), kpcaOpts.kerParam(2));
    mapping_fnc = @(p) rho_projection(mapping_kpca, K_fnc(p), size(p,1))';

    %%  Optimization problem to find the RO matrices | YALMIP required
    %track computing time
    tic;
    nx = size(sys.A,1); [ny, nu] = size(sys.D);

    %FOM system
    Q = [sys.A.matrices, sys.B.matrices; sys.C.matrices, sys.D.matrices];
    Qm = calcMat(Q,pDat'); %Using denormalized trajectory data!

    %ROM system
    R = sdpvar(nx + ny, nx + nu, nrho + 1,'full');
    if kpcaOpts.feedthrough == false
        R(nx+1:end,nx+1:end,:) = 0;
    end
    Rm = calcMat(R,pRed);

    %Cost function
    diff = Qm - Rm;
    cost = (1/Nt)*trace(diff'*diff);

    %Optimization    
    solverOpts = sdpsettings;
    names = fieldnames(kpcaOpts.solver);
    if ~isempty(names)
        for i = 1:numel(names)
            solverOpts.(names{i}) = kpcaOpts.solver.(names{i});
        end
    end

    optimize([],cost,solverOpts);
    tend = toc;
    cost = value(cost);  info.cost = cost;  info.time = tend;

    %Get optimization results
    R = value(R);
    R(abs(R) < eps ) = 0; % Remove numerical noise

    %% Create Reduced Order Model
    if sys.Ts == 0
        timeStr = 'ct';
    else
        timeStr = 'dt';
    end
    
    Lmat = pmatrix(cat(3,R(:,:,2:end),R(:,:,1)),'SchedulingDimension', nrho, 'SchedulingDomain',timeStr);
    A = Lmat(1:nx, 1:nx);
    B = Lmat(1:nx, nx+1:end);
    C = Lmat(nx+1:end, 1:nx);
    D = Lmat(nx+1:end, nx+1:end);
    sysr = LPVcore.lpvss(A, B, C, D, sys.Ts);
    sysr = LPVcore.copySignalProperties(sysr, sys, 'full');

    % Assign expected bounds on scheduling variables
    for i = 1:nrho
        sysr.SchedulingTimeMap.Range{i} = [min(info.Phi(:,i)), max(info.Phi(:,i))];
    end
    
    % Save reduced order matrices
    info.Ar = A.matrices;
    info.Br = B.matrices;
    info.Cr = C.matrices;
    info.Dr = D.matrices;

end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%
%                            LOCAL FUNCTIONS                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Feature space normalization
function K_norm = K_normalization(K)
    N = size(K,1);
    one_mat = ones(size(K)).*(1/N);
    K_norm = K - one_mat*K - K*one_mat + one_mat*K*one_mat;
end

%Projection of alpha in the feature space
function rho = rho_projection(alpha,K,Nt)
nrho = size(alpha,2);
rho = zeros(nrho,Nt);
%NOTE: the triple for loop is necessary to obtain the reduced order
% scheduling map.
for t = 1:Nt
    for r = 1:nrho
        for j = 1:size(alpha,1)
            rho(r,t) = rho(r,t) + alpha(j,r)*K(j,t);
        end
    end
end
end

% Data normalization function
function [dataBar, Ncal, Ncal_inv, N, n] = normalizeData(data, activate)
if activate == 1
    data_mean = mean(data, 2);
    dataDiff = max(data,[], 2) - min(data,[],2);
    dataDiff(abs(dataDiff) <= sqrt(eps)) = 1;

    Ncal = @(x) (x - data_mean) ./ dataDiff;
    Ncal_inv = @(x) dataDiff .* x + data_mean;

    N = dataDiff;
    n = data_mean;

    dataBar = Ncal(data);
else
    dataBar = data;
    Ncal = @(x) x;
    Ncal_inv = @(x) x;
    N = 0;
    n = 0;
end
end

function Qm = calcMat(Q,Rho)
    Nt = size(Rho,2);
    np = size(Rho,1);
    Q0 = repmat(reshape(Q(:,:,1),[],1),1,Nt);
    Qi = reshape(Q(:,:,2:end),numel(Q(:,:,1)),np)*Rho;
    Qm = reshape(permute(reshape(Q0+Qi,size(Q,1),size(Q,2),[]),[1 3 2]),size(Q(:,:,1)).*[Nt 1]);
end

function alpha = KPCA_projection(K, nrho, Np, display)
    Nt = size(K,2);
    if display == true
        % Notion of accuracy based on the eigenvalues
        lDiag = abs(eig(K));
        total_variation = sum(lDiag);
        variation_m = zeros(Np,1);
        for i=1:Np
            variation_m(i) = sum(lDiag(1:i));
        end
        Expected_accuracy = variation_m./total_variation*100;
        Expected_accuracy = Expected_accuracy(1:Np-1);
        % Display accuracy indeces
        Reduction_target = (1:Np-1)';
        T = table(Reduction_target, Expected_accuracy); disp(T);
        fprintf('Expected accuracy = %f for reduction target nrho = %i.\n', Expected_accuracy(nrho), nrho);
        fprintf('Try other kernel settings or increase nrho for a higher expected accuracy.\n');
    end

    % Obtain projection. Note that Nt must be taken into account!
    % This solves A*V = B*V*D, A*V = Khat*alpha, B*V*D = lambda*Khat*alpha
    [alpha, lambda] = eigs(K,eye(Nt).*Nt,nrho,'largestabs');
    % Eigenvalue normalization
    alpha = (alpha*real(diag(1./sqrt(diag(lambda)'))));
end


%Kernel definition
function K = Get_kernel(ker,Xn,Xt,s,c)
    % Performs a kernel function on column vectors 'u' and 'v'. String 'ker'
    % can take one of the following forms:
    %                  'linear'  - for linearly separable data
    %                  'rbf'     - rbf function for linearly non-separable data
    %                  'poly'    - polynomial for linearly non-separable data
    %                  'sigmoid' - sigmoid function
    %                  'multiquad' - multiquadratic function
    %
    % Parameters 's', 'c' are kernel specific parameters with the following
    % meanings for
    %
    % linear:    s = not applicable,
    %            c = not applicable
    % poly:      s = degree of polynomial function, i.e. k = (u'*v + 1)^s
    %            c = not applicable
    % rbf:       s = sigma/variance of the Gaussian function
    %                s can be a vector of length 'n' if input data has length
    %                'n'
    %            c = not applicable
    % sigmoid:   k = tanh(s*u'*v + c)
    % multiquad: k = sqrt( (norm(u-v,2))^2 + s^2 );
    N = size(Xn,2);
    T = size(Xt,2);
    K = zeros(N,T);
    for i=1:N
        for j=1:T
            K(i,j) = kernel(ker,Xn(:,i),Xt(:,j),s,c);
        end
    end
end
function k = kernel(ker,u,v,s,c)
    switch lower(ker)
        case 'linear'
            k = u'*v;
        case 'poly'
            k = (u'*v + c)^s;
        case 'poly_n'
            k = ((u'*v+c)^s)/sqrt((u'*u+c)^s*(v'*v+c)^s);
        case 'rbf'
            sig = diag(1./s.^2);
            k = exp(-(u-v)'*sig*(u-v) );
        case 'sigmoid'
            k = tanh(s*u'*v + c);
        case 'multiquad'
            k = sqrt( (norm(u-v,2))^2 + s^2 );
        otherwise
            warning(['Invalid Kernel choice. Using a sigmoid kernel with ', ...
                '[s, c] = [0.1 0.1] instead.'])
            k = tanh(0.1*u'*v + 0.1);
    end
end

% Check input lpv system function
function mustBeValidSys(P)
    mustBeA(P, {'LPVcore.lpvlfr', 'LPVcore.lpvss'});
    if isa(P, 'LPVcore.lpvlfr')
        if ~hasStaticSchedulingDependence(P)
            eidType = 'LPVcore:staticDependencyCheck';
            msgType = ['The system has dynamic scheduling dependency, ' ...
                'which is currently not supported.'];
            throwAsCaller(MException(eidType, msgType));
        end
    end
end

% Check input data function
function mustBeValidData(data)
    mustBeA(data, {'lpviddata', 'double'});
    if isa(data,'double')
        mustBeNumeric(data);
        if ~ismatrix(data)
            eidType = 'LPVcore:datatype';
            msgType = ['''data'' must be an ''lpviddata''' ...
                ' object or a numeric matrix'];
            throwAsCaller(MException(eidType, msgType));
        end
        mustBeFinite(data);
        mustBeNonNan(data);
    end
end
