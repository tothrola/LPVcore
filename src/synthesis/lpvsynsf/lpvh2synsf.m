function [K,gam,Xcl] = lpvh2synsf(P,nu,synopts)
%LPVH2SYN Generalized H2 suboptimal state-feeback LPV controller 
%synthesis. 
%
%   Syntax:
%       [K, gam, Xcl] = lpvh2synsf(P, nu)
%
%       [K, gam, Xcl] = lpvh2synsf(P, nu, synopts)
%
%   Inputs:
%       P: LPV system as LPVcore.lpvss or lpvgridss object. In case of a
%           LPVcore.lpvss object, the scheduling dependency should be
%           affine.
%       nu (int): The number of controlled variables, i.e. the last nu
%           inputs of the plant P that are connected to the output of the
%           state-feedback controller K.
%       synopts (lpvsynsfOptions): Stucture containing LPV state-feedback
%           synthesis options. See <a href="matlab:helpPopup 
%           lpvsynsfOptions">lpvsynsfOptions</a>.
%
%	Outputs:
%       K: State-feedback controller matrix as pmatrix object.
%       gam: Upperbound for the gain of the closed-loop system.
%       Xcl: Matrix of closed-loop storage/Lyapunov function, i.e. the
%           Lypunov function is of the form V(x) = x'*Xcl*x. Either 
%           constant or parameter dependent.
%
%   See also LPVSYNSF.
%

arguments
    P             {mustBeCompatibleLPVsys}
    nu      (1,1) {mustBeNumeric, mustBePositive}
    synopts (1,1) lpvsynsfOptions = lpvsynsfOptions
end

synopts.performance = 'h2';

[K,gam,Xcl] = lpvsynsf(P,nu,synopts);