%COMBINEGRID creates a combination of all gridpoints. COMBINEGRID is
%an alternative (and faster) implementation of the combvec command.
%
%   Syntax:
%       out = combineGrid(A1, A2, ...)
%
%   Inputs:
%       A1, A2, ... (double): matrices or vectors that are to be combined,
%           where each column represents a seperate grid point.
%
%   Outputs:
%       out (double): Combination of all grid points as matrix, where each
%           row corresponds to a dimension.
%

function out = combineGrid(varargin)
    n = nargin;
    s = cellfun(@size, varargin, 'UniformOutput', false);
    s = vertcat(s{:});
    m = s(:,1);
    l = s(:,2);
    N = prod(l);
    out = zeros(sum(m), N);
    out(1:m(1),:) = repmat(varargin{1}, 1, N/l(1));
    if n > 2
        for i = 2:n-1
            out(sum(m(1:i-1))+1:sum(m(1:i)), :) = repmat(...
                              reshape(...
                                  repmat(varargin{i}, prod(l(1:i-1)), 1), ...
                                      m(i), []), ...
                              1, prod(l(i+1:end)));
        end
    end
    out(sum(m(1:end-1))+1:end, :) = ...
        reshape(repmat(varargin{n}, prod(l(1:n-1)), 1), m(end), []);
end