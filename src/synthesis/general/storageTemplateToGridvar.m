function [X,Xd] = storageTemplateToGridvar(template, nx, gridOpt, domain, sdpopt, diff)
%STORAGETEMPLATETOGRIDVAR Summary of this function goes here
%   Detailed explanation goes here

arguments
    template
    nx
    gridOpt
    domain
    sdpopt = 'symmetric'
    diff = true
end

% Create pmatrix with sdpvar as matrices
Xp = simplify(0 + template);
nb = Xp.numbfuncs;
Xp.matrices = sdpvar(nx, nx, nb, sdpopt);

dim = gridOpt.dim; dimCell = num2cell(dim);
rhoValues = gridOpt.rhoValues';
npStor = gridOpt.npStorage;

% Evaluate (sdpvar) pmatrix at gridpoints
Xval = feval(Xp, rhoValues(:, 1:npStor));
Xval = reshape(Xval, nx, nx, dimCell{:}, ...
    gridOpt.repMatrices{end-npStor+1:end});

if diff
    switch domain
        case 'ct'
            Xpd = pdiff(Xp, 1);
            if all(Xpd.timemap.Map == 1)
                Xpdval = feval(Xpd, rhoValues(:, npStor+1:end));
            else
                Xpdval = feval(Xpd, rhoValues);
            end
        case 'dt'
            Xpd = pshift(Xp, 1);
            Xpdval = feval(Xpd, rhoValues(:, npStor+1:end));
    end
    Xpdval = reshape(Xpdval, nx, nx, dimCell{:}, ...
        gridOpt.repMatrices{end-npStor+1:end});
    Xd = gridvar(Xpdval);
else
    Xd = [];
end
X = gridvar(Xval);
end

