function [gamValue,lambdaValue,extraVarValue,solverError] = linfLineSearch(linfSolve,linfOpt,verbose,backoff,extraVarWidth)
%LINFLINESEARCH Performe a line search for Linf based analysis and synthesis.
%
%   Syntax:
%       [gamValue,lambdaValue,linfSolution,solverError] = 
%                               linfLineSearch(linfSolve, linfOpt, verbose)
%
%   Inputs:
%       linfSolve (optimizer): Optimizer object corresponding to the Linf
%           analysis or synthesis problem.
%       linfOpt (struct): Structure containing the options for Linf 
%           synthesis. See <a href="matlab:help linfOptions"
%           >linfOptions</a>.
%       verbose (logical): Logical indicating if progress messages should
%           be displayed (1) or not (0);
%       backoff (double): Value corresponding how far to backoff from 
%           optimal solution in case of issues;
%       extraVvarWidth (int array): Array of integers corresponding to the
%           width of each extra variable in the optimizer;
%
%   Outputs:
%       gamValue: Minimized induced Linf norm as double.
%       lambdaValue: Value of lambda as double corresponding to gamValue.
%       extraVarValue: Values of the extra variables corresponding to
%           gamValue.
%       solverError: Logical indicating if problem has encountered an
%           error.
%
%   See also LPVSYN, LPVSYNSF, LPVNORM.
%


%% Variables
lambdaGrid = linfOpt.lambdaGrid;
accLambda = linfOpt.accuracyLambda;
maxIt = linfOpt.maxIterations;

nExtraVar = numel(extraVarWidth);

nLambda = 1; nGamma = 2;
nInit = numel(lambdaGrid);
solLambGamm = zeros(nGamma,nInit);
solLambGamm(nLambda,:) = lambdaGrid;

[linfSolution,solverProblem] = linfSolve(lambdaGrid,zeros(1,nInit));
solLambGamm(nGamma,:) = linfSolution{1};

solverError = false;    %   set to false (changed to true if error encountered)

% Resolve with backoff if there are numerical issues
if ~any(solverProblem == 0)
    resolveIndex = (solverProblem~=1) & (solverProblem~=0);
    if ~all(resolveIndex == 0)
        solLambGammResolve = solLambGamm(:,resolveIndex);
        [linfSolutionResolve,solverProblemResolve] = linfSolve(solLambGammResolve(nLambda,:),...
            solLambGammResolve(nGamma,:)*backoff);
        % Set solutions for gamma to resolved solutions
        solLambGamm(nGamma,resolveIndex) = solLambGammResolve(nGamma,:)*backoff;
        solverProblem(resolveIndex) = solverProblemResolve;

        for i = 1:nExtraVar
            extraVarResolveIndex = repmat((1:extraVarWidth(i))',1, sum(resolveIndex)) + (find(resolveIndex) - 1) * extraVarWidth(i);
            linfSolution{i + 1}(:, extraVarResolveIndex(:)) = linfSolutionResolve{i + 1};
        end
    end
end

% If no solution can be found error
if all(solverProblem~=0)
    solverError = true;
    warningMsg = ['No solution found during the initial line search for ',...
        'lambda. Solver returned error codes: ',num2str(solverProblem),...
        '. Consider increassing the number of points or changing the ',...
        'points of lambdagrid, see help linfOptions.'];
    warning(warningMsg);
    gamValue = [];
    lambdaValue = [];
    extraVarValue = [];
    return;
end

% Find minimal value of gamma
[gamma0,idx] = min(solLambGamm(nGamma,:));
lambda0 = solLambGamm(nLambda,idx);
extraVar0 = cell(nExtraVar, 1);
for i = 1:nExtraVar
    extraVarIndex = (1:extraVarWidth(i)) + (idx - 1) * extraVarWidth(i);
    extraVar0{i} = linfSolution{i+1}(:, extraVarIndex(:));
end

if verbose; fprintf('Initial search results:\n\t gamma=%.4g\t\tlambda=%.4g\n',gamma0,lambda0); end
if idx == 1
    lambdamax = solLambGamm(nLambda,idx+1);
    lambdamin = solLambGamm(nLambda,idx);

    solGammInit = solLambGamm(nGamma,[idx,idx+1]);

    t1 = cell(nExtraVar, 1);
    t2 = cell(nExtraVar, 1);
    for i = 1:nExtraVar
        extraId1 = (1:extraVarWidth(i)) + (idx - 1) * extraVarWidth(i);
        extraId2 = (1:extraVarWidth(i)) + (idx) * extraVarWidth(i);
        t1{i} = linfSolution{i+1}(:, extraId1);
        t2{i} = linfSolution{i+1}(:, extraId2);
    end
    extraVarInit = {t1, t2};

    warning(['Minimum gamma found at first index of initial lambda grid. '...
        'Consider lowering values of lambdagrid, see help linfOptions.'])
elseif idx == nInit
    lambdamax = solLambGamm(nLambda,idx);
    lambdamin = solLambGamm(nLambda,idx-1);

    solGammInit = solLambGamm(nGamma,[idx-1,idx]);

    t1 = cell(nExtraVar, 1);
    t2 = cell(nExtraVar, 1);
    for i = 1:nExtraVar
        extraId1 = (1:extraVarWidth(i)) + (idx - 2) * extraVarWidth(i);
        extraId2 = (1:extraVarWidth(i)) + (idx - 1) * extraVarWidth(i);
        t1{i} = linfSolution{i+1}(:, extraId1);
        t2{i} = linfSolution{i+1}(:, extraId2);
    end
    extraVarInit = {t1, t2};

    warning(['Minimum gamma found at last index of initial lambda grid. '...
        'Consider increasing values of lambdagrid, see help linfOptions.'])
else
    lambdamax = solLambGamm(nLambda,idx+1);
    lambdamin = solLambGamm(nLambda,idx-1);

    solGammInit = solLambGamm(nGamma,[idx-1,idx+1]);

    t1 = cell(nExtraVar, 1);
    t2 = cell(nExtraVar, 1);
    for i = 1:nExtraVar
        extraId1 = (1:extraVarWidth(i)) + (idx - 2) * extraVarWidth(i);
        extraId2 = (1:extraVarWidth(i)) + (idx) * extraVarWidth(i);
        t1{i} = linfSolution{i+1}(:, extraId1);
        t2{i} = linfSolution{i+1}(:, extraId2);
    end
    extraVarInit = {t1, t2};
end
if verbose; fprintf('---------------------------------------------\n'); end
solGammInit(isnan(solGammInit)) = Inf;

% Golden ratio search
% See algorithm description here:
% https://gitlab.com/tothrola/LPVcore/-/issues/321
if verbose; fprintf('Golden section search:\n'); end
% if verbose; fprintf('Iter:\tGamma:\t\tLambda (min):\tLambda (max):\n\n'); end
if verbose; fprintf('Iter:\tLambda [Gamma] (min):\tLambda [Gamma] (max):\n\n'); end
gr = (sqrt(5) + 1) / 2; % Golden ratio
x1 = lambdamax-(lambdamax-lambdamin)/gr;
x2 = lambdamin + (lambdamax-lambdamin)/gr;

% Create lambda range using golden ratio
lambdaGrid = [lambdamin,x1, x2, lambdamax];
solGamm = zeros(1,4); solGamm([1,4]) = solGammInit;   % solGamm corresponds to gamma for lambda at lambdaGrid
extraVarGrid = cell(4,1);
extraVarGrid([1,4]) = extraVarInit;

% Determine gamma for the golden ratio range
[linfSolution,solverProblem] = linfSolve(lambdaGrid(2:3),[0 0]);
sol = linfSolution{1};
sol(solverProblem ~= 0) = Inf;  % Set to Inf if not solved correctly
solGamm(2:3) = sol;

t1 = cell(nExtraVar, 1);
t2 = cell(nExtraVar, 1);
for i = 1:nExtraVar
    extraId1 = (1:extraVarWidth(i))';
    extraId2 = (1:extraVarWidth(i))' + extraVarWidth(i);
    t1{i} = linfSolution{i+1}(:, extraId1);
    t2{i} = linfSolution{i+1}(:, extraId2);
end
extraVarGrid([2,3]) = {t1, t2};

if verbose;fprintf('%i\t%.4g [%.4g]\t\t%.4g [%.4g]\n',0,lambdaGrid(1),solGamm(1),lambdaGrid(4),solGamm(4));end

cnt = 1;                % Reset counter
while ((lambdaGrid(4)-lambdaGrid(1)) > accLambda) && ...
      (abs(solGamm(1)-solGamm(4)) >= 1e-3)  &&...
      ~all(isinf(solGamm([1,4]))) &&...
      (cnt <= maxIt)
    
    if all(isinf(solGamm(2:3)))
        lambdaGrid([1,4]) = (lambdaGrid([1,4])-lambda0)*.5+lambda0;
        lambdaGrid(2) = lambdaGrid(4)-(lambdaGrid(4)-lambdaGrid(1))/gr;
        lambdaGrid(3) = lambdaGrid(1) + (lambdaGrid(4)-lambdaGrid(1))/gr;
        [linfSolution,solverProblem] = linfSolve(lambdaGrid,[0 0 0 0]);
        for j = 1:4
            for i = 1:nExtraVar
                extraId = (1:extraVarWidth(i))' + (j - 1) * extraVarWidth(i);
                extraVarGrid{j}{i} = linfSolution{i+1}(:, extraId);
            end
        end
        solGamm = linfSolution{1};
        solGamm(solverProblem ~=0 ) = Inf; % Set to Inf if not solved correctly
    else
        % determine new lambda range
        [~,idx] = min(solGamm(2:3));
        % gamma @ lambdaGrid(2) < gamma @ lambdaGrid(3)
        if idx == 1
            % Update lambdaGrid and corresponding solGamm
            % Set new test point to NaN
            lambdaGrid = [lambdaGrid(1),NaN,lambdaGrid(2:3)];   
            solGamm = [solGamm(1),NaN,solGamm(2:3)];
            extraVarGrid = {extraVarGrid{1}, extraVarGrid{1}, extraVarGrid{2:3}};

            % Compute solution at new lambdaGrid(2)
            lambdaGrid(2) = lambdaGrid(4)-(lambdaGrid(4)-lambdaGrid(1))/gr;

            [linfSolution,solverProblem] = linfSolve(lambdaGrid(2),0);
            solGamm(2) = linfSolution{1};
            if (solverProblem ~= 0) || (solverProblem ~= 1)
                [linfSolution,solverProblem] = linfSolve(lambdaGrid(2),solGamm(2)*backoff);
                solGamm(2) = solGamm(2)*backoff;
            end
            for i = 1:nExtraVar
                extraVarGrid{2}{i} = linfSolution{i+1};
            end
            if solverProblem ~= 0; solGamm(2) = Inf; end
        % gamma @ lambdaGrid(3) < gamma @ lambdaGrid(2)
        else
            % Update lambdaGrid and corresponding solGamm
            % Set new test point to NaN
            lambdaGrid = [lambdaGrid(2:3),NaN,lambdaGrid(4)];
            solGamm = [solGamm(2:3),NaN,solGamm(4)];
            extraVarGrid = {extraVarGrid{2:3}, extraVarGrid{4}, extraVarGrid{4}};

            % Compute solution at new lambdaGrid(3)
            lambdaGrid(3) = lambdaGrid(1) + (lambdaGrid(4)-lambdaGrid(1))/gr;

            [linfSolution,solverProblem] = linfSolve(lambdaGrid(3),0);
            solGamm(3) = linfSolution{1};
            if (solverProblem ~= 0) || (solverProblem ~= 1)
                [linfSolution,solverProblem] = linfSolve(lambdaGrid(3),solGamm(3)*backoff);
                solGamm(3) = solGamm(3)*backoff;
            end
            for i = 1:nExtraVar
                extraVarGrid{3}{i} = linfSolution{i+1};
            end
            if solverProblem ~= 0; solGamm(3) = Inf; end
        end
    end
    if verbose;fprintf('%i\t%.4g [%.4g]\t\t%.4g [%.4g]\n',cnt,lambdaGrid(1),solGamm(1),lambdaGrid(4),solGamm(4));end
    cnt = cnt+1;
end

if all(isinf(solGamm))
    gamValue = gamma0;
    lambdaValue = lambda0;
    extraVarValue = extraVar0;
else
    [gamValue,idx] = min(solGamm);
    lambdaValue = lambdaGrid(idx);
    extraVarValue = extraVarGrid{idx};
end
if verbose;fprintf('Golden section search results:\n\t gamma = %.4g\t\tlambda = %.4g\n',gamValue,lambdaValue);end
end