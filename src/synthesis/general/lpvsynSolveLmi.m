function [diagnostics,LMI,gamValue] = lpvsynSolveLmi(LMI,gam,var,synopts)
%LPVSYNSOLVELMI Solves the LMI for the controller synthesis problem.
%
%   Syntax:
%       [diagnostics,LMI,gamValue] = lpvsynSolveLmi(LMI,gam,var,synopts)
%
%   Inputs:
%       LMI (lmi): YALMIP LMI array containing LMI constraints for the
%           optimization problem.
%       gam (sdpvar): YALMIP sdpvar corresponding to the closed-loop gain 
%           objective variable, empty in case of passivity.
%       var (struct): Structure containing other optimization variables. 
%           (Only used for Linf based synthesis).
%       synopts (structure): Stucture containing LPV synthesis options, see
%           <a href="matlab:help lpvsynOptions">lpvsynOptions</a>.
%
%   Outputs:
%       diagnostics: Diagnostic output structure of YALMIP optimize 
%           function.
%       LMI: LMI array containing the LMI constraints for the controller
%           synthesis problem.
%       gamValue: Value of the closed-loop gain objective as double.
%   
%   See also LPVSYN, LPVSYNSF.
%

%% Variables
perfType = synopts.performance;
solOpt = synopts.solverOptions;
linfOpt = synopts.linfOptions;
verbose = synopts.verbose;
backoff = synopts.backoff;

%%
switch perfType
    %% Linf synthesis
    case 'linf'
        % Extract variables
        lambda = var.lambda;
        mu = var.mu;
        gamConstraint = sdpvar(1);
        LMI = LMI+(gam>=gamConstraint);
        extraVar = {mu};
        extraVarWidth = cellfun(@(x) size(x, 2), extraVar);
        linfSolve = optimizer(LMI,gam,solOpt,{lambda,gamConstraint},[{gam},extraVar]);
        [gamValue,lambdaValue,~,solverError] = linfLineSearch(linfSolve,linfOpt,verbose,backoff,extraVarWidth);
        
        if solverError
            error('Controller synthesis failed.')
        else
            LMI = replace(LMI,lambda,lambdaValue);
    
            diagnostics.problem = 0;
            diagnostics.info = '';
        end
    %% L2, passive and H2 synthesis
    otherwise
        diagnostics = optimize(LMI,gam,solOpt);
        gamValue = value(gam);
end
end