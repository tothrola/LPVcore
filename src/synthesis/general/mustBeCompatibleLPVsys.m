function mustBeCompatibleLPVsys(P)
% Check to see if system is compatible for lpvsyn, lpvsynsf, and lpvnorm
    mustBeA(P, {'LPVcore.lpvgridss','LPVcore.lpvss','LPVcore.lpvidss'});
    
    if isa(P,'LPVcore.lpvss') || isa(P,'LPVcore.lpvidss')
        if ~all([isaffine(P.A),isaffine(P.B),isaffine(P.C),isaffine(P.D)])
            eidType = 'LPVcore:affineCheck';
            msgType = ['System is not affinely dependent on' ...
                ' its parameters.'];
            throwAsCaller(MException(eidType, msgType));
        end

        if any(isinf(cell2mat(P.SchedulingTimeMap.Range)),"all")
            eidType = 'LPVcore:schedulingRangeCheck';
            msgType = ['The system has one or more scheduling ' ...
                'parameters that have an infinite scheduling range.'];
            throwAsCaller(MException(eidType, msgType));
        end

        if ~hasStaticSchedulingDependence(P)
            eidType = 'LPVcore:staticDependencyCheck';
            msgType = ['The system has dynamic scheduling dependency, ' ...
                'which is currently not supported.'];
            throwAsCaller(MException(eidType, msgType));
        end
    end
end