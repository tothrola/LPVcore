classdef (CaseInsensitiveProperties) poleConstraintOptions
%poleConstraintOptions Constructs matrices for closed-loop pole constraints
%use for synthesis. Multiple name-value pairs can be given for multiple
%constraints. An empty value, '[]', indicates the constraint is disabled.
%
%   Syntax:
%       out = poleConstraintOptions(...)
%   
%   Optional name-value pairs:
%       'Circle': Constraint eigenvalues to |eig| < value, i.e. an open
%           circle with a radius of 'value'.
%       'LeftHalfPlane': Constraint eigenvalues to Re(eig) < value, i.e.
%           a left half-plane.
%       'RightHalfPlane': Constraint eigenvalues to Re(eig) > value, i.e.
%           a right half-plane.
%       'Sector': Constraint eigenvalues to Re(eig)tan(value) < -|Im(eig)|,
%           i.e. a sector in the left half complex plane with angle of
%           'value' [rad].
%       'Custom': Custom pole constraint given as a structure with fields,
%           Q, S, T, and U, corresponding to the constraint:
%           [ I ]* [ Q  S ] [ I ]
%           [ z ]  [ S' R ] [ z ] < 0,
%           for all closed-loop eigenvalues 'z', where R >= 0 s.t. it is 
%           factorizable as R = T' U^(-1) T, with U > 0.
%
%   Outputs:
%       out: Structure containing Q, S, T and U, matrices corresponding to
%           the pole constraint.
%
%   See also LPVSYNOPTIONS, LPVSYN.
%
    
    % Sorted alphabetically (except for custom at the end)
    properties (Dependent)
        Circle
        LeftHalfPlane
        RightHalfPlane
        Sector
        Custom
    end

    properties (Access = protected, Hidden)
        RightHalfPlane_ = [];
        LeftHalfPlane_ = [];
        Sector_ = [];
        Circle_ = [];
        Custom_ = [];
    end

    properties (SetAccess = protected, Hidden)
        Q = [];
        S = [];
        T = [];
        U = [];
    end

    properties (Constant, Hidden)
        % Matrices corresponding to constraints
        % Naming should be '{propertyName}Mat'
        
        RightHalfPlaneMat = struct( ...
            'Q', @(value) 2*value, ...
            'S', @(value) -1, ...
            'T', @(value) 0, ...
            'U', @(value) 1 ...
            );
        LeftHalfPlaneMat = struct( ...
            'Q', @(value) -2*value, ...
            'S', @(value) 1, ...
            'T', @(value) 0, ...
            'U', @(value) 1 ...
            );
        SectorMat = struct( ...
            'Q', @(value) zeros(2), ...
            'S', @(value) [sin(value),cos(value);...
                           -cos(value),sin(value)], ...
            'T', @(value) zeros(1,2), ...
            'U', @(value) 1 ...
            );
        CircleMat = struct( ...
            'Q', @(value) -value^2, ...
            'S', @(value) 0, ...
            'T', @(value) 1, ...
            'U', @(value) 1 ...
            );
        CustomMat = struct(...
            'Q', @(value) value.Q,...
            'S', @(value) value.S,...
            'T', @(value) value.T,...
            'U', @(value) value.U ...
            );
    end
    
    methods
        function obj = poleConstraintOptions(options)
            arguments
                options.Circle         {mustBeScalarOrEmpty, mustBePositive} = [];
                options.LeftHalfPlane  {mustBeScalarOrEmpty}                 = [];
                options.RightHalfPlane {mustBeScalarOrEmpty}                 = [];
                options.Sector         {mustBeScalarOrEmpty, mustBePositive} = [];
                options.Custom         {mustBeCustomStruct}                  = [];
            end

            names = fieldnames(options);

            for i = 1:length(names)
                obj.(names{i}) = options.(names{i});
            end
        end

        % RightHalfPlane
        function out = get.RightHalfPlane(obj)
            out = obj.RightHalfPlane_;
        end

        function obj = set.RightHalfPlane(obj,value)
            obj.RightHalfPlane_ = value;

            obj = buildConstraints(obj);
        end

        % LeftHalfPlane
        function out = get.LeftHalfPlane(obj)
            out = obj.LeftHalfPlane_;
        end

        function obj = set.LeftHalfPlane(obj,value)
            obj.LeftHalfPlane_ = value;

            obj = buildConstraints(obj);
        end

        % Sector
        function out = get.Sector(obj)
            out = obj.Sector_;
        end

        function obj = set.Sector(obj,value)
            obj.Sector_ = value;

            obj = buildConstraints(obj);
        end

        % Circle
        function out = get.Circle(obj)
            out = obj.Circle_;
        end

        function obj = set.Circle(obj,value)
            obj.Circle_ = value;

            obj = buildConstraints(obj);
        end

        % Custom
        function out = get.Custom(obj)
            out = obj.Custom_;
        end

        function obj = set.Custom(obj,value)
            obj.Custom_ = value;

            obj = buildConstraints(obj);
        end
    end

    methods (Access = protected, Hidden)
        function obj = buildConstraints(obj)
            % Clear old matrices
            obj.Q = [];
            obj.S = [];
            obj.T = [];
            obj.U = [];
            
            % Rebuild the matrices
            names = fieldnames(obj);
            nNames = numel(names);
            
            for i = 1:nNames
                if ~isempty(obj.(names{i}))
                    obj = addConstraintMatrices(obj,names{i},obj.(names{i}));
                end
            end

            % Remove unnecessary rows from T (and corresponding rows + 
            % columns from U)
            zeroRowL = all(obj.T == 0,2);
            obj.T(zeroRowL,:)= [];
            obj.U = obj.U(~zeroRowL,~zeroRowL);
            
            % If all rows are removed, R = 0 --> R = [] (to prevent 
            % unnecessary constraint)
            if isempty(obj.U)
                obj.U = [];
                obj.T = [];
            end
        end

        function obj = addConstraintMatrices(obj,name,value)
            name = [name,'Mat'];

            obj.Q = blkdiag(obj.Q,obj.(name).Q(value));
            obj.S = blkdiag(obj.S,obj.(name).S(value));
            obj.T = blkdiag(obj.T,obj.(name).T(value));
            obj.U = blkdiag(obj.U,obj.(name).U(value));
        end
    end
end

%% Local functions
%% Argument validation functions
% Check custom value
function mustBeCustomStruct(x)
    if ~isempty(x)
        qCheck = isnumeric(x.Q) && issymmetric(x.Q);
        sCheck = isnumeric(x.S) && (size(x.S,1) == size(x.Q,1)) && ...
            (size(x.S,2) == size(x.T,2));
        tCheck = isnumeric(x.T) && (size(x.T,1) == size(x.U,1));
        uCheck = isnumeric(x.U) && issymmetric(x.U) && all(eig(x.U) > 0);

    checks = (qCheck && sCheck && tCheck && uCheck);
    else
        checks = true;
    end
    if ~checks
        eidType = 'LPVcore:poleConstraintOptions:checkCustom';
        msgType = ['The structure should contain a Q, S, T and U field, ',...
                   'where each field is a matrix of the correct size '...
                   'and U is positive definite. See "help poleConstraintOptions".'];
        throwAsCaller(MException(eidType, msgType));
    end
end