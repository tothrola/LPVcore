%% Factorize a symbolic matrix M
function [M0, Mi, funList, Np] = makeAffineSym(M, opt)
    % 'Factorize' symbolic matrix A in affine form:
    %
    % M = M0 + Σ_{i=1}^Np Mi(:,:,i) * funList{i}
    %
    arguments
        M
        opt {mustBeMember(opt, {'element','factor'})} = 'factor';
    end

    sizeM = size(M);
    
    M0 = zeros(sizeM);
    Mi = zeros([sizeM,0]);
   
    N = numel(M);
    funList = cell(N,1);
    Np = 0;

    switch opt
        % Somewhat 'smart' approach, but can result in more
        % individual scheduling-variables than number of elements 
        % of M
        case 'factor'
            for i = 1:N
                % Try to convert a_ij to double
                try 
                    val = double(M(i));
                    M0(i) = val;
                % Otherwise a_ij is a function
                catch
                    type = symFunType(M(i));
                    switch type
                        % If a_ij = s_1 * s_2 * s_3 ...
                        case "times"
                            c = children(M(i));
                            tempFun = 1;
                            tempVal = 1;
                            Np = Np + 1;
                            for j = 1:length(c)
                                % s_k is a number
                                try 
                                    val = double(c{j});
                                    tempVal = tempVal * val;
                                % s_k is a function
                                catch
                                    tempFun = tempFun * c{j};
                                end
                            end
                            funList{Np} = tempFun;
                            [ii,jj] = ind2sub(sizeM, i);
                            Mi(ii,jj,Np) = tempVal;
                        % If a_ij = s_1 + s_2 + s_3 ...
                        case "plus"
                            c = children(M(i));
                            for j = 1:length(c)
                                % s_k is a number
                                try 
                                    val = double(c{j});
                                    M0(i) = M0(i) + val;
                                % s_k is a function
                                catch
                                    typePlus = symFunType(c{j});
                                    switch typePlus
                                        % s_k = r_1 * r_2 * r_3 ...
                                        case "times"
                                            cPlus = children(c{j});
                                            tempFun = 1;
                                            tempVal = 1;
                                            Np = Np + 1;
                                            for k = 1:length(cPlus)
                                                % r_l is a number
                                                try
                                                    val = double(cPlus{k});
                                                    tempVal = tempVal * val;
                                                % r_l is a function
                                                catch 
                                                    tempFun = tempFun * cPlus{k};
                                                end
                                            end
                                            funList{Np} = tempFun;
                                            [ii,jj] = ind2sub(sizeM, i);
                                            Mi(ii,jj,Np) = tempVal;
                                        % s_k is another function
                                        otherwise
                                            Np = Np + 1;
                                            funList{Np} = M(i);
                                            [ii,jj] = ind2sub(sizeM, i);
                                            Mi(ii, jj, Np) = 1;
                                    end
                                end
                            end
                        % a_ij is another function
                        otherwise
                            Np = Np + 1;
                            funList{Np} = M(i);
                            [ii,jj] = ind2sub(sizeM, i);
                            Mi(ii, jj, Np) = 1;
                    end
                end
            end
        % 'Dumb' approach. Each element of 'M' which is not a scalar is
        % considered a scheduling-variable (duplicates are not
        % checked).
        case 'element'
            for i = 1:N
                try
                    M0(i) = double(M(i));
                catch
                    Np = Np + 1;
                    Mi = cat(3,Mi,zeros(sizeM));
                    [ii,jj] = ind2sub(sizeM, i);
                    Mi(ii, jj, Np) = 1;
                    funList{Np} = M(i);
                end
            end
    end
    % Remove duplicate functions and combine corresponding matrices 
    funList(cellfun(@isempty, funList)) = [];       % remove empty cells
    funListString = cellfun(@string, funList);
    [~,ia,ic] = unique(funListString,'stable');

    funList = funList(ia);  % unique functions in funList
    Np = length(funList);

    MiTemp = Mi;

    % Create new Mi matrix with duplicates combined
    Mi = zeros([sizeM, Np]);
    for i = 1:Np
        Mi(:,:,i) = sum(MiTemp(:,:,i == ic), 3);
    end
end