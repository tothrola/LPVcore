function [lpvsys, eta] = nlss2lpvss(sys, opt, factorOpt)
%NLSS2LPVSS Convert a nonlinear state-space representation to an LPV
%state-space representation.
%
%   Syntax:
%       [lpvsys, eta] = nlss2lpvss(sys) converts NLSS object sys to an LPV
%       state-space model with scheduling map eta using (numerical)
%       integration of the Jacobians.
%
%       [lpvsys, eta] = nlss2lpvss(sys, opt) converts NLSS object sys to an
%       LPV state-space model, where opt can either be 'numerical' 
%       (default) or 'analytical' to specify if integration is performed 
%       numerically or computed analytically.
%
%       [lpvsys, eta] = nlss2lpvss(sys, opt, factorOpt) converts NLSS 
%       object sys to an LPV state-space model, where factorOpt can either 
%       'element' (default) or 'factor' to specify if each nonlinear 
%       element of the matrix is taken as scheduling variable ('element') 
%       or an algorithm tries to split each element into a sum of 
%       scheduling variables ('factor'). While 'factor' can result in a 
%       simpler scheduling map and simpler affine state-space 
%       representation, it can result in more scheduling variables than 
%       'element'.
%
%   Inputs:
%       obj (nlss): Nonlinear state-space representation to be converted.
%       opt (string): Integration method, 'numerical' or 'analytical',
%           Default value: 'numerical'.
%       factorOpt (string): Matrix function factorization method, 
%           'element', or 'factor'. Default value: 'element'.
%
%   Outputs:
%       sys: LPVCORE.LPVSS object.
%
%
%See also LPVCORE.LPVSS, LPVCORE.NLSS.
%

    arguments
        sys {mustBeValidSysForConversion}
        opt {mustBeMember(opt, {'numerical','analytical'})} = 'numerical'
        factorOpt {mustBeMember(factorOpt, {'element','factor'})} = 'element'
    end
    
    dfsys = LPVcore.difform(sys);

    if strcmp(opt, 'numerical')
        holdFlag = true;
    else
        holdFlag = false;
    end

    if isct(sys)
        timeStr = 'ct';
    else
        timeStr = 'dt';
    end

    % Embedding
    lm = sym('lm', 'real');
    x = sys.xSym;
    u = sys.uSym;

    A = dfsys.Asym;
    B = dfsys.Bsym;
    C = dfsys.Csym;
    D = dfsys.Dsym;

    Al = subs(A, [x;u], [lm*x;lm*u]);
    Bl = subs(B, [x;u], [lm*x;lm*u]);
    Cl = subs(C, [x;u], [lm*x;lm*u]);
    Dl = subs(D, [x;u], [lm*x;lm*u]);

    Aps = int(Al, lm, 0, 1, 'Hold', holdFlag);
    Bps = int(Bl, lm, 0, 1, 'Hold', holdFlag);
    Cps = int(Cl, lm, 0, 1, 'Hold', holdFlag);
    Dps = int(Dl, lm, 0, 1, 'Hold', holdFlag);

    M = [Aps,Bps;Cps,Dps];

    [M0, Mi, funSym, Np] = LPVcore.makeAffineSym(M, factorOpt);

    % LTI case
    if Np == 0
        warning("System is LTI. Returning ss object.")
        A = M0(1:sys.Nx,1:sys.Nx);
        B = M0(1:sys.Nx,sys.Nx+1:end);
        C = M0(sys.Nx+1:end,1:sys.Nx);
        D = M0(sys.Nx+1:end,sys.Nx+1:end);

        lpvsys = ss(A, B, C, D, sys.Ts);
        eta = [];

    % LPV case
    else
        % Construct scheduling map
        eta = LPVcore.constructSchedulingMap(funSym, sys, Np);
    
        % Construct pMatrices
        Mp = pmatrix(cat(3, Mi, M0), 'SchedulingDimension', Np, ...
            'SchedulingDomain', timeStr);
    
        Ap = Mp(1:sys.Nx,1:sys.Nx);
        Bp = Mp(1:sys.Nx,sys.Nx+1:end);
        Cp = Mp(sys.Nx+1:end,1:sys.Nx);
        Dp = Mp(sys.Nx+1:end,sys.Nx+1:end);
    
        % Construct system
        lpvsys = LPVcore.lpvss(Ap, Bp, Cp, Dp, sys.Ts);
    end
end