function Mv = gridFunEval(M, pointsCell, gridLength)
% Evaluate matrix function M at points in pointCell and return matrix
    if ~isnumeric(M)
        Mc = cellfun(M, pointsCell{:}, 'UniformOutput', false);
        Mv = cat(3, Mc{:});
        Mv = reshape(Mv, [size(Mv,[1 2]), gridLength]);
    else
        Mv = M;
    end
end