classdef difform
%DIFFORM Differential form of nonlinear state-space representation
%
%   Syntax:
%       dfsys = difform(sys) creates an object dfsys representing the
%       differential form of the nlss object sys. The differential form is
%       for a continous-time nlss object of the form
%           dδx/dt = A(x(t),u(t)) δx(t) + B(x(t),u(t)) δu(t)
%            δy(t) = C(x(t),u(t)) δx(t) + D(x(t),u(t)) δu(t)
%       or for a discrete-time nlss object of the form
%          δx(t+1) = A(x(t),u(t)) δx(t) + B(x(t),u(t)) δu(t)
%            δy(t) = C(x(t),u(t)) δx(t) + D(x(t),u(t)) δu(t)
%       where A, B, C, and D correspond to the Jacobians df/dx, df/du, 
%       dh/dx, and dh/du, respectively.
%
%   Inputs:
%       sys (nlss): Nonlinear state-space representation. The system must
%           have a symbolic representation.
%
%See also NLSS.
%

    properties (Dependent = true)
        PrimalForm
    end

    properties (Access = private)
        PrimalForm_
    end

    properties (SetAccess = protected)
        A
        B
        C
        D
        Nx
        Nu
        Ny
        Ts
        TimeUnit
    end

    properties (Hidden, SetAccess = protected)
        Asym    % Internal symbolic represenation
        Bsym
        Csym
        Dsym
        xSym
        uSym
        isSimplified = 'none'
        Adep        = [1 1]   % Dependency index of x and u
        Bdep        = [1 1]
        Cdep        = [1 1]
        Ddep        = [1 1]
    end

    methods
        function obj = difform(sys)
            arguments
                sys {mustBeSymbolicSys}
            end
            obj.PrimalForm = sys;
        end

        function obj = set.PrimalForm(obj, sys)
            obj.PrimalForm_ = sys;

            obj.Ts = sys.Ts;
            obj.Nx = sys.Nx;
            obj.Nu = sys.Nu;
            obj.Ny = sys.Ny;
            
            obj.xSym = sys.xSym;
            obj.uSym = sys.uSym;
                   
            obj.Asym = jacobian(sys.fSym, obj.xSym);
            obj.Bsym = jacobian(sys.fSym, obj.uSym);
            obj.Csym = jacobian(sys.hSym, obj.xSym);
            obj.Dsym = jacobian(sys.hSym, obj.uSym);
            
            obj.A = convertToFunction(obj.Asym, obj.xSym, obj.uSym);
            obj.B = convertToFunction(obj.Bsym, obj.xSym, obj.uSym);
            obj.C = convertToFunction(obj.Csym, obj.xSym, obj.uSym);
            obj.D = convertToFunction(obj.Dsym, obj.xSym, obj.uSym);

            obj.isSimplified = 'none';
            obj.Adep = [1 1];
            obj.Bdep = [1 1];
            obj.Cdep = [1 1];
            obj.Ddep = [1 1];

            obj.TimeUnit = sys.TimeUnit;
        end

        function value = get.PrimalForm(obj)
            value = obj.PrimalForm_;
        end
        
        function out = isct(obj)
            out = (obj.Ts == 0);
        end

        function index = dependency(obj, verbose)
            %DEPENDENCY Returns the indices of the states and inputs on which the A, B,
            %C, and D matrix function of the differential form depends on.
            %
            %   Syntax: 
            %       index = dependency(dfsys)
            %
            %       index = dependency(dfsys, verbose)
            %   
            %   Inputs:
            %       dfsys (difform): Differential form for which the dependency is
            %           returned.
            %       verbose (logical): Logical determining if dependency should be
            %           be printed in the Command Windows (1) or not (0).
            %           Default value: 1.
            %
            %   Outputs:
            %       index (struct): Structure with the dependency of each matrix w.r.t.
            %           x and u.
            %
            arguments
                obj
                verbose {mustBeNumericOrLogical} = 1
            end
            if nargin == 1
                verbose = 1;
            end
            [dep, index] = getDependency(obj);
            
            if verbose
                fprintf(['\nState-space matrix dependency of' ...
                    ' differential form:\n\n'])
                dependencyPrint('A', dep{1});fprintf('\n');
                dependencyPrint('B', dep{2});fprintf('\n');
                dependencyPrint('C', dep{3});fprintf('\n');
                dependencyPrint('D', dep{4});fprintf('\n\n');
            end
        end
        
        function [obj, index] = simplify(obj, opt)
            %SIMPLIFY Simplifies the A, B, C, and D matrix functions of the
            %differential form by removing unused arguments from the function
            %representation.
            %
            %   Syntax: 
            %       [dfsysOut, index] = simplify(dfsys)
            %
            %       [dfsysOut, index] = simplify(dfsys, opt)
            %   
            %   Inputs:
            %       dfsys (difform): Differential form to be simplified.
            %       opt (string): Option for the simplifcation. Either 'full', to fully
            %           simplify the arguments of each matrix function individually;
            %           'partial', to ensure each simplified matrix function takes the
            %           same input arguments; or 'none', to unsimplify the differential
            %           form. Default value: 'partial'.
            %
            %   Outputs:
            %       dfsysOut (difform): Simplified differential form.
            %       index (struct): Structure with the dependency of each matrix w.r.t.
            %           x and u. Same output as dependency(dfsys).
            %
            %   Example:
            %       >> sys = LPVcore.nlss(@(x,u) [x(2)^2 + u; x(1)], @(x,u) ...
            %                                                      sin(x(1)), 2, 1, 1);
            %       >> dfsys = LPVcore.difform(sys)
            % 
            %       dfsys = 
            % 
            %       Continuous-time differential form
            % 
            %           (d/dt)δx = A(x,u) δx + B(x,u) δu
            %                 δy = C(x,u) δx + D(x,u) δu
            % 
            %       with
            % 
            %           Nr. of inputs:             1
            %           Nr. of outputs:            1
            %           Nr. of states:             2
            %       
            %       >> [dfsys2,index] = simplify(dfsys)
            %       
            %       dfsys2 = 
            %       
            %       Continuous-time differential form
            %       
            %           (d/dt)δx = A(x) δx + B δu
            %                 δy = C(x) δx + D δu
            %       
            %       with
            %       
            %           Nr. of inputs:             1
            %           Nr. of outputs:            1
            %           Nr. of states:             2
            %       
            %       
            %       index = 
            %       
            %         struct with fields:
            %       
            %           A: [1×1 struct]
            %           B: [1×1 struct]
            %           C: [1×1 struct]
            %           D: [1×1 struct]
            %
            %See also LPVCORE.DIFFORM.DEPENDENCY.
            %
            
            arguments
                obj
                opt {mustBeMember(opt, {'partial','full','none'})} = 'partial';
            end

            % Restore full model if system is already simplified
            if ~strcmp(obj.isSimplified, 'none') || strcmp(opt, 'none')
                obj.PrimalForm = obj.PrimalForm;
            end

            [dep, index] = getDependency(obj);
            obj.isSimplified = opt;

            if strcmp(opt, 'partial')
                xDep = unique([dep{1}{1};dep{2}{1};dep{3}{1};dep{4}{1}]);
                uDep = unique([dep{1}{2};dep{2}{2};dep{3}{2};dep{4}{2}]);

                if ~isempty(dep{1}{1}) || ~isempty(dep{1}{2})
                    dep{1}{1} = xDep;
                    dep{1}{2} = uDep;
                end
                if ~isempty(dep{2}{1}) || ~isempty(dep{2}{2})
                    dep{2}{1} = xDep;
                    dep{2}{2} = uDep;
                end
                if ~isempty(dep{3}{1}) || ~isempty(dep{3}{2})
                    dep{3}{1} = xDep;
                    dep{3}{2} = uDep;
                end
                if ~isempty(dep{4}{1}) || ~isempty(dep{4}{2})
                    dep{4}{1} = xDep;
                    dep{4}{2} = uDep;
                end
            end

            if ~strcmp(opt, 'none')
                obj.Adep = [numel(dep{1}{1}) > 0, numel(dep{1}{2}) > 0];
                obj.Bdep = [numel(dep{2}{1}) > 0, numel(dep{2}{2}) > 0];
                obj.Cdep = [numel(dep{3}{1}) > 0, numel(dep{3}{2}) > 0];
                obj.Ddep = [numel(dep{4}{1}) > 0, numel(dep{4}{2}) > 0];
    
                obj.A = convertToFunction(obj.Asym, dep{1}{1}, ...
                    dep{1}{2}, 1);
                obj.B = convertToFunction(obj.Bsym, dep{2}{1}, ...
                    dep{2}{2}, 1);
                obj.C = convertToFunction(obj.Csym, dep{3}{1}, ...
                    dep{3}{2}, 1);
                obj.D = convertToFunction(obj.Dsym, dep{4}{1}, ...
                    dep{4}{2}, 1);
            end
       end
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         LOCAL FUNCTIONS                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check if system has symbolic representation
function mustBeSymbolicSys(sys)
    if ~sys.symbolic
        eidType = 'LPVcore:difform';
        msgType = ['The system does not have a symbolic representation.' ...
            ' Unable to compute its differential form.'];
        throwAsCaller(MException(eidType, msgType));
    end
end

%% Convert symbolic matrix to function
function Mf = convertToFunction(M, x, u, smp)
    if nargin == 3
        smp = 0;
    end

    nx = numel(x);
    nu = numel(u);
    
    % As vars we take [x;x(1)] such that the function is always vectorized
    % (such that for a scalar x we get @(x) x(1,:) instead of @(x) x).
    if smp
        stringXU = {};
        stringIn = {};
        if nx > 0
            argM{1} = [x;x(1)];
            stringXU = cat(2, stringXU, 'x');
        else
            argM{1} = [];
        end
        if nu > 0
            argM{2} = [u;u(1)];
            stringXU = cat(2, stringXU, 'u');
        else
            argM{2} = [];
        end
        argM = argM(~cellfun('isempty', argM));
    else
        argM = {[x;x(1)], [u;u(1)]};
        stringXU = {'x','u'};
    end
    switch length(stringXU)
        case 1
            stringIn = {'in1'};
        case 2
            stringIn = {'in1', 'in2'};
    end
    
    if smp && ~nx && ~nu
        Mf = double(M);
    else
        Mf = matlabFunction(M, "Vars", argM);
        Mf = str2func(replace(func2str(Mf), stringIn, stringXU));
    end

end

%% Get dependency on x and u of each matrix
function [dep, index] = getDependency(obj)
    index = struct;
    dep = cell(4,1);

    [dep{1},index.A] = getStateAndInputDependency(obj.Asym, obj.xSym, ...
                                                                obj.uSym);
    [dep{2},index.B] = getStateAndInputDependency(obj.Bsym, obj.xSym, ...
                                                                obj.uSym);
    [dep{3},index.C] = getStateAndInputDependency(obj.Csym, obj.xSym, ...
                                                                obj.uSym);
    [dep{4},index.D] = getStateAndInputDependency(obj.Dsym, obj.xSym, ...
                                                                obj.uSym);
end

%% Function to print dependency in console
function dependencyPrint(str, depM)
    if isempty(depM{1}) && isempty(depM{2}) 
        fprintf('    %s: \tNone', str);
    else
        fprintf('    %s: \t( ', str);
    end
    for i = 1:length(depM{1})
        fprintf('%s', char(depM{1}(i)) );
        if i == length(depM{1})
            fprintf(' )');
        else
            fprintf(', ');
        end
    end
    if ~isempty(depM{1}) && ~isempty(depM{2}) 
        fprintf(' & ( ')
    end
    for i = 1:length(depM{2})
        fprintf('%s', char(depM{2}(i)) );
        if i == length(depM{2})
            fprintf(' )');
        else
            fprintf(', ');
        end
    end
end

%% Compute dependency on x and u of a matrix M
function [dep, index] = getStateAndInputDependency(M, x, u)
    arg = symvar(M);
    nArg = numel(arg);

    xArgIndex = find(jacobian(x, arg) * ones(nArg, 1));
    xArg = x(xArgIndex);
    if isempty(xArgIndex)
        xArgIndex = [];
    end

    uArgIndex = find(jacobian(u, arg) * ones(nArg, 1));
    uArg = u(uArgIndex);
    if isempty(uArgIndex)
        uArgIndex = [];
    end

    dep = {xArg, uArg};
    index = struct('x', xArgIndex, 'u', uArgIndex);
end