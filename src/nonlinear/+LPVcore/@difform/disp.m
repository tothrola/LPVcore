function disp(obj)
% DISP Provide info on the difform object
%
%   Syntax:
%       disp(sys)
%
%   Inputs:
%       sys: difform object
%
    narginchk(1, 1);
    
    if isempty(obj)
        fprintf('Empty differential form');
        return;
    end
    
    if isct(obj)
        Qstr  = '(d/dt)';
        emptyQstr = '      ';
    else
        Qstr  = '(q^-1)';
        emptyQstr = '      ';
    end
    
    % Model type
    if isct(obj)
        timeStr = 'Continuous-time';
    else
        timeStr = 'Discrete-time';
    end
    fprintf('%s differential form\n\n', timeStr);
    
    % State and output equation
    % Show depenency and take care of allignment
    [Astr, spaceA] = getDepStr(obj.Adep);
    [Bstr, spaceB] = getDepStr(obj.Bdep);
    [Cstr, spaceC] = getDepStr(obj.Cdep);
    [Dstr, spaceD] = getDepStr(obj.Ddep);

    if spaceA > spaceC
        spaceAstr = '';
        spaceCstr = join(repmat("",spaceA-spaceC+1,1));
    elseif spaceC > spaceA
        spaceAstr = join(repmat("",spaceC-spaceA+1,1));
        spaceCstr = '';
    else
        spaceAstr = '';
        spaceCstr = '';
    end

    if spaceB > spaceD
        spaceBstr = '';
        spaceDstr = join(repmat("",spaceB-spaceD+1,1));
    elseif spaceD > spaceB
        spaceBstr = join(repmat("",spaceD-spaceB+1,1));
        spaceDstr = '';
    else
        spaceBstr = '';
        spaceDstr = '';
    end

    fprintf('    %sδx = %sA%s δx + %sB%s δu\n', ...
        Qstr, spaceAstr, Astr, spaceBstr, Bstr);
    fprintf('    %sδy = %sC%s δx + %sD%s δu\n\n', ...
        emptyQstr, spaceCstr, Cstr, spaceDstr, Dstr);
    
    % Description of input-scheduling-output
    fprintf('with\n\n');
    fprintf('    Nr. of inputs:             %d\n', obj.Nu);
    fprintf('    Nr. of outputs:            %d\n', obj.Ny);
    fprintf('    Nr. of states:             %d\n', obj.Nx);
    fprintf('\n');
    
    % Sample time
    if ~isct(obj)
        if obj.Ts == -1; TsStr = 'unspecified';
        else;            TsStr = sprintf('%g %s',obj.Ts,obj.TimeUnit);                      end
        
        fprintf('%s: %s.\n', 'Sample time', TsStr);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         LOCAL FUNCTIONS                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [str, space] = getDepStr(dep)
    if all(dep == [1,1])
        str     = '(x,u)';
    elseif all(dep == [1,0])
        str     = '(x)';
    elseif all(dep == [0,1])
        str     = '(u)';
    else
        str     = '';
    end
    space = length(str);
end