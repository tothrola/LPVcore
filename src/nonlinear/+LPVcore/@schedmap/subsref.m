function varargout = subsref(P, S)
%SUBSREF Subscripted referencing for schedmap objects.
%
%   P = eta(X, U, D)
%
%   Same behavior as P = eval(eta, X, U, D)
%
%   See also LPVcore.schedmap.

if isscalar(S) && strcmp(S.type, '()') && numel(S.subs) == 3
    varargout{1} = eval(P, S.subs{:}); %#ok<EV2IN>
else
    [varargout{1:nargout}] = builtin('subsref', P, S);
end