classdef schedmap
%SCHEDMAP Constructs a scheduling map object.
%
%   Syntax:
%       sched = schedmap(map, Nx, Nu, Nd)
%
%   Inputs:
%       map (function_handle): Function handle of the form 
%          '@(x,u,d) [... x(1,:) ... d(3,:); ... u(2,:) ... x(1,:) ...; ...]'
%           which represents the scheduling
%           map.
%       Nx (integer): Size of the states.
%       Nu (integer): Size of the control inputs.
%       Nd (integer): Size of the exogenous input.
%
%   Example:
%       sched = LPVcore.schedmap(@(x,u,d) [sin(x(1,:)).^2; cos(x(2,:)) .* u(1,:)], 2, 1, 0);
%
%   The map can then be evaluated by calling, for example:
%       xTraj = randn(2, 10);
%       uTraj = randn(1, 10);
%       p = sched(xTraj, uTraj, []);
%
    
    properties
        map {mustBeCompatibleFunction}
        Nx  {mustBeInteger, mustBeNonnegative}
        Nu  {mustBeInteger, mustBeNonnegative}
        Nd  {mustBeInteger, mustBeNonnegative}
    end

    properties (SetAccess = private)
        Np
    end
    
    methods
        function obj = schedmap(map, Nx, Nu, Nd)
            narginchk(4,4);

            obj.map = map;
            obj.Nx = Nx;
            obj.Nu = Nu;
            obj.Nd = Nd;

            pCheck = obj.map(zeros(Nx, 1), zeros(Nu, 1), zeros(Nd, 1));

            obj.Np = length(pCheck);
        end
        
        function out = eval(obj, x, u, d)
            N = max([size(x, 2), size(u, 2), size(d, 2)]);

            % Checks
            if isempty(x) && obj.Nx == 0
                x = zeros(1, N); 
            else
                assert(size(x, 1) == obj.Nx, 'Number of states incorrect, state input should be a [Nx, N] matrix');
                assert(size(x, 2) == N, 'Number of input samples not consistent with number of other samples.');
            end
            if isempty(u) && obj.Nu == 0
                u = zeros(1, N);
            else
                assert(size(u, 1) == obj.Nu, 'Number of inputs incorrect, state input should be a [Nu, N] matrix');
                assert(size(u, 2) == N, 'Number of input samples not consistent with number of other samples.');
            end
            if isempty(d) && obj.Nd == 0
                d = zeros(1, N); 
            else
                assert(size(d, 1) == obj.Nd, 'Number of exogenous inputs incorrect, state input should be a [Nd, N] matrix');          
                assert(size(d, 2) == N, 'Number of exogenous input samples not consistent with number of other samples.');
            end

            % Eval map
            out = obj.map(x, u, d);
        end

        function disp(obj)
            fprintf('Scheduling-map:\n\n');
            fprintf('   p = η(x,u,d)\n\n');
            fprintf('with\n\n');
            fprintf('    Nr. of scheduling signals: %d\n\n', obj.Np);
        end
    end

    methods (Access = private)
        function B = subsref_(A, X, U, D)
            %SUBSREF_ Wrapper around SUBSREF for use in class methods.
            %
            %   For more info: https://nl.mathworks.com/matlabcentral/answers/101830-why-is-the-subsref-method-not-called-when-used-from-within-a-method-of-a-class
            %
            S.type = '()';
            S.subs{1} = X;
            S.subs{2} = U;
            S.subs{3} = D;
            B = subsref(A, S);
        end
    end
end

function mustBeCompatibleFunction(f)
    if ~isempty(f)
        mustBeA(f, 'function_handle')
        if ~startsWith(func2str(f), '@(x,u,d)')
            eidType = 'LPVcore:funCheck';
            msgType = 'Custom function handle must start with "@(x,u,d)".';
            throwAsCaller(MException(eidType, msgType));
        end
    end
end