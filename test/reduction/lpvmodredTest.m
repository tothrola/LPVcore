function tests = lpvmodredTest
%LPVMODREDTEST Test behavior of LPVMODRED
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(~) % Called once before the tests in this file are run
end

%% Test functions
function testLtibalred(testCase)
    %% Create full-order model
    p = preal('p', 'dt');
    Delta = diag([p, p^2, p^3]);
    nz = size(Delta, 1); nw = size(Delta, 2);
    ny = 1; nu = 2; nx = 10;
    % State reduction order
    rx = 7;
    % Generate a random G block
    rng(2);
    G = drss(nx, nz + ny, nw + nu);
    assert(isstable(G));
    % Scale G to ensure a stable LPV-LFR form
    G = G / hinfnorm(G);
    % Construct LPV-LFR form
    sys = LPVcore.lpvlfr(Delta, G);

    %% Balanced reduction of LTI part using reducespec implementation
    if ~isMATLABReleaseOlderThan('R2023b')
        [sysr, info] = lpvmodred(sys, rx, [], 'ltibalred');
        verifyClass(testCase, sysr, 'LPVcore.lpvlfr');
        verifyClass(testCase, info, 'struct');
        verifyEqual(testCase, sysr.Nx, rx);
        verifyEqual(testCase, sysr.Np, sys.Np);
    
        %% Compare full-order model and reduced-order model
        N = 20;
        u = rand(N, nu); p = rand(N, sys.Np);
        [y, ~] = lsim(sys, p, u);  % FOM
        yr = lsim(sysr, p, u);     % ROM
        verifyEqual(testCase, yr, y, 'AbsTol', 5E-2);
        % The default settings for ltibalred method should preserve
        % high-frequency gain.
        verifyEqual(testCase, evalfr(sys.G, Inf), evalfr(sysr.G, Inf));
    end

    %% Balanced reduction of LTI part using balred implementation (deprecated)
    % The deprecated BALRED implementation should be selected if a
    % BALREDOPTIONS is passed
    % In that case, the returned INFO object should be of type
    % LTIPACK.BALREDINFO
    [sysr, info] = lpvmodred(sys, rx, [], 'ltibalred', ...
        balredOptions('StateProjection', 'Truncate'));
    verifyClass(testCase, sysr, 'LPVcore.lpvlfr');
    verifyClass(testCase, info, 'ltipack.balredInfo');
    % The passed settings for ltibalred method should preserve
    % high-frequency gain.
    verifyEqual(testCase, evalfr(sys.G, Inf), evalfr(sysr.G, Inf));
end

function testDeltaBalRed(testCase)
    %% Create full-order model
    p = preal('p', 'dt');
    q = preal('q', 'dt');
    Delta = 0.5 * (rand(6) * p + rand(6) * p^2 + rand(6) * q + rand(6) * q^3);
    nz = size(Delta, 1); nw = size(Delta, 2);
    ny = 1; nu = 2; nx = 10;
    % Delta block reduction order
    rd = 2;
    % Generate a random G block
    rng(2);
    G = drss(nx, nz + ny, nw + nu);
    assert(isstable(G));
    % Scale G to ensure a stable LPV-LFR form
    G = G / hinfnorm(G);
    % Construct LPV-LFR form
    sys = LPVcore.lpvlfr(Delta, G);

    %% Balanced reduction of LTI part
    [sysr, info] = lpvmodred(sys, [], rd, 'deltabalred');
    verifyClass(testCase, sysr, 'LPVcore.lpvlfr');
    verifyClass(testCase, info, 'struct');
    verifyEqual(testCase, sysr.Nz, rd);
    verifyEqual(testCase, sysr.Nw, rd);
    verifyEqual(testCase, sysr.Np, sys.Np);

    %% Compare full-order model and reduced-order model
    N = 20;
    u = rand(N, nu); p = rand(N, sys.Np);
    [y, ~] = lsim(sys, p, u);  % FOM
    yr = lsim(sysr, p, u);     % ROM
    verifyEqual(testCase, yr, y, 'AbsTol', 0.1);
end

function testDeltaSvdRed(testCase)
    %% Example 2: delta block reduction
    p = preal('p', 'dt');
    rng(1);
    A = -eye(2) + p * diag([1, 2]) - p^2 * rand(2);
    B = randn(2, 1) + p *randn(2, 1) + p^2 * randn(2, 1);
    C = randn(1, 2) + p *randn(1, 2) + p^2 * randn(1, 2);
    D = 0;
    sys = LPVcore.lpvss(A, B, C, D, 1);
    rd = 4;
    [sysr, info] = lpvmodred(sys, [], rd, 'deltasvdred');
    
    % Singular values are returned in info struct
    verifySize(testCase, info.DeltaSingularValues, [size(sys.Delta, 1), 1]);
    verifyEqual(testCase, info.DeltaSingularValues, sort(info.DeltaSingularValues, 1, 'descend'));
    % Delta dimension is reduced to requested size
    verifySize(testCase, sysr.Delta, [rd, rd]);
    % State dimension is unchanged
    verifyEqual(testCase, sysr.Nx, 2);

    % Check accuracy
    N = 20;
    u = rand(N, sys.Nu); p = rand(N, sys.Np);
    y = lsim(sys, p, u); yr = lsim(sysr, p, u);
    verifyLessThan(testCase, norm(y - yr), 2);
end

function testMomentMatching(testCase)
    %% Create full-order model
    p = preal('p', 'dt');
    nx = 10;
    rng(1);
    A = p * diag(rand(nx, 1)) + (0.1 + 0.1 * p^2) * rand(nx);
    B = randn(nx, 1);
    C = randn(1, nx);
    D = 0;
    sys = LPVcore.lpvss(A, B, C, D, 1);

    % The minimum ROM dimension with the 'momentmatching' method is 7
    rx = 7;
    [sysr, info] = lpvmodred(sys, rx, [], 'momentmatching');
    verifyEqual(testCase, sysr.Nx, rx);
    verifyEqual(testCase, info.NumStepsMatched, 1);
    % Check whether using rx = 6 throws an error
    rx = 6;
    verifyError(testCase, @() lpvmodred(sys, rx, [], 'momentmatching'), '');
end