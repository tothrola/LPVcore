function tests = lpvpcaredTest
%LPVPCAREDTEST Test behavior of LPVPCARED
    tests = functiontests(localfunctions);
end

%% Test functions
function testLpvpcared(testCase)
    %% Create the LPV system with 8 scheduling-variables
    a = 5.6794;     b = 1.473;      c = 1.7985;
    d = 4e-1;       e = 4e-1;       f = 2;
    n = 1;
    
    param = [a,b,c,d,e,f,n];

    %% Nonlinear simulation of the robot arm
    uFun = @(t) [2 * sin(t), -cos(t)];
    
    t = linspace(0, 10, 5000);
    
    odefun = @(t,x) robotArm(x, uFun(t), param);
    [t, x] = ode45(odefun, t, zeros(4,1));

    %% Compute the scheduling trajectory
    pTraj = schedMap(x', param)';
    N = numel(t);

    %% Create the LPV system with 10 scheduling-variables
    np = size(pTraj, 2);
    
    p = cell(np,1);
    for i = 1:np
        p{i} = preal(sprintf(sprintf('p(%%0%id)', numel(num2str(np))), i));
    end
    
    A = [ 0,         0,        1,    0;
          0,         0,        0,    1;
          c*d*p{3}, -b*e*p{4}, p{5}, b*p{6};
         -b*d*p{7},  a*e*p{8}, p{9}, p{10}];
    
    B = [ 0,         0;
          0,         0;
          c*n*p{1}, -b*n*p{2};
         -b*n*p{2},  a*n*p{1}];
    
    C = [eye(2),zeros(2)];
    D = zeros(2);
    
    sys = LPVcore.lpvss(A,B,C,D);
    sysdt = c2d(sys, mean(diff(t)));

    %% Reduce the scheduling dimension using LPVPCARED
    nrho = 5;
    rng(1);
    % Trajectory pca
    [sysTraj, mapTraj, infoTraj] = lpvpcared(sys, nrho, pTraj);
    
    % Standard pca
    [sysStd, mapStd, infoStd] = lpvpcared(sys, nrho, pTraj,'standard');

%     % Same but with state-space array as input
%     sysLocal = ss(feval(sys.A, pTraj), feval(sys.B, pTraj), ...
%                   feval(sys.C, pTraj), feval(sys.D, pTraj));
% 
%     rng(1);
%     [sysr2, map_fcn2, info2] = lpvpcared(sysLocal, nrho, pTraj);

    % DT case
    rng(1);
    [sysTrajdt, mapTrajdt, infoTrajdt] = lpvpcared(sysdt, nrho, pTraj);

    %% Scheduling map
    rhoTraj = mapTraj(pTraj);
    rhoStd = mapStd(pTraj);
    rhoTrajdt = mapTrajdt(pTraj);

    % Simulation
    y_sys = lsim(sys, pTraj, uFun(t), t);
    yTraj = lsim(sysTraj, rhoTraj, uFun(t), t);
    yStd = lsim(sysStd, rhoStd, uFun(t), t);

    y_sysdt = lsim(sysdt, pTraj, uFun(t), t);
    yTrajdt = lsim(sysTrajdt, rhoTrajdt, uFun(t), t);
    

    %% Tests
    % Verify reduced ct system properties
    verifyEqual(testCase, sysTraj.Np, nrho);
    verifyEqual(testCase, sysStd.Np, nrho);
    verifyLessThan(testCase, goodnessOfFit(yTraj, y_sys, 'mse'), 5e-2);
    verifyLessThan(testCase, goodnessOfFit(yStd, y_sys, 'mse'), 2e1);
    verifyEqual(testCase, size(rhoTraj), [N, nrho]);
    verifyEqual(testCase, size(rhoStd), [N, nrho]);
    verifyLessThan(testCase, infoTraj.eta, 1e2);
    verifyLessThan(testCase, infoStd.eta, 1e3);

    % Verify reduced dt system properties
    verifyEqual(testCase, sysTrajdt.Np, nrho);
    verifyLessThan(testCase, goodnessOfFit(yTrajdt, y_sysdt, 'mse'), 5e-2);
    verifyEqual(testCase, size(rhoTrajdt), [N, nrho]);
    verifyLessThan(testCase, infoTrajdt.eta, 1e2);

    % CT vs DT properties
    verifyLessThan(testCase, abs(infoTraj.eta-infoTrajdt.eta), 1e1);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%                             LOCAL FUNCTIONS                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xdot = robotArm(x, u, par)
    a = par(1);
    b = par(2);
    c = par(3);
    d = par(4);
    e = par(5);
    f = par(6);
    n = par(7);

    q1 = x(1);
    q2 = x(2);
    q1dot = x(3);
    q2dot = x(4);

    u = [u(1);u(2)];

    M = [a,                b * cos(q1 - q2);
         b * cos(q1 - q2), c];

    C = [b * sin(q1 - q2) * q2dot^2 + f * q1dot;
         -b * sin(q1 - q2) * q1dot^2 + f * (q2dot - q1dot)];

    g = [-d * sin(q1);
         -e * sin(q2)];

    qddot = M \ (n * u - C - g);
    
    xdot = [q1dot;q2dot;qddot];
end

function p = schedMap(x, par)
    a = par(1);
    b = par(2);
    c = par(3);
    f = par(6);

    q1 = x(1,:);
    q2 = x(2,:);
    q1d = x(3,:);
    q2d = x(4,:);

    cosd = cos(q1-q2);
    sind = sin(q1-q2);

    h = 1 ./ (a * c - b^2 * cosd.^2);
    
    p = [h;
         cosd .* h;
         sincFun(q1) .* h;
         cosd .* sincFun(q2) .* h;
         (-b^2 * cosd .* sind .* q1d - (c + b * cosd) * f) .* h;
         (-c * sind .* q2d + cosd * f) .* h;
         cosd .* sincFun(q1) .* h;
         sincFun(q2) .* h;
         (a * b * sind .* q1d + f * (a + b * cosd)) .* h;
         (b^2 * sind .* cosd .* q2d - a * f) .* h];
end

% sinc function
function  y = sincFun(x)
    y = sin(x)./x;
    y(x == 0) = 1;
end