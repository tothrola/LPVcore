function tests = lpvbalredTest
%LPVBALREDTEST Test behavior of LPVBALRED
% TODO: Add support for discrete-time systems
    tests = functiontests(localfunctions);
end

%% Test functions
function testLpvbalred(testCase)
    %% Create the stable LPV system with 20 states and 9 scheduling-variables
    Nm = 10; param = [1 0.5 1 0.5 1]; % [M k d kwall dwall]
    nl_index.springs = 1:5-1;
    nl_index.springs_wall = 1:5;
    nl_index.dampers = []; nl_index.dampers_wall = [];
    k = @(x) x^3; % Spring connecting mass blocks
    d = @(x,xdot) 0; % Damper connecting mass blocks
    k_wall = @(x) x^3; % Spring connecting each mass with the wall
    d_wall = @(x,xdot) 0; % Damper connecting each mass with the wall
    
    [fMSD,state,input] = msdGenerator(Nm,nl_index,k,d,k_wall,d_wall);

    f = @(x,u) fMSD(x,u,param);
    % Output: position and velocity of the last Mass
    h = @(x,u) [x(Nm,:); x(end,:)];
    nx = length(state); nu = length(input); ny = 2;
    nlss = LPVcore.nlss(f,h,nx,nu,ny, 0, true);
    Ts = 0.1;
    nlssdt = c2d(nlss,Ts,"eul");

    [sys, schedMap] = LPVcore.nlss2lpvss(nlss,"analytical","factor");
    [sysdt, schedMapdt] = LPVcore.nlss2lpvss(nlssdt,"analytical","factor");

    %% Nonlinear simulation of the Mass-Spring-Damper system
    tsim = 20; t=(0:Ts:tsim)';
    u = sum(2.*rand(1,3).*sin(2*pi.*(linspace(0.2,2,3)).*t + (0 + (pi-0).*rand(1,3))),2);
    [~, ~, x] = sim(nlss, u, t, zeros(nlss.Nx,1));
    [~, ~, xdt] = sim(nlssdt, u, t, zeros(nlssdt.Nx,1));

    %% Compute the scheduling trajectory
    pTraj = schedMap.map(x', u')';
    pTrajdt = schedMapdt.map(xdt', u')';
    N = numel(t);

    %% Reduce the state-order using LPVBALRED
    %Choose the scheduling grid. In this case, we pick some samples from the
    %scheduling trajectory.
    pgrid = pTraj(1:10:end,:);
    pgriddt = pTrajdt(1:10:end,:);
    %State-order reduction target
    rx = 2;

    rng(1);
    [sysr, ~, ~] = lpvbalred(sys, rx, pgrid);

    rng(1);
    [sysrdt, ~, ~] = lpvbalred(sysdt, rx, pgriddt);

    %% Freq. domain and Time domain
    % Freq. domain LTI array
    pCheck = num2cell(pTraj(randperm(N, 10), :),2);
    Gsys = extractLocal(sys, pCheck);    Gsys = cat(3, Gsys{:}); 
    Gsysr = extractLocal(sysr, pCheck);  Gsysr = cat(3, Gsysr{:});

    Gsysdt = extractLocal(sysdt, pCheck);    Gsysdt = cat(3, Gsysdt{:});
    Gsysrdt = extractLocal(sysrdt, pCheck);  Gsysrdt = cat(3, Gsysrdt{:});

    % Time-domain Simulation
    y_sys = lsim(sys, pTraj, u, t);
    y_sysr = lsim(sysr, pTraj, u, t);
 
    y_sysdt = lsim(sysdt, pTrajdt, u, t);
    y_sysrdt = lsim(sysrdt, pTrajdt, u, t);

    %% Tests
    % Verify reduced system properties
    verifyEqual(testCase, sysr.Nx, rx);
    verifyLessThan(testCase, mean(norm(Gsysr - Gsys)), 5e-2);
    verifyLessThan(testCase, goodnessOfFit(y_sysr, y_sys, 'mse'), 5e-2);

    % Verify reduced dt system properties
    verifyEqual(testCase, sysrdt.Nx, rx);
    verifyLessThan(testCase, mean(norm(Gsysrdt - Gsysdt)), 5e-2);
    verifyLessThan(testCase, goodnessOfFit(y_sysrdt, y_sysdt, 'mse'), 5e-2);
   
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%                             LOCAL FUNCTIONS                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [f,x,u] = msdGenerator(M,nl_index,k,d, k_wall, d_wall)
%MSDGENERATOR generates the (nonlinear) equations of motion of a custom
%interconnection of Mass-Spring-Damper systems. Each mass is interconnected
% with the adjacent mass blocks, and to an infinitely rigid wall. 
% Each connection contains a linear (and nonlinear) spring and damper. 
% The linear parameters and nonlinear functions must be defined by the user.
% The external force input "u" is applied to the last mass block M.
    nl_k_idx = nl_index.springs;
    nl_d_idx = nl_index.dampers;

    nl_k_wall_idx = nl_index.springs_wall;
    nl_d_wall_idx = nl_index.dampers_wall;

    if ~isempty(nl_k_idx) && (max(nl_k_idx) >= M || min(nl_k_idx) <= 0)
        error("Invalid choice of nonlinear springs indices (between blocks). Values must be between 1 and M-1.");
    end
    if ~isempty(nl_k_wall_idx) && (max(nl_k_wall_idx) > M || min(nl_k_wall_idx) <= 0)
        error("Invalid choice of nonlinear spring indices (connected to wall). Values must be between 1 and M.")
    end

    if ~isempty(nl_d_idx) && (max(nl_d_idx) >= M || min(nl_d_idx) <= 0)
        error("Invalid choice of nonlinear damper indices (between blocks). Values must be between 1 and M-1.")
    end
    if ~isempty(nl_d_wall_idx) && (max(nl_d_wall_idx) > M || min(nl_d_wall_idx) <= 0)
        error("Invalid choice of nonlinear damper indices (connected to wall). Values must be between 1 and M.")
    end
    %% Function init
    syms m k0 d0 k0_wall d0_wall real
    syms u real
    q = sym('q',[M,1],'real');
    qd = sym('qd',[M,1],'real');
    param = [m k0 d0 k0_wall d0_wall];

    %Nonlinear spring and dampers between masses
    klin = k0;
    knonlin = cell(M-1,1);
    knonlin(:) = {@(x) 0};
    knonlin(nl_k_idx) = {k};

    dlin = d0;
    dnonlin = cell(M-1,1);
    dnonlin(:) = {@(x, xdot) 0};
    dnonlin(nl_d_idx) = {d};

    %Nonlinear spring and dampers connecting each mass block to an infinitely rigid wall
    wall_klin = k0_wall;
    wall_knonlin = cell(M,1);
    wall_knonlin(:) = {@(x) 0};
    wall_knonlin(nl_k_wall_idx) = {k_wall};

    wall_dlin = d0_wall;
    wall_dnonlin = cell(M,1);
    wall_dnonlin(:) = {@(x, xdot) 0};
    wall_dnonlin(nl_d_wall_idx) = {d_wall};

    %Equations of motion
    qdd = sym(NaN(M,1));
    for i=1:M
        if i==1 && M==1
            % Special Case: Only a single block
            Ftot = u;
        elseif i==1
            % Total force on mass
            Ftot = Fs(q(2)-q(1),klin,knonlin{1}) + Fd(q(2)-q(1),qd(2)-qd(1), dlin,dnonlin{1});
        elseif i==M
            % Total force on mass
            Ftot = u - Fs(q(M)-q(M-1),klin,knonlin{M-1}) - Fd(q(M)-q(M-1), qd(M)-qd(M-1),dlin,dnonlin{M-1});
        else
            % Total force on mass
            Ftot = - Fs(q(i)-q(i-1),klin,knonlin{i-1}) - Fd(q(i)-q(i-1), qd(i)-qd(i-1),dlin,dnonlin{i-1}) ...
                + Fs(q(i+1)-q(i),klin,knonlin{i}) + Fd(q(i+1)-q(i), qd(i+1)-qd(i),dlin,dnonlin{i});
        end
        % Connect each block by an additional spring/damper to wall
        Ftot = Ftot - Fs(q(i),wall_klin,wall_knonlin{i}) - Fd(q(i),qd(i),wall_dlin,wall_dnonlin{i});

        % Acceleration
        qdd(i) = Ftot/m;
    end
    
    x = [q; qd];
    f_ = [qd; qdd];
    f = matlabFunction(f_,'Vars',{x,u,param});
end

%% Local function to compute spring force
function y = Fs(x,klin,knonlin)
    y = klin*x + knonlin(x);
end

%% Local function to compute damping force
function y = Fd(x,xdot,dlin,dnonlin)
    y = dlin*xdot + dnonlin(x,xdot);
end