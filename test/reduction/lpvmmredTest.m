function tests = lpvmmredTest
%LPVMMREDTEST Test behavior of LPVMMRED
    tests = functiontests(localfunctions);
end

%% Test functions
function testSISODT(testCase)
    rng(1);
    ny = 1;
    nu = 1;
    time = 'dt';
    nx = 100;
    % Number of time samples to match exactly in ROM
    % The greater N, the higher the dimension of the ROM
    N = 2;
    
    % Build FOM
    p = preal('p', time);
    A0 = rand(nx);
    A1 = rand(nx);
    A0 = A0 ./ (3*max(abs(eig(A0))));
    A1 = A1 ./ (3*max(abs(eig(A1))));
    A = A0 + p * A1;
    B = rand(nx, nu);
    C = rand(ny, nx);
    D = rand(ny, nu);
    
    sys = LPVcore.lpvss(A, B, C, D);
    
    %% Reduction
    sysr_T = lpvmmred(sys, N, 'T');
    sysr_R = lpvmmred(sys, N, 'R');
    sysr_O = lpvmmred(sys, N, 'O');
    r_T = size(sysr_T.A, 1);
    r_R = size(sysr_R.A, 1);
    r_O = size(sysr_O.A, 1);
    verifyEqual(testCase, r_T, 12);
    verifyEqual(testCase, r_R, 12);
    verifyEqual(testCase, r_O, 12);
    
    %% Time-domain simulation
    Nt = N * 20;
    t = linspace(0, 1, Nt);
    u = randn(Nt, nu);
    p = rand(Nt, sys.Np);
    
    y = lsim(sys, p, u, t);
    yr_T = lsim(sysr_T, p, u, t);
    yr_R = lsim(sysr_R, p, u, t);
    yr_O = lsim(sysr_O, p, u, t);

    verifyEqual(testCase, y, yr_T, 'RelTol', 1E-3);
    verifyEqual(testCase, y, yr_R, 'RelTol', 1E-3);
    verifyEqual(testCase, y, yr_O, 'RelTol', 1E-3);
end