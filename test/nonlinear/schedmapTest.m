function tests = schedmapTest
%nlssTest Test nlss class.
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    np = 4;
    map = @(x,u,d) [sin(x(1,:)).^2; cos(x(2,:)); 3 * u(1,:); d(1,:) * d(2,:)];
    schmap = LPVcore.schedmap(map, 2, 1, 2);

    testCase.TestData.np = np;
    testCase.TestData.map = map;
    testCase.TestData.schmap = schmap;
end

%% Test SCHEDMAP creation
function testCreation(testCase)
    np = testCase.TestData.np;
    map = testCase.TestData.map;
    schmap = testCase.TestData.schmap;

    % Test properties
    verifyEqual(testCase, schmap.Np, np);
    verifyEqual(testCase, schmap.map, map);

    % Test errors
    verifyError(testCase, @() LPVcore.schedmap(@(x) x, 1, 0, 0), 'LPVcore:funCheck');
    verifyError(testCase, @() LPVcore.schedmap(@(u) u, 0, 1, 0), 'LPVcore:funCheck');
    verifyError(testCase, @() LPVcore.schedmap(@(d) d, 0, 0, 1), 'LPVcore:funCheck');
end

%% Test EVAL method
function testEval(testCase)
    map = testCase.TestData.map;
    schmap = testCase.TestData.schmap;

    xTest = randn(2,1);
    uTest = randn(1);
    dTest = randn(2,1);

    pTest = map(xTest, uTest, dTest);
    pEval = eval(schmap, xTest, uTest, dTest);
    pAltEval = schmap(xTest, uTest, dTest);

    verifyEqual(testCase, pEval, pTest);
    verifyEqual(testCase, pAltEval, pTest);
end