function tests = nlssTest
%nlssTest Test nlss class.
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    f = @(x,u) [-0.1 * x(2, :); -0.3 * sin(x(1, :)).^2 + u(1, :)];
    h = @(x,u) [cos(x(1, :));x(1, :);x(2, :)];

    nx = 2;
    nu = 1;
    ny = 3;
    Ts = 0.1;

    sys1 = LPVcore.nlss(f, h, nx, nu, ny);
    sys2 = LPVcore.nlss(f, h, nx, nu, ny, 0, false);
    sys3 = LPVcore.nlss(f, h, nx, nu, ny, Ts);
    sys4 = LPVcore.nlss(f, h, nx, nu, ny, Ts, false);

    testCase.TestData.f = f;
    testCase.TestData.h = h;
    testCase.TestData.nx = nx;
    testCase.TestData.nu = nu;
    testCase.TestData.ny = ny;
    testCase.TestData.Ts = Ts;
    testCase.TestData.sys = {sys1, sys2, sys3, sys4};
end

%% Test NLSS creation
function testCreation(testCase)
    rng(1);

    f = testCase.TestData.f;
    h = testCase.TestData.h;
    nx = testCase.TestData.nx;
    nu = testCase.TestData.nu;
    ny = testCase.TestData.ny;
    Ts = testCase.TestData.Ts;

    sys = testCase.TestData.sys;

    xTest = randn(nx, 1);
    uTest = randn(nu, 1);

    fTest = f(xTest, uTest);
    hTest = h(xTest, uTest);

    % Test various properties
    for i = 1:4
        verifyEqual(testCase, nx, sys{i}.Nx);
        verifyEqual(testCase, nu, sys{i}.Nu);
        verifyEqual(testCase, ny, sys{i}.Ny);

        if i <= 2
            verifyEqual(testCase, 0, sys{i}.Ts);
        else
            verifyEqual(testCase, Ts, sys{i}.Ts)
        end

        verifyEqual(testCase, sys{i}.f(xTest, uTest), fTest);
        verifyEqual(testCase, sys{i}.h(xTest, uTest), hTest); 
    end
end

%% Test ISCT method
function testIsCT(testCase)
    sys = testCase.TestData.sys;
    for i = 1:4
        if i <= 2
            verifyEqual(testCase, true, isct(sys{i}));
        else
            verifyEqual(testCase, false, isct(sys{i}));
        end    
    end
end

%% Test C2D method
function testC2d(testCase)
    % Get test data
    f = testCase.TestData.f;
    h = testCase.TestData.h;
    Ts = testCase.TestData.Ts;

    nx = testCase.TestData.nx;
    nu = testCase.TestData.nu;

    sys = testCase.TestData.sys;

    % Test point
    rng(1);

    xTest = randn(nx, 1);
    uTest = randn(nu, 1);

    % Manual forward Euler
    fdtEul = @(x,u) x + Ts * f(x, u);
    hdtEul = h;

    fdtEulTest = fdtEul(xTest, uTest);
    hdtEulTest = hdtEul(xTest, uTest);

    % Manual RK4
    phi1 = f;
    phi2 = @(x,u) f(x + Ts * phi1(x,u) / 2, u);
    phi3 = @(x,u) f(x + Ts * phi2(x,u) / 2, u);
    phi4 = @(x,u) f(x + Ts * phi3(x,u), u);
    fdtRk4 = @(x,u) x + Ts * (phi1(x,u) + 2 * phi2(x,u) + ...
        2 * phi3(x,u) + phi4(x,u)) / 6;
    hdtRk4 = h;

    fdtRk4Test = fdtRk4(xTest, uTest);
    hdtRk4Test = hdtRk4(xTest, uTest);

    % Compare
    for i = 1:2
        sysdtEul = c2d(sys{i}, Ts, "eul");
        verifyEqual(testCase, sysdtEul.f(xTest, uTest), fdtEulTest);
        verifyEqual(testCase, sysdtEul.h(xTest, uTest), hdtEulTest);

        sysdtRk4 = c2d(sys{i}, Ts, "rk4");
        verifyEqual(testCase, sysdtRk4.f(xTest, uTest), fdtRk4Test, "AbsTol", eps);
        verifyEqual(testCase, sysdtRk4.h(xTest, uTest), hdtRk4Test);
    end

    % Check if errors on DT system
    for i = 3:4
        verifyError(testCase,  @() c2d(sys{i}, Ts), "LPVcore:sysCheck")
    end
end

%% Test SIM method
function testSim(testCase)
    % Get test data
    f = testCase.TestData.f;
    h = testCase.TestData.h;
    Ts = testCase.TestData.Ts;

    nx = testCase.TestData.nx;

    sys = testCase.TestData.sys;

    % Test data
    rng(1);

    x0 = randn(nx, 1);
    ufun = @(t) sin(t);
    tRange = [0, 10];
    tLinspace = 0:Ts:10;
    u = ufun(tLinspace)';

    odefun = @(t, x) f(x, ufun(t));
    odefunVec = @(t, x) f(x, interp1(tLinspace, u, t, "previous"));

    % Compare CT - u function
    for i = 1:2
        % Sim
        [y, tout, x] = sim(sys{i}, ufun, tRange, x0);

        % Manual
        [toutManual, xManual] = ode45(odefun, tRange, x0);
        yManual = h(xManual', ufun(toutManual)')';

        verifyEqual(testCase, y, yManual);
        verifyEqual(testCase, tout, toutManual);
        verifyEqual(testCase, x, xManual);
    end

    % Compare CT - other solver
    for i = 1:2
        % Sim
        [y, tout, x] = sim(sys{i}, ufun, tRange, x0, "ode89");

        % Manual
        [toutManual, xManual] = ode89(odefun, tRange, x0);
        yManual = h(xManual', ufun(toutManual)')';

        verifyEqual(testCase, y, yManual);
        verifyEqual(testCase, tout, toutManual);
        verifyEqual(testCase, x, xManual);
    end

    % Compare CT - u vector
    for i = 1:2
        % Sim
        [y, tout, x] = sim(sys{i}, u, tLinspace, x0);

        % Manual
        [toutManual, xManual] = ode45(odefunVec, tLinspace, x0);
        yManual = h(xManual', u')';

        verifyEqual(testCase, y, yManual);
        verifyEqual(testCase, tout, toutManual);
        verifyEqual(testCase, x, xManual);
    end

    % Compare DT - u function
    for i = 3:4
        % Sim
        [y, tout, x] = sim(sys{i}, ufun, tRange, x0);

        %
        toutManual = (tRange(1):Ts:tRange(2))';
        N = numel(toutManual);
        xManual = zeros(nx, N);
        xManual(:,1) = x0;

        uManual = ufun(toutManual);
        for k = 1:N-1
            xManual(:, k+1) = f(xManual(:,k), uManual(k));
        end
        xManual = xManual';

        yManual = h(xManual', uManual')';

        verifyEqual(testCase, y, yManual);
        verifyEqual(testCase, tout, toutManual);
        verifyEqual(testCase, x, xManual);
    end

    % Compare DT - u vector
    for i = 3:4
        % Sim
        [y, tout, x] = sim(sys{i}, u, tLinspace, x0);

        %
        toutManual = (tRange(1):Ts:tRange(2))';
        N = numel(toutManual);
        xManual = zeros(nx, N);
        xManual(:,1) = x0;

        for k = 1:N-1
            xManual(:, k+1) = f(xManual(:,k), u(k));
        end
        xManual = xManual';

        yManual = h(xManual', u')';

        verifyEqual(testCase, y, yManual);
        verifyEqual(testCase, tout, toutManual);
        verifyEqual(testCase, x, xManual);
    end
end