function tests = difformTest
%nlssTest Test nlss class.
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    f = @(x,u) [-0.1 * x(2, :); -0.3 * sin(x(1, :)).^2 + u(1, :).^2];
    h = @(x,u) [cos(x(1, :));x(1, :);x(2, :)^3];

    % Manual Jacobian
    A = @(x,u) [0 -0.1; -0.6 * sin(x(1,:)) * cos(x(1,:)), 0];
    B = @(x,u) [0; 2 * u(1,:)];
    C = @(x,u) [-sin(x(1,:)), 0;
                   1,            0;
                   0,            3 * x(2,:)^2];
    D = @(x,u) [0;0;0];

    %
    nx = 2;
    nu = 1;
    ny = 3;
    Ts = 0.1;

    sys1 = LPVcore.nlss(f, h, nx, nu, ny);
    sys2 = LPVcore.nlss(f, h, nx, nu, ny, 0, false);
    sys3 = LPVcore.nlss(f, h, nx, nu, ny, Ts);
    sys4 = LPVcore.nlss(f, h, nx, nu, ny, Ts, false);

    testCase.TestData.f = f;
    testCase.TestData.h = h;
    testCase.TestData.A = A;
    testCase.TestData.B = B;
    testCase.TestData.C = C;
    testCase.TestData.D = D;
    testCase.TestData.nx = nx;
    testCase.TestData.nu = nu;
    testCase.TestData.ny = ny;
    testCase.TestData.Ts = Ts;
    testCase.TestData.sys = {sys1, sys2, sys3, sys4};
end

%% Test DIFFORM creation
function testCreation(testCase)
    rng(1);

    A = testCase.TestData.A;
    B = testCase.TestData.B;
    C = testCase.TestData.C;
    D = testCase.TestData.D;
    nx = testCase.TestData.nx;
    nu = testCase.TestData.nu;
    sys = testCase.TestData.sys;

    xTest = randn(nx, 1);
    uTest = randn(nu, 1);

    Atest = A(xTest, uTest);
    Btest = B(xTest, uTest);
    Ctest = C(xTest, uTest);
    Dtest = D(xTest, uTest);

    % Test various properties
    for i = 1:2:4
        dfsys = LPVcore.difform(sys{i});

        verifyEqual(testCase, sys{i}.Nx, dfsys.Nx);
        verifyEqual(testCase, sys{i}.Nu, dfsys.Nu);
        verifyEqual(testCase, sys{i}.Ny, dfsys.Ny);
        verifyEqual(testCase, sys{i}.Ts, dfsys.Ts);

        verifyEqual(testCase, dfsys.A(xTest, uTest), Atest, "AbsTol", eps);
        verifyEqual(testCase, dfsys.B(xTest, uTest), Btest, "AbsTol", eps);
        verifyEqual(testCase, dfsys.C(xTest, uTest), Ctest, "AbsTol", eps);
        verifyEqual(testCase, dfsys.D(xTest, uTest), Dtest, "AbsTol", eps);
    end

    % Test if it fails if symbolic representation of nlss is disabled
    for i = 2:2:4
        verifyError(testCase, @() LPVcore.difform(sys{i}), 'LPVcore:difform');
    end
end

%% Test ISCT method
function testIsCT(testCase)
    sys = testCase.TestData.sys;
    for i = 1:2:4
        dfsys = LPVcore.difform(sys{i});
        if i <= 2
            verifyEqual(testCase, true, isct(dfsys));
        else
            verifyEqual(testCase, false, isct(dfsys));
        end    
    end
end

%% Test DEPENDENCY method
function testDependency(testCase)
    sys = testCase.TestData.sys;

    for i = 1:2:4
        dfsys = LPVcore.difform(sys{i});
        
        index = dependency(dfsys, false);
        
        verifyEqual(testCase, index.A, struct('x', 1, 'u', []));
        verifyEqual(testCase, index.B, struct('x', [], 'u', 1));
        verifyEqual(testCase, index.C, struct('x', [1; 2], 'u', []));
        verifyEqual(testCase, index.D, struct('x', [], 'u', []));
    end
end

%% Test SIMPLIFY method
function testSimplify(testCase)
    rng(1);

    f = @(x,u) [-0.1 * x(2, :); -0.3 * sin(x(2, :)).^2 + u^2];
    h = @(x,u) [cos(x(2, :));x(1, :);x(2, :)];

    sys{1} = LPVcore.nlss(f, h, 2, 1, 3);
    sys{2} = LPVcore.nlss(f, h, 2, 1, 3, 0.1);

    Apartial = @(x2, u) [0, -0.1; 0, -0.6 * cos(x2) * sin(x2)];
    Bpartial = @(x2, u) [0;2 * u];
    Cpartial = @(x2, u) [0, -sin(x2);1 0;0 1];
    Dpartial = [0;0;0];

    Afull = @(x2) [0, -0.1; 0, -0.6 * cos(x2) * sin(x2)];
    Bfull = @(u) [0;2 * u];
    Cfull = @(x2) [0, -sin(x2);1 0;0 1];
    Dfull = [0;0;0];

    x2Test = randn(1);
    uTest = randn(1);

    xTest = randn(2,1);

    for i = 1:numel(sys)
        dfsys = LPVcore.difform(sys{i});
        index = dependency(dfsys, 0);
        
        % Partial option
        [dfsPartial, indexPartial] = simplify(dfsys, 'partial');
        
        verifyEqual(testCase, dfsPartial.A(x2Test, uTest), Apartial(x2Test, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsPartial.B(x2Test, uTest), Bpartial(x2Test, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsPartial.C(x2Test, uTest), Cpartial(x2Test, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsPartial.D, Dpartial, "AbsTol", eps);

        verifyEqual(testCase, indexPartial, index);

        % Full option
        [dfsFull, indexFull] = simplify(dfsys, 'full');
        
        verifyEqual(testCase, dfsFull.A(x2Test), Afull(x2Test), "AbsTol", eps);
        verifyEqual(testCase, dfsFull.B(uTest), Bfull(uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsFull.C(x2Test), Cfull(x2Test), "AbsTol", eps);
        verifyEqual(testCase, dfsFull.D, Dfull, "AbsTol", eps);

        verifyEqual(testCase, indexFull, index);

        % Test unsimplify
        dfs1 = simplify(dfsPartial, 'none');
        verifyEqual(testCase, dfsys.A(xTest, uTest), dfs1.A(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.B(xTest, uTest), dfs1.B(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.C(xTest, uTest), dfs1.C(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.D(xTest, uTest), dfs1.D(xTest, uTest), "AbsTol", eps);

        dfs2 = simplify(dfsFull, 'none');
        verifyEqual(testCase, dfsys.A(xTest, uTest), dfs2.A(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.B(xTest, uTest), dfs2.B(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.C(xTest, uTest), dfs2.C(xTest, uTest), "AbsTol", eps);
        verifyEqual(testCase, dfsys.D(xTest, uTest), dfs2.D(xTest, uTest), "AbsTol", eps);
    end
end