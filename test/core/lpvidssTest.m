function tests = lpvidssTest
%LPVSSTEST Test behavior of LPVIDSS
% TODO: CT tests and other functionalities besides lsim
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    rng(7); % make results reproducible
    
    %DT systems
    sys1 =  drss(2); %SISO
    sys2 =  drss(2,2,2); %MIMO
    sys3 =  drss(2,2,1); %SIMO
    sys4 =  drss(2,1,2); %MISO
    
    eta1 = LPVcore.schedmap(@(x,u,d) x(1,:),2,1,0);
    eta2 = LPVcore.schedmap(@(x,u,d) x(1,:),2,2,0);
    eta3 = LPVcore.schedmap(@(x,u,d) x(1,:),2,1,0);
    eta4 = LPVcore.schedmap(@(x,u,d) x(1,:),2,2,0);
    
    testCase.TestData.sys1 = sys1;
    testCase.TestData.sys2 = sys2;
    testCase.TestData.sys3 = sys3;
    testCase.TestData.sys4 = sys4;

    testCase.TestData.eta1 = eta1;
    testCase.TestData.eta2 = eta2;
    testCase.TestData.eta3 = eta3;
    testCase.TestData.eta4 = eta4;
end

function testLsim(testCase)
%Create lpvidss objects and execute self-scheduled simulation. The purpose 
% is to check if the lsim parser of lpvidss objects is ok.
sys1 = testCase.TestData.sys1;
sys2 = testCase.TestData.sys2;
sys3 = testCase.TestData.sys3;
sys4 = testCase.TestData.sys4;

eta1 = testCase.TestData.eta1;
eta2 = testCase.TestData.eta2;
eta3 = testCase.TestData.eta3;
eta4 = testCase.TestData.eta4;

% Simulation properties
Nsim = 20;
Ts = 0.1;
t = 0:Ts:Nsim-1;
N = numel(t);
u1 = randn(N, 1);
u2 = randn(N, 2);
u3 = randn(N, 1);
u4 = randn(N, 2);
NoiseVariance = 1e-4;
x0 = zeros(2,1);

% Scheduling parameters
pd=preal('p','dt');

%SISO case
K = randn(2,1);
e = rand(N,1);
A = 0.1.*sys1.A + 0.1.*rand(2,2)*pd;
sys_ = LPVcore.lpvidss(A,sys1.B,sys1.C,sys1.D,'innovation',K, ...
    [], NoiseVariance,Ts); 
[~] = lsim(sys_,eta1,u1,t,x0,'w',e);  % Generate data + noise signal



%MIMO case
K = randn(2,2);
e = rand(N,2);
A = 0.1.*sys2.A + 0.1.*rand(2,2)*pd;
sys_ = LPVcore.lpvidss(A,sys2.B,sys2.C,sys2.D,'innovation',K, ...
    [], NoiseVariance,Ts); 
[~]  = lsim(sys_,eta2,u2,t,x0,'w',e);  % Generate data + noise signal


%SIMO case
K = randn(2,2);
e = rand(N,2);
A = 0.1.*sys3.A + 0.1.*rand(2,2)*pd;
sys_ = LPVcore.lpvidss(A,sys3.B,sys3.C,sys3.D,'innovation',K, ...
    [], NoiseVariance,Ts); 
[~]  = lsim(sys_,eta3,u3,t,x0,'w',e);  % Generate data + noise signal

%MISO case
K = randn(2,1);
e = rand(N,1);
A = 0.1.*sys4.A + 0.1.*rand(2,2)*pd;
sys_ = LPVcore.lpvidss(A,sys4.B,sys4.C,sys4.D,'innovation',K, ...
    [], NoiseVariance,Ts); 
[~]  = lsim(sys_,eta4,u4,t,x0,'w',e);  % Generate data + noise signal

end