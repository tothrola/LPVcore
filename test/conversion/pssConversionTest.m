function tests = pssConversionTest
%PSSCONVERSIONTEST Verify pssConversion function (LPVTools must be
%added to the path for these functions to pass).
    tests = functiontests(localfunctions);
end

%% Setup
function setupOnce(testCase)
rng(2);

nx = 4;
nu = 2;
ny = 3;

nP = {5 3 4};

G1 = rss(nx, ny, nu, nP{:});
dom = rgrid({'p1','p2','p3'}, ...
    {linspace(-5,2,nP{1}), linspace(-3,2,nP{2}), linspace(-1,1,nP{3})});
G2 = c2d(G1, .1);

sys1 = pss(G1, dom);
sys2 = pss(G2, dom);

sys1.StateUnit = repmat({'cm'},nx,1);
sys1.InputName = 'b';
sys1.InputUnit = repmat({'N'},nu,1);
sys1.OutputName = 'c';
sys1.OutputUnit = repmat({'km'},ny,1);
sys1.Name = 'dynamical systemm';
sys1.Notes = 'A note';
sys1.UserData = 'user data';

% Add data to testcase
testCase.TestData.sys1 = sys1;
testCase.TestData.sys2 = sys2;
end

function testCT(testCase)
    sys = testCase.TestData.sys1;
    sysConv = LPVcore.pssConversion(sys);
    
    verifyTrue(testCase, all(norm(sysConv.ModelArray-sys.Data,'inf') <= 1e-12, "all"));
    
    % Check properties
    propertyNames = {'StateUnit','InputName',...
        'InputUnit','OutputName','OutputUnit',...
        'Name','Notes','UserData'};
    
    for i = 1:length(propertyNames)
        verifyEqual(testCase, sys.(propertyNames{i}), sysConv.(propertyNames{i}));   
    end
end

function testDt(testCase)
    sys = testCase.TestData.sys2;
    sysConv = LPVcore.pssConversion(sys);

    verifyEqual(testCase, sysConv.Ts, sys.Ts)
    
    verifyTrue(testCase, all(norm(sysConv.ModelArray-sys.Data,'inf') <= 1e-12, "all"));
end