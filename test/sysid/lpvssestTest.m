function tests = lpvssestTest
%LPVSSESTTEST Test identification using LPVSSEST
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(testCase) % Called once before the tests in this file are run
    %% Model structures for testing
    % Model structure 1
    rng(42);
    p = preal('p', 'dt');
    nx = 2;
    nu = 1;
    ny = 2;

    A = p * 0.1 * randn(nx);
    B = randn(nx, nu) * p^2 + randn(nx, nu);
    C = randn(ny, nx);
    D = zeros(ny, nu);
    K = 0.2 * randn(nx, ny);
    
    Ainit = 0.9 * A;
    Binit = 1.1 * B;
    Cinit = 1.22 * C;
    Dinit = D;
    Kinit = 1.4 * K;
    
    NoiseVariance = 0.05 * eye(ny);
    Ts = 0.1;
    
    sys_cell{1} = LPVcore.lpvidss(A, B, C, D, 'innovation', K, [], ...
        NoiseVariance, Ts);
    sys_init_cell{1} = LPVcore.lpvidss(Ainit, Binit, Cinit, Dinit, 'innovation', Kinit, [], ...
        NoiseVariance, Ts);

    % Model structure 2
    rng(42);
    p = preal('p', 'dt');
    q = preal('q', 'dt');
    nx = 5;
    nu = 4;
    ny = 3;

    A = p * 0.1 * randn(nx) + q^2 * 0.1 * rand(nx);
    B = randn(nx, nu) * p^2 + q * randn(nx, nu) + randn(nx, nu);
    C = randn(ny, nx) + randn(ny, nx) * q * p;
    D = zeros(ny, nu);
    K = 0.2 * randn(nx, ny);
    
    Ainit = 0.66 * A;
    Binit = 1.67 * B;
    Cinit = 0.78 * C;
    Dinit = D;
    Kinit = 1.45 * K;
    
    NoiseVariance = 0.5 * eye(ny);
    Ts = 1;
    
    sys_cell{2} = LPVcore.lpvidss(A, B, C, D, 'innovation', K, [], ...
        NoiseVariance, Ts);
    sys_init_cell{2} = LPVcore.lpvidss(Ainit, Binit, Cinit, Dinit, 'innovation', Kinit, [], ...
        NoiseVariance, Ts);

    testCase.TestData.sys_cell = sys_cell;
    testCase.TestData.sys_init_cell = sys_init_cell;
end

%% Test functions
function testEGN(testCase)
    %% Enhanced Gauss-Newton search method
    rng(42);

    for i=1:numel(testCase.TestData.sys_cell)
        sys = testCase.TestData.sys_cell{i};
        sys_init = testCase.TestData.sys_init_cell{i};
        N = 200;
        u = randn(N, sys.Nu);
        p = -0.5 + rand(N, sys.Np);
    
        [y, ~, ~, y_p] = lsim(sys, p, u);
        fprintf('SNR is %.2f dB\n', LPVcore.snr(y_p, y - y_p));
        data = lpviddata(y, p, u, sys.Ts);
        options = lpvssestOptions;
        searchOptions = options.SearchOptions;
        searchOptions.maxIter = 5;
        options = lpvssestOptions('Display', 'off', ...
            'Initialization', 'template', 'SearchOptions', searchOptions);
        sys_est = lpvssest(data, sys_init, options);
    
        verifyEqual(testCase, sys_est.Ts, sys.Ts);
    
        % Check prediction error has decreased
        data_p = lpviddata(y_p, p, u, sys.Ts);
        [~, fit_est] = compare(data_p, sys_init, sys_est, 1);
    
        %% Validation
        u_val = randn(N, sys.Nu);
        p_val = -0.5 + rand(N, sys.Np);
        [~, ~, ~, y_val] = lsim(sys, p_val, u_val);
        data_val = lpviddata(y_val, p_val, u_val, sys.Ts);
    
        [~, fit_val] = compare(data_val, sys_init, sys_est, 1);
        fprintf('Results on noise-free output:\n');
        for j=1:sys.Ny
            fprintf('\tOutput channel %d\n', j);
            fprintf('\t\tEstimation fit (init --> est): %.2f%% --> %.2f%%\n', fit_est{1}(j), fit_est{2}(j));
            fprintf('\t\tValidation fit (init --> est): %.2f%% --> %.2f%%\n', fit_val{1}(j), fit_val{2}(j));
            % Check that the estimation improved the system in terms of prediction
            % error
            verifyTrue(testCase, fit_est{1}(j) <= 70);
            verifyTrue(testCase, fit_val{1}(j) <= 70);
            verifyTrue(testCase, fit_est{2}(j) >= 85);
            verifyTrue(testCase, fit_val{2}(j) >= 85);
        end
    end
end

function testOEInit(testCase)
    rng(1);
    p = preal('p', 'dt');
    A = 0.1 + 0.1 * p; B = 1;
    C = 1; D = 1; K = 0;
    NoiseVariance = 0.01;
    Ts = 1;
    
    template_sys = LPVcore.lpvidss(A, B, C, D, 'innovation', K, [], NoiseVariance,Ts);
    
    N = 10;
    u = randn(N, template_sys.Nu);
    p = rand(N, template_sys.Np);
    y = lsim(template_sys, p, u);
    data = lpviddata(y, p, u, template_sys.Ts);
    sys = lpvssest(data, template_sys, lpvssestOptions('Display', 'off'));
    
    [~, fit] = compare(data, sys);
    verifyGreaterThan(testCase, fit, 95);
end

