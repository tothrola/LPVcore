function tests = bjTest
%BJTEST Test identification using LPVBJ
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(testCase) % Called once before the tests in this file are run
    rng(42);   % set random number generator seed so that results are reproducible

    p = preal('p', 'dt');
    B = {1, 0.5 * p};
    F = {1, -0.1 * p};
    C = {1, 0.2 * p + p^2};
    D = {1, 0.4 * p^2};
    sys = LPVcore.lpvidpoly([], B, C, D, F);
    sys.NoiseVariance = 1E-3;
    N = 100;
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    y = lsim(sys, p, u);
    testCase.TestData.sys = sys;
    testCase.TestData.data = lpviddata(y, p, u);
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    y = lsim(sys, p, u);
    testCase.TestData.val_data = lpviddata(y, p, u);
end

%% Test functions
function testBj_SISO(testCase)
    true_sys = testCase.TestData.sys;
    data = testCase.TestData.data;
    val_data = testCase.TestData.val_data;
    template_sys = randlpvidpoly(true_sys);
    % Gradient-based
    lpvbj(data, template_sys, ...
        lpvbjOptions('Verbose', true, 'Regularization', struct('Lambda', 1E-5)));
    % Pseudo-LS
    est_sys = lpvbj(data, template_sys, ...
        lpvbjOptions('SearchMethod', 'pseudols'));
    % Compute fit w.r.t. validation data
    [~, fit] = compare(val_data, est_sys, 1);
    fprintf('Fit LPV-BJ: %.2f%%\n', fit);
    verifyEqual(testCase, fit, 95.82, 'AbsTol', 1);
end

function testBj_MIMO(testCase)
    rng(1);
    ny = 3;
    nu = 2;
    Ts = 10;
    p = preal('p', 'dt');
    q = preal('q', 'dt');
    F = {eye(ny), 0.1 * (rand(ny) + rand(ny) * p + rand(ny) * q)};
    B = {rand(ny, nu) * p * q, rand(ny, nu) * p + rand(ny, nu) * p * q};
    C = {eye(ny), 0.1 * rand(ny)};
    D = {eye(ny), 0.1 * rand(ny) * p};
    sys = LPVcore.lpvidpoly([], B, C, D, F);
    sys.Ts = Ts;
    % Cholesky factor of noise variance
    P = 0.01 * (rand(ny) + eye(ny));
    sys.NoiseVariance = P * P';
    % Simulate data
    N = 10 * nparams(sys, 'free');
    u = rand(N, nu);
    p = rand(N, sys.Np);
    [y, ~, e] = lsim(sys, p, u);
    % Print SNR per output channel
    for i=1:ny
        SNR = LPVcore.snr(y(:, i) - e(:, i), e(:, i));
        fprintf('SNR of output channel %i: %.1f dB\n', i, SNR);
    end
    data = lpviddata(y, p, u, Ts);
    template_sys = (sys);
    % Sub-test 1: try LPVBJ using the exact system and a zero step size (i.e., the
    % returned system should exactly equal the true system).
    sys_zero_stepsize = lpvbj(data, template_sys, ...
        lpvbjOptions('Verbose', true, ...
        'Initialization', 'template', ...
        'SearchOptions', struct('StepSize', 0), ...
        'Regularization', struct('Lambda', 0)));
    verifyEqual(testCase, getpvec(sys_zero_stepsize), getpvec(sys));

    % Sub-test 2: try LPVBJ using a small perturbation of the true system and a non-zero step size (i.e., the
    % returned system should be very close to the true system).
    params_true = getpvec(sys);
    params_perturbed = (1 + 0.02 * rand(size(params_true))) .* params_true;
    init_sys = setpvec(sys, params_perturbed);
    sys_close_init = lpvbj(data, init_sys, ...
        lpvbjOptions('Verbose', true, ...
        'Initialization', 'template', ...
        'SearchOptions', struct('StepSize', 1), ...
        'Regularization', struct('Lambda', 0)));
    verifyEqual(testCase, norm(getpvec(sys_close_init) - getpvec(sys)) / norm(getpvec(sys)), 0, 'AbsTol', 0.02);
    [~, fit] = compare(data, sys_close_init, 1);
    fprintf('Fit LPV-BJ (close init): %.2f%%\n', fit);
    verifyEqual(testCase, fit, [94.9; 96.2; 95.3], 'AbsTol', 1);

    % Sub-test 3: try LPVBJ using random system and a non-zero step size
    init_sys = randlpvidpoly(sys);
    sys_rand_init = lpvbj(data, init_sys, ...
        lpvbjOptions('Verbose', true, ...
        'SearchOptions', struct('StepSize', 1), ...
        'Regularization', struct('Lambda', 0)));
    verifyEqual(testCase, norm(getpvec(sys_rand_init) - getpvec(sys)) / norm(getpvec(sys)), 0, 'AbsTol', 0.30);
    [~, fit] = compare(data, sys_rand_init, 1);
    fprintf('Fit LPV-BJ  (rand init): %.2f%%\n', fit);
    verifyEqual(testCase, fit, [94.9; 96.5; 95.3], 'AbsTol', 1);
end