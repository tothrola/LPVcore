function tests = rivTest
%RIVTEST Test identification using RIV
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(~) % Called once before the tests in this file are run
    rng(1);
end

%% Test functions
function testRiv(testCase)
    Ts = 0.1;  % s
    NoiseVariance = 1E-1;

    p = preal('p', 'dt');

    B = {1+p, 1+p};
    F = {1, 0.1+0.1*p};

    sys = LPVcore.lpvidpoly([], B, {1, 1}, {1, 1}, F, NoiseVariance, Ts);

    %% Data generation
    N = 1000;  % samples

    % Train
    uData = randn(N, sys.Nu);
    pData = rand(N, sys.Np);
    tData = Ts * (1:N);
    [y, ~, ~, yp] = lsim(sys, pData, uData, tData);
    fprintf('SNR is %.1f dB\n', LPVcore.snr(y, y - yp));

    data_est = lpviddata(y, pData, uData, Ts);

    %% Identification
    % The system will be identified with the provided template model
    % structure. Once with randomly initialized coefficients ('template')
    % and once with an ARX pre-estimation ('polypre')
    template_sys = randlpvidpoly(sys);
    initialization_methods = {'template', 'polypre'};

    for i=1:numel(initialization_methods)
        fprintf('Testing with initialization method %s\n', initialization_methods{i});
        tic;
        sys_est_laurain = lpvriv(data_est, template_sys, ...
            lpvrivOptions('Implementation', 'laurain', 'Display', 'off', ...
                'Initialization', initialization_methods{i}));
        toc;
        [~, fit] = compare(data_est, sys_est_laurain);
        fprintf('\tFit with Laurain implementation is %.5f%%\n', fit);
        verifyEqual(testCase, fit, 85.8, 'AbsTol', 5);
        
        tic;
        sys_est_lpvcore = lpvriv(data_est, template_sys, ...
            lpvrivOptions('Implementation', 'lpvcore', 'Display', 'off', ...
                'Initialization', initialization_methods{i}));
        toc;
        [~, fit] = compare(data_est, sys_est_lpvcore);
        fprintf('\tFit with LPVcore implementation is %.5f%%\n', fit);
        verifyEqual(testCase, fit, 85.8, 'AbsTol', 5);
    
        % Check whether the identified parameters are similar
        pvec_true = getpvec(sys, 'free');
        pvec_laurain = getpvec(sys_est_laurain, 'free');
        pvec_lpvcore = getpvec(sys_est_lpvcore, 'free');
        pvec_diff = norm(pvec_laurain - pvec_lpvcore) / norm(pvec_true);
        fprintf('\tRel. 2-norm difference in parameters between implementations: %d\n', ...
            pvec_diff);
    end
    
    
    %% Invalid invocations
    % Too few arguments
    verifyError(testCase, @() lpvriv(data_est), 'MATLAB:narginchk:notEnoughInputs');
    % Inequal parametrizations
    template_sys = LPVcore.lpvidpoly([], {1, p}, {1, 1}, {1, 1}, {1, p}, NoiseVariance, Ts);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
    % Non-constant noise process
    template_sys = LPVcore.lpvidpoly([], B, {1, p}, {1, 1}, F, NoiseVariance, Ts);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
    % Continuous-time system
    template_sys = LPVcore.lpvidpoly([], {preal('p', 'ct')}, {1, 1}, {1, 1}, {1, preal('p', 'ct')}, NoiseVariance, 0);
    verifyError(testCase, @() lpvriv(data_est, template_sys), '');
end

function testRivDynamicDependence(testCase)
    % Testing LPVRIV with a more challenging system:
    %   * Dynamic dependence
    %   * MIMO
    % We test both prediction and simulation focus.
    p = preal('p', 'dt');
    pd = pshift(p, -1);
    pdd = pshift(p, -2);
    
    ny = 3;
    nu = 2;
    
    F1 = 0.1 * (diag(rand(ny, 1)) + diag(rand(ny, 1)) * pd + diag(rand(ny, 1)) * pdd);
    B0 = rand(ny, nu) + p * rand(ny, nu) + pd * rand(ny, nu);
    B1 = rand(ny, nu) + p * rand(ny, nu) + pd * rand(ny, nu);
    B2 = rand(ny, nu) + p * rand(ny, nu) + pd * rand(ny, nu);
    B3 = rand(ny, nu) + p * rand(ny, nu) + pd * rand(ny, nu);
    C1 = 0.1 * rand(ny);
    D1 = 0.1 * rand(ny);

    F = {eye(ny), F1};
    B = {B0, B1, B2, B3};
    C = {eye(ny), C1};
    D = {eye(ny), D1};
    
    sys = LPVcore.lpvidpoly([], B, C, D, F);
    sys.Ts = 1;
    sys.NoiseVariance = 0.001 * eye(ny);
    
    % Generate data
    N = 100;
    u = rand(N, nu);
    p = rand(N, sys.Np);
    [y, ~, e] = lsim(sys, p, u);
    data = lpviddata(y, p, u, sys.Ts);

    % Define the minimum channel-wise fit that the identified systems
    % should have w.r.t. data generating system
    fit_min = [95, 95, 95];
    
    % Subtest 1: LPV-RIV starting from the exact system
    sys_est = lpvriv(data, sys, lpvrivOptions('Display', 'off'));
    
    % Check results
    verifySize(testCase, getpvec(sys_est), size(getpvec(sys)));
    [~, fit] = compare(data, sys_est);
    for i=1:ny
        SNR = LPVcore.snr(y(:, i) - e(:, i), e(:, i));
        fprintf('Output %d: SNR=%.2f dB, fit=%.2f%%\n', i, SNR, fit(i));
    end
    verifyGreaterThan(testCase, fit, fit_min);

    % Subtest 2: LPV-RIV starting from random system
    sys_init = setpvec(sys, getpvec(sys, 'free') + 0.1);
    [~, fit_init] = compare(data, sys_init);
    sys_est = lpvriv(data, sys_init, lpvrivOptions('Display', 'off'));
    
    % Check results
    verifySize(testCase, getpvec(sys_est), size(getpvec(sys)));
    [~, fit] = compare(data, sys_est);
    for i=1:ny
        SNR = LPVcore.snr(y(:, i) - e(:, i), e(:, i));
        fprintf('Output %d: SNR=%.2f dB, init. fit=%.2f%%, optim. fit=%.2f%%\n', ...
            i, SNR, fit_init(i), fit(i));
    end
    verifyGreaterThan(testCase, fit, fit_min);

    % Subtest 3: LPV-RIV starting from random system with prediction focus
    sys_est = lpvriv(data, sys_init, ...
        lpvrivOptions('Display', 'off', 'Focus', 'prediction'));
    
    % Check results
    verifySize(testCase, getpvec(sys_est), size(getpvec(sys)));
    [~, fit] = compare(data, sys_est);
    for i=1:ny
        SNR = LPVcore.snr(y(:, i) - e(:, i), e(:, i));
        fprintf('Output %d: SNR=%.2f dB, init. fit=%.2f%%, optim. fit=%.2f%%\n', ...
            i, SNR, fit_init(i), fit(i));
    end
    verifyGreaterThan(testCase, fit, fit_min);

end
