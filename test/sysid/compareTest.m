function tests = compareTest
%COMPARETEST Test compare function
    tests = functiontests(localfunctions);
end

%% File fixtures
function setupOnce(testCase) % Called once before the tests in this file are run
    rng(42);   % set random number generator seed so that results are reproducible

    p = preal('p', 'dt');
    B = {1, 0.5 * p};
    F = {1, 0.5 * p};
    sys = LPVcore.lpvidpoly([], B, [], [], F);
    sys.NoiseVariance = 1E-10;
    N = 100;
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    y = lsim(sys, p, u);
    testCase.TestData.sys = sys;
    testCase.TestData.data = lpviddata(y, p, u);
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    y = lsim(sys, p, u);
    testCase.TestData.val_data = lpviddata(y, p, u);
end

%% Test functions
function testOe(testCase)
    true_sys = testCase.TestData.sys;
    val_data = testCase.TestData.val_data;
    [ySim, fit, ic] = compare(val_data, true_sys, 1);
    verifySize(testCase, ic, [2, 1]);
    % True IC is 0, so estimated IC should be small
    verifyLessThan(testCase, norm(ic), 1e-3);
    verifyTrue(testCase, norm(ySim - val_data.y) < 1E-3, ...
        'Compare did not return valid output.');
    verifyTrue(testCase, fit >= 0.999, ...
        'Compare did not return perfect fit for true system.');
    % Check argument combinations and plotting
    compare(val_data, true_sys, 1);
    compare(val_data, true_sys, 'g', 1);
    compare(val_data, true_sys, 'g');
    close all;
end

function testOeDynamicDependence(testCase)
    % In case the OE model has dynamic dependence,
    % then the automatic initial condition estimation is not (yet)
    % supported in COMPARE.
    p = preal('p', 'dt');
    B = {1, 0.5 * pshift(p, -2)};
    F = {1, 0.5 * pshift(p, +2)};
    sys = LPVcore.lpvidpoly([], B, [], [], F);
    sys.NoiseVariance = 1E-10;
    N = 100;
    u = randn(N, sys.Nu);
    p = rand(N, sys.Np);
    y = lsim(sys, p, u);
    data = lpviddata(y, p, u);
    [ySim, fit, ic] = compare(data, sys, 1);
    % IC estimation is not (yet) supported for dynamic dependence OE
    % models, so returned IC should be 0
    verifyEqual(testCase, ic, 0);
    verifyTrue(testCase, norm(ySim - data.y) < 1E-3, ...
        'Compare did not return valid output.');
    verifyTrue(testCase, fit >= 0.999, ...
        'Compare did not return perfect fit for true system.');
    % Check argument combinations and plotting
    compare(data, sys, 1);
    compare(data, sys, 'g', 1);
    compare(data, sys, 'g');
    close all;
end

function testSyntax(testCase)
    % Test syntax of compare for different input arguments
    true_sys = testCase.TestData.sys;
    data = testCase.TestData.val_data;
    id_sys = LPVcore.lpvidss(preal('p', 'dt'), 1, 1, 1, 'innovation');
    id_sys_io = LPVcore.lpvidpoly(1, {1, preal('p', 'dt')});
    id_sys.Ts = true_sys.Ts;
    id_sys_io.Ts = true_sys.Ts;

    % Check different calling syntaxes
    compare(data, id_sys, 'g', 1);
    compare(data, id_sys, 1);
    compare(data, id_sys, Inf);
    compare(data, id_sys, id_sys, id_sys, Inf);
    compare(data, id_sys, id_sys, 'k--', id_sys, Inf);
    compare(data, id_sys, 'g', id_sys, 'k--', id_sys, '--');
    verifyError(testCase, @() compare(id_sys), 'MATLAB:narginchk:notEnoughInputs');
    verifyError(testCase, @() compare(data, data), '');
    verifyError(testCase, @() compare(data, 'g', id_sys, 1), '');
    verifyError(testCase, @() compare(data, id_sys, 2), '');  % invalid k
    % Check output arguments
    [ySys, fit] = compare(data, id_sys, id_sys, Inf);
    verifyEqual(testCase, fit{1}, fit{2});
    verifyEqual(testCase, ySys{1}, ySys{2});
    [ySys, ~] = compare(data, id_sys, id_sys, 1);
    verifyEqual(testCase, ySys{1}, ySys{2});
    % Test with LPVIDPOLY
    [ySys, ~] = compare(data, id_sys_io, id_sys_io, Inf);
    verifyEqual(testCase, ySys{1}, ySys{2});
    [ySys, ~] = compare(data, id_sys_io, id_sys_io, 1);
    verifyEqual(testCase, ySys{1}, ySys{2});
    % Only DT models
    verifyError(testCase, ...
        @() compare(data, LPVcore.lpvidss(preal('p', 'ct'), 1, 1, 1, 'innovation')), '');
    close all
end

function testInitialConditionEstimation(testCase)
    % Testing initial condition estimation of lpviddata.compare using the
    % following simple input-output relation:
    %   y(k+1) = y(k) + u(k - q)
    % with q the input delay.
    % Which has as solution:
    %   y(1) = y1 (initial condition)
    %   y(2) = y1 + u(1 - q)
    %   y(3) = y1 + u(1 - q) + u(2 - q)
    %   ...
    %   y(k) = y1 + u(1 - q) + ... + u(k-1-q)
    % Its transfer function is:
    %   H(z) = 1 / (z - 1)
    % and a minimal state-space realization is:
    %   x(z+1) = I x(z) + I u(z)
    %     y(z) = I x(z)
    %
    % We test the initial condition estimation of this system for the LPVcore
    % system representations and verify the results with the System
    % Identification Toolbox.

    for q=[0, 2]      % loop over input delay
    for ny=[1, 3, 5]  % loop over number of output channels
    % Step 1: generate data
    I = eye(ny);  % short-hand notation for identity matrix
    Ts = 1;  % Sample time
    y1 = 10 * linspace(1, ny, ny).'; % Initial condition
    N = 10;  % Number of data points
    k = linspace(1, N, N).';
    u = rand(N, ny);                   % Input data
    % Apply InputDelay
    u_delayed = [zeros(q, ny); ...
        u(1:end-q, :)];
    y = y1.' + cumsum([zeros(1, ny); u_delayed(1:end-1, :)]);    % Output data
    p = zeros(size(k));                  % Scheduling data (we consider an LTI example)
    data = iddata(y, u, Ts);
    lpvdata = lpviddata(y, p, u, Ts);
    
    % Step 2: construct models
    numerator = num2cell(I);
    denominator = num2cell(ones(ny));
    for i=1:ny
        denominator{i, i} = [1, -1];
    end
    sys = tf(numerator, denominator, Ts);
    sys.InputDelay = q;
    rho = preal('p', 'dt');
    lpvsys_io = LPVcore.lpvio({I, -I + rho}, {zeros(ny), I}, Ts);
    lpvsys_io.InputDelay = q;
    lpvsys_ss = LPVcore.lpvss(I + rho, I, I, zeros(ny), Ts);
    lpvsys_ss.InputDelay = q;
    lpvsys_idpoly = LPVcore.lpvidpoly(I, {zeros(ny), I}, I, I, {I, -I + rho}, I, Ts);
    lpvsys_idpoly.InputDelay = q;
    
    % Step 3: apply compare and check results
    [~, ~, ic_lti] = compare(data, sys);

    % Syntax 1: without output arguments --> produces a figure
    figure;
    compare(lpvdata, lpvsys_io, lpvsys_ss, lpvsys_idpoly);
    close all;

    % Syntax 2: with output arguments
    [~, fit, ic] = compare(lpvdata, lpvsys_io, lpvsys_ss, lpvsys_idpoly);

    % Confirm that fit is 100%
    for i=1:numel(fit)
        verifyEqual(testCase, fit{i}, 100 * ones(ny, 1), 'AbsTol', sqrt(eps));
    end
    % Confirm that the initial condition of LTI compare matches the initial
    % condition of LPV compare for the state-space realization
    verifyEqual(testCase, ic{2}, ic_lti, 'AbsTol', sqrt(eps));
    end
    end
end
