%% Moment matching based model reduction
% This example script demonstrates the usage of the function LPVMMRED.
clearvars; close all; rng(1);

ny = 1;
nu = 1;
time = 'dt';

% Time domain simulation takes much longer with the continuous-time model.
% Thus, we reduce the number of states in that case.
if strcmp(time, 'ct')
    nx = 100;
else
    nx = 1000;
end
% Number of time samples to match exactly in ROM
% The greater N, the higher the dimension of the ROM
N = 2;

% Build FOM
p = preal('p', time);
A0 = rand(nx);
A1 = rand(nx);
if strcmp(time, 'dt')
    A0 = A0 ./ (3*max(abs(eig(A0))));
    A1 = A1 ./ (3*max(abs(eig(A1))));
elseif strcmp(time, 'ct')
    A0 = A0 - 3*max(real(eig(A0))) * eye(nx);
    A1 = A1 - 3*max(real(eig(A1))) * eye(nx);
end
A = A0 + p * A1;
B = rand(nx, nu);
C = rand(ny, nx);
D = rand(ny, nu);

sys = LPVcore.lpvss(A, B, C, D);

%% Reduction
disp('Reduction mode = T'); tic;
sysr_T = lpvmmred(sys, N, 'T'); toc;
disp('Reduction mode = R'); tic;
sysr_R = lpvmmred(sys, N, 'R'); toc;
disp('Reduction mode = O'); tic;
sysr_O = lpvmmred(sys, N, 'O'); toc;
r_T = size(sysr_T.A, 1);
r_R = size(sysr_R.A, 1);
r_O = size(sysr_O.A, 1);

% Alternatively, the same algorithm is wrapped by the more general function LPVMODRED.
% The difference between calling LPVMMRED and LPVMODRED is that LPVMMRED
% requires the user to specify the number of time steps to match, while
% LPVMODRED requires the user to specify the maximum state dimension (an
% iterative procedure then determines the appropriate number of time
% steps).
[sysr_modred, info] = lpvmodred(sys, 12, [], 'momentmatching', ...
    struct('verbose', true, 'mode', 'T'));
% The number of time steps matched is stored in the info structure
disp(info);

%% Time-domain simulation
Nt = N * 20;
t = linspace(0, 1, Nt);
u = randn(Nt, nu);
p = rand(Nt, sys.Np);

disp('Simulating FOM'); tic;
y = lsim(sys, p, u, t); toc;
disp('Simulating ROM mode T'); tic;
yr_T = lsim(sysr_T, p, u, t); toc;
disp('Simulating ROM mode R'); tic;
yr_R = lsim(sysr_R, p, u, t); toc;
disp('Simulating ROM mode O'); tic;
yr_O = lsim(sysr_O, p, u, t); toc;

%% Visualize results
figure;
subplot(3, 1, 1);
plot(u, 'k-*'); hold on; grid on;
plot(p, 'k--o');
legend('Input', 'Scheduling');
title('Input & scheduling');
subplot(3, 1, 2);
plot(y, 'k-*'); hold on; grid on;
plot(yr_T, 'r-*');
plot(yr_R, 'g-*');
plot(yr_O, 'b-*');
legend(sprintf('FOM (n=%i)', nx), ...
    sprintf('ROM mode T (n=%i)', r_T), ...
    sprintf('ROM mode R (n=%i)', r_R), ...
    sprintf('ROM mode O (n=%i)', r_O));
title('Output');
subplot(3, 1, 3);
plot(abs(y - yr_T), 'r-*'); hold on;
plot(abs(y - yr_R), 'g-*');
plot(abs(y - yr_O), 'b-*');
if strcmp(time, 'dt')
    xline(N, 'k--', sprintf('Exact match R & O (N=%i)', N));
    xline(2*N, 'k--', sprintf('Exact match T (N=%i)', 2*N));
end
xlabel('time sample');
title('Error'); grid on;
sgtitle('Time-domain simulation');
