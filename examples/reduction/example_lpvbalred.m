%% Example script demonstrating usage of LPVBALRED
%
%
% Scheduling reduction using LPVBALRED to an interconnection of
% Mass-Spring-Damper system example from:
%   "Olucha, E. Javier, Bogoljub Terzin, Amritam Das, and Roland Tóth. 
%   “On the Reduction of Linear Parameter-Varying State-Space Models.” 
%   arXiv, April 2, 2024. http://arxiv.org/abs/2404.01871."

clearvars; close all; rng(1);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%                            EXAMPLE                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Reproducing the MSD with Nm = 5, nx = 10, np = 9
Nm = 5; param = [1 0.5 1 0.5 1]; % [M k d kwall dwall]

nl_index.springs = 1:Nm-1;
nl_index.springs_wall = 1:Nm;
nl_index.dampers = []; nl_index.dampers_wall = [];

k = @(x) x^3; % Spring connecting mass blocks
d = @(x,xdot) 0; % Damper connecting mass blocks
k_wall = @(x) x^3; % Spring connecting each mass with the wall
d_wall = @(x,xdot) 0; % Damper connecting each mass with the wall

[fMSD,state,input] = msdGenerator(Nm,nl_index,k,d,k_wall,d_wall);

f = @(x,u) fMSD(x,u,param); 
% Output: position and velocity of the last Mass
h = @(x,u) [x(Nm,:); x(end,:)];
nx = length(state); nu = length(input); ny = 2;
nlss = LPVcore.nlss(f,h,nx,nu,ny, 0, true);
[sys, schedulingMap] = LPVcore.nlss2lpvss(nlss,"analytical","factor");

% NOTE: LPVBALRED is able to handle Discrete-Time models as well. For
% example, we could discretize the nonlinear system "nlss" with 
% nlssdt = c2d(nlss,Ts,"eul");
% and continue this example in discrete-time instead


%Nonlinear simulation of the MSD
tsim = 25; Ts = 0.1; t=(0:Ts:tsim)';
u = sum(2.*rand(1,3).*sin(2*pi.*(linspace(0.2,2,3)).*t + (0 + (pi-0).*rand(1,3))),2);
[~, ~, x] = sim(nlss, u, t, zeros(nlss.Nx,1));


% Compute the scheduling trajectory
pTraj = schedulingMap.map(x', u')';
N = numel(t);

%% Reduce the state-order using LPVBALRED
%Choose the scheduling grid. In this case, we pick some samples from the
%scheduling trajectory.
pgrid = pTraj(1:10:end,:);

%State-order reduction target
rx = 2; 

%Execute the reduction
[sysred, Tred, info] = lpvbalred(sys, rx, pgrid);

%% Frequency domain verification
rng(2); nCheck = 10;

pCheck = num2cell(pTraj(randperm(N, nCheck), :),2);

G = extractLocal(sys, pCheck);      G = cat(3, G{:});
Gh = extractLocal(sysred, pCheck);  Gh = cat(3, Gh{:});

lw = 2;     % lineWidth 
fs = 12;    % axis fontsize
f3 = figure(3);
bodemag(G,Gh); grid on;
legend('Full LPV, Nx = 10', ...
    sprintf('Reduced LPV, Rx = %i', rx), 'FontSize', fs);

%% Time domain verification
%Note that for self-scheduled simulations, we can compose the reduced-state
%order scheduling map \hat{eta} as: \hat{p} = \hat{eta} (Tred*x_r, u)

y_sys = lsim(sys, pTraj, u, t);
y_sysr = lsim(sysred, pTraj, u, t);


f4 = figure(4);
tiledlayout(2,1,"TileSpacing","compact","Padding","compact");

nexttile
plot(t, x(:,5), LineWidth = lw);  hold on;
plot(t, y_sys(:,1), '--', LineWidth = lw);
plot(t, y_sysr(:,1), '-.', LineWidth = lw);
grid on;
ylabel('$x_5$ [m]', Interpreter = 'latex', FontSize = fs);

nexttile
plot(t, x(:,10), LineWidth = lw);  hold on;
plot(t, y_sys(:,2), '--', LineWidth = lw);
plot(t, y_sysr(:,2), '-.', LineWidth = lw);
grid on;
ylabel('$\dot{x}_5$ [m/s]', Interpreter = 'latex', FontSize = fs);
xlabel('$t$ [s]', Interpreter = 'latex', FontSize = fs)
leg = legend('Nonlinear', 'Full LPV, Nx = 10', ...
    sprintf('Reduced LPV, Rx = %i', sysred.Nx), ...
    'FontSize', fs - 2,'Orientation', 'Horizontal');
leg.Layout.Tile = 'north';

f4.Position(1) = f3.Position(1) + f3.Position(3) + 1;
