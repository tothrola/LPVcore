%% Example script demonstrating usage of LPVPCARED
%
% The trajectory mode of LPVPCARED script simplifies the scheduling 
% dependence of a given LPVLFR object by 1) lowering the dimension of the 
% scheduling signal; and  2) affine dependency on scheduling signal

% The standard PCA mode of LPVPCARED is a simpler version of the trajectory
% mode, where the current implementation is restricted to affine LPV-SS 
% systems. It can be used to lower the dimension of the 
% scheduling signal

%% Example 1: Using "trajectory" PCA to reduce the scheduling dimension and 
% obtain an affine dependency
clearvars; close all; rng(1);

% Set up the full model to be reduced with LPVPCARED
p = preal('p', 'ct');
q = preal('q', 'ct');
% State and input-output dimensions are parametrized
nx = 5;
ny = 1;
nu = 1;

% Generate state-space coefficients. Note that this is system is NOT affine
% in the scheduling parameters. Therefore, the "standard" PCA is not
% applicable in such case
A = (p + q + 0.5 * p^2) * eye(nx);
B = p * ones(nx, nu);
C = p^2 * ones(ny, nx);
D = 10 * ones(ny, nu);
sys = LPVcore.lpvss(A, B, C, D);

% Generate simulation data
N = 100;
u = randn(N, sys.Nu);
rho = randn(N, sys.Np);
t = linspace(0, 1, N);
y = lsim(sys, rho, u, t);
data = lpviddata(y, rho, u, t(2)-t(1));

% Use LPVPCARED in trajectory mode to generate a system SYSR with 
% the following reductions in complexity:
%   1) Lower scheduling signal dimension from 2 to 1
%   2) Affine dependence in scheduling signal (instead of basis-affine
%           dependence in SYS)

[sysrTraj, mapping_Traj, infoTraj] = lpvpcared(sys, 1, data, 'trajectory');

% Besides the reduced-complexity model SYSR, a function handle MAPPING_FNC
% is returned that is used to convert the original scheduling signal
% (associated with SYS) to the reduced scheduling signal associated with 
% SYSR. It takes as input argument an N-by-Nrho matrix of N time samples of
% the original scheduling signal (dimension Nrho) and returns an 
% N-by-Nrho_sysr matrix with the reduced scheduling signal.
rho_sysrTraj = mapping_Traj(rho);

% Subsequently, we can perform simulations with SYSR
yrTraj = lsim(sysrTraj, rho_sysrTraj, u, t);

% The third output of LPVPCARED is a structure with additional information.
% For example, the singular values of the data matrix, which can be plotted
% to determine an appropriate scheduling signal dimension.
figure(1);
semilogy(infoTraj.DataMatrixSingularValues, 'r*-'); grid on; hold on;
title('Singular values of data matrix');
xline(sysrTraj.Np, 'r--', 'sysr.Np');
ylabel('Output [-]');
xlabel('Time [s]');

% Validate the reduced-complexity model by comparing a time-domain
% simulation of the input-output behavior between SYS and SYSR
figure(2);
subplot(2, 1, 1);
plot(t, y, 'k'); hold on; grid on;
plot(t, yrTraj, 'r--');
title('Output response');
legend(sprintf('Original (Np = %i)', sys.Np), ...
    sprintf('Affine trajectory/reduced (Np = %i)', sysrTraj.Np));
subplot(2, 1, 2);
semilogy(t, abs(y - yrTraj), 'r--'); grid on;
title(sprintf('Error of Traj. PCA (fit = %.3f)', goodnessOfFit(yrTraj, y, 'NRMSE')));
ylabel('$|y - \hat{y}|$');
xlabel('Time [s]');
%% Example 2: Reducing the scheduling dimension of the Mass-Spring-Damper 
% affine LPV-SS system example from:
%   "Olucha, E. Javier, Bogoljub Terzin, Amritam Das, and Roland Tóth. 
%   “On the Reduction of Linear Parameter-Varying State-Space Models.” 
%   arXiv, April 2, 2024. http://arxiv.org/abs/2404.01871."

% Comparison between the "trajectory"and "standard" modes

%Reproducing the MSD with Nm = 5, nx = 10, np = 9
Nm = 5; param = [1 0.5 1 0.5 1]; % [M k d kwall dwall]
nl_index.springs = 1:Nm-1;
nl_index.springs_wall = 1:Nm;
nl_index.dampers = []; nl_index.dampers_wall = [];

k = @(x) x^3; % Spring connecting mass blocks
d = @(x,xdot) 0; % Damper connecting mass blocks
k_wall = @(x) x^3; % Spring connecting each mass with the wall
d_wall = @(x,xdot) 0; % Damper connecting each mass with the wall

[fMSD,state,input] = msdGenerator(Nm,nl_index,k,d,k_wall,d_wall);

f = @(x,u) fMSD(x,u,param); 
% Output: position and velocity of the last Mass
h = @(x,u) [x(Nm,:); x(end,:)];
nx = length(state); nu = length(input); ny = 2;
nlss = LPVcore.nlss(f,h,nx,nu,ny, 0, true);
[sys, schedulingMap] = LPVcore.nlss2lpvss(nlss,"analytical","factor");

%Nonlinear simulation of the MSD
tsim = 25; Ts = 0.1; t=(0:Ts:tsim)';
u = sum(2.*rand(1,3).*sin(2*pi.*(linspace(0.2,2,3)).*t + (0 + (pi-0).*rand(1,3))),2);
[~, ~, x] = sim(nlss, u, t, zeros(nlss.Nx,1));

% Compute the scheduling trajectory
pTraj = schedulingMap.map(x', u')';

% Use LPVPCARED in trajectory mode
[sysrTraj, mapping_Traj, infoTraj] = lpvpcared(sys, 2, pTraj, 'trajectory');

% Use LPVPCARED in standard mode
[sysrStd, mapping_Std, infoStd] = lpvpcared(sys, 2, pTraj,'standard');

% Use the function handle MAPPING_FNC to project the original scheduling
% signal into the reduced scheduling signal associated with the reduced
% affine LPV-SS systems.
rho_sysrTraj = mapping_Traj(pTraj);
rho_sysrStd = mapping_Std(pTraj);

% Subsequently, we can perform simulations with SYSR
y = lsim(sys, pTraj, u, t);
yrTraj = lsim(sysrTraj, rho_sysrTraj, u, t);
yrStd = lsim(sysrStd, rho_sysrStd, u, t);

% The third output of LPVPCARED is a structure with additional information.
% For example, the singular values of the data matrix, which can be plotted
% to determine an appropriate scheduling signal dimension.
figure(3); clf(3);
semilogy(infoTraj.DataMatrixSingularValues, 'r*-'); grid on; hold on;
semilogy(infoStd.DataMatrixSingularValues, 'b*-');
title('Singular values of data matrix');
xline(sysrTraj.Np, 'r--', 'sysr.Np');
xline(sysrStd.Np, 'b--', 'sys.Np');

% Validate the reduced-complexity model by comparing a time-domain
% simulation of the input-output behavior between SYS and SYSR
figure(4); clf(4);
subplot(2, 1, 1);
plot(t, y(:,1), 'k'); hold on; grid on;
plot(t, yrTraj(:,1), 'r--');
plot(t, yrStd(:,1), 'b--');
title('Output response');
ylabel('Position [m]');
xlabel('Time [s]');
legend(sprintf('Original (Np = %i)', sys.Np), ...
    sprintf('Affine trajectory/reduced (Np = %i)', sysrTraj.Np),...
    sprintf('Affine standard/reduced (Np = %i)', sysrStd.Np));
subplot(2, 1, 2);
semilogy(t, abs(y(:,1) - yrTraj(:,1)), 'r--'); grid on; hold on;
semilogy(t, abs(y(:,1) - yrStd(:,1)), 'b--');
ylabel('$|y - \hat{y}|$');
xlabel('Time [s]');
title(sprintf('Error of Traj. PCA (fit = %.3f) and Standard PCA (fit = %.3f) ', goodnessOfFit(yrTraj(:,1), y(:,1), 'NRMSE'),goodnessOfFit(yrStd(:,1), y(:,1), 'NRMSE')));


%% Example 1 from [1]
% TODO: fix issues with this example
% [1] "Affine linear parameter-varying embedding of non-linear models with
% improved accuracy and minimal overbounding" (IET Control Theory &
% Applications, 2021)
clearvars; close all; rng(1);

a1 = preal('a1', 'ct');
a2 = preal('a2', 'ct');
a3 = preal('a3', 'ct');

A = [1 + 2 * a1, 3 + a2; 2 + 3 * a3, 20 * a1 + 5 * a2];
B = [3 * a3 + 7 * a2; 1];
C = [a1, 0];
D = 0;

sys = LPVcore.lpvss(A, B, C, D, 0, 'SVDTolerance', -1);

N = 3000;
Ts = 1E-3;
t = linspace(0, (N-1)*Ts, N).';

alpha = [2 * sin(10*t).^2, 5*cos(20*t + pi/5).^2, sin(10*t).*cos(20*t)];

[sysr, mapping_fnc, info] = lpvpcared(sys, 2, alpha);

rho = mapping_fnc(alpha);

a11 = 1 + 2 * alpha(:, 1);
a22 = 20 * alpha(:, 1) + 5 * alpha(:, 2);
b11 = 3 * alpha(:, 3) + 7 * alpha(:, 2);

Atilde = feval(sysr.A, rho);
Btilde = feval(sysr.B, rho);
a11_tilde = reshape(Atilde(1, 1, :), [], 1);
a22_tilde = reshape(Atilde(2, 2, :), [], 1);
b11_tilde = reshape(Btilde(1, 1, :), [], 1);

figure;
subplot(2, 3, 1);
plot(t, a11, 'k--'); grid on; hold on;
plot(t, a11_tilde, 'g');

subplot(2, 3, 2);
plot(t, a22, 'k--'); grid on; hold on;
plot(t, a22_tilde, 'g');

subplot(2, 3, 3);
plot(t, b11, 'k--'); grid on; hold on;
plot(t, b11_tilde, 'g');

subplot(2, 3, 4);
plot(t, a11 - a11_tilde, 'g'); grid on;
ylim([-1.5, 2]);

subplot(2, 3, 5);
plot(t, a22 - a22_tilde, 'g'); grid on;
ylim([-6, 8]);

subplot(2, 3, 6);
plot(t, b11 - b11_tilde, 'g'); grid on;
ylim([-15, 12]);