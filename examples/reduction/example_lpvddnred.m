%% Example script demonstrating usage of LPVDNNRED
%
% In this example we apply scheduling reduction using LPVDNNRED to the
% two-link robot arm example from "Scheduling Dimension Reduction of LPV 
% Models - A Deep Neural Network Approach" by P.J.W. Koelewijn and R. Toth 
% (ACC, 2020).

clearvars; close all; rng(1);

%% Robot arm physical parameters
a = 5.6794;     b = 1.473;      c = 1.7985;
d = 4e-1;       e = 4e-1;       f = 2;
n = 1;

param = [a,b,c,d,e,f,n];

%% Nonlinear simulation of the robot arm
uFun = @(t) [3 * sin(t), -cos(t)];

t = linspace(0, 20, 5000);

odefun = @(t,x) robotArm(x, uFun(t), param);
[t, x] = ode45(odefun, t, zeros(4,1));

%% Compute the scheduling trajectory
pTraj = schedMap(x', param)';
N = numel(t);

%% Create the LPV system with 10 scheduling-variables
np = size(pTraj, 2);

p = cell(np,1);
for i = 1:np
    p{i} = preal(sprintf(sprintf('p(%%0%id)', numel(num2str(np))), i));
end

A = [ 0,         0,        1,    0;
      0,         0,        0,    1;
      c*d*p{3}, -b*e*p{4}, p{5}, b*p{6};
     -b*d*p{7},  a*e*p{8}, p{9}, p{10}];

B = [ 0,         0;
      0,         0;
      c*n*p{1}, -b*n*p{2};
     -b*n*p{2},  a*n*p{1}];

C = [eye(2),zeros(2)];
D = zeros(2);

sys = LPVcore.lpvss(A,B,C,D);

%% Reduce the scheduling dimension using LPVDNNRED
nrho = 2;
% Increase the number of epochs if needed to achieve better performance
num_epochs = 10;
[sysr, map_fcn, info] = lpvdnnred(sys, nrho, pTraj, ...
    'trainingOptions', struct('epochs', num_epochs));

%% Verification of reduced system
% Bode
rng(2);
nCheck = 10;

pCheck = pTraj(randperm(N, 10), :);
rhoCheck = map_fcn(pCheck);

pCell = mat2cell(pCheck, ones(nCheck, 1), np);
rhoCell = mat2cell(rhoCheck, ones(nCheck, 1), nrho);

G = extractLocal(sys, pCell);      G = cat(3, G{:});
Gh = extractLocal(sysr, rhoCell);  Gh = cat(3, Gh{:});

f1 = figure(1);
bodemag(G,Gh); grid on;
legend('Full LPV, Np = 10', ...
    sprintf('Reduce LPV, Np = %i', nrho), 'FontSize', 13);

% Simulation
y_sys = lsim(sys, pTraj, uFun(t), t);
y_sysr = lsim(sysr, map_fcn(pTraj), uFun(t), t);

lw = 2;     % lineWidth
fs = 15;    % axis fontsize

f2 = figure(2);
tiledlayout(2,1,"TileSpacing","compact","Padding","compact");

nexttile
plot(t, x(:,1), LineWidth = lw);  hold on;
plot(t, y_sys(:,1), '--', LineWidth = lw);
plot(t, y_sysr(:,1), '-.', LineWidth = lw);
grid on;
ylabel('$q_1$ [rad]', Interpreter = 'latex', FontSize = fs);

nexttile
plot(t, x(:,2), LineWidth = lw);  hold on;
plot(t, y_sys(:,2), '--', LineWidth = lw);
plot(t, y_sysr(:,2), '-.', LineWidth = lw);
grid on;
ylabel('$q_2$ [rad]', Interpreter = 'latex', FontSize = fs);
xlabel('$t$ [s]', Interpreter = 'latex', FontSize = fs)
legend('Nonlinear', 'Full LPV, Np = 10', ...
    sprintf('Reduce LPV, Np = %i', nrho), ...
    'FontSize', fs - 2);

f2.Position(1) = f1.Position(1) + f1.Position(3) + 1; % place next to other
                                                      % figure

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%                             LOCAL FUNCTIONS                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xdot = robotArm(x, u, par)
    a = par(1);
    b = par(2);
    c = par(3);
    d = par(4);
    e = par(5);
    f = par(6);
    n = par(7);

    q1 = x(1);
    q2 = x(2);
    q1dot = x(3);
    q2dot = x(4);

    u = [u(1);u(2)];

    M = [a,                b * cos(q1 - q2);
         b * cos(q1 - q2), c];

    C = [b * sin(q1 - q2) * q2dot^2 + f * q1dot;
         -b * sin(q1 - q2) * q1dot^2 + f * (q2dot - q1dot)];

    g = [-d * sin(q1);
         -e * sin(q2)];

    qddot = M \ (n * u - C - g);
    
    xdot = [q1dot;q2dot;qddot];
end

function p = schedMap(x, par)
    a = par(1);
    b = par(2);
    c = par(3);
    f = par(6);

    q1 = x(1,:);
    q2 = x(2,:);
    q1d = x(3,:);
    q2d = x(4,:);

    cosd = cos(q1-q2);
    sind = sin(q1-q2);

    h = 1 ./ (a * c - b^2 * cosd.^2);
    
    p = [h;
         cosd .* h;
         sincFun(q1) .* h;
         cosd .* sincFun(q2) .* h;
         (-b^2 * cosd .* sind .* q1d - (c + b * cosd) * f) .* h;
         (-c * sind .* q2d + cosd * f) .* h;
         cosd .* sincFun(q1) .* h;
         sincFun(q2) .* h;
         (a * b * sind .* q1d + f * (a + b * cosd)) .* h;
         (b^2 * sind .* cosd .* q2d - a * f) .* h];
end

% sinc function
function  y = sincFun(x)
    y = sin(x)./x;
    y(x == 0) = 1;
end