%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%                  Tutorial 5: Local LPV identification
%                   written by R. Toth and P. den Boef
%                         version (01-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this fith part of the tutorial, you'll be able to 
%       (a) set up data containers and mdoel structures for local
%           identification / modelling with LPVcore
%       (b) learn how to interpolate locally identified models to get a
%           global LPV description
%
%   After completing the tutorial you may follow T6 on model reduction with
%   LPVcore
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time 
%       CT Continuous Time 

