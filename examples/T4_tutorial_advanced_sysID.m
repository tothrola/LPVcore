%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%           Tutorial 4: Advanced LPV identification methods
%                   written by R. Toth and P. den Boef
%                         version (01-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this third part of the tutorial, you'll be able to 
%       (a) Use instrumental variable-based schemes to estimate LPV 
%           polynomial models
%       (b) Specify which parameters to identify in a model structure and
%           this way enable structured identification and use of gray box
%           approaches
%
%   After completing the tutorial you may follow T5 on model reduction with
%   LPVcore 
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time    
%       PEM Prediction Error Minimization

clearvars; close all; clc;
rng(29);   % set random number generator seed so that results are 
           % reproducible

%% 1.b Define unbalanced disc model parameters

% We will keep investigating our unbalanced disc system from T1 

K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.01;                 % Sampling time


%% 2.a Data generating systems 

% Similar to T2, define the data-generating system as an IO model

pd=preal('p','dt');
pd1=pshift(pd,-1);  
pd2=pshift(pd,-2);               % Dynamic dependence, shifted back 
                                    % 2 samples
a0=1;                               % Coefficients of the output side         
a1=Ts/tau-2;               
a2=1-Ts/tau+((M*g*l)/J)*(Ts^2)*pd2;  
b2=Km/tau*Ts^2;                     % Coefficient of the input side

c0=1;                               % Noise model
c1=0.75*pshift(pd,-1);
d0=1;
d1=-0.25+0.25*pshift(pd,-1);

% We simplify the noise model for the sake of simplicity
c1=peval(c1,0.75);
d1=peval(d1,0.75);

% Create and idpolyobject => BJ model
sys = LPVcore.lpvidpoly([],{0, 0, b2},{c0,c1},{d0,d1},{a0, a1, a2}); 
sys.Ts = Ts;                   % Sampling time
sys.InputName = {'Voltage'};   % Naming of the signals
sys.OutputName = {'Angular position'};
% TODO: set NoiseVariance back to 4.5e-4 (SNR approx. 20 dB)
sys.NoiseVariance = 4.5*10^-8;     % Specify noise variance

%% 2.b Create data sets

% Next, we generate synthetic data using sys to mimic measurement conducted 
% on the real system.  
 
N=2000;         % Number of samples
p_mag=0.25;     % Scheduling variation
 
% Estimation data
u=idinput(N,'rgs');  % White noise input signal
p=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]); % RBS scheduling signal

% Note that for the sake of simplicity, 'p' is generated independently from
% the scheduling map and the associated angular position. This is not a
% limitation of the toolbox but used to simplify the example.

[y,~,~,yp] = lsim(sys,p,u);       % Generate data including noise signal
                                  % y - noisy output
                                  % yp - noise free output 
 
fprintf('The SNR is %f dB.\n', LPVcore.snr(yp,y-yp));  % Signal-to-noise ratio
 
data_est = lpviddata(y,p,u,Ts);      % Create an LPV data object from the 
                                     % generated data
plot(data_est);                      % Show data
 
% Create a noisy and a noise free validation data set (called test sets 
% in machine learning)

uv=idinput(N,'rgs');
pv=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]);
[yv,~,~,yvp] = lsim(sys,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise free data set 

%% 3.a Refined Instrumental Variable (RIV) method

% Instrumental variable approaches have been very successful in LTI
% identification and their variants have been also developed for the LPV
% case. In LPVcore, the approach detailed in [1] is implemented with the
% script LPVRIV. 

% Let's define a template for characterizing the model strucutre
% Process model, F part:
fm0=1;
fm1=1+pd2;  % pd + pd1
fm2=fm1;
% Process model, B part (TODO: set all coefficients back to fm1)
bm0=1e-9;
bm1=1e-9;
bm2=0.0025646;  % fm1;
% Noise model, C part:
cm0=1;
cm1=1;
% Noise model, D part:
dm0=1;
dm1=1;
% Due to technical reasons the current LPV-RIV approach can only estiamte
% LTI noise models. For the exact technical reasons see [1].

template_riv = LPVcore.lpvidpoly([],{bm0,bm1,bm2},{cm0,cm1},{dm0,dm1},{fm0,fm1,fm2});

% The full RIV scheme can be called by LPVRIV. In case no noise model is
% specified in the model structure template, the simplified form of the IV
% estimation called SRIV is executed. Initialization of the approach 
% involves a default ARX pre-estimation. However, custom initialization can 
% be also accomplished via the template. Also the maximum number of allowed 
% IV iterations can be set. See LPVRIVOPTIONS for all options and their
% effect.

% First call the SRIV without noise model estimation
template_sriv = LPVcore.lpvidpoly([],{bm0,bm1,bm2},[],[],{fm0,fm1,fm2});

options= lpvrivOptions;
options.MaxIterations=10;
options.Display='on';
options.Initialization='polypre';
options.Focus='prediction';
options.Regularization=eps;

%%%%%%%%%%% TEMPORAL %%%%%%%%%%%%%
options.Initialization='template';
%obj.Implementation = 'laurain';
optionsoe = lpvoeOptions(...
    'Initialization', 'polypre', ... % LPV ARX model estimation-based initialization 
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
m_oe = lpvoe(data_est, template_sriv, optionsoe);
template_sriv =m_oe;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m_sriv = lpvriv(data_est,template_sriv,options);


% To show manual initialization, we can use the previous estimate
options.Initialization='template';
m_sriv = lpvriv(data_est, m_sriv, options);

% Finally, we can estiamte a BJ-type of model with the full RIV algorithm
options.Initialization='polypre';
m_riv = lpvriv(data_est, template_riv, options);

% Display prediction and simulation results on the validation data set
figure;
compare(data_val_1,m_sriv,m_riv,1);
figure;
compare(data_val_2,m_sriv,m_riv);

% [1] Laurain, V., M. Gilson, R. Toth and H. Garnier: Refined instrumental 
%     variable methods for identification of LPV Box-Jenkins models, 
%     Automatica, Vol. 46, No. 6, (2010), pp. 959-967.

%% 3.b OBF based model structures and identification 

% OBF based model structures involve a filter bank of orthonormal basis
% functions generated by a set of pole locations and parameter-varying
% expansion coefficient blocks preceding or following the OBF bank or 
% they are in feedback connection with it, see [2] for details. In 
% LPVcore only the first two options are implemented currently, called 
% the  Wiener and Hammerstein configuration.   

% Let's assemble an SS representation of the plant just like in T3
pm = preal('p','dt');       % Define DT scheduling variable
A=eye(2)+Ts*[-1/tau, -(M*g*l/J)*pm; 1 0];
B=Ts*[Km/tau; 0];
C=[0 1];
D=0;
sys_ss=LPVcore.lpvidss(A,B,C,D,'innovation',[], [], [],Ts);

% Define pole locations of the inner function to generate the OBFs.
v=[eig(peval(sys_ss.A,1));eig(peval(sys_ss.A,1))];
% Number of extensions of inner function
ne = 3;

% Next step is to determine the parametrization of the Wiener and Hammerstein
% blocks. Each block may be set to empty ("[]") to exclude it from being
% used. Note that a scalar expression will be expanded to match the number
% of coefficients (ne * numel(v) + 1).

% Wiener filter
Wf =[0,ones(1, ne * numel(v)) * 2 + pm];

% Hammerstein filter
Hf=[];

% Creat a template for identification
template_obf = LPVcore.lpvidobf(v, ne, Wf, Hf, [], Ts);
disp(template_obf)

% Identification (corresponding to a LS fit, see [2])

options = lpvobfestOptions; %('InitialState', 'estimate');
[m_obf, est_x0] = lpvobfest(data_est, template_obf, options);

figure;
compare(data_est, m_obf);
figure;
compare(data_est, m_obf,1);

% Now let's try the Hammerstein configuration:

% Wiener filter
Wf =[];

% Hammerstein filter
Hf=[0,ones(1, ne * numel(v)) * 2 + pm]';

% Create a template for identification
template_obf = LPVcore.lpvidobf(v, ne, Wf, Hf, [], Ts);
disp(template_obf)

options = lpvobfestOptions('InitialState', 'estimate');
[m_obf, est_x0] = lpvobfest(data_est, template_obf, options);

figure;
compare(data_est, m_obf);
figure;
compare(data_est, m_obf,1);


% [2] Toth, R.: Modeling and identification of linear parameter-varying 
%     systems. Lecture Notes in Control and Information Sciences, Vol. 
%     403, Springer, Heidelberg, 2010.

%% 3.c Use of non-free parameters

% Just like in the native System Identification toolbox, parameters can be
% specified to be non-free in the template. This means that those parameters
% will not be estimated with any identification approach. By default, the
% value "0" in a specified template tells to the scripts that parameter
% should be kept zero and not estimated. 
% 
% Investigate the true model of the system defined in terms of "sys" in 2.a

disp(sys)

% Regarding parametrization, the script reports:
% Parameterization:
%   Polynomial orders:  Nb=2  Nc=1  Nd=1  Nf=2
%   Number of coefficients: 8 (free: 4)
%
% This means that at all locations where the original system representation
% have coefficients with zeros, the corresponding parameter will be excluded
% from the identification. Compared to this, the model structure below has
% the same scheduling dependency, model order and noise structure, but it
% is fully parameterized as no zeros are taken in the template and all
% coefficients are allowed to be dependent on p(k-2). 

% Process model
fm0=1;
fm1=1+pd+pd1+pd2;
fm2=fm1;
% Process model, B part:
bm0=fm1;
bm1=fm1;
bm2=fm1;
% Noise model
cm0=1;
cm1=1+pd2;
dm0=1;
dm1=1+pd2;

template_full = LPVcore.lpvidpoly([],{bm0, bm1, bm2},{cm0,cm1},{dm0,dm1},{fm0,fm1,fm2});

disp(template_full)

% Regarding parametrization, the script reports:
% Parameterization:
%   Polynomial orders:  Nb=2  Nc=1  Nd=1  Nf=2
%   Number of coefficients: 24 (free: 24)

% compared to this

% Process model
fm0=1;
fm1=1;
fm2=fm1+pd2;
bm0=1;
bm1=1;
bm2=1;
% Noise model
cm0=1;
cm1=1;
dm0=1;
dm1=1;

template_sparse = LPVcore.lpvidpoly([],{bm0, bm1, bm2},{cm0,cm1},{dm0,dm1},{fm0,fm1,fm2});

disp(template_sparse)


% When a model estimated with restricted parameterization, but still
% including a representation of the data-generating system, not only the
% complexity of the optimization problem can be reduced drastically, but
% also the variance of the estimated model. Observe the difference between
% the outcomes of the estimation with the two structures:

options = lpvpolyestOptions(...
    'Initialization', 'polypre', ... 
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
options_sparse = lpvpolyestOptions('Display', 'on', 'Regularization', struct('Lambda', 1E-12));
m_bj_sparse = lpvpolyest(data_est, template_sparse, options_sparse);
m_bj_full = lpvpolyest(data_est, template_full, options);

figure;
compare(data_val_2,m_bj_full,m_bj_sparse);
figure;
compare(data_val_1,m_bj_full,m_bj_sparse,1);

% Furthermore, we can also keep parameters fixed during the estimation
% process if we are convinced that some subparts of the system are well
% known. This enables a certain level of gray box identification. 

template_sparse.F.Free % Describes the free parameters in the model structure, by 
% setting any of these false, forces gradient-based estimation schemes to
% stick to the parameter specified in the template

% Let us specify the coefficient of f1 in the F polynomial to be non-free
template_fixed=m_bj_full;
template_fixed.F.Value{2}(:,:,1)=sys.F.Value{2};
template_fixed.F.Value{2}(:,:,2:4)=0;


template_fixed.F.Free{2}(:,:,1:4)=false;

% Currently, grey box mode is only accessible via manual initialization 
% as LPVARX is not able to handle fixed parameters

options.Initialization='template';

m_bj_fixed = lpvpolyest(data_est, template_fixed, options);

% See that the fixed parameters were not changed during estimation:

m_bj_fixed.F


%% 3.d MIMO identification 

% We can define a MIMO version of our unbalanced disc system where the
% second output is a filtered version of 

a0=diag([a0,1]);
a1=[a1,0;-0.5, -0.25*pshift(pd,-1)];
a2=diag([a2,0]);
b0=zeros(2,1);
b1=[0; Ts];
b2=[b2;0];                    

c0=eye(2);                               % Noise model
c1=eye(2)*0.75*pshift(pd,-1);
d0=eye(2);
d1=eye(2)*(-0.25+0.25*pshift(pd,-1));

% Create and idpolyobject => BJ model
sys = LPVcore.lpvidpoly([],{b0, b1, b2},{c0,c1},{d0,d1},{a0, a1, a2}); 
sys.Ts = Ts;                   % Sampling time
sys.InputName = {'Voltage'};   % Naming of the signals
sys.OutputName = {'Angular position','Extra output'};
sys.NoiseVariance = 4.5*10^-4;     % Specify noise variance

N=2000;         % Number of samples
p_mag=0.25;     % Scheduling variation
 
% Estimation data
u=idinput(N,'rgs');  % White noise input signal
p=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]); % RBS schedling signal
[y,~,~,yp] = lsim(sys,p,u);       % Generate data including noise signal
data_est = lpviddata(y,p,u,Ts);      % Create an LPV data object from the 
plot(data_est);

% Validaiton data
uv=idinput(N,'rgs');
pv=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]);
[yv,~,~,yvp] = lsim(sys,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise free data set 

options = lpvbjOptions(...
    'Initialization', 'polypre', ... % LPV RIV model estimation-based initialization (default for OE)
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');

m_bj_mimo = lpvbj(data_est, sys, options);

figure;
compare(data_val_2,m_bj_mimo);
figure;
compare(data_val_1,m_bj_mimo,1);


%% 4.a Additional examples

rng(42); 

%% Define The Data Generating System
p= preal('p','dt','Range',[-1,1]);
a1=1-0.5*p-0.1*p^2;
a2=0.5-0.7*p-0.1*p^2;
b0=0.5-0.4*p+0.01*p^2;
b1=0.2-0.3*p-0.02*p^2;
c1=0.1*p;
d1=-1;
d2=0.2+0.1*p;
Ts=1;

sys = LPVcore.lpvidpoly([],{b0,b1},{1,c1},{1 d1 d2},{1, a1, a2}); % Create and idpolyobject => ARX model
sys.Ts = Ts;                   % Sampling time
sys.InputName = {'Input'};   % Naming of the signals
sys.OutputName = {'Output'};
sys.NoiseVariance = 0.0007;    % Specify noise variance


%% Data generation
N=4000;

% Estimation data
ue=1-2*rand(N,1);
pe=(0.5*sin(0.35*pi*(0:N-1))+0.5)';

[ye,~,~,yp] = lsim(sys,pe,ue);       % Generate data including noise signal
                                  % y - noisy output
                                  % yp - noise free output 
                                  
data_est = lpviddata(ye,pe,ue,Ts);   % Create an LPV data object
plot(data_est);                   % Show data

fprintf('The SNR is %f dB.\n', LPVcore.snr(yp,ye-yp));
 
% Create a noisy and a noise free validation data set
uv=idinput(N,'rgs');
pv=(0.5*sin(0.15*pi*(0:N-1)+0.25)+0.5)';
[yv,~,~,yvp] = lsim(sys,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise free data set 

%% LPV-ARX
am=1+p+p^2+p^3;
A_init={1, am, am};
B_init={am, am, am};
template_arx = LPVcore.lpvidpoly(A_init,B_init);
options = lpvarxOptions('Display','on');
m_arx = lpvarx(data_est, template_arx, options);
 
%% LPV-ARMAX
cm=1+p;
C_init={1, cm, cm};
template_armax = LPVcore.lpvidpoly(A_init, B_init,C_init);
opts = lpvpolyestOptions(...
    'Initialization', 'polypre', ...
    'Regularization', ...
        struct('Lambda', 1E-5), ...
    'SearchOptions', ...
        struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 10), ...
    'Display', 'on');
m_armax = lpvpolyest(data_est, template_armax, opts);

%% LPV-IV estimate
template_iv = LPVcore.lpvidpoly([], B_init,{1,1,1},{1,1,1},A_init);
template_iv.Ts = 1;
opts = lpvrivOptions('MaxIterations', 10, 'Tolerance', 1E-4, 'Display', 'on','Focus','prediction');
m_iv=lpvriv(data_est, template_iv, opts); 


%% LPV-OE
opts = lpvpolyestOptions(...
    'Initialization', 'polypre', ...
    'Regularization', ...
        struct('Lambda', 1E-5), ...
    'SearchOptions', ...
        struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 10), ...
    'Display', 'on');
template_oe = LPVcore.lpvidpoly([], B_init,[],[],A_init);
m_oe = lpvpolyest(data_est, template_oe, opts);

%% LPV-BJ
D_init=C_init;
opts = lpvpolyestOptions(...
    'Regularization', ...
        struct('Lambda', 1E-5), ...
    'SearchOptions', ...
        struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 10), ...
    'Display', 'on');
template_bj = LPVcore.lpvidpoly([], B_init,C_init,D_init,A_init);
m_bj = lpvpolyest(data_est, template_bj, opts);
 

%% Comparing results
figure;
compare(data_val_2,m_arx,m_armax,m_oe,m_iv,m_bj);
figure;
compare(data_val_1,m_arx,m_armax,m_oe,m_iv,m_bj,1);





