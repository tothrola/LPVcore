%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%                    Tutorial 8: LPV gridded control design
%        written by E. Javi Olucha Delgado, R. Toth, and P. den Boef
%                         version (05-06-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this eighth part of the tutorial, you'll be able to 
%       (a) Define an LPVcore.lpvgridss object based on a set of
%       linearizations of a system, and obtain its trimming values in case
%       the linearization family is not around equilibrium points.
%       (b) Execute CT local gridded-based LPV synthesis with LPVcore.
%       (c) Execute DT gridded-based LPV synthesis with LPVcore, using an
%       an identified system from Tutorial 3.
%       (d) Implement the discrete version of a gridded-based controller in
%       closed-loop with a CT nonlinear plant, and execute simulations.
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time 
%       CT Continuous Time 

clearvars; close all; clc;
rng(42);   % set random number generator seed so that results are 
           % reproducible
          
%% 1.b Define unbalanced disc nonlinear model
% We re-use the unbalanced disc model introduced in T7.

% The nonlinear dynamic model of the unbalanced disc are:
%   x1dot = x2
%   x2dot = -mgl/J*sin(x1) - 1/tau x2 + Km/tau u
%   y = x1
% Where theta = x1 is the angular position and omega = x2 the angular speed

% Model parameters
K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.01;      % Sampling time for discretization


%% 2.a Grid-based LPV model

% In order to obtain a grid-based model, we obtain a grid of system
% linearizations. This can be done in multiple ways. For this tutorial, we
% show how to do this using the 'Symbolic Math Toolbox' from MATLAB. In case the
% symbolic toolbox is not available, we can derive the jacobians manually.

% Let's start by defining the linearization family. For this example, only x1
% influences in the plant dynamics, so a grid over x1 will suffice
% to cover the dynamic variations.
x1star = linspace(-pi,pi,10);
x2star = 0;
ustar = 0;
NGrid = size(x1star,2);

% Now, let's linearize the system using a first-order Taylor expansion. 
% We need to take into account the input trimmings, as it will be shown 
% below in the implementation. In addition, we will embed the correction 
% term f(xstar,ustar) (or zero-order term of the Taylor expansion) in the 
% input trimming.

% In this example, we will use the controller in feedback-loop with the
% plant. In such case, we don't need to take into account the output
% trimming. We will still save it just to show its effect in the
% simulation:
A = zeros(2,2,NGrid); B = zeros(2,1,NGrid);
C = zeros(1,2,NGrid); D = zeros(1,1,NGrid);
f_star = zeros(2,NGrid); trimU = zeros(1,NGrid); trimY = zeros(1,NGrid);
v = ver;
if any(strcmp('Symbolic Math Toolbox', {v.Name}))
    % Define state and input as symbolic variables
    x = sym('x',[2 1]); u = sym('u');
    % Define the nonlinear system
    xdot = [x(2); (-M*g*l/J*sin(x(1)) -1/tau*x(2) + Km/tau*u)];
    y = x(1);
    % Obtain Jacobians
    JA = jacobian(xdot,x);
    JB = jacobian(xdot,u);
    JC = jacobian(y,x);
    JD = jacobian(y,u);
    % Obtain grid of state-space matrices and input/output trimmings
    for i=1:NGrid
        A(:,:,i) = double(subs(JA,x1star(i)));
        B(:,:,i) = double(subs(JB,x1star(i)));
        C(:,:,i) = double(subs(JC,x1star(i)));
        D(:,:,i) = double(subs(JD,x1star(i)));
        f_star(:,i) = double(subs(xdot, [x(1), x(2), u],[x1star(i), x2star, ustar]));
        trimU(:,i) = -ustar -pinv(B(:,:,i))*f_star(:,i); %Embedding of the correction term into the input trimming
        trimY(:,i) = -double(subs(y, [x(1), x(2), 'u'],[x1star(i), x2star, ustar])); %Output trimming
    end
else
    xdot = @(x,u) [x(2); (-M*g*l/J*sin(x(1)) -1/tau*x(2) + Km/tau*u)];
    y = @(x,u) x(1);
    % Manual derivation of the jacobians
    % x1dot = [ 0 1 ]x + 0 u
    % x2dot = [-mgl/J*cos(x1)  -1/tau] x + Km/tau u
    JA = @(x,u) [0, 1; M*g*l/J*cos(x(1)), -1/tau];
    JB = @(x,u) [0; Km/tau];
    JC = @(x,u) [1, 0];
    JD = @(x,u) 0;

    % Obtain grid of state-space matrices and offsets
    for i=1:NGrid
        A(:,:,i) = JA(x1star(i),0);
        B(:,:,i) = JB(x1star(i),0);
        C(:,:,i) = JC(x1star(i),0);
        D(:,:,i) = JD(x1star(i),0);
        f_star(:,i) = xdot([x1star(i); x2star],ustar);
        trimU(:,i) = -ustar -pinv(B(:,:,i))*f_star(:,i);
        trimY(:,i) = -y([x1star(i); x2star],ustar);
    end
end

% Note that, in this scenario, we are using a local LPV modeling approach
% by using the linearization. In this case, we can see from either JA:
display(JA)
% or the manual derivation of the Jacobians that the scheduling map
% becomes:

% p = eta(x,u), where eta(x,u) = cos(x(1));

% Lastly, let's create an LPVcore.lpvgridss object that contains the array
% of LTI models and the grid:
linGrid = struct('x1', x1star);
GridG = ss(A,B,C,D);
UnbalancedDisk = LPVcore.lpvgridss(GridG, linGrid);

UnbalancedDisk.InputName = 'u';          % input voltage
UnbalancedDisk.OutputName = 'theta';     % position

%% 3.a Construction of the Weighted Generalized Plant

% We use the same mixed sensitivity approach introduced in T7.
% Unweighted generalized plant (P):
%
%                          d
%           ▲              │   
%         e │  ┌ ─ ─ ┐ yk  ▼  u  ┌─────┐
% r ──▶(+)-─┴─▶   K   ─┬─▶(+)--─▶│  G  │──┐ theta
%       ▲      └ ─ ─ ┘ │         └─────┘  │
%       │              ▼                  │
%       │                                 │
%       │  ┌────┐                         │ 
%       └──│ -1 │◀────────────────-───────┘
%          └────┘        
%
%   G: Gridded LPV model (unbalanced disk)
%   K: To-be-synthesized LPV controller
%
%   r: reference
%   e: tracking error
%   u: control input
%   d: disturbance
%   u: input voltage of unbalanced disc
%   theta: anglular position of unbalanced disc

S1 = sumblk('e = r - theta'); S2 = sumblk('u = d + yk');
inputChannels = {'r','d','yk'};    outputChannels = {'e','yk','e'};
P = connect(UnbalancedDisk, S1, S2, inputChannels, outputChannels);
ny = 1;     % number of measured outputs ('e' with size 1 goes into controller)
nu = 1;     % number of controlled inputs ('u' with size 1 comes out of controller)

% Next, we design the input and output weighting filters
Wr = db2mag(-10)*makeweight(db2mag(10),[1*2*pi, db2mag(-3)], 0);
Wd = db2mag(-30)*makeweight(db2mag(10),[0.5*2*pi, db2mag(-3)], 0);
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(40), [1*2*pi, db2mag(3)], db2mag(-2));
Wu = makeweight(db2mag(-20), [20*2*pi, db2mag(3)], db2mag(20));
Wz = blkdiag(We, Wu);

opts = bodeoptions; opts.FreqUnits = 'Hz'; opts.PhaseVisible = 'off'; opts.Title.FontSize = 10; opts.Grid = 'on';
f1 = figure(1); clf(f1);
subplot(1,2,1); opts.Title.String = 'Input Weighting Filters (Ww)';  bodeplot(Ww,opts);
subplot(1,2,2); opts.Title.String = 'Output Weighting Filters (Wz^{-1})'; bodeplot(inv(Wz),opts);


% And we construct the Weighted Generalized Plant (Pw)
%                  ┌────┐          ┌────┐
%                  │ We |─▶ z1     │ Wd │◀─ w2
%                  └────┘          └────┘
%                     ▲               │   
%        ┌────┐       │  ┌ ─ ─ ┐      ▼     ┌─────┐
% w1 -──▶│ Wr │──▶(+)─┴─▶   K   ─┬-─▶(+)─-─▶│  G  │──┐ 
%        └────┘    ▲     └ ─ ─ ┘ │          └─────┘  │
%                  │             │  ┌────┐           │
%                  │             └─▶│ Wu │─▶ z2      │
%                  │  ┌────┐        └────┘           │   
%                  └──│ -1 │◀─────────────────────--─┘
%                     └────┘                            

Pw = blkdiag(Wz,eye(ny))*P*blkdiag(Ww,eye(nu));

%% 3.b Synthesis of output-feedback LPV controllers
% Now that we constructed the weighted generalized plant, let's synthesize
% various output-feedback LPV controllers using LPVcore and compare them!
% For comparison purposes, we design the same controllers as in T7, but for the
% gridded case.


% Let's start with a first LPV controller K1 that optimizes the closed-loop L2-gain from 
% w to z:
synOpt = lpvsynOptions('performance', 'l2');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K1, gammaK1] = lpvsyn(Pw, ny, nu, synOpt);
fprintf('\nPrinting results:\n\n');
fprintf('Obtained closed-loop L2-gain during synthesis: %.3g\n\n',gammaK1);


% Now, we can synthesize a second LPV controller K2 that optimizes the
% closed-loop l2 gain where only the controller dynamics are scheduled and
% the direct feedthrough is set to 0:
%            ┌───────────┐                
%            │ Ak(p)| Ck │                           
%    K2(p) = │------|----│                            
%            │ Bk   | 0  │                     
%            └───────────┘                
%
synOpt = lpvsynOptions('performance', 'l2', 'DirectFeedthrough',false,'Dependency',[1 1 0 0]);
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K2, gammaK2] = lpvsyn(Pw, ny, nu, synOpt);
fprintf('\nPrinting results:\n\n');
fprintf('Obtained closed-loop l2-gain during synthesis: %.3g\n\n',gammaK2);
% It is informative to inspect the performance weight enforcement after the
% controller synthesis:
CL = lft(Pw,K2); %Closed-loop with the weighted generalized plant and K2.
f2 = figure(2); clf(f2);
bodemag(inv(Wz)*CL,'k'); hold on; %#ok<MINV> 
bodemag(inv(Wz)*ones(2),'r'); grid on; %#ok<MINV> 
title('$W_z^{-1} (P_w \star K_2)$ vs. $W_z^{-1}$','Interpreter','latex');


%As a third LPV controller K3, let's use the generalized H2 norm as performance metric:

synOpt = lpvsynOptions('performance', 'h2');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K3, gammaK3] = lpvsyn(Pw, ny, nu, synOpt);
gammaVerify = lpvnorm(lft(Pw,K3),'h2','solverOptions', sdpsettings('solver','mosek','verbose',0));    
fprintf('Verified closed-loop H2-gain: %.3g\n\n',gammaVerify)

%% 3.c Synthesis of state-feedback LPV controllers

% As our fourth LPV controller K4, following T7, let's synthesize a 
% state-feedback controller that minimizes the closed-loop Linf-gain:

synOpt = lpvsynsfOptions(Backoff = 1.04, Performance = 'linf');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K4, gammaK4] = lpvsynsf(Pw, nu, synOpt);

% In the gridded scenario, we can verify gamma as follows:
nw = Pw.Nu-nu;
CL = LPVcore.lpvgridss(ss(Pw.A + pagemtimes(Pw.B(:,nw+1:end,:),K4), Pw.B(:,1:nw), ...
                   Pw.C + pagemtimes(Pw.D(:,nw+1:end,:),K4), Pw.D(:,1:nw)), linGrid);
gammaVerify = lpvnorm(CL,'linf'); 
fprintf('Verified closed-loop Linf-gain of state-feedback K4: %.3g\n\n',gammaVerify)

%% 4. Discrete-time LPV synthesis with identified system

% In the gridded LPV synthesis, in contrast with the current capabilities for
% global LPV synthesis, the LPV system matrices B and C are allowed to be
% scheduling dependent. Therefore, for this tutorial, we are going to show
% how to synthesize a DT controller using one of the identified LPV models
% from T3.

%Load one of the identified models in T3:

IdentifiedSys = importdata('data/LPVPEMUnbalanced_Disk.mat');

%Note that the IdentifiedSys.D --> 0. Therefore, we can set it to 0, as it
%is required for the synthesis:
IdentifiedSys.D = 0;

%Now, we can generate an array of LTI models as follows:
localGrid = struct(IdentifiedSys.SchedulingName{1}, linspace(-1,1,10));
UnbalancedDiskIdLocals = (extractLocal(IdentifiedSys,localGrid));

%Create LPVcore.lpvgridss object:
UnbalancedDiskId = LPVcore.lpvgridss(UnbalancedDiskIdLocals,localGrid);
UnbalancedDiskId.InputName = 'u';          % input voltage
UnbalancedDiskId.OutputName = 'theta';     % position

%Construct unweighted generalized plant as in 3.a:
S1 = sumblk('e = r - theta'); S2 = sumblk('u = d + yk');
inputChannels = {'r','d','yk'};    outputChannels = {'e','yk','e'};
PDT = connect(UnbalancedDiskId, S1, S2, inputChannels, outputChannels);

% Next, we design the discrete-time filters
Wr = db2mag(-10)*makeweight(db2mag(10),[1*2*pi, db2mag(-3)], 0, Ts);
Wd = db2mag(-30)*makeweight(db2mag(10),[0.5*2*pi, db2mag(-3)], 0, Ts);
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(40), [1*2*pi, db2mag(3)], db2mag(-2),Ts);
Wu = makeweight(db2mag(-20), [20*2*pi, db2mag(3)], db2mag(20),Ts);
Wz = blkdiag(We, Wu);

opts = bodeoptions; opts.FreqUnits = 'Hz'; opts.PhaseVisible = 'off'; opts.Title.FontSize = 10;
f3 = figure(3); clf(f3);
subplot(1,2,1); opts.Title.String = 'Input Weighting Filters (Ww)';  bodeplot(Ww,opts);
subplot(1,2,2); opts.Title.String = 'Output Weighting Filters (Wz^{-1})'; bodeplot(inv(Wz),opts);

% Construct the DT weighted generalized plant
PwDT = blkdiag(Wz,eye(ny))*PDT*blkdiag(Ww,eye(nu));

% Lastly, we can synthesize a DT LPV controller. Note that, often,
% the optimal solution of the synthesis can lead to numerical
% instabilities, making the synthesis infeasible. We can use a "backoff"
% term in these cases:
synOpt = lpvsynOptions(Backoff = 1.06, Performance = 'l2', DirectFeedthrough=false, Dependency=[1 0 0 0]);
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing discrete-time output-feedback LPV controller:\n\n')
[K5, gammaK5] = lpvsyn(PwDT, ny, nu, synOpt);

%% 5. Closed-loop simulation of the Unbalanced Disk with output-feedback
% Choose an *output-feedback* controller for the simulation:
K = K1;

% As mentioned in 2.a) of this tutorial, we need to take into account the
% input trimmings in the closed-loop simulation. This is not the case for
% the output trimmings. Note that the trimmings are not required for the
% identified system model.
switchTrimU = 1; % 1 (default): Activate input trimming.
switchTrimY = 0; % 0 (default): De-activate output trimming.

% We create the simulation instances for Unbalanced_disk.slx, following T7:
Tsim = 10; % simulation time 
Tdummy = 0:0.1:Tsim; % dummy time vector
N = length(Tdummy);
refval = [0, pi/4, 0, pi/2, 0, 3*pi/4, 0, pi, 0]; 
refx1 = kron(refval',ones(N,1)); refx1 = refx1(1:size(refval,2):end); % reference that we want to track
reference = timeseries(refx1, Tdummy);
disturbance = timeseries(0.1*randn(N,1),Tdummy); % some input disturbance

% Discretize the controller if it is in continuous time:
if isct(K)
    %Option 1: discretize LTI systems and create DT LPVGRIDSS object
    Ksim = LPVcore.lpvgridss(c2d(K.ModelArray,Ts,'tustin'),K.SamplingGrid);
    % Check the discretization:
    f4 = figure(4); clf(f4);
    bodemag(K,'k'); hold on; bodemag(Ksim,'r'); grid on;
    title('Controller discretization check')
else
    Ksim = K;
    % No trimmings required for the identified system
    switchTrimU = 0;
    switchTrimY = 0;
end

% Simulate the NL system with the DT controller
load_system('data/Unbalanced_disk_gridded_K_2022b.slx');
sim("data/Unbalanced_disk_gridded_K_2022b.slx")

% Plot simulation results
f5 = figure(5); clf(f5);
tiledlayout(2,1, 'TileSpacing', 'tight','Padding','tight');
nexttile; hold on; set(gca, 'FontSize', 12)
plot(simout.ref.Time, simout.ref.Data,'k', 'LineWidth',2, 'DisplayName','Reference')
plot(simout.theta.Time, simout.theta.Data, 'LineWidth',2, 'DisplayName','Angular position')
ylabel('$\theta(t)$ [rad]'); legend('reference','$\theta(t)$','Location','northwest');
nexttile; hold on; set(gca, 'FontSize', 12)
plot(simout.omega.Time, simout.omega.Data, 'LineWidth', 2, 'DisplayName','Angular speed')
xlabel('Time [s]'); ylabel('$\omega(t)$ [rad/s]');