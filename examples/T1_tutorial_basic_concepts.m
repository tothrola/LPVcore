%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%                Tutorial 1: Basic concepts and LPV models
%                   written by R. Toth and P. den Boef
%                         version (01-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this first part of the tutorial, you'll be able to 
%       (a) construct an LPV model of a unbalanced disc system.
%       (b) simulate the time-behavior of the model under excitation.
%
%   After completing the tutorial you may follow T2 how to do LPV model 
%   identification with LPVcore 
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       CT  Continuous Time
%       DT  Discrete Time    

clearvars; close all; clc;

%% 1.b Quick test to verify path
% To install LPVcore, it suffices to add the "src" directory to your MATLAB
% path. For some functionality, additional toolboxes are required. Consult
% README.md for more information.

try
    preal('p');
catch
    error('Please ensure the "src" directory is added to your MATLAB path.');
end

% Commands have a simple help, but detailed documentation also is available
% via:
 
doc preal

%% 1.c Define model parameters

% First let's define the dynamical equations of the system that we intend 
% to study in our tutorial. 

% [ d/dt omega ] = [-1/tau  0 ] [ omega ] + [Km/tau] u - [Mgl/J sin(theta)] 
% [ d/dt theta ]   [  0     0 ] [ theta ]   [   0  ]     [       0        ]
%  
%  whrere 
%    u     = input voltage
%    theta = angular position of the mass
%    omega = angular velocity of the mass
%    
%  and the parameters are

K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 

% Note that this is a simplified model of the system where the fast 
% electric dynamics are lumped into the back emf constant and the torque 
% constant. In this model the bottom angular position is taken as theta=0.
% Also note that the term mgl/J sin(theta) makes the system nonlinear.

% For LPV model conversion we will take advantage of that 
%
%         sinc(theta)*theta=sin(theta) 
%
% Hence, we can introduce a scheduling variable that is p=sinc(theta) to
% convert this nonlinear system representation to an LPV one.


%% 2.a Creating PV coefficients (continuous time)

% The original nonlinear model can be written in an IO representation form:
%
%    d^2/dt^2 theta + 1/tau d/dt theta + Mgl/J sin(theta) = Km/tau u
%
% This is what we would like write to a CT LPV-IO model form 
%
%    d^2/dt^2 theta + 1/tau d/dt theta + (Mgl/J * p) theta = Km/tau u
%
% with the scheduling choice p=sinc(theta). For this we need to learn how
% represent PV model coefficient and scheduling variables in LPVcore


% The general form of a CT LPV-IO model is
%
%   \sum_{i=0}^{n_a=2} a_i(p(t))* d^i/dt^i y(t) 
%                          = \sum_{j=0}^{n_b=0} b_j(p(t))* d^j/dt^j u(t)
%
% To define it we need the following ingredients:


p = preal('p','ct');        % Define scheduling variable with name p
                            % 'CT' means Continuous Time
                                                   
a0=(M*g*l/J)*p;        % coefficient with linear static dependence          
a1=1/tau;              % constant coefficient;
a2=1;                  % constant coefficient;

b0=Km/tau;             % constant coefficient;


A=[a0,a1,a2];
B=[b0];
 
% Let's discuss these ingredients in detail


%% 2.b The scheduling variable

% Creating a scheduling variable in continuous time CT and DT is done with 
% PREAL:

p = preal('p', 'ct');
disp(p);

q = preal('q', 'dt');
disp(q)

% Note that 'p' is a very simple instance of the PMATRIX class used to
% represent scheduling dependent coefficients or matrices. This class will 
% be discussed in more detail later in the tutorial. For now, focus on the 
% TIMEMAP property of p. A TIMEMAP is an object that translates a 
% scheduling variable to an "extended" scheduling rho to represent so 
% called dynamic dependence. In this case, the  "extended" scheduling is 
% the same as the original scheduling signal. However, some LPV 
% representations contain derivatives (CT) or time-shifted (DT) versions of
% the scheduling. To maintain a consistent representation of the LPV system
% regardless of operations that produce such dynamic dependence of the 
% coefficients, the scheduling is mapped to the extended scheduling 
% variable using the TIMEMAP object, i.e.:
%   
%  CT:
%
% [p(t), d/dt p(t), d^2/dt^2 p(t)] -> TIMEMAP ->[rho1(t), rho2(t), rho3(t)]
%
%  DT:
%
% [p(t), p(t-1), p(t-2)] ---> TIMEMAP ---> [rho1(t), rho2(t), rho3(t)]
%
% Differentiated or time-shifted versions of the scheduling can be obtained
% in several ways:
%
%  (i)   by calling PDIFF (CT) or PSHIFT (DT) on the scheduling

pd = pdiff(p, 1);
disp(pd)

qd = pshift(q, 1);
disp(qd)

%   (ii)  at construction time via preal

pdd = preal('p', 'ct', 'Dynamic', 2);
disp(pdd)

%   (iii) directly modifying the timemap (only for advanced users, not 
%         treated here).
 
% The range and unit of a scheduling variable can be specified
q = preal('q', 'dt', ...
    'Range', [-1, +1], ...
    'Unit', 'cm');
disp(q)

% You can always view the hidden properties/ingredients of the objects in
% LPV core by calling

properties(pd)


%% 2.c Parameter-varying matrices

% As we have seen, a scheduling variable is an instance of the PMATRIX 
% class. The PMATRIX class is a generalisation of a single scheduling 
% variable, i.e., 1*p(t), to parameter-varying scalar coefficients and even
% matrices of the following form:
%
%   A(p(t)) = A1 * phi_1(p(t)) + ... + An * phi_n(p(t))
%
% where A1, ..., An are regular matrices and phi_1, ..., phi_n are basis
% functions that map the (extended) scheduling rho to a scalar.
%
% The easiest way to create them is by combining a PREAL with a regular
% double. We have already created a scalar PMATRIX and a vector:

a0=(M*g*l/J)*p;
A=[a0,a1,a2];
disp(A);

% Besides the TIMEMAP, which we have seen in the previous section, PMATRIX
% has two other important properties: 'matrices' and 'bfuncs'. The former
% contains the constant matrices A1, ..., An in a 3D-array. The 'bfuncs'
% property stores the basis functions phi_1, ..., phi_n in a cell array.
% Currently, the following basis functions are supported:
%   1. PBCONST: phi(rho) = 1
%   2. PBAFFINE: phi(rho) = rho_i
%   3. PBPOLY: phi(rho) = rho_1^(d1) * ... * rho_m^(dm)
%   4. Custom made basis functions, e.g. trigonometrical, are also allowed
% The PMATRIX we created in the last line, has a single PBAFFINE basis
% function:

disp(A.bfuncs{1});

% Possible operations (scalar case):
% + - * with scheduling variables and division of pmatrix by constants 
sp=preal('s','ct');

a0+p+sp+1
a0-p-sp-1
a0*p
a0*p*sp
a0/4

% Possible operations (vectorial/matrix case):
% + - * with matrices and division of pmatrix by constants 

A'              % transposition
A.'
2 * A           % scalar and matrix multiplication
A*sp            
A' * A          % vectorial/matrix multiplication
A' * (A * sp)
A .* A          % element-wise multiplication
A + A           % addition and subtraction
A - 2 * A
kron(A, A)      % Kronecker product      
[A, A; A, A]    % horizontal and vertical concatenation
S=ans(1:2,1:2)  % element referencing
blkdiag(A, A) 
S^2             % exponentiation
S.^2 
[A;A;A] / rand(3,3)  % division
A ./ rand(1,3)  % elementwise division

% Note that performing arithmetics on a PMATRIX will automatically transform
% the basis functions in an appropriate way. For example, multiplying two
% PMATRIX objects composed of affine basis functions of the same scheduling
% variable results in a polynomial PMATRIX object (i.e, with PBPOLY basis 
% functions).

S = preal('p') * preal('p');
disp(S.bfuncs{1});

% The value of a PMATRIX at a certain frozen scheduling value can be
% extracted. In this operation, derivatives and time-shifted versions of p 
% are handled automatically.

A_f = freeze(A, 1)

% Sometimes, a PMATRIX contains many terms. For example, when the following
% PMATRIX is created for arbitrary degree d:
%
%   P = a_0 + a_1 p + a_2 * p^2 + ... + a_d * p^d
%
% In this case, the BasisType parameter can be specified as 'poly'
% (polynomial) and the exact coefficient values of the expression can be 
% given as an array. Example with coefficients a_0=1, a_1=2, a_3=3, etc.:

d = 5;
S = pmatrix(reshape(1:d, 1, 1, d), 'BasisType', 'poly');
fplot(@(p) freeze(S, p), [-1, 1]);

% Direct definition of parameter varying matrix functions is also possible
% To create the following polynomial parameter-varying matrix 
%   
%       S = [1 2] + [3 4] * p(t) + [5 6] * p(t)^2
%
disp(pmatrix( cat(3,[1 2], [3 4], [5 6]), ...
    'BasisType', 'poly', ...
    'BasisParametrization', {[], 1, 2}, ...
    'SchedulingName',{'p'}))
 
% To create
%       S = [1 2] + [3 4] * p(t) + [5 6] * p(t)*Speed(t)^2
%
% where m(t) = p(t) use
%
disp(pmatrix( cat(3,[1 2], [3 4],[5 6]), ...
    'BasisType', 'poly', ...
    'BasisParametrization', {[],[1 0],[1, 2]}, ...
    'SchedulingName',{'p','s'}))
 
% To create the following (custom) parameter-varying matrix 
%   
%       S = [1 2]* cos(p(t)) + [3 4] * sin(p(t)).^2 + [5 6]
%
% where m(t) = p(t) use
%
S=pmatrix( cat(3,[1 2], [3 4], [5 6]), ...
    'BasisType', 'custom', ...
    'BasisParametrization', ...
        {@(rho)cos(rho(:,1)), @(rho)sin(rho(:,1)).^2, []}, ...
    'SchedulingMap', 0, ...
    'SchedulingName', {'p'});
disp(S)

% For more information on creating PMATRIX objects, see
% examples/core/example_pmatrix.m.


%% 3.a Creating LPV-SS representations (continuous time)

% Using parameter-varying matrices, parameter-varying system
% representations can be constructed. The LPVCORE.LPVSS class implements 
% LPV system representations in a state-space form with basis-affine 
% scheduling-dependent state-space matrices.

% Define the PV matrices
pc = preal('p','ct', 'Range',[-1/(2*pi-pi/2), 1]) % Define CT scheduling 
A=[-1/tau, -(M*g*l/J)*pc; 1 0];
B=[Km/tau; 0];
C=[0 1];
D=0;

sys_ss=LPVcore.lpvss(A,B,C,D);
sys_ss.InputName = {'Voltage'};
sys_ss.OutputName = {'Position'};

% Let's see what we created:

disp(sys_ss);

% To analyzie dimensions:
size(sys_ss)

% To simulate the response of the created representation let's define input
% and scheduling signals:

Ts = 0.01;              % Sampling time
N = 5000;               % Number of generated samples
t = Ts*(0:1:(N-1));     % Time steps
u = sin((1:N)*Ts*2)';   % Input signal
p_mag=0.25;             
p=(1-p_mag)+p_mag*cos((1:N)*Ts)'; % scheduling signal 

lsim(sys_ss, p, u, t);  % Simulating the system response and displaying it

% You can also compute the local bode plots of the system for frozen values
% of p:

figure;
bode(sys_ss,[0.67:0.01:1]');

%% 3.b Creating LPV-SS representations (discrete time)

% To show more functionalities we will develop a model of the system in DT 
% using an Euler discretization.  

pd = preal('p','dt','Range', [-1/(2*pi-pi/2), 1]) % Define DT scheduling 
A=eye(2)+Ts*[-1/tau, -(M*g*l/J)*pd; 1 0]; % Manual Euler discretization
B=Ts*[Km/tau; 0];

sys_dss=LPVcore.lpvss(A,B,C,D,Ts);

lsim(sys_dss,p,u,t); 

% comparing the DT and CT models:

y_ss=lsim(sys_ss,p,u,t); 
y_dss=lsim(sys_dss,p,u,t); 
figure;
plot(t,y_ss);
hold on;
stairs(t,y_dss);
title('Comparision of CT and DT LPV model responses');
xlabel('Time (seconds)'); ylabel('Amplitude');
legend('CT-LPV-SS model','DT-LPV-SS model');

% We could have computed this DT model, by discretizing the CT model
% directly:

c2d(sys_ss,Ts,'exact-zoh-euler')

% Note that due to the developing native functionalities of Matlab, some 
% functions are required to be overshadowed by LPVcore. While this works 
% with functions, objects can be overshadowed. Hence referencing them 
% requires a preamble "LPVcore.", like

help LPVcore.lpvss

%% 3.c  Creating LPV-IO representations (discrete time)

% System representations are also available in an input-output (filter) 
% form with basis-affine scheduling-dependent input-output coefficients.
% LPV-IO models are considered in filter form and are parametrized by two
% cell arrays, A and B, of PMATRIX objects. Each element of the cell array
% corresponds to a different order of the backward time-shift operator, 
% i.e.:
%
%   A_{i+1}(p) q^{-i}
%
% where q^{-i}y(k) = y(k-i). A filters the output, B filters the input. 
% A must be monic (A{1} = 1).

Ts = 0.01;              % Sampling time
pd  = preal('p','dt','Range', [-1/(2*pi-pi/2), 1]);  
                           % Define scheduling variable with name p
                           % 'dt' means discrete time     
                           % 'Range' defines range (min-max of the sinc)
                                 
pd2= pshift(pd,-2);         % Due to non-commutativity of multiplication 
                           % with the time operator, DT-IO representation
                           % of the NL system requires dynamic dependency
                            
                       
a0=1;                      % Coefficients of the output side         
a1=Ts/tau-2;               
a2=1-Ts/tau+(M*g*l)/J*Ts^2*pd2;  % Coefficient with dynamic dependence;
 
b2=Km/tau*Ts^2;             % Coefficient of the input side

% Let's create our DT LPV-IO representation
sys_dio = LPVcore.lpvio({1, a1 a2}, {0 0 b2}, Ts);  
sys_dio.InputName = {'Voltage'};
sys_dio.OutputName = {'Position'};

% comparing the DT and CT models:
y_dio=lsim(sys_dio,p,u,t); 
figure;
plot(t,y_ss);
hold on;
stairs(t,y_dio);
title('Comparision of CT and DT LPV model responses');
xlabel('Time (seconds)'); ylabel('Amplitude');
legend('CT-LPV-SS model','DT-LPV-IO model')

%% 3.d  Creating LPV-IO representations (continuous time)

% As we need a monic representation for IO models, we need to divide by 
% "a0" the coefficients of the direct IO form (see 2.a). Unfortunately,
% handling division by scheduling variables would increase tremendously the
% complexity of the software implementation, hence this is not supported by
% LPVcore directly. However, we can still define a CT-LPV-IO representation
% in the following way:

pci  = preal('pci','ct');  
a0_inv=pci/(M*g*l/J);    

a0=1;
a1=1/tau*a0_inv;       % PV coefficient;
a2=1*a0_inv;           % PV coefficient;

b0=Km/tau*a0_inv;      % PV coefficient;

sys_io = LPVcore.lpvio({a0, a1 a2}, b0);  

% comparing the CT model responses
y_io=lsim(sys_io,1./p,u,t); 
figure;
plot(t,y_ss);
hold on;
stairs(t,y_io);
title('Comparision of CT LPV model responses');
xlabel('Time (seconds)'); ylabel('Amplitude');
legend('CT-LPV-SS model','CT-LPV-IO model')

