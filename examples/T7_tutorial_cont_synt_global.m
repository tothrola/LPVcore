%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%                    Tutorial 7: global LPV control design
%        written by E. Javi Olucha Delgado, R. Toth, and P. den Boef
%                         version (05-06-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this seventh part of the tutorial, you'll be able to
%       (a) Embed a nonlinear system into an LPV model
%       (b) Synthesize state-feedback and output-feedback global LPV controllers
%       (c) Execute time-domain simulations with the nonlinear system and
%       the synthesized controllers in Simulink
%
%   After completing the tutorial you may follow T8 on local grid-based LPV
%   control design with LPVcore.
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time
%       CT Continuous Time

clearvars; close all; clc;
rng(42);   % set random number generator seed so that results are 
           % reproducible

          
%% 1.b Define unbalanced disc nonlinear model
% We will keep investigaitng our unblanced disc system from T1.
% The nonlinear dynamic model of the unbalanced disc are:
%   x1dot = x2
%   x2dot = -mgl/J*sin(x1) - 1/tau x2 + Km/tau u
%   y = x1
% Where theta = x1 is the angular position and omega = x2 the angular speed

% Model parameters
K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.005;      % Sampling time for discretization

%% 2.a Manual global LPV embedding

% For (global) LPV model conversion we will take advantage of that 
%
%         sinc(theta)*theta=sin(theta) 
%
% Hence, we can introduce a scheduling variable that is p=sinc(theta) to
% convert this nonlinear system representation to an LPV one:
p = preal('sinc(x1)','ct','Range',[-0.22, 1]);
etaManual = @(theta) sin(theta)./theta;
% We can now construct the state space matrices that define the LPV-SS rep:
A = [0, 1; -M*g*l*p/J, -1/tau];
B = [0; Km/tau];
C = [1, 0];
D = 0;
UnbalancedDiskManual = LPVcore.lpvss(A,B,C,D);
UnbalancedDiskManual.InputName = 'u';          % input voltage
UnbalancedDiskManual.OutputName = 'theta';     % position

%% 2.b Automatic global LPV embedding

%LPVcore is able to construct nonlinear state-space models in either
%continuous or discrete time. In addition, LPVcore provides algorithms to
%automatically embed nonlinear models in LPV models.

%We first construct the nonlinear state-space model as:
f = @(x,u) [x(2); (-M*g*l/J*sin(x(1)) -1/tau*x(2) + Km/tau*u)];
h = @(x,u) x(1); %Output
nx = 2; ny = 1; nu = 1;
nlsys = LPVcore.nlss(f, h, nx, nu, ny);

%Next, we execute the automatic global LPV embedding:
[UnbalancedDiskAuto,etaAuto] = LPVcore.nlss2lpvss(nlsys,"analytical");
UnbalancedDiskAuto.InputName = 'u';          % input voltage
UnbalancedDiskAuto.OutputName = 'theta';     % position
%And we can set the bounds on the scheduling parameter:
UnbalancedDiskAuto.SchedulingTimeMap.Range = [-0.22, 1];

%Note that LPVcore.nlss2lpvss provides the scheduling map as well 
%p = eta(x,u), denoted as etaAuto in this tutorial. For more info on
%the automatic global LPV embedding, see LPVcore/examples/nonlinear/example_nlss.m

%We can now compare the manual and automatic LPV models at random angular
%positions
angleRandom = 10*rand(1,4);
f1 = figure(1); clf(f1);
bode(UnbalancedDiskManual, 'k', etaManual(angleRandom)'); hold on
bode(UnbalancedDiskAuto, 'r--', etaAuto.map(angleRandom)');

%% 3.a Construction of the Weighted Generalized Plant

% In this example, we use a mixed-sensitivity approach to design the
% controller.
% We first construct the unweighted generalized plant (P)
%                          d
%           ▲              │   
%         e │  ┌ ─ ─ ┐ yk  ▼  u  ┌─────┐
% r ──▶(+)-─┴─▶   K   ─┬─▶(+)--─▶│  G  │──┐ theta
%       ▲      └ ─ ─ ┘ │         └─────┘  │
%       │              ▼                  │
%       │                                 │
%       │  ┌────┐                         │ 
%       └──│ -1 │◀────────────────-───────┘
%          └────┘                          
%
%   G: Global LPV model (unbalanced disk)
%   K: To-be-synthesized LPV controller
%
%   r: reference
%   e: tracking error
%   u: control input
%   d: disturbance
%   u: input voltage of unbalanced disc
%   theta: anglular position of unbalanced disc
S1 = sumblk('e = r - theta'); S2 = sumblk('u = d + yk');
inputChannels = {'r','d','yk'};    outputChannels = {'e','yk','e'};
P = connect(UnbalancedDiskAuto, S1, S2, inputChannels, outputChannels);
ny = 1;     % number of measured outputs ('e' with size 1 goes into controller)
nu = 1;     % number of controlled inputs ('u' with size 1 comes out of controller)

% Next, we design the input and output weighting filters
Wr = db2mag(-10)*makeweight(db2mag(10),[2*2*pi, db2mag(-3)], 0);
Wd = db2mag(-30)*makeweight(db2mag(10),[0.5*2*pi, db2mag(-3)], 0);
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(40),[10*2*pi, db2mag(3)], db2mag(-6));
Wu = makeweight(db2mag(-30), [20*2*pi, db2mag(3)], db2mag(30));
Wz = blkdiag(We, Wu);

opts = bodeoptions; opts.FreqUnits = 'Hz'; opts.PhaseVisible = 'off'; opts.Title.FontSize = 10; opts.Grid = 'on';
f2 = figure(2); clf(f2);
subplot(1,2,1); opts.Title.String = 'Input Weighting Filters (Ww)';  bodeplot(Ww,opts);
subplot(1,2,2); opts.Title.String = 'Output Weighting Filters (Wz^{-1})'; bodeplot(inv(Wz),opts);

% And we construct the Weighted Generalized Plant (Pw)
%                  ┌────┐          ┌────┐
%                  │ We |─▶ z1     │ Wd │◀─ w2
%                  └────┘          └────┘
%                     ▲               │   
%        ┌────┐       │  ┌ ─ ─ ┐      ▼     ┌─────┐
% w1 -──▶│ Wr │──▶(+)─┴─▶   K   ─┬-─▶(+)─-─▶│  G  │──┐ 
%        └────┘    ▲     └ ─ ─ ┘ │          └─────┘  │
%                  │             │  ┌────┐           │
%                  │             └─▶│ Wu │─▶ z2      │
%                  │  ┌────┐        └────┘           │   
%                  └──│ -1 │◀─────────────────────--─┘
%                     └────┘                            
Pw = blkdiag(Wz,eye(ny))*P*blkdiag(Ww,eye(nu));

%% 3.b Synthesis of output-feedback LPV controllers
% Now that we constructed the weighted generalized plant, let's synthesize
% several output-feedback LPV controllers using LPVcore and compare them!

% Let's start with a first LPV controller K1 that optimizes the closed-loop L2-gain from 
% w to z, i.e.
% min γ  s.t. ‖ z ‖_2 <= γ ‖ w ‖_2.
synOpt = lpvsynOptions('performance', 'l2');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K1, gammaK1] = lpvsyn(Pw, ny, nu, synOpt);
fprintf('\nPrinting results:\n\n');
fprintf('Obtained closed-loop L2-gain during synthesis: %.3g\n\n',gammaK1);



% Now, we can synthesize a second LPV controller K2 that optimizes the
% closed-loop l2 gain where only the controller dynamics are scheduled and
% the direct feedthrough is set to 0:
%            ┌───────────┐                
%            │ Ak(p)| Ck │                           
%    K2(p) = │------|----│                            
%            │ Bk   | 0  │                     
%            └───────────┘                
%
synOpt = lpvsynOptions('performance', 'l2', 'DirectFeedthrough',false,'Dependency',[1 1 0 0]);
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K2, gammaK2] = lpvsyn(Pw, ny, nu, synOpt);
fprintf('\nPrinting results:\n\n');
fprintf('Obtained closed-loop l2-gain during synthesis: %.3g\n\n',gammaK2);
% It is informative to inspect the performance weight enforcement after the
% controller synthesis:
CL = lft(Pw,K2); %Closed-loop with the weighted generalized plant and K2.
f3 = figure(3); clf(f3);
bodemag(inv(Wz)*CL,'k',etaAuto.map(angleRandom)'); hold on; %#ok<MINV> 
bodemag(inv(Wz)*ones(2),'r'); %#ok<MINV> 
title('$W_z^{-1} (P_w \star K_2)$ vs. $W_z^{-1}$','Interpreter','latex');



% Other performance metrics, such as the induced L∞-gain, the generalized
% H2 norm or passivity are available as well. As a third LPV controller K3,
% let's use the generalized H2 norm as performance metric:

synOpt = lpvsynOptions('performance', 'h2');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K3, gammaK3] = lpvsyn(Pw, ny, nu, synOpt);
gammaVerify = lpvnorm(lft(Pw,K3),'h2','solverOptions', sdpsettings('solver','mosek','verbose',0));    
fprintf('Verified closed-loop H2-gain: %.3g\n\n',gammaVerify)

%% 3.c Synthesis of state-feedback LPV controllers

% As a next step, we are going to synthesize some state-feedback LPV
% controllers using the weighted generalized plant constructed in 3.a.

% As our fourth LPV controller K4, let's synthesize a state-feedback
% controller that minimizes the closed-loop Linf-gain. Now, we don't have to
% provide the number of output channels in the syntax, as the synthesis
% will use all the states of the model:
synOpt = lpvsynsfOptions(Backoff = 1.04, Performance = 'linf');
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing controller:\n\n')
[K4, gammaK4] = lpvsynsf(Pw, nu, synOpt);

% Verify
nw = Pw.Nu-nu;
CL = LPVcore.lpvss(Pw.A+Pw.B(:,nw+1:end)*K4, Pw.B(:,1:nw), ...
                   Pw.C+Pw.D(:,nw+1:end)*K4, Pw.D(:,1:nw));
gammaVerify = lpvnorm(CL,'linf'); 
fprintf('Verified closed-loop Linf-gain: %.3g\n\n',gammaVerify)

%% 4. Discrete-time LPV synthesis

% LPVcore is able to discretize LPV models and synthesize LPV controllers
% in discrete-time as well.
% Let's start by discretizing the Unbalanced Disk LPV system.
% Note that, we could also use discrete models identified with LPVcore, as
% long as the identified structure does not contain parameter-dependent B
% and C matrices (this is a requirement for the current methods of GLOBAL
% LPV synthesis).

UnbalancedDiskDT = c2d(UnbalancedDiskAuto,Ts,'exact-zoh-euler');
UnbalancedDiskDT.InputName = 'u';          % input voltage
UnbalancedDiskDT.OutputName = 'theta';     % position
UnbalancedDiskDT.SchedulingTimeMap.Range = [-0.22, 1];
% Check the system discretization:
f4 = figure(4); clf(f4);
bodemag(UnbalancedDiskAuto, 'k',[-0.22 0 1]'); hold on;
bodemag(UnbalancedDiskDT,'r-.',[-0.22 0 1]');
title('System discretization check')

% Now, we construct the generalized plant with as in 3.a, but with the discretized Plant:
S1 = sumblk('e = r - theta'); S2 = sumblk('u = d + yk');
inputChannels = {'r','d','yk'};    outputChannels = {'e','yk','e'};
PDT = connect(UnbalancedDiskDT, S1, S2, inputChannels, outputChannels);

% Next, we design the discrete-time filters
Wr = db2mag(-10)*makeweight(db2mag(10),[1*2*pi, db2mag(-3)], 0, Ts);
Wd = db2mag(-30)*makeweight(db2mag(10),[0.5*2*pi, db2mag(-3)], 0, Ts);
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(40), [10*2*pi, db2mag(3)], db2mag(-2),Ts);
Wu = makeweight(db2mag(-10), [45*2*pi, db2mag(3)], db2mag(30),Ts);
Wz = blkdiag(We, Wu);

opts = bodeoptions; opts.FreqUnits = 'Hz'; opts.PhaseVisible = 'off'; opts.Title.FontSize = 10;
f5 = figure(5); clf(f5);
subplot(1,2,1); opts.Title.String = 'Input Weighting Filters (Ww)';  bodeplot(Ww,opts);
subplot(1,2,2); opts.Title.String = 'Output Weighting Filters (Wz^{-1})'; bodeplot(inv(Wz),opts);

% And construct the discrete-time weighted generalized plant
PwDT = blkdiag(Wz,eye(ny))*PDT*blkdiag(Ww,eye(nu));

% Now, we can synthesize a discrete-time LPV controller. Note that, often,
% the optimal solution of the synthesis can lead to numerical
% instabilities, making the synthesis infeasible. We can use a "backoff"
% term in these cases:
synOpt = lpvsynOptions(Backoff = 1.06, Performance = 'l2', DirectFeedthrough=false, Dependency=[1 0 0 0]);
synOpt.SolverOptions.solver = 'mosek';
fprintf('Synthesizing discrete-time output-feedback LPV controller:\n\n')
[K5, gammaK5] = lpvsyn(PwDT, ny, nu, synOpt);

%% 5. Closed-loop simulation of the Unbalanced Disk with an output-feedback
% LPV controller

% In addition, LPVcore contains Simulink blocks that allow the
% implementation of LPV models. With them, it is possible to run time-domain
% simulations. As an example for this tutorial, we have implemented the
% Unbalanced Disk model, and we are going to show how to run a closed-loop
% simulation.

% Choose a controller for the simulation:
K = K5;

% Discretize the controller if it is in continuous time:
if isct(K)
    Ksim = c2d(K,Ts,'exact-zoh-tustin');
    % Check the discretization:
    discretization_check = struct('p', 100*rand(1,5));
    f5 = figure(5); clf(f5);
    bodeplot(extractLocal(K,discretization_check)); hold on;
    bodeplot(extractLocal(Ksim,discretization_check));
    title('Controller discretization check')
else
    Ksim = K;
end
% We create the simulation instances for Unbalanced_disk.slx
Tsim = 10; % simulation time
Tdummy = 0:0.1:Tsim; % dummy time vector
N = length(Tdummy);
refval = [0, pi/4, 0, pi/2, 0, 3*pi/4, 0, pi, 0]; 
refx1 = kron(refval',ones(N,1)); refx1 = refx1(1:size(refval,2):end); % reference that we want to track
reference = timeseries(refx1, Tdummy);
disturbance = timeseries(0.1*randn(N,1),Tdummy); % some input disturbance

load_system('data/Unbalanced_disk_2022b.slx');
sim("data/Unbalanced_disk_2022b.slx")

% Plot simulation results
f6 = figure(6); clf(f6);
tiledlayout(2,1, 'TileSpacing', 'tight','Padding','tight');
nexttile; hold on; set(gca, 'FontSize', 12)
plot(simout.ref.Time, simout.ref.Data,'k', 'LineWidth',2, 'DisplayName','Reference')
plot(simout.theta.Time, simout.theta.Data, 'LineWidth',2, 'DisplayName','Angular position')
ylabel('$\theta(t)$ [rad]'); legend('reference','$\theta(t)$','Location','northwest');
nexttile; hold on; set(gca, 'FontSize', 12)
plot(simout.omega.Time, simout.omega.Data, 'LineWidth', 2, 'DisplayName','Angular speed')
xlabel('Time [s]'); ylabel('$\omega(t)$ [rad/s]');