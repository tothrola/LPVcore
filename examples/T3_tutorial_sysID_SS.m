%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%        Tutorial 3: Data generation and SySID with LPV-SS models
%                   written by R. Toth and P. den Boef
%                         version (01-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this third part of the tutorial, you'll be able to 
%       (a) set up data generation and create model structures for sysID
%       (b) estimate LPV-SS models using subspace identification and
%           prediction error minimization
%
%   After completing the tutorial you may follow T4 on some advanced 
%         identification concepts with LPVcore 
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time    
%       SID Subspace Identification 
%       PEM Prediction Error Minimization

clearvars; close all; clc;
rng(42);   % set random number generator seed so that results are 
           % reproducible

flag=[];
flag.simplified=1; % Simplified example due to some numerical instability of  
                  % subspace identification

%% 1.b Define unbalanced disc model parameters

% We will keep investigating our unbalanced disc system from T1 first to 
% show how system identification of LPV-SS models can be executed in LPVcore

K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.01;                 % Sampling time

%% 2.a IDSS objects

% LPVcore has algorithms for identifying LPV models from data.
% Broadly speaking, these algorithms can be divided into two classes:
%   1. Local methods: based on local linearizations of the underlying
%   system, i.e. the scheduling signal is kept constant during experiments.
%   2. Global methods: the scheduling signal is varied during the
%   experiments.
% In this tutorial, global methods for the identification of LPV-SS
% models are discussed. For this purpose, we will introduce LPV IDSS 
% objects. This model type is similar to IDSS models in the System
% Identification toolbox, but with scheduling-dependent matrices. 
% For an example of a local method to identify an LPV-SS model, see 
% part T5 of this tutorial series. 


pd = preal('p','dt');       % Define DT scheduling variable

% Define the matrices associated with a LPV DT representation of the
% unbalanced disc dynamics obtained via Euler's discretization 
A=eye(2)+Ts*[-1/tau, -(M*g*l/J)*pd; 1 0];
B=Ts*[Km/tau; 0];
C=[0 1];
D=0;

% Next, we can create an IDSS object to define the DT LPV-SS form of the 
% unbalanced disc system as a data generating object. These objects are very
% similar to LPV-SS representations, except that they are also capable to
% represent noise models and also describe estimated model specific
% information, just like in the LTI case. In the LPV case, LPV-SS models 
% are often considered with an innovation type of noise structure:
%
%   x(k+1) = A(p(k))x(k) + B(p(k))u(k) + K(p(k))e(k)
%   y(k)   = A(p(k))x(k) + B(p(k))u(k) + e(k)
%
% where e(k) is a vectorial white noise process with dimension equal to the
% dimension of y. Alternatively, it is possible to define and estimate
% LPV-SS models with general noise structure in the form of:
%
%   x(k+1) = A(p(k))x(k) + B(p(k))u(k) + G(p(k))v(k),
%   y(k)   = C(p(k))x(k) + D(p(k))u(k) + H(p(k))e(k),
%
% where the white noise processes e and v are normally distributed, i.e.,
%
%  e(k) ~ N(0,NoiseVariance)   or    [v(k);e(k)]  ~ N(0,NoiseVariance).


% Let's define a SS representation of the noise model we considered in T2
% to make comparison with the PEM-IO methods possible. We had
%  
%   v(k) + d1(k)v(k-1) = e(k)+ c1(k)e(k-1)
%
% as the noise model with coefficients
%
% c1=0.75*pshift(pd,-1);
% d1=-0.25+0.25*pshift(pd,-1);
%
% which has a state-space realization
%
%   v(k) = x(k) + e(k)
% x(k+1) = -pshift(d1,1)(k) x(k) + pshift(c1-d1,1)(k) e(k)

c1=0.75*pshift(pd,-1);
d1=-0.25+0.25*pshift(pd,-1);

Ae= -pshift(d1,1);
Be= pshift(c1-d1,1);
Ce= 1;
De= 1;

% Combined representation
Ac=mdiag(A,Ae);
Bc=[B;0];
Kc=[zeros(2,1); Be];
Cc=[C,Ce];
Dc=D;
NoiseVariance = 4.5*10^-4;     % Specify noise variance


%%%%%%% Simplified form %%%%%%%%%
if flag.simplified
    rng(1);
    Ac=A;
    Bc=B;
    Kc=[0.108; 0.108];
    Cc=C;
    Dc=D;
    NoiseVariance = 0.001^2;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Create an idss object with innovation noise model 
sys_ss=LPVcore.lpvidss(Ac,Bc,Cc,Dc,'innovation',Kc, [], NoiseVariance,Ts); 
sys_ss.InputName = {'Voltage'};      % Naming of the signals
sys_ss.OutputName = {'Position'};

%% 2.b Create data sets

% Next we generate synthetic data using sys to mimic measurement conducted 
% on the real system.

N=2000;         % Number of samples
p_mag=0.25;     % Scheduling variation
 
% Estimation data
u=idinput(N,'rgs');
p=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]);
% Note that for the sake of simplicity, 'p' is generated independently from
% the scheduling map and the associated angular position. This is not a
% limitation of the toolbox but used to simplify the example.
 
[y,~,~,yp] = lsim(sys_ss,p,u);    % Generate data including noise signal
                                  % y - noisy output
                                  % yp - noise free output 
                                  
fprintf('The SNR is %f dB.\n', LPVcore.snr(yp,y-yp));  % Signal-to-noise ratio                            
                                  
 
data_est = lpviddata(y,p,u,Ts);      % Create an LPV data object
plot(data_est);                      % Show data
 
% Create a noisy and a noise free validation data set
uv=idinput(N,'rgs');
pv=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]);
[yv,~,~,yvp] = lsim(sys_ss,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise free data set 

%% 3.a LPV subspace identificaiton

% The model structure for the identification methods is defined through a
% template system. The exact value of the parameters in the template is not
% important only the corresponding structure of the dependence that  
% is defined by them. This is used by the algorithms to estimate the 
% coefficients (i.e., the parameters).
 
% Next, we will define an LPVIDSS template with more general dependence 
% than our data-generating system. 

n=2;           % selected model order

% We define a state-space template with the selected model order and affine 
% dependency of the matrix functions on the scheduling. Currently, subspace
% identification is restricted to affine or basis affine dependency of the 
% the to-be-identified model. 

template_ss=LPVcore.lpvidss(ones(n,n)+ones(n,n)*pd,ones(n,1)+ones(n,1)*pd,ones(1,n),ones(1,1),'innovation',ones(n,1)+ones(n,1)*pd, [], [],Ts);

%%%%%%% Simplfied form %%%%%%%%%
if flag.simplified
    template_ss=LPVcore.lpvidss(ones(n,n)+ones(n,n)*pd,ones(n,1)+ones(n,1)*pd,ones(1,n),ones(1,1),'innovation',ones(n,1), [], [],Ts);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Subspace identification has many associated options that one can set with 
% LPVSIDOPTIONS. One important aspect is the past window size. Taking the
% past window to alrge values ensures unbiasedness of the estimate, but
% seriously increases the computational load. A default value of 6 is used if
% this is not specified. In case of out of memory problems, the past window
% size is required to be lowered. To decrase the computational load the
% scripts also implement subspace identification via kernelization, which
% is the default option.

p_window=3;    % past window

options = lpvsidOptions;
options.PastWindowSize=p_window;
options.Display='on';
options.Kernelization=true;
options.KernelizationRegularizationConstant=1e-5;

m_ss_sid = lpvsid(data_est, template_ss, options);
 
% Plotting validation results 
[ys_ss_sid,fit_ss_sid]=compare(data_val_2,m_ss_sid);  % Simulation
 
f1=figure;
plot([yvp, ys_ss_sid]);
title('Simulated model response');
xlabel('Time sample [-]');
ylabel('Amplitude [-]');
leg1=[]; leg1{1}='Measured output (noise free)'; leg1{2}=sprintf('SS-SID (%.2f %%)',fit_ss_sid);
legend(leg1);
 
[ys_ss_sid,fit_ss_sid]=compare(data_val_1,m_ss_sid,1);  % Prediction
 
f2=figure;
plot([yv, ys_ss_sid]);
title('Predicted model response');
xlabel('Time sample [-]');
ylabel('Amplitude [-]');
leg2=[]; leg2{1}='Measured output'; leg2{2}=sprintf('SS-SID (%.2f %%)',fit_ss_sid);
legend(leg2);

%% 3.b LPV-PEM SS identification
 
% As a next step, let's use LPV-SS PEM. Note that this method uses a
% nonlinear optimisation to minimise the prediction error of LPV-SS model
% estimate. While it is capable to achieve consistent estimation of LPV-SS
% models compared to LPV subspace methods, it requires efficient
% initialisation. For this purpose various initialisation methods are 
% implemented using PEM or SID based initialization. The default option is 
% to use PEM-OE based initialisation as currently that provides the most 
% nummerically reliable pre-estimate.
 
% First set the template structure to be similar as what we used for
% subspace:
template_ss=m_ss_sid; 
 
% Use default gradient search with DDLC, without specifying automatic
% initialization the estimation will be initialized by the parameters of the
% template, i.e. in this case with the subspace estimate.

options = lpvssestOptions('Display','on');
m_ss_pem= lpvssest(data_est, template_ss, options);
 
% Plotting validation results 
[ys_ss_pem,fit_ss_pem]=compare(data_val_2,m_ss_pem);  % Simulation
 
figure(f1);
hold on;
plot(ys_ss_pem);
leg1{3}=sprintf('SS-PEM (%.2f %%)',fit_ss_pem);
legend(leg1);
 
[ys_ss_pem,fit_ss_pem]=compare(data_val_1,m_ss_pem,1);  % Prediction
 
figure(f2);
hold on;
plot(ys_ss_pem);
leg2{3}=sprintf('SS-PEM (%.2f %%)',fit_ss_pem);
legend(leg2);

%save('data/LPVPEMUnbalanced_Disk',"m_ss_pem");

