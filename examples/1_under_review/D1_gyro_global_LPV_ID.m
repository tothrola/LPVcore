%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Demo
%           Demo 1: Global LPV identification of a 3DOF gyroscope
%                   written by R. Toth and P. den Boef
%                         version (10-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Identification without Subspace

%% Data Loading

% Estiamtion data set
clearvars; close all; clc;

% Load noisy simualtion data obtained from the gyroscope simulator under 
%
%     u_k = 0.5 sin(omega Ts k) + w_k     (exciation signal, gimbal 2)
%
% with omega a randmom frequency with uniform distribution U(1,2), Ts=0.01
% and w_k a white noise process with normal distribution N(0,1/3). During
% experiment, gimbal 3 was locked, while the velocity of the flywheel 
% (consdiered gimbal 1) was controlled by a low bandwidth controller with 
% random multi-level sequence, with an amplitude in [30, 50] rad/sec and 
% dwell time of [400, 800] samples. Measurments of the velocity of the 
% outer frame (gimbal 4) were obtained
% 
%     y_k = v4_k + e_k
% 
% with white measurment noise e_k with normal distribution N(0,2.2E-5) 
% corresponding to 35dB SNR.


load('./data/gyro_est_dat_1_noise.mat');
data_LTI_est=iddata(exp_y,exp_u,Ts); % data for LTI identification
p=[cos(out.q.Data(:,2)),out.qd.Data(:,1),sin(out.q.Data(:,2))]; %assembling
                                     % the scheduling variable
data_LPV_est=lpviddata(exp_y,p,exp_u,Ts); % data for LPV identification


% Validation data sets corresponding to independnet measurment with
% randomly generated input and reference signals

load('./data/gyro_val_dat_10.mat');
data_LTI_val=iddata(exp_y,exp_u,Ts);
data_LTI2_val=iddata(exp_y,[exp_u,exp_p(:,1)],Ts);
p=[cos(out.q.Data(:,2)),out.qd.Data(:,1),sin(out.q.Data(:,2))];
data_LPV_val=lpviddata(exp_y,p,exp_u,Ts);

load('./data/gyro_val_dat_11.mat');
data_LTI_val_2=iddata(exp_y,exp_u,Ts);
data_LTI2_val_2=iddata(exp_y,[exp_u,exp_p(:,1)],Ts);
p=[cos(out.q.Data(:,2)),out.qd.Data(:,1),sin(out.q.Data(:,2))];
data_LPV_val_2=lpviddata(exp_y,p,exp_u,Ts);

load('./data/gyro_val_dat_12.mat');
data_LTI_val_3=iddata(exp_y,exp_u,Ts);
data_LTI2_val_3=iddata(exp_y,[exp_u,exp_p(:,1)],Ts);
p=[cos(out.q.Data(:,2)),out.qd.Data(:,1),sin(out.q.Data(:,2))];
data_LPV_val_3=lpviddata(exp_y,p,exp_u,Ts);

N=length(exp_y);
t = (0:Ts:(Ts*(N-1)))';

%% LTI identification 

% The corresponding model orders have been optimized via residual analysis 

% PEM-IO based estimates
m_oe=oe(data_LTI_est,[2, 2, 0]);
m_arx=arx(data_LTI_est,[4,4,0]);
m_oe_ss=ss(m_oe);      % state-space form of the OE estiamte
m_arx_ss=ss(m_arx);    % state-space form of the ARX estiamte

% PEM-SS estiamtes with subspace based initialisation 
m_ss_ct=ssest(data_LTI_est,4);  % continous-time model estiamte
m_ss_dt=ssest(data_LTI_est,4,'TS',Ts); % discrete-time model estiamte

% Comparision of the models on the validation data sets
figure(1);
compare(data_LTI_val,m_oe,m_ss_ct,m_ss_dt,m_arx,Inf)
figure(2);
compare(data_LTI_val_2,m_oe,m_ss_ct,m_ss_dt,m_arx,Inf)
figure(3);
compare(data_LTI_val_3,m_oe,m_ss_ct,m_ss_dt,m_arx,Inf)

% Residual analysis
figure(4);
resid(data_LTI_val,m_oe,m_ss_ct,m_ss_dt,m_arx)
legend('oe','ss ct','ss dt','arx');

% Frequency response plots of the estiamted models
figure(5);
bode(m_oe,m_ss_ct,m_ss_dt,m_arx);
legend('oe','ss ct','ss dt','arx');
grid on;


%% Black box LPV-IO  identification 

% Scheduling variables
s2 = preal('sin(q2)', 'dt');
c2 = preal('cos(q2)', 'dt');
q1d = preal('q1d', 'dt');

% Generating shifted affine modeling strucutre 
A = randn(1) +  randn(1)*q1d + randn(1)*s2 + randn(1)*c2;
B = randn(1)+  randn(1)*q1d + randn(1)*s2 + randn(1)*c2;
na=5;  
nb=5;
A_poly=eye(1);
for k=1:na
    if k==1 
        A_poly={A_poly, pshift(A,-k)};
    else
        A_poly={A_poly{:,1:k}, pshift(A,-k)};
    end
end
B_poly=rand(1);
for k=1:nb
    if k==1 
        B_poly={B_poly, pshift(B,-k)};
    else
        B_poly={B_poly{:,1:k}, pshift(B,-k)};
    end
end

%% LPV-ARX estimation

% Define template
template_arx = LPVcore.lpvidpoly(A_poly, B_poly, [], [], [], 0, Ts);
template_arx.InputName = {'Current Gimbal 2'};
template_arx.InputUnit = {'A'};
template_arx.OutputName = {'q4d'};
template_arx.OutputUnit = {'rad/s'};
disp(template_arx);

% Identification
options = lpvarxOptions('Display', 'on');
m_LPV_arx = lpvarx(data_LPV_est, template_arx, options);
m_LPV_arx_ss=lpvio2ss(m_LPV_arx.A,m_LPV_arx.B,na,nb,Ts);

%% LPV-OE estimation 
template_oe = LPVcore.lpvidpoly([], m_LPV_arx.B, [], [], m_LPV_arx.A, 0, Ts);
template_oe.InputName = {'Current Gimbal 2'};
template_oe.InputUnit = {'A'};
template_oe.OutputName = {'q4d'};
template_oe.OutputUnit = {'rad/s'};

options_oe=lpvoeOptions('Display', 'on', ...
    'SearchOptions', ...
        struct('StepSize', 10, 'StepTolerance', 1E-10), ...
    'Regularization', ...
        struct('Lambda', 1),'Initialization','template');
m_LPV_oe = lpvoe(data_LPV_est, template_oe, options_oe);
m_LPV_oe_ss=lpvio2ss(m_LPV_oe.F,m_LPV_oe.B,na,nb,Ts);


%% LPV-SS estimation - subspace 

n=5;           % model order
p_window=6;    % past window
template_ss= LPVcore.lpvidss(ones(n,n)+ones(n,n)*q1d+ones(n,n)*s2+ones(n,n)*c2,ones(n,1)+ones(n,1)*q1d+ones(n,1)*s2+ones(n,1)*c2,ones(1,n),ones(1,1),'innovation',ones(n,1), [], [],Ts);
options_sid = lpvsidOptions('Display', 'on', 'PastWindowSize', p_window,'KernelizationRegularizationConstant',1E-5);
m_LPV_ss_sid = lpvsid(data_LPV_est, template_ss, options_sid);

%% LPV-SS estiamtion - PEM

template_ss=m_LPV_oe_ss; 
options_pem_ss = lpvssestOptions;
options_pem_ss.Display='on';
options_pem_ss.Initialization='template';
m_LPV_ss_pem= lpvssest(data_LPV_est, template_ss, options_pem_ss);

%% Comparision of LPV results

figure(4);
compare(data_LPV_val,m_LPV_arx,m_LPV_oe,m_LPV_ss_sid,m_LPV_ss_pem,1)
figure(1);
compare(data_LPV_val,m_LPV_arx,m_LPV_oe,m_LPV_ss_sid,m_LPV_ss_pem,Inf)
figure(2);
compare(data_LPV_val_2,m_LPV_arx,m_LPV_oe,m_LPV_ss_sid,m_LPV_ss_pem,Inf)
figure(3);
compare(data_LPV_val_3,m_LPV_arx,m_LPV_oe,m_LPV_ss_sid,m_LPV_ss_pem,Inf)

p_nom=[1,40,0]; % nominal point
m_oe_fr=ss(freeze(m_LPV_oe_ss.A,p_nom),freeze(m_LPV_oe_ss.B,p_nom),freeze(m_LPV_oe_ss.C,p_nom),freeze(m_LPV_oe_ss.D,p_nom),Ts);
m_ss_pem_fr=ss(freeze(m_LPV_ss_pem.A,p_nom),freeze(m_LPV_ss_pem.B,p_nom),freeze(m_LPV_ss_pem.C,p_nom),freeze(m_LPV_ss_pem.D,p_nom),Ts);
m_arx_fr=ss(freeze(m_LPV_arx_ss.A,p_nom),freeze(m_LPV_arx_ss.B,p_nom),freeze(m_LPV_arx_ss.C,p_nom),freeze(m_LPV_arx_ss.D,p_nom),Ts);
m_ss_sid_fr=ss(freeze(m_LPV_ss_sid.A,p_nom),freeze(m_LPV_ss_sid.B,p_nom),freeze(m_LPV_ss_sid.C,p_nom),freeze(m_LPV_ss_sid.D,p_nom),Ts);
figure(5);
bode(m_arx_fr,m_oe_fr,m_ss_sid_fr,m_ss_pem_fr);
legend('lpv arx','lpv oe','lpv ss sid','lpv ss pem');


%% Comparision of local aspects
 
%%%%%%%%%%%%%%%%%%%%%%%   LOCAL FUNCTIONS   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function lpv_ss=lpvio2ss(A,B,na,nb,Ts)
% LPV-SS realsiation (Shifted form, SISO, nb<=na)
lpv_ss=[];
A_SS=[];
B_SS=[];
s2 = preal('sin(q2)', 'dt');
c2 = preal('cos(q2)', 'dt');
q1d = preal('q1d', 'dt');
for k=1:na
    %A_SS=[A_SS;pshift(A.MatNoMonic{k+1},k)];
    A_SS=[A_SS;-(A.MatNoMonic{k}.matrices(:,:,1)+A.MatNoMonic{k}.matrices(:,:,2)*c2+A.MatNoMonic{k}.matrices(:,:,3)*q1d+A.MatNoMonic{k}.matrices(:,:,4)*s2)];
end
for k=2:nb+1
    %B_SS=[B_SS;pshift(B.Mat{k+1},k)];
    B_SS=[B_SS;(B.Mat{k}.matrices(:,:,1)+B.Mat{k}.matrices(:,:,2)*c2+B.Mat{k}.matrices(:,:,3)*q1d+B.Mat{k}.matrices(:,:,4)*s2)];
end
if na>nb
    B_SS=[B_SS;zeros(na-nb,1)];
end
B_SS=B_SS+A_SS*B.Mat{1}.matrices(:,:,1);
A_SS=[A_SS,[eye(na-1); zeros(1,na-1)]];
C_SS=[1,zeros(1,na-1)];
D_SS=B.Mat{1}.matrices(:,:,1);
lpv_ss=LPVcore.lpvidss(A_SS,B_SS,C_SS,D_SS,'innovation',zeros(na,1), [], [],Ts);
end
 
