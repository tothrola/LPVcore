function [y,p,yp,pp]=unb_sim_arx(u,NoiseVariance)

K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.01;                 % Sampling time


N=length(u);
%mx=zeros(2,N);
my=zeros(N+2,1);
mu=[0;0;u];
mstd=sqrt(NoiseVariance);
me=mstd*randn(N+2,1);

mA0=eye(2)+Ts*[-1/tau, 0; 1, 0];
mA1=Ts*[0, -(M*g*l/J); 0, 0];
mB=Ts*[Km/tau; 0];
mC=[0 1];
mD=0;

ma0=1;                               % Coefficients of the output side         
ma1=Ts/tau-2;               
ma2=1-Ts/tau;
ma2p=((M*g*l)/J)*(Ts^2);  
mb2=Km/tau*Ts^2;                     % Coefficient of the input side

for k=3:N+2
my(k)=-ma1*my(k-1)-ma2*my(k-2)-ma2p * sin(my(k-2))+mb2*mu(k-2); 
end
yp=my(3:N+2);
pp=sinc(yp);

for k=3:N+2
my(k)=-ma1*my(k-1)-ma2*my(k-2)-ma2p * sin(my(k-2))+mb2*mu(k-2)+me(k); 
end
y=my(3:N+2);
p=sinc(yp);


% for k=2:N
% mx(:,k)=mA0*mx(:,k-1)+mA1*sin(x(2,k))+mB*u(k);
% end
% my=C*mx;
% mx=mx';
% yp=my';
% pp=sinc(my);



