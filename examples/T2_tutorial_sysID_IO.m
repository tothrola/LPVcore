%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%        Tutorial 2: Data generation and SySID with LPV-IO models
%                   written by R. Toth and P. den Boef
%                         version (01-04-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this second part of the tutorial, you'll be able to 
%       (a) set up data generation and create model structures for sysID
%       (b) estimate LPV-IO models using prediction error minimization
%
%   After completing the tutorial you may follow T3 how to do LPV state-
%   space model identification with LPVcore 
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time    
%       PEM Prediction Error Minimization

clearvars; close all; clc;
rng(42);   % set random number generator seed so that results are 
           % reproducible

%% 1.b Define unbalanced disc model parameters

% We will keep investigating our unbalanced disc system from T1 first to 
% show how system identification of LPV-IO models can be executed in LPVcore

K=53.6*10^-3;   % [Nm/A] Motor torque constant
R=9.50;         % [Ohm] Motor resistance
L=0.84*10^-3;   % [H] Motor inductance
J=2.2*10^-4;    % [Nm^2] Complete disk inertia
b=6.6*10^-5;    % [Nms/rad] Friction coefficient
M=0.07;         % [kg] Additional mass
l=0.042;        % [m] Mass distance from the disk center
g=9.8;          % [m/s^2] Gravitational acceleration 
tau=(R*J)/(R*b+K^2);       % lumped back emf constant
Km=K/(R*b+K^2);            % lumped torque constant 
Ts = 0.01;                 % Sampling time

%% 2.a IDPOLY objects 

% LPVcore has algorithms for identifying LPV models from data.
% Broadly speaking, these algorithms can be divided into two classes:
%   1. Local methods: based on local linearizations of the underlying
%   system, i.e. the scheduling signal is kept constant during experiments.
%   2. Global methods: the scheduling signal is varied during the
%   experiments.
% In this tutorial, a global methods for the identification of LPV-IO
% models is discussed. For this purpose we will introduce LPV IDPOLY 
% objects. This model type is similar to IDPOLY models in the System
% Identification toolbox, but with scheduling-dependent coefficients. 
% For an example of a local method to identify an LPV models, see 
% part T5 of this tutorial series. 

pd2= preal('p','dt','Dynamic',-2);  % Dynamic dependence, shifted back 
                                    % 2 samples
a0=1;                               % Coefficients of the output side         
a1=Ts/tau-2;               
a2=1-Ts/tau+((M*g*l)/J)*(Ts^2)*pd2;  
b2=Km/tau*Ts^2;                     % Coefficient of the input side

% Next we can create an IDPOLY object to define the DT LPV-IO form of the 
% unbalanced disc system as a data generating object. These objects are very
% similar to LPV-IO representations, except that they are also capable to
% represent noise models and also describe estimated model specific
% information, just like in the LTI case. 

A = {1, a1, a2}; 
B = {0, 0, b2};

sys = LPVcore.lpvidpoly(A,B); % Create an idpoly object                                                
sys.Ts = Ts;                   % Sampling time
sys.InputName = {'Voltage'};   % Naming of the signals
sys.OutputName = {'Angular position'};
sys.NoiseVariance = 0.25*10^-7;    % Specify noise variance

% Just like in the System Identification toolbox, model structures such as
% ARX, ARMA, ARMAX, OE, BJ, etc. models can be described in terms of matrix
% polynomials in the backward shift operator q^-1. Coefficients of these 
% polynomials are collected in A, B, C, D, F pmatrix or cell array of 
% pmatrix objects, which are used LPVIDPOLY to create the corresponding 
% model structure. In the above case only the A and B part are defined,
% hence the corresponding system will be ARX.

%% 2.b Create data sets

% Next we generate synthetic data using sys to mimic measurment conducted 
% on the real system.  
 
N=2000;         % Number of samples
p_mag=0.25;     % Scheduling variation
 
% Estimation data
u=idinput(N,'rgs');  % White noise input signal
p=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]); % RBS scheduling signal

% Note that for the sake of simplicity, 'p' is generated independently from
% the scheduling map and the associated angular position. This is not a
% limitation of the toolbox but used to simplify the example.
 
[y,~,~,yp] = lsim(sys,p,u);       % Generate data including noise signal
                                  % y - noisy output
                                  % yp - noise free output 
 
fprintf('The SNR is %f dB.\n', LPVcore.snr(yp,y-yp));  % Signal-to-noise ratio
 
data_est = lpviddata(y,p,u,Ts);      % Create an LPV data object from the 
                                     % generated data
plot(data_est);                      % Show data
 
% Create a noisy and a noise free validation data set (called test sets 
% in machine learning)

uv=idinput(N,'rgs');
pv=(1-p_mag)+p_mag*idinput(N,'rbs',[0,0.05]);
[yv,~,~,yvp] = lsim(sys,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise free data set 

%% 3.a LPV-ARX identification

% The model structure for the identification methods is defined through a
% template system. The exact value of the parameters in the template is not
% important only the corresponding structure of the dependence that  
% is defined by them. This is used by the algorithms to estimate the 
% coefficients (i.e., the parameters).
 
% Next we will define an LPVIDPOLY template with more general dependence 
% than our data-generating system. 

% Create specification of the model structure. Create various shifted 
% instances of the scheduling  
pd=preal('p','dt');
pd1=pshift(pd,-1);
pd2=pshift(pd,-2);
 
% Dimension of the coefficients and their functional dependency creates a
% template for the model structure. The individual parameter values do not
% matter
am1=1+pd+pd1+pd2;
am2=am1;
bm0=am1;
bm1=am1;
bm2=am1;
template_sys = LPVcore.lpvidpoly({1, am1, am2}, {bm0, bm1, bm2});

% perform ARX identification with the specified model structure, which
% corresponds to a least squares estimate
options = lpvarxOptions('Display','on');
m_arx = lpvarx(data_est, template_sys, options);

% Note that LPVARX does not make use of the value of the coefficients in
% the template (it only uses the structure). However, in case of advanced 
% methods, like LPVPOLYEST, the parameter values can be used as initial 
% seeds for the optimization. One can see that convergence really depends 
% on the initialization hence, automatic initialization is implemented for
% advanced techniques. However, one can also request to use the coefficient
% values in the template as an initialization, which makes it easy to
% reuse previous estimates as the initialization of these methods. 

% Simulate the estiamted model with no noise on the validation data
ys_arx = lsim(m_arx,pv,uv,[],zeros(N,1));        
fit_arx = bfr(yvp,ys_arx); % BFR score of the model fit

% Simualtion response can be also obtained by using compare
[ys_arx,fit_arx]=compare(data_val_2,m_arx);

% For prediction use compare(data_val,m_arx,1)

% Now we can display our results by either plotting them manually:
figure;
plot([yvp, ys_arx]);
title('LPV-ARX estimation');
xlabel('Time sample [-]');
ylabel('Amplitude [-]');
legend('Measured output (noise free)', 'Simulated output')

% or with compare
figure;
compare(data_val_2,m_arx);

% Compare the estimates 
disp(m_arx.A.Mat{1})
disp(sys.A.Mat{1})
 
disp(m_arx.A.Mat{2})
disp(sys.A.Mat{2})
 
disp(m_arx.A.Mat{3})
disp(sys.A.Mat{3})

%% 4.a More complicated noise structure
 
% To make our unbalanced disk example more realistic and challenging, let's 
% assume that the noise is a colored out noise which is dependent on the 
% scheduling (i.e., the angular position) according to
 
c0=1;
c1=0.75*pshift(pd,-1);
d0=1;
d1=-0.25+0.25*pshift(pd,-1);
 
% Create and idpolyobject => BJ model
sys = LPVcore.lpvidpoly([],{0, 0, b2},{c0,c1},{d0,d1},{a0, a1, a2}); 
sys.Ts = Ts;                   % Sampling time
sys.InputName = {'Voltage'};   % Naming of the signals
sys.OutputName = {'Angular position'};
sys.NoiseVariance = 4.5*10^-4;     % Specify noise variance
 
%Create the data sets
[y,~,~,yp] = lsim(sys,p,u);  
data_est = lpviddata(y,p,u,Ts);       % noisy estimation data set
fprintf('The SNR is %f dB.\n', LPVcore.snr(yp,y-yp));  % Signal-to-noise ratio
[yv,~,~,yvp] = lsim(sys,pv,uv);       
data_val_1 = lpviddata(yv,pv,uv,Ts);  % noisy validation data set
data_val_2 = lpviddata(yvp,pv,uv,Ts); % noise-free validation data set 

%% 4.b Identification with various model structures
 
% Let's identify the resulting data-generating system with increasingly
% complex model structures:
 
% LPV-ARX estimate
template_arx = LPVcore.lpvidpoly({1, am1, am2}, {bm0, bm1, bm2});
options = lpvarxOptions('Display','on');
m_arx = lpvarx(data_est, template_arx, options);
 
% LPV-ARMAX
% Assume a moving average part for the noise model 

cm0=1;
cm1=0.5*randn(1,2)*[1,pd]'; % same structure for the coefficients as in the ARX case
template_armax = LPVcore.lpvidpoly(m_arx.A, m_arx.B,{cm0,cm1});
options = lpvarmaxOptions(...
    'Initialization', 'default', ... % LPV-ARX model estimate based initialization (default for ARMAX)
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
m_armax = lpvarmax(data_est, template_armax, options);


% LPV-OE
% Assume an output noise model and initialize the estimation from the ARX
% results
template_oe = LPVcore.lpvidpoly([], m_arx.B,[],[],m_arx.A);
options = lpvoeOptions(...
    'Initialization', 'polypre', ... % LPV ARX model estimation-based initialization 
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
m_oe = lpvoe(data_est, template_oe, options);
            
% LPV-BJ
% Finally, let's use a model structure capable to fully capture the noise
cm0=1;
cm1=0.5*ones(1,2)*[1,pd]'; 
dm0=1;
dm1=0.25*ones(1,2)*[1,pd]'; 
options = lpvbjOptions(...
    'Initialization', 'polypre', ... % LPV ARX model estimation based initialization 
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for numerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
template_bj = LPVcore.lpvidpoly([], m_oe.B,{cm0,cm1},{dm0,dm1},m_oe.F);
m_bj = lpvbj(data_est, template_bj, options);
            
% Note that manual initialization can also be used 

 
% Finally let's compare the estimation results 
 
% Comparison in terms of simulation error:
 
[ys_arx,fit_arx]=compare(data_val_2,m_arx);
[ys_armax,fit_armax]=compare(data_val_2,m_armax);
[ys_oe,fit_oe]=compare(data_val_2,m_oe);
[ys_bj,fit_bj]=compare(data_val_2,m_bj);

figure;
plot(yvp);
hold on;
plot(ys_arx);
plot(ys_armax);
plot(ys_oe);
plot(ys_bj);
grid on; title('Simulated model responses');
legend('true',sprintf('arx (%.2f %%)',fit_arx),sprintf('armax (%.2f %%)',fit_armax),sprintf('oe (%.2f %%)',fit_oe),sprintf('bj (%.2f %%)',fit_bj));
 
 
% Comparison in terms of prediction error:

[ys_arx,fit_arx]=compare(data_val_1,m_arx,1);
[ys_armax,fit_armax]=compare(data_val_1,m_armax,1);
[ys_oe,fit_oe]=compare(data_val_1,m_oe,1);
[ys_bj,fit_bj]=compare(data_val_1,m_bj,1);
 
figure;
plot(yv);
hold on;
plot(ys_arx);
plot(ys_armax);
plot(ys_oe);
plot(ys_bj);
grid on; title('Predicted model responses');
legend('true',sprintf('arx (%.2f %%)',fit_arx),sprintf('armax (%.2f %%)',fit_armax),sprintf('oe (%.2f %%)',fit_oe),sprintf('bj (%.2f %%)',fit_bj));


 
%% 4.c General PEM with POLYEST

% Note that the most general form of estimation is with POLYEST which can
% estiamte any combination of polynomial model structures. For example,
% instead of calling BJ, one can directly call

template_bj = LPVcore.lpvidpoly([], m_oe.B,{cm0,cm1},{dm0,dm1},m_oe.F);
options = lpvpolyestOptions(...
    'Initialization', 'polypre', ... % LPV ARX model estimation based initialization 
    'Regularization', struct('Lambda', 1E-5), ...  % Regularization for nummerical stability (not needed in this case)
    'SearchOptions', ... % Hyperparameters of the involved optimization (default)
    struct('StepSize', 1, 'StepTolerance', 1E-8, 'MaxIterations', 100), ...
    'Display', 'on');
m_bj = lpvpolyest(data_est, template_bj, options);

% which should give exactly the same results as the LPVBJ script before

