%% In these examples, we show the LPVSYNSF function
clear; close all; clc;

%% Example 1: L2-gain optimal LPV controller design
%
% In this example we're going to design an L2-gain optimal state-feedback 
% LPV controller for a mass-spring-damper (MSD) system with varying spring
% constant, see also Example 1 in example_lpvsyn.m. The objective for the 
% controller will be to track (constant) references and  reject (constant) 
% disturbances.
%

fprintf([repelem('-',76),'\n'])
fprintf('Example 1:\n')  
fprintf(['L2-gain optimal state-feedback LPV controller synthesis for' ...
    ' mass-spring-damper system\nwith varying spring constant\n\n']);

% Parameters
k  = preal('k', 'ct', 'Range', [1 12]);
d  = 2;
m  = 0.5;

% Create LPV model of MSD system
A = [0 1;-k/m -d/m];
B = [0;1/m];
C = [1 0];
D = 0;

G = LPVcore.lpvss(A,B,C,D);     % plant to be controlled
G.InputName = 'ug';
G.OutputName = 'yg';

% Generalized plant design (P)
%   
%                 ┌ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┐
%   w1            ╷   ┌────┐                                     P  ╷
%     ────────────╷──▶│ Wr │────────────────────────────┐           ╷
%                 ╷   └────┘                            │           ╷
%   w2            ╷   ┌────┐                            │           ╷
%     ────────────╷──▶│ Wd │──┐                         │           ╷
%                 ╷   └────┘  │                         │           ╷
%        ┌ ─ ─ ┐  ╷           ▼     ┌─────┐   ┌────┐    ▼    ┌────┐ ╷   z1
%    ┌──▶   K   ──╷────┬────▶(+)───▶│  G  │──▶│ -1 │──▶(+)──▶│ We │─╷──▶
%    │   └ ─ ─ ┘  ╷ u  │         ug └─────┘yg └────┘         └────┘ ╷
%    │            ╷    │                                     ┌────┐ ╷   z2
%    │            ╷    └────────────────────────────────────▶│ Wu │─╷──▶
%    │            ╷                                          └────┘ ╷
%    │            └ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┬ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┘ 
%    │                                        │
%    └────────────────────────────────────────┘
%                  x
%
%   G:  Plant (MSD)
%   W*: Weighting filters
%   P:  Generalized Plant (interconnection of G and filters W)
%   K:  To-be-synthesized state-feedback controller
%
%   x:  States of P
%   z*: Generalized performance channels
%   w*: Generalized disturbance channels
%   u:  Control Input

nu = 1;     % number of controlled inputs

Wr = 1; 
Wd = 1;
Ww = blkdiag(Wr, Wd);

We = makeweight(db2mag(60),[0.5*2*pi, db2mag(0)], db2mag(-6));
Wu = makeweight(db2mag(-30), [10*2*pi, db2mag(0)], db2mag(40));
Wz = blkdiag(We, Wu);

Puw = connect(G,...
              sumblk('ug = u + w2'), ...
              sumblk('z1 = w1 - yg'), ...
              {'w1','w2','u'},...
              {'z1','u'});

P = Wz*Puw*blkdiag(Ww,eye(nu));

% Set synthesis options
synOpt = lpvsynsfOptions('performance', 'l2');

% Synthesis
%
% Synthesize a state-feedback LPV controller that optimizes the closed-loop 
% L2-gain from w to z, i.e.
%
% min γ  s.t. ‖ z ‖_2 <= γ ‖ w ‖_2
%

fprintf('Synthesizing controller:\n\n')

[K, gamma, X] = lpvsynsf(P, nu, synOpt);

fprintf('\nPrinting results:\n\n')
fprintf('Obtained closed-loop L2-gain: %.3g\n\n',gamma)

% Verify
nw = P.Nu-nu;

CL = LPVcore.lpvss(P.A+P.B(:,nw+1:end)*K, P.B(:,1:nw), ...
                   P.C+P.D(:,nw+1:end)*K, P.D(:,1:nw));
gammaVerify = lpvnorm(CL,'l2');    % Might be slightly different to 
                                   % numerical computation.

fprintf('Verified closed-loop L2-gain: %.3g\n\n',gammaVerify)