%% Example script demonstrating usage of LSIM
%
% In this example we demonstrate how to run time-domain simulations of
% LPVcore.lpvlfr objects (and LPVcore.lpvlfr sub-classes, such as
% LPVcore.lpvss, LPVcore.lpvio, etc). The time-domain simulations can be
% run in self-scheduling mode or with a given scheduling trajectory. For
% this example, the two-link robot arm example from 
% [1] "Scheduling Dimension Reduction of LPV Models - A Deep Neural Network
% Approach" by P.J.W. Koelewijn and R. Toth (ACC, 2020)
% is used.

clearvars; close all; rng(1);

%% Define the two-link robot arm 
% Physical parameters
a = 5.6794;     b = 1.473;      c = 1.7985;
d = 4e-1;       e = 4e-1;       f = 2;
n = 1;

param = [a,b,c,d,e,f,n];

% The nonlinear dynamic equations are included in this script, in the local
% function robotArm.

%% Nonlinear simulation of the robot arm
% We first run time-domain simulations of the original nonlinear system,
% which will be later compared against the LPV representation.
uFun = @(t) [3 * sin(t), -cos(t)];
Ts = 0.01;
t = 0:Ts:20;
odefun = @(t,x) robotArm(x, uFun(t), param);
[t, x] = ode45(odefun, t, zeros(4,1));
N = numel(t);

%% Create the LPV system with 10 scheduling-variables from the example [1]
% Note that an automatic LPV-SS embeding could be obtained as well with
% LPVcore. See LPVcore\examples\nonlinear\example_nlss2lpvss.m
np = 10;
p = cell(np,1);
for i = 1:np
    p{i} = preal(sprintf(sprintf('p(%%0%id)', numel(num2str(np))), i));
end

A = [ 0,         0,        1,    0;
      0,         0,        0,    1;
      c*d*p{3}, -b*e*p{4}, p{5}, b*p{6};
     -b*d*p{7},  a*e*p{8}, p{9}, p{10}];

B = [ 0,         0;
      0,         0;
      c*n*p{1}, -b*n*p{2};
     -b*n*p{2},  a*n*p{1}];

C = [eye(2),zeros(2)];
D = zeros(2);

sys = LPVcore.lpvss(A,B,C,D);

% And a discrete-time version of the LPV system
sysDt = c2d(sys,Ts,'exact-zoh-euler'); 

%% Compute the scheduling trajectory
% LPVcore includes the LPVcore.schedmap object to create scheduling maps
% and facilitate the obtention of scheduling trajectories

% Create scheduling map object. First, define the scheduling map function,
% i.e. a function that maps states, inputs and/or exogenous signals to
% scheduling signals
map = @(x, u, d) schedMap(x, d, param);

% Now, we can create the scheduling map object. Note that we need to
% provide the number of states, inputs and exogenous signals, and these
% must be consistent with the LPVcore.lpvlfr object.
nx = 4; % Nr. of states
nu = 2; % Nr. of inputs
nd = 1; % Nr. of exogenous signals
sched = LPVcore.schedmap(map,nx,nu,nd);

% Compute the scheduling trajectory
pTraj = sched(x',zeros(2,N),zeros(1,N))';

%% Self-scheduled simulation
% To execute a time-domain self-scheduled simulation, we just need to
% provide the LPV system, the scheduling map object, and the input and time
% vectors. Other options, such as the initial state, noise signals or
% solver options are available, depending on which type of LPV system we
% have. For detailed information, see LPVcore.parselsimOpts.

% In this example, for demonstration purposes we included a fictitious
% exogenous signal in the scheduling map. Therefore, the self-scheduling
% simulation will require an exogenous input as well. Note that the input
% and exogenous signals can be given as a function handle (recommended), or
% as a numeric vector. 

% Define a fictitious exogenous signal. Make it 0 to match the original
% nonlinear system, which did not have any exogenous signal.
dFun = @(t) (0.0001 * sin(t)).*0;

% Now run the self-scheduled simulation
y_self = lsim(sys, sched, uFun, t, 'd', dFun, 'Solver', @ode45);

%% Simulation with a given scheduling trajectory
% To simulate with a given scheduling trajectory, just use the trajectory
% itself instead the scheduling map in the lsim function!
% An exogenous signal is not required in this case, as it is already
% included in the generation of the scheduling trajectory.

% The dimensions of the trajectory must be [Nt, Np]:
y_traj = lsim(sys, pTraj, uFun, t, 'Solver', @ode45);

% In addition, one can use a faster but less accurate simulation option:
y_trajFast = lsim(sys, pTraj, uFun, t, 'Solver', @ode45, 'SolverInterpolationMode', 'accurate');
%% Discrete-time simulation
% LPVcore can simulate discrete-time systems in the same way as the
% continuous-time ones shown above. For this, the LPV object must be in
% discrete time.

% Discrete-time self-scheduling simulation
y_Dtself = lsim(sysDt, sched, uFun, t, 'd', dFun);
% Discrete-time simulation with a given trajectory
y_Dttraj = lsim(sysDt, pTraj, uFun, t);

% Lastly, note that a quick simulation plot can be shown by simply calling
% the lsim function without output arguments. This can be done for
% continuous or discrete-time systems:
lsim(sysDt, sched, uFun, t, 'd', dFun);

%% Plot time-domain simulations and test their accuracy vs the original nonlinear system

lw = 2;     % lineWidth
fs = 15;    % axis fontsize

f1 = figure(7); clf(7);
tiledlayout(2,2,"TileSpacing","compact","Padding","compact");

tile(1) = nexttile;
plot(t, x(:,1), LineWidth = lw);  hold on;
plot(t, y_self(:,1), '--', LineWidth = lw);
plot(t, y_traj(:,1), '--', LineWidth = lw);
plot(t, y_Dtself(:,1), '--', LineWidth = lw);
plot(t, y_Dttraj(:,1), '--', LineWidth = lw);
grid on;
ylabel('$q_1$ [rad]', Interpreter = 'latex', FontSize = fs);

tile(2) = nexttile(3);
plot(t, x(:,2), LineWidth = lw);  hold on;
plot(t, y_self(:,2), '--', LineWidth = lw);
plot(t, y_traj(:,2), '--', LineWidth = lw);
plot(t, y_Dtself(:,2), '--', LineWidth = lw);
plot(t, y_Dttraj(:,2), '--', LineWidth = lw);
grid on;
ylabel('$q_2$ [rad]', Interpreter = 'latex', FontSize = fs);
xlabel('$t$ [s]', Interpreter = 'latex', FontSize = fs)
legend(tile(1),'Nonlinear', 'Self-scheduled', 'Sched trajectory-based', ...
    'DT self', 'DT trajectory', ...
     'FontSize', fs - 2, 'Location','NorthWest');
title(tile(1), 'Nonlinear and LPV simulations', FontSize=fs)

tile(3) = nexttile(2);
plot(t, mag2db(abs(x(:,1) - y_self(:,1))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,1) - y_traj(:,1))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,1) - y_Dtself(:,1))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,1) - y_Dttraj(:,1))), LineWidth = lw);  hold on;
grid on;
ylabel('$|q_1 - \hat{q}_1 |$ [dB]', Interpreter = 'latex', FontSize = fs);

tile(4) = nexttile(4);
plot(t, mag2db(abs(x(:,2) - y_self(:,2))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,2) - y_traj(:,2))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,2) - y_Dtself(:,2))), LineWidth = lw);  hold on;
plot(t, mag2db(abs(x(:,2) - y_Dttraj(:,2))), LineWidth = lw);  hold on;
grid on;
ylabel('$|q_2 - \hat{q}_2 |$ [dB]', Interpreter = 'latex', FontSize = fs);
xlabel('$t$ [s]', Interpreter = 'latex', FontSize = fs)
legend(tile(3), 'Self-scheduled', 'Sched trajectory-based', ...
    'DT self', 'DT trajectory', ...
     'FontSize', fs - 2, 'Location','Northeast');
title(tile(3), 'Absolute error between the nonlinear and the LPV simulation', FontSize=fs)

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%                             LOCAL FUNCTIONS                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xdot = robotArm(x, u, par)
    a = par(1);
    b = par(2);
    c = par(3);
    d = par(4);
    e = par(5);
    f = par(6);
    n = par(7);

    q1 = x(1);
    q2 = x(2);
    q1dot = x(3);
    q2dot = x(4);

    u = [u(1);u(2)];

    M = [a,                b * cos(q1 - q2);
         b * cos(q1 - q2), c];

    C = [b * sin(q1 - q2) * q2dot^2 + f * q1dot;
         -b * sin(q1 - q2) * q1dot^2 + f * (q2dot - q1dot)];

    g = [-d * sin(q1);
         -e * sin(q2)];

    qddot = M \ (n * u - C - g);
    
    xdot = [q1dot;q2dot;qddot];
end

function p = schedMap(x, d, par)
    a = par(1);
    b = par(2);
    c = par(3);
    f = par(6);

    q1 = x(1,:);
    q2 = x(2,:);
    q1d = x(3,:);
    q2d = x(4,:);

    cosd = cos(q1-q2);
    sind = sin(q1-q2);

    h = 1 ./ (a * c - b^2 * cosd.^2);
    
    % Note that the exogenous signal d(1,:) is included below for
    % demonstration purposes, but it does not belong to the original
    % scheduling map!
    p = [h + d(1,:); 
         cosd .* h;
         sincFun(q1) .* h;
         cosd .* sincFun(q2) .* h;
         (-b^2 * cosd .* sind .* q1d - (c + b * cosd) * f) .* h;
         (-c * sind .* q2d + cosd * f) .* h;
         cosd .* sincFun(q1) .* h;
         sincFun(q2) .* h;
         (a * b * sind .* q1d + f * (a + b * cosd)) .* h;
         (b^2 * sind .* cosd .* q2d - a * f) .* h];
end

% sinc function
function  y = sincFun(x)
    y = sin(x)./x;
    y(x == 0) = 1;
end