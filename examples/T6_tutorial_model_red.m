%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           LPVcore Tutorial
%                    Tutorial 6: LPV model reduction
%       written by E. Javi Olucha Delgado, R. Toth, and P. den Boef
%                         version (05-06-2024)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1.a Introduction
%
%   Welcome to LPVcore!
%   This tutorial series show the most important concepts of the toolbox.
%   After following this sixth part of the tutorial, you'll be able to 
%       (a) Reduce the state-order dimension of LPV systems
%       (b) Reduce the scheduling dimension of LPV systems
%       (c) Compare among the different reduction methods both in the
%               frequency (local) and time-domain (global)
%
%   Additionally, it is worth remarking that:
%       1. This tutorial is shown for CT systems, but the methods can
%           handle DT systems as well.
%       2. For a combined state-order and scheduling dimension reduction,
%           one can apply the reduction methods sequentially.
%
%   After completing the tutorial you may follow T7 on control design with
%   LPVcore
%
%   We will use the following abbreviations:
%       LPV Linear Parameter-Varying
%       PV  Parameter Varying
%       DT  Discrete Time 
%       CT Continuous Time 

clearvars; close all; clc;
rng(7);   % set random number generator seed so that results are reproducible

%% 1.b Define the Mass-Spring-Damper system interconnection
% The MSD system interconnection is extracted from:
%   "Olucha, E. Javier, Bogoljub Terzin, Amritam Das, and Roland Tóth. 
%   “On the Reduction of Linear Parameter-Varying State-Space Models.” 
%   arXiv, April 2, 2024. http://arxiv.org/abs/2404.01871."

% Reproducing the MSD with Nm = 5, nx = 10, np = 9
% System parameters
Nm = 5; param = [1 0.5 1 0.5 1]; % [M k d kwall dwall]
nl_index.springs = 1:Nm-1;
nl_index.springs_wall = 1:Nm;
nl_index.dampers = []; nl_index.dampers_wall = [];
k = @(x) x^3; % Spring connecting mass blocks
d = @(x,xdot) 0; % Damper connecting mass blocks
k_wall = @(x) x^3; % Spring connecting each mass with the wall
d_wall = @(x,xdot) 0; % Damper connecting each mass with the wall

% Generate the dynamic equations of the MSD
[fMSD,state,input] = msdGenerator(Nm,nl_index,k,d,k_wall,d_wall);

% Construction of the nonlinear state-space
f = @(x,u) fMSD(x,u,param); 
h = @(x,u) [x(Nm,:); x(end,:)]; % Output: position and velocity of the last mass
nx = length(state); nu = length(input); ny = 2;
nlss = LPVcore.nlss(f,h,nx,nu,ny, 0, true);

% Automatic LPV-SS embedding with LPVcore
[sys, schedulingMap] = LPVcore.nlss2lpvss(nlss,"analytical","factor");

%Nonlinear simulation of the MSD
tsim = 25; Ts = 0.1; t=(0:Ts:tsim)';
u = sum(2.*randn(1,3).*sin(2*pi.*(linspace(0.2,2,3)).*t + (0 + (pi-0).*randn(1,3))),2);
[~, ~, x] = sim(nlss, u, t, zeros(nlss.Nx,1));

% Compute the scheduling trajectory: it will be required for some
% methods
pTraj = schedulingMap.map(x', u')';
N = numel(t);

%% 2. LPV state-order reduction
% LPV core currently implements the following methods for state-order
% reduction: 'ltibalred', 'lpvobliquered', 'lpvmmred', 'lpvbalred'

% We will provide an overview of all these state-order reduction methods 
% in this section, and compare them afterwards. A more detailed use of each
% method can be found in LPVcore/examples/reduction


%% 2.a. State-order reduction with 'ltibalred'
% This method is embedded as an option inside the "lpvmodred" function.
% First, we choose the state-order reduction target, i.e, we want to reduce
% the state-order of our LPV model from nx -> rx
rx = 2; 
% Execute the state reduction
[sysr_ltibalred, info_ltibalred] = lpvmodred(sys, rx, [], 'ltibalred');
% We can now evaluate the Hankel Singular Values (HSV) of the LTI part of 
% the reduced model to analyse see "how much information is left out".
figure(1); semilogy(info_ltibalred.HSV); grid on;
xline(rx, 'r', 'rx');
xlabel('Order (Number of States)');
ylabel('State Contribution');
title('Hankel Singular Values of sysr.G (ltibalred)');

%% 2.b. State-order reduction with 'lpvobliquered'
% For the LPV oblique reduction method, we need to provide a grid with
% local LPV models. The "lpvobliquered" method provides a grid of reduced
% local models, which we will need to interpolate to recover a global
% reduced LPV model. The scalability of this method depends on the type of
% grid and the shceduling dimension. In the example of this tutorial, our
% system has 9 scheduling variables, which leads to prohibitive equidistant
% grids in terms of computation complexity. Therefore, we will proceed with
% a scattered grid.

% The extraction of the local models, the reduction and the posterior
% interpolation can be easily done with LPV as follows.

% We first define the grid. In this case, we pick some samples from the
%scheduling trajectory.
pGridScattered = pTraj(1:20:end,:);

% Next, we extract the local models from the original LPV-SS system at the
% specified grid-points:
localModels = extractLocal(sys, num2cell(pGridScattered,2));
assert(isstable(localModels)); % The local models are required to be stable

% Now we construct a scattered LPVcore.lpvgridss object
pScattered.SchedulingNames = sys.SchedulingTimeMap.Name;
pScattered.SchedulingData = pGridScattered;
sysScattered = LPVcore.lpvgridss(cat(3,localModels{:}), pScattered);

% Next, we execute the reduction
[sysrgrid, ~]  = lpvobliquered(sysScattered, rx);

% Lastly, we recover the global model by interpolating the local models.
% Currently, the only interpolation method for scattered grids in LPVcore
% is the 'behavioral' one. We need to define a structure including the
% LPV-SS system of reference (in this case the original model), and
% optionally the hinfstruct options

opts.init_sys = sys; % LPV-SS model of reference
opts.hinfstructOptions = hinfstructOptions;
opts.hinfstructOptions.Display = 'iter'; % hinfstruct options
sysr_lpvobliquered  = lpvinterpss(sysrgrid, 'behavioral', opts);

% Note that lpvinterpss contains many options for equidistant grids, 
% which are more convenient for smaller scheduling dimensions. Please see
% doc lpvinterpss to find all the possibilities.
%% 2.c. State-order reduction with 'lpvmmred'
% For the "lpvmmred" method we are able to choose how many moments we want
% to match. The downside is that we don't have explicit control on the
% reduced state dimension, but a lower number of moments leads to a lower
% rx. Besides this limitation, this method is proven to be one of the most
% reliable and efficient, and works with unstable models as well. In 
% addition, it can be used to obtain minimal LPV-SS representations.

% "lpvmmred" has 3 different operating modes: 'R', 'O', 'T'. For a detailed
% explanation, please see help lpvmmred, or see a comparison in
% LPVcore/examples/reduction/example_lpvmmred.m
moments = 1;
sysr_lpvmmred = lpvmmred(sys, moments, 'R');

%% 2.d. State-order reduction with 'lpvbalred'
% "lpvbalred", similar to "lpvobliquered" relies on local models. In
% contrast, "lpvbalred" applies the reduction directly on the global model,
% and therefore we don't need to interpolate a posteriori. We re-use the 
% scattered grid from 2.b.

% Execute the redution. We can choose a solver for Yalmip. The default
% solver option is 'sdpt3'. For more optimization options, see doc lpvbalred 
[sysr_lpvbalred, Tred, ~] = lpvbalred(sys, rx, pGridScattered,'sdpt3');

%% 2.e. Local state-order reduction comparison
% Frequency domain validation. Define a validation grid, different from the
% grids used for the reduction.
pGridValidation = pTraj(2:30:100,:);

% Now, we can plot a comparison of the Full Order Model (FOM) vs. the
% different Reduced Order Models (ROM). LPVcore provides custom "bode" and
% "bodemag" functions to ease the comparison at the chosen points.
figure(2);
subplot(221)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_ltibalred,pGridValidation);
title('FOM vs. ROM: ltibalred'); legend('FOM','ltibalred');
subplot(222)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_lpvobliquered,pGridValidation); % This line takes time
title('FOM vs. ROM: lpvobliquered'); legend('FOM','lpvobliquered');
subplot(223)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_lpvmmred,pGridValidation);
title('FOM vs. ROM: lpvmmred'); legend('FOM','lpvmmred');
subplot(224)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_lpvbalred,pGridValidation);
title('FOM vs. ROM: lpvbalred'); legend('FOM','lpvbalred');

%% 2.f. Global state-order reduction comparison
% Time-domain validation
ySys = lsim(sys, pTraj, u, t); 
ySysr_ltibalred = lsim(sysr_ltibalred, pTraj, u, t);
ySysr_lpvobliquered = lsim(sysr_lpvobliquered, pTraj, u, t);
ySysr_lpvmmred = lsim(sysr_lpvmmred, pTraj, u, t);
ySysr_lpvbalred= lsim(sysr_lpvbalred, pTraj, u, t);

% Set plot options
clr = [[0 0 0];[0 0.4470 0.7410];[0.8500 0.3250 0.0980]];
lw = 1.5;

% Plot time-domain simulations and the absolute error
figure(3); clf(3);
tiledlayout(2,2,"TileSpacing","compact","Padding","compact");
nexttile;
colororder(clr)
plot(t, ySys(:,1),'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_ltibalred(:,1),'LineWidth', lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_ltibalred(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: ltibalred'); 
legend('FOM','ltibalred', 'error'); xlabel('Time [s]'); hold off
nexttile;
plot(t, ySys(:,1),'LineWidth', lw); hold on; grid on; 
plot(t, ySysr_lpvobliquered(:,1),'LineWidth', lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_lpvobliquered(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvobliquered'); 
legend('FOM','lpvobliquered', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1),'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_lpvmmred(:,1),'LineWidth',lw); ylabel('Position [m]');
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_lpvmmred(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvmmred'); 
legend('FOM','lpvmmred', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1), 'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_lpvbalred(:,1),'LineWidth',lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_lpvbalred(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvbalred'); 
legend('FOM','lpvbalred', 'error'); xlabel('Time [s]');

%% 3. Scheduling dimension reduction
% LPV core currently implements the following methods for scheduling
% dimension reduction: 'lpvpcared:standard', 'lpvpcared:trajectory', 
% 'lpvkpca', 'lpvdeltabalred', 'lpvaered', 'lpvdnnred'

% We will provide an overview of all these scheduling dimension reduction 
% methods in this section, and compare them afterwards. A more detailed use
% of each method can be found in LPVcore/examples/reduction

%% 3.a. Sched. dimension reduction with "lpvpcared: standard mode"
% The lpvpcared method implements two distinct modes: the standard and the
% trajectory mode.
% First, we choose the scheduling dimension reduction target, i.e,
% we want to reduce the sched. dimension of our LPV model from np -> rp
rp = 2; 
[sysr_PcaStd, map_PcaStd] = lpvpcared(sys, rp, pTraj,'standard');

%% 3.b. Sched. dimension reduction with "lpvpcared: trajectory mode"
[sysr_PcaTraj, map_PcaTraj] = lpvpcared(sys, rp, pTraj,'trajectory');

%% 3.c. Sched. dimension reduction with "lpvkpcared"
% "lpvkpcared" implements pca with a nonlinear projection in an hyper-space
% to learn the features of the sched. map. The LPVcore implementation
% provides different kernel functions, for which we can manually choose
% their hyper parameters. Please, see doc lpvkpcared to find all the
% available options. After the feature extraction, an optimization step is
% executed to obtain the reduced order matrices.

% Choose the kernel and its hyper parameters
kernel = 'sigmoid'; hyperParam = [0.1 0];

% Solver structure with the options for Yalmip
solver.verbose = 0; solver.solver = 'SDPT3';

% Note that we can force the ROM to have no feedthrough with the setting
% feedthrough = false. You can try different kernels and hyperparameters 
% to improve the performance.
[sysr_KPCA, map_KPCA, ~] = lpvkpcared(sys, rp, pTraj, feedthrough = false, ker = kernel, kerParam = hyperParam, solver = solver);

%% 3.d. Sched. dimension reduction with "lpvdeltabalred"
% Embedded in "lpvmodred". "deltabalred" is a Delta block reduction based
% on interchanging the order of the state and latent variables. As this
% method uses the LFR representation of the LPV-SS model, the output is an
% LPV-LFR model. Note that the reduction is applied on the Delta block, and
% not directly in the scheduling dimension!
[sysr_Deltabalred, info_Deltabalred] = lpvmodred(sys, [], rp, 'deltabalred');

% See the reduction in the size of the Delta blocks:
disp(['Size of the original delta block: ', num2str(size(sys.Delta))])
disp(['Size of the reduced-order delta block: ', num2str(size(sysr_Deltabalred.Delta))])

%% 3.e. Sched. dimension reduction with "lpvaered"
% The "lpvaered" method uses an AutoEncoder (AE) type of neural network to
% learn a reduced-order scheduling map. Then, as in "lpvkpcared", an 
% optimization step is executed to obtain the ROM model.

% We can define additional training settings for the AE:
training = struct('EncoderTransferFunction',"logsig","ShowProgressWindow",false);

% We can deffine additional settings for the optimization step as well:
solver = struct('solver','SDPT3','verbose',0);
% Please, see doc lpvaered to find all the possibilities.

% Execute the sched. dimension reduction:
[sysr_AE, map_AE, ~] = lpvaered(sys, rp, pTraj, "maxEpochs",100,"useGPU",...
    true,'trainingOptions',training,'feedthrough',false,'solverOptions',solver);

%% 3.f. Sched. dimension reduction with "lpvdnnred"
% The "lpvdnnred" method uses a Deep Neural Network (DNN) to directly learn
% both the reduced-order sched. map and the reduced-order model, skipping
% the optimization step required in "lpvaered".

% Please, see doc lpvdnnred to find all the options for "lpvdnnred"
num_epochs = 100;
[sysr_DNN, map_DNN, ~] = lpvdnnred(sys, rp, pTraj, ...
    'trainingOptions', struct('epochs', num_epochs));

%% 3.g. Local state-order reduction comparison
% Frequency domain validation. We can re-use the validation grid from 2.e.
% As the sched. dimension of the reduced systems is smaller than
% the original, we have to use the mapping functions provided by each 
% reduction methods to map the original scheduling map into the reduced 
% order one, i.e. p_red = map_fnc(p_original).
% Note that lpvdeltabalred does not require this mapping as it is already
% embedded in the resulting LFR.

figure(4); clf(4);
subplot(321)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_PcaStd, map_PcaStd(pGridValidation));
title('FOM vs. ROM: lpvpcared Standard'); legend('FOM','lpvpcared:std');
subplot(322)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_PcaTraj, map_PcaTraj(pGridValidation)); 
title('FOM vs. ROM: lpvpcared Trajectory'); legend('FOM','lpvpcared:Traj');
subplot(323)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_KPCA, map_KPCA(pGridValidation));
title('FOM vs. ROM: lpvkpcared'); legend('FOM','lpvkpcared');
subplot(324)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_Deltabalred,pGridValidation);
title('FOM vs. ROM: lpvdeltabalred'); legend('FOM','lpvdeltabalred');
subplot(325)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_AE, map_AE(pGridValidation));
title('FOM vs. ROM: lpvaered'); legend('FOM','lpvaered');
subplot(326)
bodemag(sys,pGridValidation); hold on; grid on;
bodemag(sysr_DNN, map_DNN(pGridValidation));
title('FOM vs. ROM: lpvdnnred'); legend('FOM','lpvdnnred');

%% 3.h. Global state-order reduction comparison
% Time-domain validation. We proceed as in 2.f., but now using the mapping
% for the reduced schedulling maps as indicated in 3.g.

ySysr_PcaStd = lsim(sysr_PcaStd, map_PcaStd(pTraj), u, t);
ySysr_PcaTraj = lsim(sysr_PcaTraj, map_PcaTraj(pTraj), u, t);
ySysr_KPCA = lsim(sysr_KPCA, map_KPCA(pTraj), u, t);
ySysr_Deltabalred = lsim(sysr_Deltabalred, pTraj, u, t);
ySysr_AE = lsim(sysr_AE, map_AE(pTraj), u, t);
ySysr_DNN = lsim(sysr_DNN, map_DNN(pTraj), u, t);

% Plot time-domain simulations and the absolute error
figure(5); clf(5);
tiledlayout(3,2,"TileSpacing","compact","Padding","compact");
nexttile;
colororder(clr)
plot(t, ySys(:,1),'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_PcaStd(:,1),'LineWidth', lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_PcaStd(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvpcared Standard'); 
legend('FOM','lpvpcared:std', 'error'); xlabel('Time [s]'); hold off
nexttile;
plot(t, ySys(:,1),'LineWidth', lw); hold on; grid on; 
plot(t, ySysr_PcaTraj(:,1),'LineWidth', lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_PcaTraj(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvpcared Trajectory'); 
legend('FOM','lpvpcared:Traj', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1),'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_KPCA(:,1),'LineWidth',lw); ylabel('Position [m]');
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_KPCA(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvkpcared'); 
legend('FOM','lpvkpcared', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1), 'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_Deltabalred(:,1),'LineWidth',lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_Deltabalred(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvdeltabalred'); 
legend('FOM','lpvdeltabalred', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1), 'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_AE(:,1),'LineWidth',lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_AE(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvaered'); 
legend('FOM','lpvaered', 'error'); xlabel('Time [s]');
nexttile;
plot(t, ySys(:,1), 'LineWidth',lw); hold on; grid on; 
plot(t, ySysr_DNN(:,1),'LineWidth',lw); ylabel('Position [m]'); 
yyaxis right
plot(t, mag2db(abs(ySys(:,1) - ySysr_DNN(:,1)))); ylim([-100 0]);
ylabel('$\|y_1 - \hat{y}_1\|$ [dB]'); title('FOM vs. ROM: lpvdnnred'); 
legend('FOM','lpvdnnred', 'error'); xlabel('Time [s]');
