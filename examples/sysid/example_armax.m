%% In this example, it is shown how to use the LPVARMAX identification scheme 

clearvars; close all; clc; rng(1);

%% Example 1: LPV-ARMAX Identification
%
% LPV-ARMAX Identification with polynomial dependency. We take the
% identification setting and data-generating system of [1].

% Create the parameter-varying matrices
p1 = preal('pos', 'dt');

a1 = 1   - 0.5*p1 - 0.3*p1*p1;
a2 = 0.5 - 0.7*p1 - 0.5*p1*p1;

b1 = 0.5 - 0.4*p1 + 0.1*p1*p1;
b2 = 0.2 - 0.3*p1 - 0.2*p1*p1;

c1 = 0.9 * p1;
c2 = 0.1 + 0.5*p1;
c3 = 0.25 - 0.6*p1 + 0.2*p1*p1;

sys = LPVcore.lpvidpoly({1, a1,a2},{0,b1}, {1, c1, c2, c3}); 
sys.NoiseVariance = 0.01^2;


N = 4000;
u = rand(N,1);
p = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);

[y,~,~,yp,v] = lsim(sys,p,u);       % Generate data including noise signal


uv = rand(N,1);
pv = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);
[yv,~,~,yvp] = lsim(sys,pv,uv); 
data_val1 = lpviddata(yv,pv,uv); % noisy validation data set
data_val2 = lpviddata(yvp,pv,uv); % noisy validation data set


fprintf('The average SNR is %f\n', mean(LPVcore.snr(yp,v)) ); 

% Create LPV data object
data = lpviddata(y,p,u); 
plot(data);     % Show data


% LPV-ARMAX identification, unregularized. The original system is in the
% model set.

% define model structure via the template
am1=1+p1+p1*p1; 
am2=am1;
bm0=am1;
bm1=am1;
bm2=am1;
cm1=am1;
cm2=am1;
cm3=am1;

template_armax = LPVcore.lpvidpoly({1, am1, am2}, {bm0, bm1, bm2},{1, cm1, cm2,cm3});
options = lpvarmaxOptions('Display', 'on', ...
    'Regularization', struct('Lambda', 1E-7));
sysest_armax = lpvarmax(data, template_armax, options);

template_arx = LPVcore.lpvidpoly({1, am1, am2}, {bm0, bm1, bm2});
sysest_arx = lpvarx(data,template_arx); 

% Validation 
figure;
compare(data_val1,sysest_arx,sysest_armax,1);
figure;
compare(data_val2,sysest_arx,sysest_armax);



% [1]: Piga, D., Cox, P., Toth, R., & Laurain, V. (2015). LPV system
% identification under noise corrupted scheduling and output signal
% observations. Automatica, 53, 329-338.

