clear
close all
clc

addpath('./../../Tools/misc/')
load('FRFdata.mat');
load('./../../Tools/Simulator/params.mat');

s = zpk('s');

%% Identification Settings
freq = Gfrf(:, :, 1).freq;
n = 2;
q = 2*n;
dtype = 'Real';
estimD = false;
CT = true;

%% Identification
Nqd1 = length(qd1grid);
Nq2 = length(q2grid);
for ii = 1:Nqd1
    for jj = 1:Nq2
        ff = permute(Gfrf(:, :, ii, jj).resp, [3 1 2]);
        fdata = ffsid(freq*2*pi, ff, n, q, dtype, estimD, CT);
        [Ahat, Bhat, Chat, Dhat] = fdata{1:4};
        tmp = ss(Ahat, Bhat, Chat, Dhat);
        % Cancel out high-frequent zero
        Ghat(:, :, ii, jj) = minreal((-tzero(tmp))*(1/(s-tzero(tmp)))*tmp, [], false);

        % Obtain local FP models
        G(:, :, ii, jj) = gyro_localLPV(qd1grid(ii), q2grid(jj), params, false);
    end
end

bode(Gfrf, Ghat)

% save('localFitData.mat', 'Ghat', 'qd1grid', 'q2grid')