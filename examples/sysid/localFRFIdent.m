clear
close all
clc

GyroscopeSimulationFile = './../../Tools/Simulator/ID Simulator/GyroCLIdent.slx';

addpath('./../../Tools/Simulator/ID Simulator/');
addpath('./../../Tools/misc/')

load('./../../Tools/Simulator/params.mat');
load('./../../Tools/Simulator/Cdisk.mat');

%% Initialization 
Ts      = 1e-2;         % Sampling Time - Fized step solver is used

fbw_q2  = 0.18;          % Bandwidth q2 control loop
q2Int   = false;        % Integrator in q2 control loop [true/false]
Kq2     = q2IdentController(fbw_q2, params, q2Int);    % q2 stabilizing controller

flag    = [];
Noise   = [];
x0      = zeros(8, 1);

%% Configuration
qd1range        = [30, 50];     % [rad/s]
q2range         = [-pi/4, pi/4];% [rad/s]

Nqd1            = 9;            % Grid points in qd1
Nq2             = 9;            % Grid points in q2
qd1grid         = linspace(min(qd1range), max(qd1range), Nqd1);
q2grid          = linspace(min(q2range), max(q2range), Nq2);

flag.noise      = true;         % Simulate with output nosie
Noise.Variance  = 5e-7;         % Output noise variance

%% Experiment design
fs              = 1/Ts;         % fs = 1/Ts
fmin            = 0.1;          % fmin [Hz]
fmax            = fs/2;         % fmax [Hz]
fres            = 0.01;         % fres [Hz]
nP              = 10;           % Number of periods

%% Experiment design
DefFreq.fs      = fs;           % fs   [Hz]
DefFreq.fres    = fres;         % fres [Hz]
DefFreq.fmin    = fmin;         % fmin [Hz]
DefFreq.fmax    = fmax;         % fmax [Hz]
    
Nblock          = inf;          % Nonlinear detection block size

Spacing         = 'lin';        % Linear/logarithmic spacing
TypeMulti       = 'odd';        % odd / full multisines

% Generate harmonics
[ExcitedHarm, N, NewDefFreq] = HarmMultisine(DefFreq, Nblock, Spacing, TypeMulti);

AmpExcitedHarm  = ones(length(ExcitedHarm), 1);     % Amplitude per freq
u_max           = 0.2;          % Peak amplitude multisine ||sin(t)||_inf = x

% Generate random-phase multisine signal
mSine           = CalcMultisine(ExcitedHarm, N, AmpExcitedHarm);
mSine           = u_max*mSine/norm(mSine, inf);
Tend            = (N*nP-1)*Ts;  % Simulation end time
t               = 0:Ts:Tend;    % time vector

%% Data generation
options         = simset('SrcWorkspace', 'current');
if flag.noise
    Noise.e = randn(N*nP, 1)*sqrt(Noise.Variance);
else
    Noise.e = zeros(N*nP, 1);
end
for ii = 1:Nqd1
    for jj = 1:Nq2
        x0(5)   = qd1grid(ii);
        x0(2)   = q2grid(jj);

        u       = timeseries(repmat(mSine, [nP 1]), t);
        q1_ref  = timeseries(x0(5)*ones(N*nP, 1), t);
        q2_ref  = timeseries(x0(2)*ones(N*nP, 1), t);

        out(ii, jj) = sim(GyroscopeSimulationFile, [], options);
    end
end

%% Check time-domain data
idxqd1 = 1;
idxq2 = 1;

figure(1)
subplot(2, 2, 1)
plot(out(idxqd1, idxq2).tout, out(idxqd1, idxq2).q.Data(:, 2))
ylabel('$q_2$')

subplot(2, 2, 2)
plot(out(idxqd1, idxq2).tout, out(idxqd1, idxq2).u.Data)
ylabel('$u_{q_2}$')

subplot(2, 2, 3)
plot(out(idxqd1, idxq2).tout, out(idxqd1, idxq2).qd.Data(:, 2))
ylabel('$\dot{q}_2$')
xlabel('Time [s]')

subplot(2, 2, 4)
plot(out(idxqd1, idxq2).tout, out(idxqd1, idxq2).qd.Data(:, 4))
ylabel('$\dot{q}_4$')
xlabel('Time [s]')

%% Identification
method.order        = 3;
method.transient    = 1;
data.Ts             = Ts;
data.N              = N;
data.ExcitedHarm    = ExcitedHarm;
data.r              = permute(mSine, [3 4 2 1]);
Nskip               = 2;                           % Number of periods to skip

for ii = 1:Nqd1
    for jj = 1:Nq2
        qd          = out(ii, jj).qd;
        u2          = out(ii, jj).u.Data;
        qd4         = qd.Data(:, 4) + Noise.e;
        data.u      = permute(u2(N*Nskip+1:end), [3 4 2 1]);
        data.y      = permute(qd4(N*Nskip+1:end), [3 4 2 1]);
        [~, ~, freq, Ghat(:, :, ii, jj), ~, ~, ~] = RobustLocalPolyAnal(data, method);
        G(:, :, ii, jj) = gyro_localLPV(qd1grid(ii), q2grid(jj), params, false);

        % Compute SNR
        if flag.noise
            Noise.snr(ii, jj) = snr(qd4, Noise.e);
        end
    end
end
Gfrf = squeeze(frd(Ghat, freq, 'FrequencyUnit', 'Hz'));
Gfrd = frd(G, freq, 'FrequencyUnit', 'Hz');

figure(2)
bode(Gfrd, Gfrf)

% save('FRFdata.mat', 'Gfrf', 'qd1grid', 'q2grid', 'Noise', 'out', 'mSine');














































