%% In this example, it is shown how to use the COMPARE command

clearvars; close all; clc;

%% Compare method
% This example script demonstrates the functionality of the compare method
% of the lpviddata object class. The functionality is similar to that of
% the System Identification Toolbox:
%
%   * When called without output arguments, a plot is shown comparing a dataset
%       (in the form of an lpviddata object) to one or more (identified)
%       models.
%   * When called with output arguments, the fit is returned per output
%       channel.
%   * Initial conditions may be either explicitly specified, or, by
%       default, estimated based on the dataset.

%% Data-generating system
% First, let's define a data-generating system and dataset to be used with
% the compare function.

p = preal('p', 'dt');

nx = 5;  % state dimension
ny = 2;  % output dimension
nu = 1;  % input dimension

% Randomly generate an A-matrix and ensure it is stable (at least in the
% nominal case: p = 0).
A = rand(nx) * 0.1;
assert(max(abs(eig(A))) < 1);
A = A / 2 + p * A / 2;

% Construct model structure with additional random coefficients in the B
% and C matrices. No direct feedthrough.
sys = LPVcore.lpvss(A, randn(nx, nu) * 1 + p, randn(ny, nx) * 1 + randn(ny, nx) * p, [], 1);

% Generate a random initial condition for the simulation.
x0 = rand(nx, 1);

% Simulation
N = nx * 2;  % Number of time samples
u = zeros(N, sys.Nu);
p = randn(N, sys.Np);
y = lsim(sys, p, u, [], 'x0', x0);

data = lpviddata(y, p, u, sys.Ts);

% The compare script can automatically estimate the initial conditions
% based on the dataset and models. Here, 3 options are shown:
%   * The initial condition is estimated (this is the default behavior).
%   * The initial condition is set to 0.
%   * The initial condition is set to the true initial condition.
figure(1);
compare(data, sys);  % to explicitly specify: add "struct('InitialCondition', 'e')" 
sgtitle('x0 estimated (default)');
figure(2);
compare(data, sys, struct('InitialCondition', 0));
sgtitle('x0 = 0');
figure(3);
compare(data, sys, struct('InitialCondition', x0));
sgtitle('x0 true');

