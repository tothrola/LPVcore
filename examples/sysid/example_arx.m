%% In this example, it is shown how to use the LPVARX identification scheme

%  This example requires to set the current working directory to the
%  root of the toolbox.

clear all; close all; clc;
if ~exist('pmatrix','class'); cd(['..',filesep]); end  % Toolbox is not added to mathlab search path. Go to root of the toolbox

%% Example 1: LPV-ARX Identification (SISO, static dependence)

% LPV-ARX Identification with polynomial dependency. We take the
% identification setting and data-generating system of [1].

% Create the parameter-varying matrices
p1 = preal('pos', 'dt');

a1 = 1   - 0.5*p1 - 0.3*p1*p1;
a2 = 0.5 - 0.7*p1 - 0.5*p1*p1;

b1 = 0.5 - 0.4*p1 + 0.1*p1*p1;
b2 = 0.2 - 0.3*p1 - 0.2*p1*p1;


sys = LPVcore.lpvidpoly({1, a1,a2},{0,b1,b2});
sys.NoiseVariance = 0.01^2;

N = 4000;
u = rand(N,1);
p = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);

[y,~,~,yp,v] = lsim(sys,p,u);       % Generate data including noise signal


uv = rand(N,1);
pv = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);
[yv,~,~,yvp] = lsim(sys,pv,uv); 
data_val1 = lpviddata(yv,pv,uv); % noisy validation data set
data_val2 = lpviddata(yvp,pv,uv); % noisy validation data set


fprintf('The average SNR is %f\n', mean(LPVcore.snr(yp,v)) ); 

% Create LPV data object
data = lpviddata(y,p,u); 
plot(data);     % Show data


% LPV-ARX identification, unregularized. The original system is in the
% model set.

% define model structure via the template
am1=1+p1+p1*p1; 
am2=am1;
bm0=am1;
bm1=am1;
bm2=am1;
template_sys = LPVcore.lpvidpoly({1, am1, am2}, {bm0, bm1, bm2});

options = lpvarxOptions('Display','on');
sysest_arx = lpvarx(data,template_sys,options); 

% validation
figure;
compare(data_val1,sysest_arx,1);
figure;
compare(data_val2,sysest_arx);

% [1]: Piga, D., Cox, P., Toth, R., & Laurain, V. (2015). LPV system
% identification under noise corrupted scheduling and output signal
% observations. Automatica, 53, 329-338.

%% Example 2: LPV-ARX Identification (MIMO, dynamic dependence)
%
% An example with scheduling variables with a time delay. LPV-ARX
% identification is prefored with a template for the A and B polynomial.

% Create the parameter-varying matrices
p2=pshift(preal('vel', 'dt'),-1);
p3=pshift(p2,-1);


ny = 2;  nu = 3; 
B10 = randn(ny,nu);
a1 = diag([-0.5,0.55])-0.1*p1;
a2 = diag([0.3,0.35])  +0.2*p2;
b0 = B10+0.1*p1+0.2*p3;
b1 = [diag([-0.25,0.25]),[0;0]]*p1+[[0;0],diag([0.1,0.15])]*p3;


% Create LPV-IDPOLY object. A(q,p(t)) y(t) = B(q,p(t)) u(t) + e(t).
sys = LPVcore.lpvidpoly({eye(2), a1, a2},{b0,b1});
sys.NoiseVariance = diag([0.9 0.9]);

% Simulate LPV-IO object,
N = 1000;
u = randn(N,nu);
p = [sin((1:N)*0.1)', sin((1:N)*0.2)']+0.3*randn(N,2);

[y,~,~,yp,v] = lsim(sys,p,u);       % Generate data including noise signal


fprintf('The average SNR is %f\n', mean(LPVcore.snr(yp,v)) ); 

%Create LPV data object
data = lpviddata(y,p,u); 
plot(data);     % Show data

uv = randn(N,nu);
pv = [sin((1:N)*0.1)', sin((1:N)*0.2)']+0.3*randn(N,2);
[yv,~,~,yvp] = lsim(sys,pv,uv); 
data_val = lpviddata(yvp,pv,uv); % noise free data set


% LPV-ARX identification, unregularized. We create a template for the A
% and B polynomial. The original system is in the model set.

% Define an overparametrized model structure 

a1 = randn(2,2)+ randn(2,2)*p1 + randn(2,2)*p2 + randn(2,2) *p3;
a2 = randn(2,2)+ randn(2,2)*p1 + randn(2,2)*p2 + randn(2,2) *p3;
a3 = randn(2,2)+ randn(2,2)*p1 + randn(2,2)*p2 + randn(2,2) *p3;

b0 = randn(2,3)+ randn(2,3)*p1 + randn(2,3)*p2 + randn(2,3) *p3;
b1 = randn(2,3)+ randn(2,3)*p1 + randn(2,3)*p2 + randn(2,3) *p3;
b2 = randn(2,3)+ randn(2,3)*p1 + randn(2,3)*p2 + randn(2,3) *p3;
b3 = randn(2,3)+ randn(2,3)*p1 + randn(2,3)*p2 + randn(2,3) *p3;
b4 = randn(2,3)+ randn(2,3)*p1 + randn(2,3)*p2 + randn(2,3) *p3;


options = lpvarxOptions('Display','off');
sysest = lpvarx(data,LPVcore.lpvidpoly({eye(2),a1,a2,a3},{b0,b1,b2,b3,b4}),options);


% Validation in terms of simulation error 
figure;
compare(data_val,sysest);


% LPV-ARX identification, regularized
optionsRegul = lpvarxRegulOptions;
sysest = lpvarx(data,LPVcore.lpvidpoly({eye(2),a1,a2,a3},{b0,b1,b2,b3,b4}),options,optionsRegul);

% Validation in terms of simulation error 
figure;
compare(data_val,sysest);
    