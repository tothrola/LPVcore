%% In this example, we show how to use the pseudo-LS PEM identification scheme
% Note that pseudo linear regression is currently a not recommended
% estimation method. Use gradient approaches. Functionality is provided due
% to legacy reasons.

clearvars; close all; clc;
rng(1);

%% Example 1: Simple LPV-PEM identification
%
% Create the parameter-varying matrices
p1 = preal('pos', 'dt');

A = {1, 0.1 * p1};
b1 = 0.5 - 0.4*p1 + 0.1*p1*p1;
b2 = 0.2 - 0.3*p1 - 0.2*p1*p1;
B = {0, b1, b2};
C = {1, 1 - 0.3*p1 - 0.2*p1*p1};
D = {1, 1 - 0.1*p1};
f1 = 1   - 0.5*p1 - 0.3*p1*p1;
f2 = 0.5 - 0.7*p1 - 0.5*p1*p1;
F = {1, f1, f2};

% Data-generating system (ground truth)
sysGt = LPVcore.lpvidpoly({1}, B, C, D, F);
sysGt.NoiseVariance = 0.08^2;

% Generate model structures
template_oe = LPVcore.lpvidpoly({1}, B, {1}, {1}, F);
template_arx = LPVcore.lpvidpoly(F, B);
template_armax = LPVcore.lpvidpoly(F, B, C);
template_bj = LPVcore.lpvidpoly({1}, B, C, D, F);

% Generate data
N = 400;
u = rand(N,1);
p = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);
[y,~,~,yp,v] = lsim(sysGt, p, u);

fprintf('The SNR is %f\n', LPVcore.snr(yp,v)); 

% Create LPV data object
data = lpviddata(y,p,u); 

plot(data);     % Show data

% Execute sysID

options = lpvarxOptions('Display','on');

sys_est_oe = lpvoe(data, randlpvidpoly(template_oe), ...
    lpvoeOptions('SearchMethod', 'pseudols', 'Display', 'on','Initialization','polypre'));
sys_est_arx = lpvarx(data, randlpvidpoly(template_arx), ...
    options);
sys_est_armax = lpvarmax(data, randlpvidpoly(template_armax), ...
    lpvarmaxOptions('SearchMethod', 'pseudols', 'Display', 'on','Initialization','polypre'));

sys_est_bj = lpvbj(data, randlpvidpoly(template_bj), ...
    lpvbjOptions('SearchMethod', 'pseudols', 'Display', 'on','Initialization','polypre'));


% validation
% u_val = rand(N,1);
% p_val = 0.2+0.4 * sin(0.035*pi*(1:N)')+ 0.3*rand(N,1);
% [y,~,~,y_val,v] = lsim(sysGt, p, u);
% data_val = lpviddata(y_val,p_val,u_val); % validation data set

data_val = lpviddata(yp,p,u); % validation data set


[~,fit]=compare(data_val,sys_est_arx,sys_est_armax,sys_est_oe,sys_est_bj);

fprintf('The fit is %f (ARX) vs. %f (ARMAX) vs. %f (OE) vs. %f (BJ)\n', ...
    fit{1}, fit{2}, fit{3}, fit{4});

compare(data_val,sys_est_arx,sys_est_armax,sys_est_oe,sys_est_bj);


%% Example 2: LPV-BJ Identification with Regularization
%
% An example of LPV-BJ identification with an overparametrized model
% structure. The effect of tuning the regularization parameter Lambda is
% demonstrated by looking at the fit on a validation data set.

rng(1); clearvars; close all;

p = preal('p', 'dt');
noiseVariance = 0.1;

sys = LPVcore.lpvidpoly(1, [1, 2], ...
    {1, 0.1 * p^2, 0.5, 0.6 * p}, {1, 0, 0, 0, 0.4}, {1, 0, -0.1 * p, 0.1}, noiseVariance);
% For the template system, we will use an overparametrized BJ system for which
% each term of C, D and F depends affinely on p^1 and p^2.
t = 1 + p + p^2;
T = {1, t, t, t, t};  % a monic term and 4 delay terms
template_sys = LPVcore.lpvidpoly(1, [1, 2], ...
    T, T, T, noiseVariance);

% Create training and validation datasets
N = 100;
u = randn(N, sys.Nu);
p = randn(N, sys.Np);
y = lsim(sys, p, u);
data = lpviddata(y, p, u);

uVal = randn(N, sys.Nu);
pVal = randn(N, sys.Np);
yVal = lsim(sys, pVal, uVal);
dataVal = lpviddata(yVal, pVal, uVal);

% Values of lambda to iterate over
lambda = [0.1, 1, 10, 100];
fit = NaN(size(lambda));

for i=1:numel(lambda)
    fprintf('Regularized LPV-BJ identification with Lambda = %.1f\n', ...
        lambda(i));
    
    % Regularization is parametrized through the "Regularization"
    % structure. For more information, see LPVBJOPTIONS.
    regularization.Lambda = lambda(i);

    sysest = lpvbj(data, template_sys, lpvbjOptions('SearchMethod', 'pseudols', 'Display', 'on', ...
        'Regularization', regularization, ...
        'SearchOptions', struct('MaxIterations', 10)));

    [~, fit(i)] = compare(dataVal, sysest);
    fprintf('Fit: %.1f%%\n', max(fit(i), 0));
end

semilogx(lambda, max(fit, 0), 'k-*'); grid on;
title('Fit to validation data');
ylabel('Fit (%)');
xlabel('\lambda'); 
    

