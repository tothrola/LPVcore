%% In this example, it is shown how to use the LPVSSESET identification scheme

clearvars; close all; clc; rng(1);

%% Data generating system
p = preal('p', 'dt');
A = 0.1 + 0.1 * p;
B = 1;
C = 1;
D = 1;
K = 0;
NoiseVariance = 0.01;
Ts = 1;
sys = LPVcore.lpvidss(A, B, C, D, 'innovation', K, [], NoiseVariance,Ts);

%% Data generation
N = 100;
u = randn(N, sys.Nu);
p = rand(N, sys.Np);
[y,~,~,yp] = lsim(sys, p, u);
data = lpviddata(y, p, u, sys.Ts);

fprintf('The average SNR is %f\n', LPVcore.snr(yp,y-yp) ); 

uv = randn(N, sys.Nu);
pv = rand(N, sys.Np);
[yv,~,~,yvp] = lsim(sys,pv,uv); 
data_val1 = lpviddata(yv,pv,uv,sys.Ts); % noisy validation data set
data_val2 = lpviddata(yvp,pv,uv,sys.Ts); % noisy validation data set



initSearchMethod = 'gradient';
sysest_ssest = lpvssest(data, sys, ...
    lpvssestOptions(...
        'Display', 'on'));


% Validation 
figure;
compare(data_val1,sysest_ssest ,1);
figure;
compare(data_val2,sysest_ssest);






